# **Unergy Blockchain Protocol**

## **Protocol description**

The Unergy Blockchain protocol (UBP) enables the creation, financing, and operation of solar energy projects in a decentralized manner. It generates returns for investors and contributes to the fight against climate change through the use of smart contracts that integrate ERC-20 tokens ([pWatts](./contracts/ERC20Project.sol), [uWatts](./contracts/ERC20UWatt.sol)), ERC-1155 tokens ([CleanEnergyAssets](./contracts/ERC1155CleanEnergyAssets.sol)), logic ([ProjectsManager](./contracts/projectsManager.sol), [UnergyLogicReserve](./contracts/UnergyLogicReserve.sol), and [UnergyBuyer](./contracts/UnergyBuyer.sol)), and data storage ([UnergyData](./contracts/UnergyData.sol)). UBP utilizes role-based access (`AccessControl`) and a Permission protocol ([PermissionGranter](./contracts/PermissionGranter.sol)) to ensure authorized access to protocol functions. UBP allows for the exchange (`Swap`) of `pWatts` (Project tokens) for `uWatts` (Reserve tokens) and also facilitates transparent storage of energy generation and energy payments. The income generated from the sale of energy produced by the projects is utilized for the development and expansion of the protocol by installing new solar energy projects, which are then tokenized and distributed to holders as profit.

## **Creating Projects**

In UBP, an administrator user (admin) can create solar energy projects by deploying the `ERC20Project` token contract (`pWatts`), with the total supply representing the future energy generation capacity of the project. Subsequently, the admin can associate this token with the protocol, configuring all the necessary technical parameters for its financing and subsequent operation.

## **Creating and Signing Milestones**

UBP allows the 'admin' user to create a series of project installation milestones, which indicate the progress of the project installation as they are executed. The 'Installer' user has the ability to execute a function in the protocol that reports a milestone as completed. When all milestones are completed, the 'Installer' user signs the protocol to mark all installation milestones as finished. Unergy verifies the installation milestones by confirming them with a signature, which is a requirement for changing the project state from 'FUNDING' to 'INSTALLED' or 'PRODUCTION.

## **Buying pWatts**

UBP enables the financing of solar energy projects by tokenizing and offering these tokens through its marketplace.

When a user successfully executes a purchase order, the `pWatts` are sent to their public address, and the `approveSwap` function (`Approve`) is executed, granting Unergy permission to utilize the user's tokens for the future swap.

## **Making Swap**

UBP allows for the swap of 'pWatts' for `uWatts` before the project enters the 'PRODUCTION' phase. This swap is automatically executed for all `pWatt` holders of a specific project and is carried out by an 'admin' user. The `pWatt` users become part of the reserve (UBP Holdings).

## **Energy Reports**

UBP allows a "meter" user to report energy measurements for a specific project. This report stores energy generation both in general (Reserve) and individually (Project) through the `CleanEnergyAssets` contract, minting new tokens that represent the generated energy. UBP stores the energy generation of a project and all projects in the reserve using multiple tokens under the ERC-1155 standard. These tokens are created (minted) when a meter reports a new energy measurement and are destroyed (burned) when an energy bill has been paid.

## **Invoice Report**

UBP allows an "admin" user to report an energy bill, which corresponds to the payment of energy for a project. The payment of an energy bill allows for the calculation and storage of the number of depreciated new `pWatts` at the time of bill payment.

UBP stores the energy payments for a project and all projects in the reserve and interacts with the ERC-1155 standard. It burns the tokens representing the energy that has been paid in the bill and creates renewable energy certificates (RECs).

With the fiat money that Unergy receives as payment for the energy bills of all projects, new `pWatts` of new projects are acquired. These new `pWatts` offset depreciation, and the remaining `pWatts` serve as reserve `pWatts` to be swapped for `uWatts` later.

## **uWatts Claim**

When converted into 'uWatts,' 'uWatt' holders can claim returns based on the number of 'uWatts' they hold.

To claim the returns of the projects, users will use the `requestClaimUwatts` function, which verifies the user's `uWatt` status and transfers the respective number of `uWatt` returns to the user.

The protocol can be used by anyone with a Web3 wallet registered on the Unergy platform.

## **Protocol Roles**

UBP has 4 fundamental roles:

- Admin: Can grant roles and execute all functions.
- ProjectAdmin: Holds `pWatt` initial supply of projects.
- Installer: Project installer user.
- Meter: Project meter.
- User: All `pWatts` and `uWatts` holders.

## **Contracts Description**

[Contracts Description](./docs/index.md)       

## **Contracts Addresses setting up**

To operate the contracts, it is important to configure the contract addresses. Below is a table showing the public address configurations for each of the protocol's contracts.


<div>
  <table class="tg">
  <thead>
    <tr>
      <th class="tg-n80v" colspan="3">Contracts addresses setup</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="tg-n80v">Contract</td>
      <td class="tg-n80v">Contracts Addresses to set</td>
      <td class="tg-n80v">Function to be executed</td>
    </tr>
    <tr>
      <td class="tg-n80v" rowspan="2">UnergyData</td>
      <td class="tg-n80v">PermissionGranter</td>
      <td class="tg-n80v">setPermissionGranterAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UWattToken</td>
      <td class="tg-n80v">setUWattsAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v" rowspan="4">ProjectsManager</td>
      <td class="tg-n80v">PermissionGranter</td>
      <td class="tg-n80v">setPermissionGranterAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyEvent</td>
      <td class="tg-n80v">setUnergyEventAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">CleanEnergyAssets</td>
      <td class="tg-n80v">setCleanEnergyAssetsAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyLogicReserve</td>
      <td class="tg-n80v">setUnergyLogicReserveAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v" rowspan="4">UnergyEvent</td>
      <td class="tg-n80v">PermissionGranter</td>
      <td class="tg-n80v">setPermissionGranterAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UWattToken</td>
      <td class="tg-n80v">setUWattsAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyBuyer</td>
      <td class="tg-n80v">setUnergyBuyerAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyLogicReserve</td>
      <td class="tg-n80v">setUnergyLogicReserveAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v" rowspan="6">UnergyBuyer</td>
      <td class="tg-n80v">PermissionGranter</td>
      <td class="tg-n80v">setPermissionGranterAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">ProjectsManager</td>
      <td class="tg-n80v">setProjectsManagerAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UWattToken</td>
      <td class="tg-n80v">setUWattsAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">CleanEnergyAssets</td>
      <td class="tg-n80v">setCleanEnergyAssetsAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyLogicReserve</td>
      <td class="tg-n80v">setUnergyLogicReserveAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyData</td>
      <td class="tg-n80v">by Intitializer</td>
    </tr>
    <tr>
      <td class="tg-n80v" rowspan="5">UnergyLogicReserve</td>
      <td class="tg-n80v">PermissionGranter</td>
      <td class="tg-n80v">setPermissionGranterAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">ProjectsManager</td>
      <td class="tg-n80v">setProjectsManagerAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">CleanEnergyAssets</td>
      <td class="tg-n80v">setCleanEnergyAssetsAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyBuyer</td>
      <td class="tg-n80v">setUnergyBuyerAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyData</td>
      <td class="tg-n80v">by Intitializer</td>
    </tr>
    <tr>
      <td class="tg-n80v" rowspan="2">UWattToken</td>
      <td class="tg-n80v">PermissionGranter</td>
      <td class="tg-n80v">setPermissionGranterAddr</td>
    </tr>
    <tr>
      <td class="tg-n80v">UnergyEvent</td>
      <td class="tg-n80v">setUnergyEventAddr</td>
    </tr>
  </tbody>
  </table>
</div>

## **Permission Granting**

To operate, the protocol must have the necessary permissions from the permissionGranter, below is a table with all the necessary permissions for the protocol operation.


<div>
  <table class="tg">
  <thead>
    <tr>
      <th class="tg-jk6d" colspan="4">Permission List</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="tg-jk6d">Access set to</td>
      <td class="tg-jk6d">Access granted by</td>
      <td class="tg-jk6d">Access received by</td>
      <td class="tg-jk6d">Accessed function name</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="44">Permissiongranter</td>
      <td class="tg-jk6d" rowspan="9">UnergyData</td>
      <td class="tg-jk6d" rowspan="3">Admin</td>
      <td class="tg-jk6d">setUWattsAddr</td>
    </tr>
    <tr>
      <td class="tg-jk6d">generateBuyTicket</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setPWattsToTheReserveAddress</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="6">UnergyLogicReserve</td>
      <td class="tg-jk6d">setAccEnergyByMeter</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setDepreciatedBalance</td>
    </tr>
    <tr>
      <td class="tg-jk6d">changeBuyTicketUsed</td>
    </tr>
    <tr>
      <td class="tg-jk6d">insertUWattsStatusSnapshot</td>
    </tr>
    <tr>
      <td class="tg-jk6d">insertHistoricalSwap</td>
    </tr>
    <tr>
      <td class="tg-jk6d">updateUWattsStatusSnapshotAtIndex</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="6">ProjectsManager</td>
      <td class="tg-jk6d" rowspan="3">Admin</td>
      <td class="tg-jk6d">createProject</td>
    </tr>
    <tr>
      <td class="tg-jk6d">updateProjectRelatedProperties</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setSignature</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="2">UnergyBuyer</td>
      <td class="tg-jk6d">updateProjectRelatedProperties</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setSignature</td>
    </tr>
    <tr>
      <td class="tg-jk6d">UnergyLogicReserve</td>
      <td class="tg-jk6d">updateProjectRelatedProperties</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="5">UnergyEvent</td>
      <td class="tg-jk6d" rowspan="3">Admin</td>
      <td class="tg-jk6d">setUWattsAddr</td>
    </tr>
    <tr>
      <td class="tg-jk6d">beforeTransferReceipt</td>
    </tr>
    <tr>
      <td class="tg-jk6d">afterTransferReceipt</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="2">uWattToken</td>
      <td class="tg-jk6d">beforeTransferReceipt</td>
    </tr>
    <tr>
      <td class="tg-jk6d">afterTransferReceipt</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="16">UnergyBuyer</td>
      <td class="tg-jk6d" rowspan="12">Admin</td>
      <td class="tg-jk6d">createMilestonesBatch</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setOriginatorSign</td>
    </tr>
    <tr>
      <td class="tg-jk6d">changeMilestoneName</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setInstallerSign</td>
    </tr>
    <tr>
      <td class="tg-jk6d">deleteMilestone</td>
    </tr>
    <tr>
      <td class="tg-jk6d">updateProjectRelatedProperties</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setMaintenancePercentage</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setPWattPrice</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setCurrentProjectValue</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setSwapFactor</td>
    </tr>
    <tr>
      <td class="tg-jk6d">setTotalPWatts</td>
    </tr>
    <tr>
      <td class="tg-jk6d">withdrawUWatts</td>
    </tr>
    <tr>
      <td class="tg-jk6d">UnergyBuyer</td>
      <td class="tg-jk6d">updateProjectRelatedProperties</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="2">UnergyEvent</td>
      <td class="tg-jk6d">updateProjectRelatedProperties</td>
    </tr>
    <tr>
      <td class="tg-jk6d">updateLastUWattStatus</td>
    </tr>
    <tr>
      <td class="tg-jk6d">UnergyLogicReserve</td>
      <td class="tg-jk6d">SetProjectState</td>
    </tr>
    <tr>
      <td class="tg-jk6d" rowspan="7">UnergyLogicreserve</td>
      <td class="tg-jk6d" rowspan="5">Admin</td>
      <td class="tg-jk6d">energyReport</td>
    </tr>
    <tr>
      <td class="tg-jk6d">invoiceReport</td>
    </tr>
    <tr>
      <td class="tg-jk6d">pWattsTransfer</td>
    </tr>
    <tr>
      <td class="tg-jk6d">swapToken</td>
    </tr>
    <tr>
      <td class="tg-jk6d">updateProjectRelatedProperties</td>
    </tr>
    <tr>
      <td class="tg-jk6d">Meters (All meters)</td>
      <td class="tg-jk6d">energyReport</td>
    </tr>
    <tr>
      <td class="tg-jk6d">UnergyEvent</td>
      <td class="tg-jk6d">updateLastUWattStatus</td>
    </tr>
    <tr>
      <td class="tg-jk6d">uWattToken</td>
      <td class="tg-jk6d">UnergyLogicReserve</td>
      <td class="tg-jk6d">mint</td>
    </tr>
  </tbody>
  </table>
</div>
