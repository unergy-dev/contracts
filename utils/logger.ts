export class Logger {
  private static instance: Logger;

  private constructor() {}

  public static getInstance(): Logger {
    if (!Logger.instance) {
      Logger.instance = new Logger();
    }

    return Logger.instance;
  }

  public log(message: string): void {
    const date = this._getDate();
    console.log(this._parse(date, message));
  }

  public warn(message: string): void {
    const date = this._getDate();
    console.warn(this._parse(date, message));
  }

  public error(message: string): void {
    const date = this._getDate();
    console.error(this._parse(date, message));
  }

  private _parse(date: string, message: string): string {
    return `[${date}] ${message}`;
  }

  private _getDate(): string {
    return new Date().toLocaleString();
  }
}

export const logger = Logger.getInstance();
