import * as fs from "fs";

export function getFile(
  path: string,
  rawContent: boolean = false
): fs.ReadStream | Error | string {
  try {
    let file;

    if (rawContent) {
      file = fs.readFileSync(path, "utf8");
    } else {
      file = fs.createReadStream(path);
    }

    return file;
  } catch (error: any) {
    return new Error(`Could not read file at path ${path}: ${error.message}`);
  }
}

export function saveFile(path: string, data: string) {
  fs.access(path, (err) => {
    if (!err) {
      fs.unlink(path, (err) => {
        if (err) {
          console.error(err);
        } else {
          fs.writeFileSync(path, data);
        }
      });
    } else {
      fs.writeFileSync(path, data);
    }
  });
}
