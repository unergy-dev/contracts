import { ethers } from "hardhat";

const { MUMBAI_RPC_URL, POLYGON_RPC_URL } = process.env;

export const provider = (networkName: string) => {
  if (networkName === "localhost") {
    return ethers.provider;
  }

  if (networkName === "polygon") {
    return new ethers.providers.JsonRpcProvider(POLYGON_RPC_URL);
  }

  if (networkName === "polygon_mumbai") {
    return new ethers.providers.JsonRpcProvider(MUMBAI_RPC_URL);
  }

  throw new Error(
    `Unknown network: ${networkName}. Plase set a provider for it in utils/provider.ts`
  );
};
