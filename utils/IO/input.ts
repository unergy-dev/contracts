import { input, password } from "@inquirer/prompts";
import {
  notEmpty,
  numberString,
  validAddress,
  validPrivateKey,
} from "../validations";

export async function userInput<T>(
  message: string,
  type: "address" | "string" | "number" | "privateKey" = "string"
): Promise<T> {
  if (type === "address") {
    return (await input({
      message,
      validate: validAddress,
    })) as T;
  }

  if (type === "number") {
    return (await input({
      message,
      validate: numberString,
    })) as T;
  }

  if (type === "privateKey") {
    return (await password({
      message,
      validate: validPrivateKey,
    })) as T;
  }

  return (await input({
    message,
    validate: notEmpty,
  })) as T;
}
