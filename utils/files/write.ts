import fs from "fs";
import path from "path";
import { logger } from "../logger";

export function write(filePath: string, data: object) {
  const directory = path.dirname(filePath);

  if (fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });
  }

  fs.writeFileSync(filePath, JSON.stringify(data, null, 2));

  logger.log("Data saved");
}
