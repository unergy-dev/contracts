import CrytoJs from "crypto-js";
import { Wallet, providers } from "ethers";

import { Key, keysDb } from "./server/db";
import { server } from "./server";
import { logger } from "./logger";
import { wait } from "./helpers";
import { TRPC_SERVER_PORT } from "./constants";

export async function getAdminWallets(
  amountOfKeys: number,
  provider: providers.Provider,
  options?: {
    poolingInterval?: number;
  }
) {
  server.listen(TRPC_SERVER_PORT);

  let adminKeys: Key[] = [];

  while (adminKeys.length !== amountOfKeys) {
    adminKeys = keysDb.key.getKeys();

    if (adminKeys.length === amountOfKeys) {
      break;
    }

    logger.log(`Received ${adminKeys.length} / ${amountOfKeys} keys`);

    await wait(options?.poolingInterval ?? 10);
  }

  const adminWallets = adminKeys.map(
    (key) => new Wallet(decrypt(key.encryptedKey), provider)
  );

  return adminWallets;
}

export function encryt(message: string): string {
  const key = process.env.CRYPTO_KEY;

  if (key) {
    const chiperText = CrytoJs.AES.encrypt(message, key).toString();
    return chiperText;
  } else {
    throw new Error("No CRYPTO_KEY found in .env");
  }
}

export function decrypt(encryptedMessage: string): string {
  const key = process.env.CRYPTO_KEY;

  if (key) {
    const bytes = CrytoJs.AES.decrypt(encryptedMessage, key);
    return bytes.toString(CrytoJs.enc.Utf8);
  } else {
    throw new Error("No CRYPTO_KEY found in .env");
  }
}
