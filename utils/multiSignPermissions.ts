import { Signer } from "ethers";
import { TransactionRequest } from "@ethersproject/providers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

import { PermissionGranter } from "../typechain-types";
import { PermissionType } from "../types/contracts.types";
import { logger } from "./logger";
import { network } from "hardhat";
import { getFeeData } from "./feeData";

export class MultiSignPermission {
  constructor(
    private readonly permissionGranter: PermissionGranter,
    private readonly admins: Array<Signer | SignerWithAddress>
  ) {}

  public async signAndSendTransaction(tx: TransactionRequest, signer: Signer) {
    const populatedTx = await signer.populateTransaction(tx);
    const txRequest = await signer.sendTransaction(populatedTx);
    await txRequest.wait();
  }

  public async setPermission(
    address: string,
    contractAddress: string,
    functionName: string,
    permissionType: PermissionType = PermissionType.PERMANENT,
    permissionValue: bigint = 0n
  ) {
    const admin = this.admins[0];

    const tx = await this.permissionGranter
      .connect(admin)
      .populateTransaction.setPermission(
        address,
        contractAddress,
        functionName,
        permissionType,
        permissionValue,
        await getFeeData()
      );

    await this.signAndSendTransaction(tx, admin);

    if (network.name !== "hardhat" && network.name !== "localhost")
      logger.log(`Permission ${functionName} for ${address} granted`);
  }

  public async setMeterPermission(
    meterAddress: string,
    logicContract: string,
    projectAddress: string
  ) {
    const admin = this.admins[0];

    const tx = await this.permissionGranter
      .connect(admin)
      .populateTransaction.setMeterPermission(
        meterAddress,
        logicContract,
        projectAddress,
        await getFeeData()
      );

    await this.signAndSendTransaction(tx, admin);
  }

  public async revokePermission(
    address: string,
    contractAddress: string,
    functionName: string,
    permissionValue: bigint = 0n
  ) {
    const admin = this.admins[0];

    const tx = await this.permissionGranter
      .connect(admin)
      .populateTransaction.revokePermission(
        address,
        contractAddress,
        functionName,
        permissionValue
      );

    await this.signAndSendTransaction(tx, admin);
  }
}
