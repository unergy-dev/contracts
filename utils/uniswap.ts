import { FeeAmount, computePoolAddress } from "@uniswap/v3-sdk";
import { Token } from "@uniswap/sdk-core";
import { network } from "hardhat";

const POOL_FACTORY_CONTRACT_ADDRESS =
  "0x1F98431c8aD98523631AE4a59f267346ea31F984";
const UWATT = new Token(
  network.config.chainId || 137,
  "0xdD875635231E68E846cE190b1396AC0295D9e577",
  18,
  "UWATT"
);
const USDT = new Token(
  network.config.chainId || 137,
  "0xc2132D05D31c914a87C6611C10748AEb04B58e8F",
  6,
  "USDT"
);

interface ExampleConfig {
  rpc: {
    local: string;
    mainnet: string;
  };
  tokens: {
    in: Token;
    amountIn: number;
    out: Token;
    poolFee: number;
  };
}

export const CurrentConfig: ExampleConfig = {
  rpc: {
    local: "http://localhost:8545",
    mainnet: "https://mainnet.infura.io/v3/0ac57a06f2994538829c14745750d721",
  },
  tokens: {
    in: UWATT,
    amountIn: 1000,
    out: USDT,
    poolFee: FeeAmount.LOW, // 500
  },
};

const currentPoolAddress = computePoolAddress({
  factoryAddress: POOL_FACTORY_CONTRACT_ADDRESS,
  tokenA: CurrentConfig.tokens.in,
  tokenB: CurrentConfig.tokens.out,
  fee: CurrentConfig.tokens.poolFee,
});

async function main() {
  console.log({ currentPoolAddress });
}

main();
