import { ethers } from "hardhat";
import {
  Info,
  Token,
  Milestone,
  ProjectConfig,
  projectConfigSchema,
} from "../types/projectConfig.types";

export class ProjectFactory {
  private readonly projectConfig: {
    info: Info;
    token: Token;
  }[];

  constructor(_projectConfig: ProjectConfig[]) {
    // Object validation
    const parsedProjectConfig = projectConfigSchema.parse(_projectConfig);
    // properties validation
    this.projectConfig = parsedProjectConfig.map((project) => {
      return this.parseTypes(project);
    });
  }

  get parsedProjectConfig() {
    return this.projectConfig;
  }

  parseTypes(_projectConfig: ProjectConfig): {
    info: Info;
    token: Token;
  } {
    const info: Info = {
      maintenancePercentage: ethers.utils.parseUnits(
        _projectConfig.info.maintenancePercentage.toString(),
        18
      ),
      initialProjectValue: ethers.utils.parseUnits(
        _projectConfig.info.initialProjectValue.toString(),
        6
      ),
      currentProjectValue: ethers.utils.parseUnits(
        _projectConfig.info.currentProjectValue.toString(),
        6
      ),
      swapFactor: ethers.utils.parseUnits(
        _projectConfig.info.swapFactor.toString(),
        18
      ),
      totalPWatts: ethers.utils.parseUnits(
        _projectConfig.info.totalPWatts.toString(),
        18
      ),
      adminAddress: new Address(_projectConfig.info.adminAddress).parse(),
      installerAddress: new Address(
        _projectConfig.info.installerAddress
      ).parse(),
      originatorAddress: new Address(
        _projectConfig.info.originatorAddress
      ).parse(),
      originatorFee: ethers.utils.parseUnits(
        _projectConfig.info.originatorFee.toString(),
        18
      ),
      assetManagerAddress: new Address(
        _projectConfig.info.assetManagerAddress
      ).parse(),
      assetManagerFeePercentage: ethers.utils.parseUnits(
        _projectConfig.info.originatorFee.toString(),
        18
      ),
      stableAddr: new Address(_projectConfig.info.stableAddr).parse(),
      meterAddrs: _projectConfig.info.meterAddrs.map((addr) =>
        new Address(addr).parse()
      ),
      milestones: new Milestones(_projectConfig.info.milestones)
        .parsedMilestones,
    };

    const token = {
      name: _projectConfig.token.name as string,
      symbol: _projectConfig.token.symbol as string,
    };

    return {
      info,
      token,
    };
  }
}

class Milestones {
  private readonly targetWeight = 100;

  constructor(private readonly milestones: Milestone[]) {
    this.validateMilestones();
  }

  get parsedMilestones() {
    return this.milestones;
  }

  private validateMilestones() {
    const totalWeight = this.milestones.reduce(
      (acc, milestone) => acc + milestone.weight,
      0
    );

    if (totalWeight !== this.targetWeight) {
      throw new Error(
        `Total weight of milestones must be ${this.targetWeight}, but is ${totalWeight}`
      );
    }
  }
}

class Address {
  constructor(private readonly address: string) {}

  parse() {
    if (ethers.utils.isAddress(this.address)) {
      return this.address;
    } else {
      throw new Error("Invalid address");
    }
  }
}
