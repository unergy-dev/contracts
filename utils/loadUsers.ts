import { ethers } from "hardhat";


export class LoadUsers {

    public async randomUsersNumber(minNumber:number, maxNumber:number) {
        // Generate a random decimal number between 0 and 1
        const randomDecimal = Math.random();
        // Scale the random decimal to fit the desired range
        const scaledRandom = randomDecimal * (maxNumber - minNumber + 1);
        // Floor the result to get an integer value in the desired range
        const randomInt = Math.floor(scaledRandom) + minNumber;
        return await this.setupSigners(randomInt);
    }

    public async setupSigners(numSigners:number) {
        const signers = [];
        // Creates numSigners wallets as participants of project
        for (let i = 0; i < numSigners; i++) {
            const signer = ethers.Wallet.createRandom();
            signers.push(signer.address);
        }
        return signers
    }
}