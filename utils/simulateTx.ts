interface SimulateTxOptions {
  from: string;
  to: string;
  data: string;
  value?: string;
  gas?: string;
  gasPrice?: string;
  maxFeePerGas?: string;
  maxPriorityFeePerGas?: string;
}

export async function simulateTx({ from, to, value, data }: SimulateTxOptions) {
  const url = process.env.ALCHEMY_RPC || "";

  const options = {
    method: "POST",
    headers: {
      accept: "application/json",
      "content-type": "application/json",
    },
    body: JSON.stringify({
      id: 1,
      jsonrpc: "2.0",
      method: "alchemy_simulateExecution",
      params: [
        {
          from,
          to,
          value: value || "0x0",
          data,
        },
      ],
    }),
  };

  return fetch(url, options)
    .then((response) => response.json())
    .then((response) => {
      // console.log({ response });
      if (response.error) {
        throw new Error(response.error.message);
      } else {
        return response.result;
      }
    })
    .catch((err) => {
      console.log({ err });
      throw err;
    });
}
