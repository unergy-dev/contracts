import fs from "fs";
import dotenv from "dotenv";
import * as __path from "path";
import { network } from "hardhat";

import { Logger, logger } from "./logger";
import { PermissionType } from "../types/contracts.types";
import { ZERO_ADDRESS } from "./constants";

dotenv.config();

export enum AddressType {
  Wallet = "Wallet",
  Contract = "Contract",
}

interface Permission {
  role: string;
  type: PermissionType;
  value: string;
}

export interface PermissionObject {
  contract: string;
  contractAddress: string;
  authorizedAddress: string;
  permission: Permission;
  addressType: AddressType;
}

type ContractInReport = {
  address: string;
  deployer: string;
};

export class Reporter {
  private contracts: Map<
    string,
    {
      address: string;
      deployer: string;
    }
  > = new Map();
  private permissions: PermissionObject[] = [];

  isEnable: boolean = true;
  private basePath = `${process.cwd()}/data/${network.name}`;
  private defaultDeploymentReportPath = `${this.basePath}/full-deployment-report.json`;
  private defaultPermissionsReportPath = `${this.basePath}/permissions-report.json`;

  constructor(private readonly logger: Logger = Logger.getInstance()) {}

  public enable() {
    this.isEnable = true;
  }

  public disable() {
    this.isEnable = false;
  }

  /**
   * Registers the address of a contract.
   * @param {string} contractName The name of the contract.
   * @param {string} address The address of the contract.
   */
  public registerContractAddress(
    contractName: string,
    address: string,
    deployer: string = ZERO_ADDRESS
  ): void {
    if (!this.isEnable) return;

    if (!contractName) throw new Error("The contract name is required");
    if (!address) throw new Error("The contract address is required");

    if (this.contracts.has(contractName)) {
      throw new Error(`The contract ${contractName} already exists`);
    } else {
      this.contracts.set(`${contractName}`, { address, deployer });
    }
  }

  /**
   * Registers a permission for a user on a contract.S
   * @param contract The name of the contract.
   * @param user The address of the user.
   * @param permission The permission to register.
   * @throws An error if the contract name, user address, or permission is missing.
   */
  public registerPermission(
    contract: string,
    contractAddress: string,
    authorizedAddress: string,
    permission: Permission,
    addressType: AddressType
  ): void {
    if (!this.isEnable) return;

    if (!contract) throw new Error("The contract name is required");
    if (!authorizedAddress) throw new Error("The user address is required");
    if (!permission) throw new Error("The permission is required");

    this.permissions.push({
      contract,
      contractAddress,
      authorizedAddress,
      permission,
      addressType,
    });
  }

  cleanEnergyReport(): void {
    const existsFile = fs.existsSync(this.defaultPermissionsReportPath);
    if (existsFile) fs.unlinkSync(this.defaultPermissionsReportPath);
  }

  addPermissionToReport(
    contract: string,
    contractAddress: string,
    authorizedAddress: string,
    permission: Permission,
    addressType: AddressType
  ): void {
    let report: any;

    try {
      // Read the contents of the file
      const reportContents = fs.readFileSync(
        this.defaultPermissionsReportPath,
        "utf8"
      );

      report = JSON.parse(reportContents);
    } catch (error) {
      report = [];
    }

    // Parse the contents as JSON

    // Add the new permission to the report
    report.push({
      contract,
      contractAddress,
      authorizedAddress,
      permission,
      addressType,
    });

    // Write the updated report back to the file
    fs.writeFileSync(
      this.defaultPermissionsReportPath,
      JSON.stringify(report, null, 2)
    );
  }

  public getPermissions(): PermissionObject[] | null {
    if (this.permissions.length === 0) return null;
    return this.permissions;
  }

  /**
   * Retrieves the address of a registered contract.
   * @param {string} contractName The name of the contract.
   * @returns {string} The address of the contract.
   */
  public getContractAddress(contractName: string): {
    address: string;
    deployer: string;
  } {
    if (this.contracts.has(contractName)) {
      return this.contracts.get(contractName)!;
    } else {
      throw new Error(`The contract ${contractName} has not been registered`);
    }
  }

  /**
   * Saves the contract addresses to a file.
   * @param {string} path The path to the file to save the report.
   */
  public saveDeploymentReport(
    path: string = this.defaultDeploymentReportPath
  ): void {
    if (!this.isEnable) return;

    if (this.contracts.size === 0) {
      console.warn("No contract addresses to save");
      return;
    }

    const json = JSON.stringify(Object.fromEntries(this.contracts), null, 2);

    try {
      fs.writeFileSync(path, json, "utf-8");
      this.logger.log(`Deployment report saved to ${path} ✅`);
    } catch (err: any) {
      this.logger.error(`Error saving deployment report: ${err.message}`);
    }

    this.clear();
  }

  public savePermissionReport(
    path: string = this.defaultPermissionsReportPath
  ) {
    if (!this.isEnable) return;

    if (this.permissions.length === 0) {
      console.warn("No permissions to save");
      return;
    }

    const json = JSON.stringify(this.permissions, null, 2);

    try {
      fs.writeFileSync(path, json, "utf-8");
      this.logger.log(`Permissions report saved to ${path} ✅`);
    } catch (error: any) {
      this.logger.error(`Error saving permissions report: ${error.message}`);
    }

    this.permissions = [];
  }

  /**
   * Clears the registered contract addresses.
   */
  public clear(): void {
    this.contracts.clear();
  }

  /**
   * Reads a report from a file and returns it as a Map.
   * If the file does not exist, it creates an empty file.
   *
   * @param {string} path - The path to the report file. Defaults to `this.defaultDeploymentReportPath`.
   * @returns Map<string, ContractInReport> - A Map where the keys are contract names and the values are `ContractInReport` objects.
   */
  public getReportFromFile(
    path: string = this.defaultDeploymentReportPath
  ): Map<string, ContractInReport> {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(__path.dirname(path), { recursive: true });
      fs.writeFileSync(path, JSON.stringify({}), "utf-8");
    }

    const fileContent = fs.readFileSync(path, "utf-8");
    const json = JSON.parse(fileContent);
    return new Map<string, ContractInReport>(Object.entries(json));
  }

  public addToDeploymentReport(
    contractName: string,
    contractsAddress: string,
    deployer: string,
    path?: string
  ): void {
    const currentReport = this.getReportFromFile();

    if (currentReport.has(contractName)) {
      throw new Error(`The contract ${contractName} already exists`);
    }

    const cleanedContractNamed = this._normalizeString(contractName);

    currentReport.set(`${cleanedContractNamed}`, {
      address: contractsAddress,
      deployer,
    });

    const json = JSON.stringify(Object.fromEntries(currentReport), null, 2);

    try {
      fs.writeFileSync(path ?? this.defaultDeploymentReportPath, json, "utf-8");
      this.logger.log(
        `Deployment report saved to ${this.defaultDeploymentReportPath} ✅`
      );
    } catch (err: any) {
      this.logger.error(`Error saving deployment report: ${err.message}`);
    }
  }

  public updateAddressInDeploymentReport(
    contractName: string,
    newAddress: string,
    deployer: string
  ) {
    const currentReport = this.getReportFromFile();

    currentReport.set(this._normalizeString(contractName), {
      address: newAddress,
      deployer,
    });

    const json = JSON.stringify(Object.fromEntries(currentReport), null, 2);

    try {
      fs.writeFileSync(this.defaultDeploymentReportPath, json, "utf-8");
      this.logger.log(`Deployment report updated ✅`);
    } catch (err: any) {
      this.logger.error(`Error updating deployment report: ${err.message}`);
    }
  }

  private _normalizeString(str: string): string {
    return str
      .normalize("NFD") // used to decompose the string into its constituent diacritical marks, using the NFD form.
      .replace(/[\u0300-\u036f]/g, "") // matches all Unicode characters that are diacritical marks
      .replace(/ /g, ""); // matches all spaces
  }
}

export const reporter = new Reporter(logger);
