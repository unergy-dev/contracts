import hre, { upgrades } from "hardhat";
import { logger } from "./logger";
import { UpgradeProxyOptions } from "@openzeppelin/hardhat-upgrades/dist/utils";
import { verify } from "./helpers";

var retries = 0;

export async function upgradeContract(
  contractName: string,
  proxyAddress: string,
  opts?: UpgradeProxyOptions
) {
  await hre.run("compile");

  const newImplFactory = await hre.ethers.getContractFactory(contractName);
  try {
    const newImpl = await hre.upgrades.upgradeProxy(
      proxyAddress,
      newImplFactory,
      opts
    );
    await newImpl.deployed();
    await verify(newImpl.address);

    logger.log(
      `New implementation (${contractName}) deployed to: ${newImpl.address}`
    );
  } catch (error) {
    if (retries < 1) {
      await upgrades.forceImport(proxyAddress, newImplFactory, opts);
      await upgradeContract(contractName, proxyAddress, opts);
      retries++;
    } else {
      logger.error(JSON.stringify(error));
      process.exit(1);
    }
  }
}
