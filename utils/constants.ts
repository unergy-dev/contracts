import { ethers } from "hardhat";
import { parseUnits } from "./helpers";
import dotenv from "dotenv";

dotenv.config();

/**
 * The default admin role from OpenZeppelin AccessControl.
 */
export const DEFAULT_ADMIN_ROLE = ethers.utils.hexZeroPad("0x00", 32);
export const ADMIN_ROLE = ethers.utils.keccak256(
  ethers.utils.toUtf8Bytes("ADMIN_ROLE")
);
export const PROTOCOL_CONTRACT_ROLE = ethers.utils.keccak256(
  ethers.utils.toUtf8Bytes("PROTOCOL_CONTRACT_ROLE")
);
export const HEALTH_CHECKER_ROLE = ethers.utils.keccak256(
  ethers.utils.toUtf8Bytes("HEALTH_CHECKER_ROLE")
);
export const DEPOSITER_ROLE = ethers.utils.keccak256(
  ethers.utils.toUtf8Bytes("DEPOSITER_ROLE")
);

/**
 * This value is used to represent the general asset in the CleanEnergyAssets contracts (it is settled in the constructor).
 */
export const GENERAL_ASSET_ID: 0 = 0;
export const STABLE_COIN_INITIAL_SUPPLY = parseUnits(20_000_000, 6);

export const assetManagerFeePercentage = parseUnits(2, 18);

export const swapFeePercentage = parseUnits(2, 18);

export const APPROVE_SWAP_AMOUNT = STABLE_COIN_INITIAL_SUPPLY;

export const maxUintHex = ethers.constants.MaxUint256._hex;

export const CLAIM_SERVICE_SIGNER_ADDRESS =
  process.env.CLAIM_SERVICE_SIGNER_ADDRESS || "";

export const POLYGON_USDC_ADDRESS =
  "0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174";

// This is used to access the balance in this account in the tests
export const POLYGON_BINANCE_HOT_WALLET_ADDRESS =
  "0xf977814e90da44bfa03b6295a0616a897441acec";

export const HARDHAT_DEFAULT_PRIVATE_KEY =
  "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80";

const _factories = async () => ({
  UnergyDataFactory: await ethers.getContractFactory("UnergyData"),
  UnergyBuyerFactory: await ethers.getContractFactory("UnergyBuyer"),
  UnergyLogicReserveFactory: await ethers.getContractFactory(
    "UnergyLogicReserve"
  ),
  ERC20UWattFactory: await ethers.getContractFactory("ERC20UWatt"),
  StableCoinFactory: await ethers.getContractFactory("ERC20StableCoin"),
  PermissionGranterFactory: await ethers.getContractFactory(
    "PermissionGranter"
  ),
  UnergyEventFactory: await ethers.getContractFactory("UnergyEvent"),
  CleanEnergyAssetFactory: await ethers.getContractFactory("CleanEnergyAssets"),
  ERC20ProjectFactory: await ethers.getContractFactory("ERC20Project"),
  ProjectsManagerFactory: await ethers.getContractFactory("ProjectsManager"),
});

export const factories = _factories().then((f) => f);

export const externalDependenciesEnabled =
  process.env.EXTERNAL_SERVICES_ENABLED === "true";

export const TRPC_SERVER_PORT = 4040;

export const ERC20_INSUFFICIENT_BALANCE_ERROR =
  "ERC20: transfer amount exceeds balance";

export const ERC20_INSUFFICIENT_ALLOWANCE_ERROR =
  "ERC20: insufficient allowance";

export const ZERO_ADDRESS = ethers.constants.AddressZero;

export const UWATT_USDT_POOL = "0x15455aE10dD0c3F896498B0EaDe476e05A929Ed2";

export const NOT_MINTED_ERROR = "NOT_MINTED";

/**
 * WAD = 10^18
 */
export const WAD = parseUnits(1, 18);
