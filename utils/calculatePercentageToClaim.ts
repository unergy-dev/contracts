export function claimCalculator(
  balance: bigint,
  supply: bigint,
  unergyBalance: bigint,
  pendingClaim?: bigint
) {
  const SCALAR = BigInt(10 ** 30);

  const calcPercentage = (
    balance: bigint,
    supply: bigint,
    pendingClaim?: bigint
  ): bigint => {
    return ((balance + (pendingClaim || BigInt(0))) * SCALAR) / supply;
  };

  const calcUWatts = (percentaje: bigint, unergyBalance: bigint): bigint => {
    return (unergyBalance * percentaje) / SCALAR;
  };

  const percentage = calcPercentage(balance, supply, pendingClaim);
  const uWatts = calcUWatts(percentage, unergyBalance);

  return uWatts;
}
