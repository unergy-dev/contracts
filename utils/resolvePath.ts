import path from "path";

export const resolvePath = (relativePath: string = "") =>
  path.join(process.cwd(), relativePath);
