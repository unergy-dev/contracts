import { ethers } from "hardhat";

export const factories = async () => ({
  DataStructure: await ethers.getContractFactory("DataStructure"),
  UnergyData: await ethers.getContractFactory("UnergyData"),
  UnergyLogicFunding: await ethers.getContractFactory("UnergyLogicFunding"),
  UnergyLogicOperation: await ethers.getContractFactory("UnergyLogicOperation"),
  ERC20UWatt: await ethers.getContractFactory("ERC20UWatt"),
  ERC20StableCoin: await ethers.getContractFactory("ERC20StableCoin"),
  PermissionGranter: await ethers.getContractFactory("PermissionGranter"),
  UnergyEvent: await ethers.getContractFactory("UnergyEvent"),
  CleanEnergyAssets: await ethers.getContractFactory("CleanEnergyAssets"),
  ERC20Project: await ethers.getContractFactory("ERC20Project"),
});
