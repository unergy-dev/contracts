export type Key = { encryptedKey: string };

const keys: Key[] = [];
export const keysDb = {
  key: {
    getKeys: () => keys,
    sendKey: (key: Key) => {
      keys.push(key);
      return key;
    },
  },
};
