import { ethers } from "hardhat";
import { ProjectInputStruct } from "../typechain-types/contracts/ProjectsManager";
import { input } from "@inquirer/prompts";

export async function validateProject(projectInput: ProjectInputStruct) {
  console.log("The project will be created with the following parameters:");

  const parsedProjectInput = {
    maintenancePercentage: ethers.utils.formatUnits(
      await projectInput.maintenancePercentage,
      18
    ),
    initialProjectValue: ethers.utils.formatUnits(
      await projectInput.initialProjectValue,
      6
    ),
    currentProjectValue: ethers.utils.formatUnits(
      await projectInput.currentProjectValue,
      6
    ),
    swapFactor: ethers.utils.formatUnits(await projectInput.swapFactor, 18),
    totalPWatts: ethers.utils.formatUnits(await projectInput.totalPWatts, 18),
    originatorFee: ethers.utils.formatUnits(await projectInput.originatorFee, 18),
    adminAddress: projectInput.adminAddr,
    installerAddress: projectInput.installerAddr,
    originatorAddress: projectInput.originator,
    stableAddress: projectInput.stableAddr,
  };

  console.table(parsedProjectInput);

  await input({
    message: "Press enter to continue.",
  });
}
