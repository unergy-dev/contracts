export enum PositionState {
  // Inexistent: If the position has not been created yet
  Inexistent,
  // Pending: If a position has been opened by a lender but not yet closed by a borrower
  Pending,
  // Active: When a borrower provides the collateral and the funds are used to take part in a project
  Active,
  // Delayed: When the project is not operational by the appropriate date (T)
  Delayed,
  // Default: If the project is not operational by the maximum date (T + T*)
  Default,
  // Canceled: If the lender withdraws the funds before the borrower provides the collateral
  Canceled,
  // Closed: If the project is in production and the pWatts are swapped
  Closed,
}
