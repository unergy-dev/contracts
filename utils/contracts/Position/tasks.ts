import { HardhatRuntimeEnvironment } from "hardhat/types";

export function parseMetadataTuple(metadata: any[], parseBigNumbers = false) {
  function parse(parseBigNumber: boolean, value: any) {
    return parseBigNumber ? value.toString() : value;
  }
  return {
    state: parse(parseBigNumbers, metadata[0]),
    lender: metadata[1],
    borrower: metadata[2],
    projectAddress: metadata[3],
    profitPercentage: parse(parseBigNumbers, metadata[4]),
    defaultProfitPercentage: parse(parseBigNumbers, metadata[5]),
    lockedAmount: parse(parseBigNumbers, metadata[6]),
    periodProfit: parse(parseBigNumbers, metadata[7]),
    lockedPeriod: parse(parseBigNumbers, metadata[8]),
    additionalPeriod: parse(parseBigNumbers, metadata[9]),
    collateral: parse(parseBigNumbers, metadata[10]),
    payedCompensation: parse(parseBigNumbers, metadata[11]),
    startDate: parse(parseBigNumbers, metadata[12]),
  };
}

export function decodeMetadataTask(
  taskArgs: { encodeString: string },
  hre: HardhatRuntimeEnvironment
) {
  const decodedMetadata = hre.ethers.utils.defaultAbiCoder.decode(
    [
      "uint",
      "address",
      "address",
      "address",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
    ],
    taskArgs.encodeString
  );

  const metadata = parseMetadataTuple(decodedMetadata as any[], true);

  console.log(`Decoded metadata:\n ${JSON.stringify(metadata, null, 2)}`);
}
