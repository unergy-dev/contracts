import { ethers } from "hardhat";
import { Position } from "../../../typechain-types";

export function Position(positionId: string): Promise<Position> {
  return ethers.getContractAt("Position", positionId);
}

export function decodeMetadata(bytesLike: string) {
  return ethers.utils.defaultAbiCoder.decode(
    [
      "uint",
      "address",
      "address",
      "address",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
      "uint",
    ],
    bytesLike
  );
}
