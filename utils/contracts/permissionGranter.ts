import { PermissionGranter } from "../../typechain-types";
import { PermissionType } from "../../types/contracts.types";
import { PROTOCOL_CONTRACT_ROLE } from "../constants";

export function permissionGranterCommunicator(pg: PermissionGranter) {
  async function setPermission(
    authorizedAddress: string,
    contractAddress: string,
    funcName: string,
    permissionType: PermissionType,
    permissionValue: number,
    opts?: {
      send: boolean;
    }
  ) {
    if (opts?.send) {
      const tx = await pg.setPermission(
        authorizedAddress,
        contractAddress,
        funcName,
        permissionType,
        permissionValue
      );
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await pg.populateTransaction.setPermission(
      authorizedAddress,
      contractAddress,
      funcName,
      permissionType,
      permissionValue
    );

    return {
      fName: "setPermission",
      data: tx.data,
    };
  }

  async function setMeterPermission(
    meterAddress: string,
    logicContractAddress: string,
    projectContractAddress: string,
    opts?: {
      send: boolean;
    }
  ) {
    if (opts?.send) {
      const tx = await pg.setMeterPermission(
        meterAddress,
        logicContractAddress,
        projectContractAddress
      );

      const receipt = await tx.wait();

      return receipt;
    }

    const tx = await pg.populateTransaction.setMeterPermission(
      meterAddress,
      logicContractAddress,
      projectContractAddress
    );

    return {
      fName: "setMeterPermission",
      data: tx.data,
    };
  }

  async function grantRole(
    role: string,
    account: string,
    opts?: {
      send: boolean;
    }
  ) {
    if (opts?.send) {
      const tx = await pg.grantRole(role, account);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await pg.populateTransaction.grantRole(role, account);
    return {
      fName: "grantRole",
      data: tx.data,
    };
  }

  async function grantProtocolContractRole(
    account: string,
    opts?: { send: boolean }
  ) {
    return grantRole(PROTOCOL_CONTRACT_ROLE, account, opts);
  }

  return {
    setPermission,
    setMeterPermission,
    grantRole,
    grantProtocolContractRole,
  };
}
