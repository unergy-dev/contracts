import { ethers } from "hardhat";
import { ProjectsManager } from "../../typechain-types";

export function projectsManagerCommunicator(pm: ProjectsManager) {
  async function configureProject(
    projectAddress: string,
    opts?: {
      send: boolean;
    }
  ) {
    if (opts?.send) {
      const tx = await pm.configureProject(projectAddress);
      const receipt = await tx.wait();
      return receipt;
    }

    const tx = await pm.populateTransaction.configureProject(projectAddress);

    return {
      fName: "configureProject",
      data: tx.data,
    };
  }

  return { configureProject };
}
