import hre from "hardhat";

import { Reporter } from "./reporter";

export class DeployerV2 {
  private readonly reporter: Reporter;

  constructor() {
    this.reporter = new Reporter(undefined);
  }

  /**
   *
   * * deploy
   *
   */
  async deploy<T>(contract: string, ...args: any[]): Promise<T> {
    const factory = await hre.ethers.getContractFactory(contract);
    const instance = await factory.deploy(...args);
    await instance.deployed();

    await this._saveDeployment(contract, instance.address);
    return instance as T;
  }

  /**
   *
   * * deployWithProxy
   *
   */
  async deployWithProxy<T>(contract: string, ...args: any[]): Promise<T> {
    const factory = await hre.ethers.getContractFactory(contract);
    const instance = await hre.upgrades.deployProxy(factory, args);
    await instance.deployed();

    await this._saveDeployment(contract, instance.address);
    return instance as T;
  }

  /**
   *
   * * deployWithDiamond
   *
   */
  async deployWithDiamond() {
    throw new Error("Not implemented");
  }

  /**
   * * Private
   */

  async _saveDeployment(contract: string, address: string) {
    const signer = (await hre.ethers.getSigners())[0];
    try {
      this.reporter.addToDeploymentReport(contract, address, signer.address);
    } catch (error) {
      console.error(`Error saving deployment: ${error}`);
    }
  }
}
