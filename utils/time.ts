export enum TimeExpression {
  // 1 day in seconds
  Day = 86400,
  // 1 week in seconds
  Week = 604800,
  // 1 month in seconds
  Month = 2592000,
  // 1 year in seconds
  Year = 31536000,
}
