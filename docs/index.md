# Solidity API

## UnergyEventAbs

### beforeTransferReceipt

```solidity
function beforeTransferReceipt(address origin, address from, address to, uint256 amount) public virtual
```

### afterTransferReceipt

```solidity
function afterTransferReceipt(address origin, address from, address to, uint256 amount) public virtual
```

## ERC20Abs

### burn

```solidity
function burn(address account, uint256 amount) public virtual
```

### burn

```solidity
function burn(uint256 amount) public virtual
```

### burnFrom

```solidity
function burnFrom(address owner, uint256 amount) public virtual
```

### approveSwap

```solidity
function approveSwap(address owner, address spender, uint256 amount) public virtual
```

### allowance

```solidity
function allowance(address owner, address spender) public virtual returns (uint256)
```

### decimals

```solidity
function decimals() public view virtual returns (uint8)
```

### mint

```solidity
function mint(address account, uint256 amount) public virtual
```

### transferFrom

```solidity
function transferFrom(address from, address to, uint256 amount) public virtual returns (bool)
```

### transfer

```solidity
function transfer(address to, uint256 amount) public virtual returns (bool)
```

### setState

```solidity
function setState(enum ProjectState state) public virtual
```

### name

```solidity
function name() public virtual returns (string)
```

### approve

```solidity
function approve(address spender, uint256 amount) public virtual
```

### balanceOf

```solidity
function balanceOf(address account) public virtual returns (uint256)
```

## ERC1155Abs

### createProjectEnergyAsset

```solidity
function createProjectEnergyAsset(string tokenName, address projectAddr) public virtual
```

### mint

```solidity
function mint(address projectAddr, uint256 amount) public virtual
```

### burn

```solidity
function burn(address projectAddr, uint256 amount) public virtual
```

## Common

### permissionGranter

```solidity
contract PermissionGranter permissionGranter
```

### constructor

```solidity
constructor(address permissionGranterAddr) public
```

### setPermissionGranterAddr

```solidity
function setPermissionGranterAddr(address permissionGranterAddr) public
```

### getPermissionGranterAddress

```solidity
function getPermissionGranterAddress() public view returns (address)
```

### _hasRoleInPermissionGranter

```solidity
function _hasRoleInPermissionGranter(address _caller, address _contract, string _functionName) internal
```

### _hasMeterPermissionOverProjectContract

```solidity
function _hasMeterPermissionOverProjectContract(address _caller, address _logicContract, address _projectContract) internal view
```

### notZeroAddress

```solidity
modifier notZeroAddress(address _address)
```

### _notZeroAddress

```solidity
function _notZeroAddress(address _address) internal pure
```

### _checkPermission

```solidity
function _checkPermission(bool _hasPermission, string _functionName, address _caller) internal pure
```

## CommonUpgradeable

### permissionGranter

```solidity
contract PermissionGranter permissionGranter
```

### __Common_init

```solidity
function __Common_init(address permissionGranterAddr) internal
```

### setPermissionGranterAddr

```solidity
function setPermissionGranterAddr(address permissionGranterAddr) public
```

### getPermissionGranterAddress

```solidity
function getPermissionGranterAddress() public view returns (address)
```

### _hasRoleInPermissionGranter

```solidity
function _hasRoleInPermissionGranter(address _caller, address _contract, string _functionName) internal
```

### _hasMeterPermissionOverProjectContract

```solidity
function _hasMeterPermissionOverProjectContract(address _caller, address _logicContract, address _projectContract) internal view
```

### notZeroAddress

```solidity
modifier notZeroAddress(address _address)
```

### _notZeroAddress

```solidity
function _notZeroAddress(address _address) internal pure
```

### _checkPermission

```solidity
function _checkPermission(bool _hasPermission, string _functionName, address _caller) internal pure
```

## CleanEnergyAssets

This contract handles the logic to keep track of energy generation
and generate Renewable Energy Certificates (RECs).

### tokenId

```solidity
uint256 tokenId
```

### recDecimals

```solidity
uint256 recDecimals
```

_is the energy limit to create a REC (Renewable Energy Certificate)_

### energyDecimals

```solidity
uint256 energyDecimals
```

### energyLimit

```solidity
uint256 energyLimit
```

### tokenNameByTokenID

```solidity
mapping(uint256 => string) tokenNameByTokenID
```

### tokenIdByProjectAddress

```solidity
mapping(address => uint256) tokenIdByProjectAddress
```

### isREC

```solidity
mapping(uint256 => bool) isREC
```

### mintedEnergyByProject

```solidity
mapping(address => uint256) mintedEnergyByProject
```

### RECIdByAddress

```solidity
mapping(address => uint256) RECIdByAddress
```

### burnedAssets

```solidity
mapping(address => uint256) burnedAssets
```

### URIUpdated

```solidity
event URIUpdated(string newURI)
```

### ZeroOrNegativeAmount

```solidity
error ZeroOrNegativeAmount()
```

### NotREC

```solidity
error NotREC(uint256 tokenId)
```

### TokenNotCreatedYet

```solidity
error TokenNotCreatedYet(address projectAddress)
```

### constructor

```solidity
constructor(address _permissionGranterAddr) public
```

### _createGeneralEnergyAsset

```solidity
function _createGeneralEnergyAsset(string _tokenName) internal
```

### createProjectEnergyAsset

```solidity
function createProjectEnergyAsset(string _tokenName, address _projectAddr) external
```

This function creates the project energy asset and their respective REC.

_This function will be called when a new project is created._

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _tokenName | string | Name of the general clean energy asset. |
| _projectAddr | address | New Project Address |

### mint

```solidity
function mint(address _projectAddr, uint256 _amount) public
```

This function creates energy assets of a project and
if the accumulated energy exceeds the limit, new RECs are created.

_This function will be called from a new energy Report._

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddr | address | New Project Address |
| _amount | uint256 | Name of the general clean energy asset. |

### burn

```solidity
function burn(address _projectAddr, uint256 _amount) external
```

### burnREC

```solidity
function burnREC(uint256 _tokenId, uint256 amount) public
```

### withdrawRECs

```solidity
function withdrawRECs(uint256 _tokenId, address _receiver, uint256 _amount) external
```

### setURI

```solidity
function setURI(string _newUri) public
```

### setEnergyLimit

```solidity
function setEnergyLimit(uint256 _energyLimit) external
```

### onERC1155Received

```solidity
function onERC1155Received(address, address, uint256, uint256, bytes) external virtual returns (bytes4)
```

### onERC1155BatchReceived

```solidity
function onERC1155BatchReceived(address, address, uint256[], uint256[], bytes) external virtual returns (bytes4)
```

### pause

```solidity
function pause() external
```

### unpause

```solidity
function unpause() external
```

### _afterTokenTransfer

```solidity
function _afterTokenTransfer(address, address from, address to, uint256[] ids, uint256[] amounts, bytes) internal virtual
```

### _beforeTokenTransfer

```solidity
function _beforeTokenTransfer(address operator, address from, address to, uint256[] ids, uint256[] amounts, bytes data) internal
```

_Hook that is called before any token transfer. This includes minting
and burning, as well as batched variants.

The same hook is called on both single and batched variants. For single
transfers, the length of the `ids` and `amounts` arrays will be 1.

Calling conditions (for each `id` and `amount` pair):

- When `from` and `to` are both non-zero, `amount` of ``from``'s tokens
of token type `id` will be  transferred to `to`.
- When `from` is zero, `amount` tokens of token type `id` will be minted
for `to`.
- when `to` is zero, `amount` of ``from``'s tokens of token type `id`
will be burned.
- `from` and `to` are never both zero.
- `ids` and `amounts` have the same, non-zero length.

To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks]._

## ERC20Project

This contract handles the logic for raising the financing needed for a new clean energy project.

### UnergyEventAddressUpdated

```solidity
event UnergyEventAddressUpdated(address unergyEventAddr)
```

### ProjectsManagerAddressUpdated

```solidity
event ProjectsManagerAddressUpdated(address projectsManagerAddr)
```

### ProjectIsNotInFundingOrInstalledState

```solidity
error ProjectIsNotInFundingOrInstalledState()
```

### TransferToHimself

```solidity
error TransferToHimself(address destinationAddress)
```

### constructor

```solidity
constructor(string name_, string symbol_, address _unergyDataAddress, address _permissionGranterAddres) public
```

### inFundingOrInstalled

```solidity
modifier inFundingOrInstalled()
```

### mint

```solidity
function mint(address account, uint256 amount) external
```

### burn

```solidity
function burn(address account, uint256 amount) external
```

### transfer

```solidity
function transfer(address to, uint256 amount) public virtual returns (bool)
```

_See {IERC20-transfer}.

Requirements:

- `to` cannot be the zero address.
- the caller must have a balance of at least `amount`._

### transferFrom

```solidity
function transferFrom(address from, address to, uint256 amount) public virtual returns (bool)
```

_See {IERC20-transferFrom}.

Emits an {Approval} event indicating the updated allowance. This is not
required by the EIP. See the note at the beginning of {ERC20}.

NOTE: Does not update the allowance if the current allowance
is the maximum `uint256`.

Requirements:

- `from` and `to` cannot be the zero address.
- `from` must have a balance of at least `amount`.
- the caller must have allowance for ``from``'s tokens of at least
`amount`._

### _beforeTokenTransfer

```solidity
function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual
```

_Hook that is called before any transfer of tokens. This includes
minting and burning.

Calling conditions:

- when `from` and `to` are both non-zero, `amount` of ``from``'s tokens
will be transferred to `to`.
- when `from` is zero, `amount` tokens will be minted for `to`.
- when `to` is zero, `amount` of ``from``'s tokens will be burned.
- `from` and `to` are never both zero.

To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks]._

### _afterTokenTransfer

```solidity
function _afterTokenTransfer(address from, address to, uint256 amount) internal virtual
```

_Hook that is called after any transfer of tokens. This includes
minting and burning.

Calling conditions:

- when `from` and `to` are both non-zero, `amount` of ``from``'s tokens
has been transferred to `to`.
- when `from` is zero, `amount` tokens have been minted for `to`.
- when `to` is zero, `amount` of ``from``'s tokens have been burned.
- `from` and `to` are never both zero.

To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks]._

### approveSwap

```solidity
function approveSwap(address holder, address spender, uint256 amount) public returns (bool)
```

This function is only allowed for addresses who has approveSwap permission.

_This approve function is called when a protocol contract needs to
give pWatts allowance to an another address (_spender)._

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| holder | address | is the public address who has pWatts. |
| spender | address | is the public address who spend pWatts. |
| amount | uint256 | is the amount allowed to spend. |

### approve

```solidity
function approve(address spender, uint256 amount) public virtual returns (bool)
```

_See {IERC20-approve}.

NOTE: If `amount` is the maximum `uint256`, the allowance is not updated on
`transferFrom`. This is semantically equivalent to an infinite approval.

Requirements:

- `spender` cannot be the zero address._

### increaseAllowance

```solidity
function increaseAllowance(address holder, uint256 addedValue) public returns (bool)
```

### decreaseAllowance

```solidity
function decreaseAllowance(address holder, uint256 subtractedValue) public returns (bool)
```

### decimals

```solidity
function decimals() public view virtual returns (uint8)
```

_Returns the number of decimals used to get its user representation.
For example, if `decimals` equals `2`, a balance of `505` tokens should
be displayed to a user as `5.05` (`505 / 10 ** 2`).

Tokens usually opt for a value of 18, imitating the relationship between
Ether and Wei. This is the value {ERC20} uses, unless this function is
overridden;

NOTE: This information is only used for _display_ purposes: it in
no way affects any of the arithmetic of the contract, including
{IERC20-balanceOf} and {IERC20-transfer}._

### transferOwnership

```solidity
function transferOwnership(address newOwner) public
```

_Transfers ownership of the contract to a new account (`newOwner`).
Can only be called by the current owner._

## ERC20UWatt

This contract handles the logic for our stable currency collateralized in clean energy.

### potentialOwner

```solidity
address potentialOwner
```

### UnergyEventAddressUpdated

```solidity
event UnergyEventAddressUpdated(address unergyEventAddr)
```

### OwnerNominated

```solidity
event OwnerNominated(address pendingOwner)
```

### TransferToHimself

```solidity
error TransferToHimself(address origin, address destinationAddress)
```

### constructor

```solidity
constructor(address unergyDataAddress_, address permissionGranterAddres_) public
```

### mint

```solidity
function mint(address account, uint256 amount) external
```

### transfer

```solidity
function transfer(address to, uint256 amount) public virtual returns (bool)
```

_See {IERC20-transfer}.

Requirements:

- `to` cannot be the zero address.
- the caller must have a balance of at least `amount`._

### transferFrom

```solidity
function transferFrom(address from, address to, uint256 amount) public virtual returns (bool)
```

_See {IERC20-transferFrom}.

Emits an {Approval} event indicating the updated allowance. This is not
required by the EIP. See the note at the beginning of {ERC20}.

NOTE: Does not update the allowance if the current allowance
is the maximum `uint256`.

Requirements:

- `from` and `to` cannot be the zero address.
- `from` must have a balance of at least `amount`.
- the caller must have allowance for ``from``'s tokens of at least
`amount`._

### burn

```solidity
function burn(uint256 amount) external
```

### burnFrom

```solidity
function burnFrom(address account, uint256 amount) external
```

### decimals

```solidity
function decimals() public pure returns (uint8)
```

_Returns the number of decimals used to get its user representation.
For example, if `decimals` equals `2`, a balance of `505` tokens should
be displayed to a user as `5.05` (`505 / 10 ** 2`).

Tokens usually opt for a value of 18, imitating the relationship between
Ether and Wei. This is the value {ERC20} uses, unless this function is
overridden;

NOTE: This information is only used for _display_ purposes: it in
no way affects any of the arithmetic of the contract, including
{IERC20-balanceOf} and {IERC20-transfer}._

### _beforeTokenTransfer

```solidity
function _beforeTokenTransfer(address _from, address _to, uint256 _amount) internal virtual
```

### transferOwnership

```solidity
function transferOwnership(address _pendingOwner) public
```

### acceptOwnership

```solidity
function acceptOwnership() external
```

### _afterTokenTransfer

```solidity
function _afterTokenTransfer(address from, address to, uint256 amount) internal
```

### _mint

```solidity
function _mint(address to, uint256 amount) internal
```

### _burn

```solidity
function _burn(address account, uint256 amount) internal
```

## IPosition

### PositionState

```solidity
enum PositionState {
  Inexistent,
  Pending,
  Active,
  Delayed,
  Default,
  Canceled,
  Closed
}
```

### PositionMetadata

```solidity
struct PositionMetadata {
  enum IPosition.PositionState state;
  address lender;
  address borrower;
  address projectAddress;
  uint256 profitPercentage;
  uint256 defaultProfitPercentage;
  uint256 lockedAmount;
  uint256 periodProfit;
  uint256 lockedPeriod;
  uint256 additionalPeriod;
  uint256 collateral;
  uint256 payedCompensation;
  uint256 startDate;
}
```

### withdrawProfit

```solidity
function withdrawProfit() external
```

### withdrawCompensation

```solidity
function withdrawCompensation() external
```

### getMetadata

```solidity
function getMetadata() external view returns (struct IPosition.PositionMetadata)
```

### approveStableSpend

```solidity
function approveStableSpend() external
```

### updateState

```solidity
function updateState(enum IPosition.PositionState _state) external
```

### setBorrower

```solidity
function setBorrower(address _borrower) external
```

### setStartDate

```solidity
function setStartDate(uint256 _startDate) external
```

## LendingManager

### WAD

```solidity
uint256 WAD
```

### stableCoin

```solidity
contract IERC20 stableCoin
```

### unergyData

```solidity
contract UnergyData unergyData
```

### unergyPM

```solidity
contract IProjectsManager unergyPM
```

### isPosition

```solidity
mapping(address => bool) isPosition
```

### uniswapV3Pool

```solidity
address uniswapV3Pool
```

### PositionCreated

```solidity
event PositionCreated(address positionAddress)
```

### PositionClosed

```solidity
event PositionClosed(address positionAddress, address borrower)
```

### LendingManager_InvalidValue

```solidity
error LendingManager_InvalidValue(uint256 upperRateLimit, uint256 bottomRateLimit, uint256 desiredProfit)
```

### LendingManager_InvalidLockedPeriod

```solidity
error LendingManager_InvalidLockedPeriod(uint256 minLockedPeriod, uint256 desiredLockedPeriod)
```

### LendingManager_InvalidRate

```solidity
error LendingManager_InvalidRate()
```

### LendingManager_InsuffitientPWatts

```solidity
error LendingManager_InsuffitientPWatts()
```

### LendingManager_PositionDoesNotExists

```solidity
error LendingManager_PositionDoesNotExists()
```

### LendingManager_PositionNotAvailable

```solidity
error LendingManager_PositionNotAvailable()
```

### LendingManager_PositionNotInDefaultWaitingPeriod

```solidity
error LendingManager_PositionNotInDefaultWaitingPeriod()
```

### LendingManager_CompensationMustBeGraterThanRegularProfit

```solidity
error LendingManager_CompensationMustBeGraterThanRegularProfit()
```

### constructor

```solidity
constructor(address _initialOwner, address _stableCoin, address _unergyData, address _uniswapV3Pool) public
```

### getProfitRatePerProject

```solidity
function getProfitRatePerProject(address _projectAddress) public view returns (uint256[2])
```

### getCompensationRatePerProject

```solidity
function getCompensationRatePerProject(address _projectAddress) public view returns (uint256[2])
```

### getPeriodRatePerProject

```solidity
function getPeriodRatePerProject(address _projectAddress) public view returns (uint256[2])
```

### getDefaultWaitingPeriod

```solidity
function getDefaultWaitingPeriod(address _projectAddress) public view returns (uint256[2])
```

### getDailyProfit

```solidity
function getDailyProfit(uint256 _lockedAmount, uint256 _annualPercentage) public pure returns (uint256)
```

### createPosition

```solidity
function createPosition(address _projectAddress, uint256 _anualRevenuePercentage, uint256 _anualCompensationPercentage, uint256 _lockedPeriod, uint256 _addtionalPeriod, uint256 _lockedAmount) external returns (address positionAddress)
```

Use this method to open a position as `Lender`.

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddress | address | The address of the project that will receive the funds. |
| _anualRevenuePercentage | uint256 | The annual profit rate that the project will pay to the Lender. |
| _anualCompensationPercentage | uint256 | The annual profit rate that the project will pay to the Lender if the project is in default. |
| _lockedPeriod | uint256 | The period of time that the Lender will lock the funds (in days). |
| _addtionalPeriod | uint256 | The maximum time the lender will wait to receive compensation (in days). |
| _lockedAmount | uint256 | The amount of stable coins that the Lender will provide. |

### closePosition

```solidity
function closePosition(address _positionAddress) external
```

Closing a position means providing collateral for an open position.

### cancelPosition

```solidity
function cancelPosition(address _positionAddress) external
```

Cancel a position means that the Lender will withdraw the funds that he has provided.

_This method can only be called by the Lender and if the position is still Pending._

### getPositions

```solidity
function getPositions() external view returns (address[])
```

### getProjectState

```solidity
function getProjectState(address _projectAddress) external view returns (enum ProjectState)
```

### setProfitRate

```solidity
function setProfitRate(address _projectAddress, uint256[2] _rate) external
```

An authorised Unergy account can set the profit Rate for each project.
        This Rate is used when a position is opened. However, subsequent changes
        will not affect positions already opened.

### setPeriodRate

```solidity
function setPeriodRate(address _projectAddress, uint256[2] _rate) external
```

### setCompensationRate

```solidity
function setCompensationRate(address _projectAddress, uint256[2] _rate) external
```

### checkDefaultRate

```solidity
function checkDefaultRate(address _projectAddress, uint256[2] _rate) external
```

### _checkProfitRate

```solidity
function _checkProfitRate(uint256 _value, address _projectAddress) internal view
```

### _checkCompensationRate

```solidity
function _checkCompensationRate(uint256 _value, address _projectAddress) internal view
```

### _checkPeriodRate

```solidity
function _checkPeriodRate(uint256 _value, address _projectAddress) internal view
```

### _checkWaitingPeriodRate

```solidity
function _checkWaitingPeriodRate(uint256 _value, address _projectAddress) internal view
```

### _checkLockedAmount

```solidity
function _checkLockedAmount(uint256 _lockedAmount, address _projectAddress) internal view
```

### _checkCompensationValue

```solidity
function _checkCompensationValue(uint256 _anualRevenuePercentage, uint256 _anualCompensationPercentage) internal pure
```

### _valueInRange

```solidity
function _valueInRange(uint256[2] _range, uint256 _value) internal pure
```

### _validRate

```solidity
function _validRate(uint256[2] _rate) internal pure
```

### _getPositionMetadata

```solidity
function _getPositionMetadata(address _positionAddress) internal view returns (struct IPosition.PositionMetadata)
```

### _buildPositionMetadata

```solidity
function _buildPositionMetadata(address _projectAddress, uint256 _anualRevenuePercentage, uint256 _anualCompensationPercentage, uint256 _lockedPeriod, uint256 _addtionalPeriod, uint256 _lockedAmount) internal view returns (struct IPosition.PositionMetadata positionMetadata, address lender)
```

### _getPeriodProfit

```solidity
function _getPeriodProfit(uint256 _lockedAmount, uint256 _annualPercentage, uint256 _lockedPeriod) internal pure returns (uint256)
```

### _getAnualProfit

```solidity
function _getAnualProfit(uint256 _lockedAmount, uint256 _annualPercentage) internal pure returns (uint256)
```

## Position

### uWatt

```solidity
contract IERC20 uWatt
```

### USDT

```solidity
contract IERC20 USDT
```

### LENDER_ID

```solidity
uint8 LENDER_ID
```

### BORROWER_ID

```solidity
uint8 BORROWER_ID
```

### unergyLogicAddress

```solidity
address unergyLogicAddress
```

### lendingManager

```solidity
contract LendingManager lendingManager
```

### Position_InvalidId

```solidity
error Position_InvalidId()
```

### Position_Unauthorized

```solidity
error Position_Unauthorized()
```

### Position_AlreadyCompensated

```solidity
error Position_AlreadyCompensated()
```

### Position_DefaultDateNotReached

```solidity
error Position_DefaultDateNotReached()
```

### Position_DelayedDateNotReached

```solidity
error Position_DelayedDateNotReached()
```

### Position_CompensationNotAvailable

```solidity
error Position_CompensationNotAvailable()
```

### Position_InvalidProjectState

```solidity
error Position_InvalidProjectState(enum ProjectState currentState)
```

### Position_InvalidPositionState

```solidity
error Position_InvalidPositionState(enum IPosition.PositionState currentState)
```

### onlyOnwerOf

```solidity
modifier onlyOnwerOf(uint256 id)
```

### constructor

```solidity
constructor(string _name, string _symbol, address _uWattAddress, address _stableAddress, address _unergyLogicAddress, address _uniV3Pool, struct IPosition.PositionMetadata _metadata) public
```

### transferFrom

```solidity
function transferFrom(address from, address to, uint256 id) public virtual
```

### tokenURI

```solidity
function tokenURI(uint256 id) public pure virtual returns (string)
```

### burn

```solidity
function burn(uint256 id) public virtual
```

### refund

```solidity
function refund() public virtual
```

### withdrawProfit

```solidity
function withdrawProfit() external
```

### withdrawCompensation

```solidity
function withdrawCompensation() external
```

### declareDelay

```solidity
function declareDelay() external
```

### declareDefault

```solidity
function declareDefault() external
```

### _transferPWatts

```solidity
function _transferPWatts(address projectAddress, address lender) internal
```

### getMetadata

```solidity
function getMetadata() external view returns (struct IPosition.PositionMetadata)
```

### approveStableSpend

```solidity
function approveStableSpend() external
```

### updateState

```solidity
function updateState(enum IPosition.PositionState _state) public
```

### setBorrower

```solidity
function setBorrower(address _borrower) external
```

### setStartDate

```solidity
function setStartDate(uint256 _startDate) external
```

### _getExpectedProfitInUWatt

```solidity
function _getExpectedProfitInUWatt() internal view returns (uint256)
```

### _onlyOnwerOf

```solidity
function _onlyOnwerOf(uint256 id) internal view
```

### _closePosition

```solidity
function _closePosition() internal
```

## UniswapV3PoolAdapter

### uWattUSDTPool

```solidity
contract IUniswapV3Pool uWattUSDTPool
```

### constructor

```solidity
constructor(address _uWattUSDTPool) public
```

### getUWattsIn

```solidity
function getUWattsIn(uint256 usdtOut) external view returns (uint256 uWattIn)
```

### getUSDTPriceInUWatt

```solidity
function getUSDTPriceInUWatt() public view returns (uint256)
```

## PermissionGranter

This contract handles the permissions of the contracts that are used in the Unergy Protocol.

### PROTOCOL_CONTRACT_ROLE

```solidity
bytes32 PROTOCOL_CONTRACT_ROLE
```

### PermissionGranted

```solidity
event PermissionGranted(address _address, address _contract, string _fname)
```

### PermissionRevoked

```solidity
event PermissionRevoked(address _address, address _contract, string _fname)
```

### NotAuthorizedToCallFunction

```solidity
error NotAuthorizedToCallFunction(string functionName, address caller)
```

### ZeroAddressNotAllowed

```solidity
error ZeroAddressNotAllowed()
```

### ArraysLengthMismatch

```solidity
error ArraysLengthMismatch()
```

### notZeroAddress

```solidity
modifier notZeroAddress(address _address)
```

### constructor

```solidity
constructor() public
```

### initialize

```solidity
function initialize(address _admin) external
```

### _authorizeUpgrade

```solidity
function _authorizeUpgrade(address newImplementation) internal
```

_Function that should revert when `msg.sender` is not authorized to upgrade the contract. Called by
{upgradeTo} and {upgradeToAndCall}.

Normally, this function will use an xref:access.adoc[access control] modifier such as {Ownable-onlyOwner}.

```solidity
function _authorizeUpgrade(address) internal override onlyOwner {}
```_

### setPermission

```solidity
function setPermission(address _address, address _contract, string _fname, enum PermissionType _permissionType, uint256 _permissionValue) external
```

This function is used to allow a `caller` to execute a function

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _address | address | The address of the caller |
| _contract | address | The contract over the caller is allowed to execute the function |
| _fname | string | The name of the function the caller is allowed to execute |
| _permissionType | enum PermissionType |  |
| _permissionValue | uint256 |  |

### setPermissionsBatch

```solidity
function setPermissionsBatch(address[] _addresses, address[] _contracts, string[] _fnames, enum PermissionType[] _permissionTypes, uint256[] _permissionValues) external
```

### _setPermission

```solidity
function _setPermission(address _address, address _contract, string _fname, enum PermissionType _permissionType, uint256 _permissionValue) internal
```

### setMeterPermission

```solidity
function setMeterPermission(address _meterAddress, address _logicContract, address _projectContract) external
```

This function is used to allow a meter account to execute the energyReport function of the logic contract

### setMeterPermissionsBatch

```solidity
function setMeterPermissionsBatch(address[] _meterAddresses, address _logicContract, address[] _projectContracts) external
```

### _setMeterPermission

```solidity
function _setMeterPermission(address _meterAddress, address _logicContract, address _projectContract) internal
```

### revokeMeterPermission

```solidity
function revokeMeterPermission(address _meterAddress, address _logicContract, address _projectContract) external
```

### getMeterPermission

```solidity
function getMeterPermission(address _meterAddress, address _logicContract, address _projectContract) external view returns (bool)
```

_This function does not relies on the `getAndUpdatePermission` because the permission is always permanent_

### revokePermission

```solidity
function revokePermission(address _address, address _contract, string _fname, uint256 _permissionValue) external
```

This function is used to revoke the permission of a `caller` to execute a function

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _address | address | { see: this.setPermission } |
| _contract | address | { see: this.setPermission } |
| _fname | string | { see: this.setPermission } |
| _permissionValue | uint256 |  |

### getPermission

```solidity
function getPermission(address _address, address _contract, string _fname) public view returns (bool)
```

The functions returns a boolean indicating if a `caller` is allowed to execute a function

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _address | address | { see: this.setPermission } |
| _contract | address | { see: this.setPermission } |
| _fname | string | { see: this.setPermission } |

### getAndUpdatePermission

```solidity
function getAndUpdatePermission(address _address, address _contract, string _fname) external returns (bool)
```

### _notZeroAddress

```solidity
function _notZeroAddress(address _address) internal pure
```

## ProjectsManager

This contract manages the project-related operations and stores the data related to it.

### potentialOwner

```solidity
address potentialOwner
```

### projectConfigured

```solidity
mapping(address => bool) projectConfigured
```

### initialTotalSupply

```solidity
mapping(address => uint256) initialTotalSupply
```

### SignatureSet

```solidity
event SignatureSet(address projectAddress, enum Signature signature)
```

### MilestoneUpdated

```solidity
event MilestoneUpdated(address projectAddress, string name)
```

### MilestonesUpdated

```solidity
event MilestonesUpdated(address projectAddress)
```

### MilestoneAdded

```solidity
event MilestoneAdded(address projectAddress, string name)
```

### ProjectCreated

```solidity
event ProjectCreated(address projectAddress)
```

### ProjectConfigured

```solidity
event ProjectConfigured(address projectAddress)
```

### ProjectUpdated

```solidity
event ProjectUpdated(address projectAddress)
```

### ProjectHolderAdded

```solidity
event ProjectHolderAdded(address projectAddress, address holder)
```

### OwnerNominated

```solidity
event OwnerNominated(address pendingOwner)
```

### ProjectDoesNotExist

```solidity
error ProjectDoesNotExist(address projectAddress)
```

### MilestoneAlreadyReached

```solidity
error MilestoneAlreadyReached(string name)
```

### MaintenancePercentageExceeded

```solidity
error MaintenancePercentageExceeded(uint256 givenMaintenancePercentage)
```

### OriginatorFeeExceeded

```solidity
error OriginatorFeeExceeded(uint256 givenOriginatorFee)
```

### ApproveSwapFailed

```solidity
error ApproveSwapFailed()
```

### ProjectAlreadyConfigured

```solidity
error ProjectAlreadyConfigured(address projectAddress)
```

### MilestoneWeightsExceeded

```solidity
error MilestoneWeightsExceeded(uint256 milestoneWeights)
```

### NotPotentialOwner

```solidity
error NotPotentialOwner(address potentialOwner)
```

### InvalidInputLength

```solidity
error InvalidInputLength(uint256 expectedLength, uint256 givenLength)
```

### ifProjectExists

```solidity
modifier ifProjectExists(address _projectAddress)
```

### constructor

```solidity
constructor() public
```

### initialize

```solidity
function initialize(address _unergyDataAddr, address _permissionGranterAddr) external
```

### _authorizeUpgrade

```solidity
function _authorizeUpgrade(address newImplementation) internal
```

_Function that should revert when `msg.sender` is not authorized to upgrade the contract. Called by
{upgradeTo} and {upgradeToAndCall}.

Normally, this function will use an xref:access.adoc[access control] modifier such as {Ownable-onlyOwner}.

```solidity
function _authorizeUpgrade(address) internal override onlyOwner {}
```_

### createProject

```solidity
function createProject(struct ProjectInput _projectInput, string _projectName, string _projectSymbol) external
```

Allows to admin user  to create a  new project

_This function deploys an ERC20 Project and configures all necessary permissions._

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectInput | struct ProjectInput |  |
| _projectName | string | the ERC20 name |
| _projectSymbol | string | the ERC20 symbol |

### configureProject

```solidity
function configureProject(address erc20ProjectAddress) external
```

### updateProject

```solidity
function updateProject(address _projectAddress, struct Project _project) external
```

### setProjectState

```solidity
function setProjectState(address _projectAddress, enum ProjectState _projectState) external
```

### addProjectHolder

```solidity
function addProjectHolder(address _projectAddress, address _holderAddress) external
```

### addProjectMilestone

```solidity
function addProjectMilestone(address _projectAddress, string _name, uint256 weight) external
```

### updateProjectMilestones

```solidity
function updateProjectMilestones(address _projectAddress, struct Milestone[] _milestones) external
```

### updateMilestoneAtIndex

```solidity
function updateMilestoneAtIndex(address _projectAddress, uint256 _index, struct Milestone _milestone) external
```

### _checkMilestoneWeights

```solidity
function _checkMilestoneWeights(struct Milestone[] _milestones) internal pure
```

### setSignature

```solidity
function setSignature(address _projectAddress, enum Signature _signature) external
```

Sets the project signature to the given value

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddress | address | the address of the project whose signature will be modified |
| _signature | enum Signature | installer or originator signature {see: Signature on Types.sol} |

### getProject

```solidity
function getProject(address _projectAddress) public view returns (struct Project)
```

This function finds a project and revert if doesn't exists

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddress | address | The address of the project to look for |

#### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | struct Project | Project |

### getProjectHolders

```solidity
function getProjectHolders(address _projectAddress) external view returns (address[])
```

### getTotalPWatts

```solidity
function getTotalPWatts(address _projectAddress) external view returns (uint256)
```

### getProjectState

```solidity
function getProjectState(address _projectAddress) external view returns (enum ProjectState)
```

### getProjectMilestones

```solidity
function getProjectMilestones(address _projectAddress) external view returns (struct Milestone[])
```

### getSignatures

```solidity
function getSignatures(address _projectAddress) external view returns (bool[2])
```

### transferOwnership

```solidity
function transferOwnership(address _pendingOwner) public
```

### acceptOwnership

```solidity
function acceptOwnership() external
```

### pause

```solidity
function pause() external
```

### unpause

```solidity
function unpause() external
```

## PermissionType

```solidity
enum PermissionType {
  UNAUTHORIZED,
  PERMANENT,
  EXECUTIONS,
  TIMELOCK,
  ONETIME
}
```

## Permission

```solidity
struct Permission {
  enum PermissionType _type;
  uint256 _value;
  uint256 _timesUsed;
}
```

## ProjectState

```solidity
enum ProjectState {
  FUNDING,
  INSTALLED,
  PRODUCTION,
  CLOSED,
  CANCELLED,
  INREFUND
}
```

## Signature

```solidity
enum Signature {
  INSTALLER,
  ORIGINATOR
}
```

## Signatures

```solidity
struct Signatures {
  bool isSignedByInstaller;
  bool isSignedByOriginator;
}
```

## Milestone

```solidity
struct Milestone {
  string name;
  bool isReached;
  uint256 weight;
  bool wasSignedByInstaller;
  bool wasSignedByOriginator;
}
```

## Project

```solidity
struct Project {
  uint256 id;
  uint256 maintenancePercentage;
  uint256 initialProjectValue;
  uint256 currentProjectValue;
  uint256 swapFactor;
  uint256 pWattsSupply;
  uint256 usdDepreciated;
  uint256 originatorFee;
  enum ProjectState state;
  struct Signatures signatures;
  address addr;
  address adminAddr;
  address installerAddr;
  address originator;
  address stableAddr;
}
```

## ProjectInput

```solidity
struct ProjectInput {
  uint256 maintenancePercentage;
  uint256 initialProjectValue;
  uint256 currentProjectValue;
  uint256 swapFactor;
  uint256 totalPWatts;
  uint256 originatorFee;
  address adminAddr;
  address installerAddr;
  address originator;
  address stableAddr;
  address assetManagerAddr;
  uint256 assetManagerFeePercentage;
}
```

## PurchaseTicket

```solidity
struct PurchaseTicket {
  address projectAddr;
  uint256 pWattPrice;
  uint256 expiration;
  address receiver;
  bool used;
}
```

## UnergyEventVersion

```solidity
enum UnergyEventVersion {
  V1,
  V2
}
```

## UnergyBuyer

This contract handles interactions with installer users, manages their payments,
and also holds pWatt to generate profits for investors.

### unergyData

```solidity
contract UnergyData unergyData
```

### potentialOwner

```solidity
address potentialOwner
```

### isRefunding

```solidity
bool isRefunding
```

### ProjectCreated

```solidity
event ProjectCreated(struct Project project)
```

### HolderUpdated

```solidity
event HolderUpdated(address projectAddr, address holder)
```

### OwnerNominated

```solidity
event OwnerNominated(address pendingOwner)
```

### StableCoinWithdraw

```solidity
event StableCoinWithdraw(address projectAddr, address reciver, uint256 amount)
```

### Refund

```solidity
event Refund(address projectAddr, address recipient, uint256 amount)
```

### UnconventionalIncomeReported

```solidity
event UnconventionalIncomeReported(address projectAddress, uint256 amount, string description)
```

### ProjectIsNotCancelled

```solidity
error ProjectIsNotCancelled(address projectAddr)
```

### UserDoesntHavePWatts

```solidity
error UserDoesntHavePWatts(address projectAddr, address user)
```

### InvalidIndex

```solidity
error InvalidIndex(uint256 maxValid, uint256 index)
```

### RefundInProcess

```solidity
error RefundInProcess()
```

### EmptyString

```solidity
error EmptyString()
```

### ZeroValueNotAllowed

```solidity
error ZeroValueNotAllowed()
```

### OriginatorCannotMakeRefund

```solidity
error OriginatorCannotMakeRefund(address _user)
```

### CanNotCallPayUWattsReward

```solidity
error CanNotCallPayUWattsReward(address caller)
```

### WeightsShouldBeEqual100

```solidity
error WeightsShouldBeEqual100(uint256 accumulatedWeight)
```

### ProjectIsNotFullySigned

```solidity
error ProjectIsNotFullySigned()
```

### MilestoneNotSignedByInstaller

```solidity
error MilestoneNotSignedByInstaller(uint256 _milestoneIndex)
```

### NotPotentialOwner

```solidity
error NotPotentialOwner(address potentialOwner)
```

### OffChainPaymentNotConfirmed

```solidity
error OffChainPaymentNotConfirmed()
```

### ConfirmationAndAcknowledgeMismatch

```solidity
error ConfirmationAndAcknowledgeMismatch()
```

### NotEnoughBalanceForMakePayment

```solidity
error NotEnoughBalanceForMakePayment(uint256 presentProjectFundingValue, uint256 paymentValue)
```

### AmountExceedsRequired

```solidity
error AmountExceedsRequired(uint256 rawPaymentValue, uint256 offChainMilestonePayment)
```

### constructor

```solidity
constructor() public
```

### initialize

```solidity
function initialize(address _unergyDataAddr, address _permissionGranterAddr) public
```

### _authorizeUpgrade

```solidity
function _authorizeUpgrade(address newImplementation) internal
```

_Function that should revert when `msg.sender` is not authorized to upgrade the contract. Called by
{upgradeTo} and {upgradeToAndCall}.

Normally, this function will use an xref:access.adoc[access control] modifier such as {Ownable-onlyOwner}.

```solidity
function _authorizeUpgrade(address) internal override onlyOwner {}
```_

### setInstallerSign

```solidity
function setInstallerSign(address _projectAddr) external
```

### offChainMilestonePaymentReport

```solidity
function offChainMilestonePaymentReport(address _projectAddr, uint256 _milestoneIndex, uint256 _amount) external
```

When the Originator signs it means that an off-chain payment has been made to the
installer for a project milestone. When the installer signs it means that they acknowledge
an off-chain payment for the given project milestone.

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddr | address | The project whose cost will be used to calculate the payment |
| _milestoneIndex | uint256 | the milestone referred to |
| _amount | uint256 | the off-chain amount paid |

### setOriginatorSign

```solidity
function setOriginatorSign(address _projectAddr, uint256 _milestoneIndex) external
```

When the originator signs it means that the project milestone was validated by the originator

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddr | address | The address of the project |
| _milestoneIndex | uint256 | The milestone index to be signed |

### wasFullySigned

```solidity
function wasFullySigned(address _projectAddr) external view returns (bool)
```

### _wasFullySigned

```solidity
function _wasFullySigned(struct Milestone[] milestones) internal pure returns (bool)
```

### _installerPayment

```solidity
function _installerPayment(struct Project project, struct Milestone milestone, uint256 _milestoneIndex) internal
```

Transfer funds to the installer based on the weight of each milestone and
project financing income.

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| project | struct Project | The project whose cost will be used to calculate the payment |
| milestone | struct Milestone | The milestone whose weight will be used to calculate the payment |
| _milestoneIndex | uint256 |  |

### setInitialProjectValue

```solidity
function setInitialProjectValue(address _projectAddr, uint256 _newInitialProjectValue) external
```

### setCurrentProjectValue

```solidity
function setCurrentProjectValue(address _projectAddr, uint256 _projectValue) external
```

### setSwapFactor

```solidity
function setSwapFactor(address _projectAddr, uint256 _swapfactor) external
```

### setProjectState

```solidity
function setProjectState(address _projectAddr, enum ProjectState _state) external
```

### _setState

```solidity
function _setState(address _projectAddr, enum ProjectState _state) internal
```

### payUWattReward

```solidity
function payUWattReward(address _to, uint256 _amount) external
```

### reportUnconventionalIncome

```solidity
function reportUnconventionalIncome(address _projectAddress, uint256 _amount, string _description) external
```

This function is used to report unconventional incomes related to a project.

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddress | address | The address of the project |
| _amount | uint256 | The amount to report |
| _description | string | This must be a short description of the reason for the report. |

### externalRefund

```solidity
function externalRefund(address _projectAddress) public
```

### refund

```solidity
function refund(address _projectAddress, address _user) external
```

### _refund

```solidity
function _refund(address _projectAddress, address _user) internal
```

### withdrawUWatts

```solidity
function withdrawUWatts() external
```

### withdrawStableCoin

```solidity
function withdrawStableCoin(address _projectAddress, address _receiver, uint256 _amount) public
```

This function is used to withdraw remaining stable coins from
 the purchase of pWatts or to return stable coins from the purchase of
 pWatts due to project cancellation.

### _withdrawStableCoin

```solidity
function _withdrawStableCoin(address _projectAddress, address _receiver, uint256 _amount) internal
```

### setUnergyDataAddr

```solidity
function setUnergyDataAddr(address _unergyDataAddr) external
```

### transferOwnership

```solidity
function transferOwnership(address _pendingOwner) public
```

### acceptOwnership

```solidity
function acceptOwnership() external
```

### pause

```solidity
function pause() external
```

### unpause

```solidity
function unpause() external
```

## UnergyData

This contract serves as the protocol storage, where the data needed
by the system to function is stored.

### projectsManagerAddress

```solidity
address projectsManagerAddress
```

### unergyBuyerAddress

```solidity
address unergyBuyerAddress
```

### unergyLogicReserveAddress

```solidity
address unergyLogicReserveAddress
```

### unergyEventAddress

```solidity
address unergyEventAddress
```

### uWattAddress

```solidity
address uWattAddress
```

### cleanEnergyAssetsAddress

```solidity
address cleanEnergyAssetsAddress
```

### potentialOwner

```solidity
address potentialOwner
```

### depreciationBalance

```solidity
uint256 depreciationBalance
```

### maintainerAddress

```solidity
address maintainerAddress
```

### accEnergyByMeter

```solidity
mapping(address => mapping(address => uint256)) accEnergyByMeter
```

### isExternalHolderAddress

```solidity
mapping(address => mapping(address => bool)) isExternalHolderAddress
```

### projectFundingValue

```solidity
mapping(address => uint256) projectFundingValue
```

### unergyEventV2Address

```solidity
address unergyEventV2Address
```

### assetManagerAddress

```solidity
mapping(address => address) assetManagerAddress
```

### assetManagerFeePercentage

```solidity
mapping(address => uint256) assetManagerFeePercentage
```

### swapFeePercentage

```solidity
uint256 swapFeePercentage
```

### stakingProtocolAddress

```solidity
address stakingProtocolAddress
```

### offChainMilestonePayment

```solidity
mapping(address => mapping(uint256 => uint256)) offChainMilestonePayment
```

### offChainPaymentReport

```solidity
mapping(address => mapping(uint256 => uint256)) offChainPaymentReport
```

### ProjectsManagerAdressUpdated

```solidity
event ProjectsManagerAdressUpdated(address projectsManagerAddress)
```

### UnergyBuyerAddressUpdated

```solidity
event UnergyBuyerAddressUpdated(address unergyBuyerAddress)
```

### UnergyLogicReserveAddressUpdated

```solidity
event UnergyLogicReserveAddressUpdated(address unergyLogicReserveAddress)
```

### UnergyEventAddressUpdated

```solidity
event UnergyEventAddressUpdated(address unergyEventAddress)
```

### UWattAddressUpdated

```solidity
event UWattAddressUpdated(address uWattAddress)
```

### CleanEnergyAssetAddressUpdated

```solidity
event CleanEnergyAssetAddressUpdated(address cleanEnergyAssetsAddress)
```

### MaintainerAddressUpdated

```solidity
event MaintainerAddressUpdated(address maintainerAddress)
```

### CleanEnergyAssetsAddressUpdated

```solidity
event CleanEnergyAssetsAddressUpdated(address cleanEnergyAssetsAddress)
```

### OwnerNominated

```solidity
event OwnerNominated(address pendingOwner)
```

### NoSnapshotFound

```solidity
error NoSnapshotFound()
```

### NotPotentialOwner

```solidity
error NotPotentialOwner(address potentialOwner)
```

### AssetManagerFeePercentageExceeded

```solidity
error AssetManagerFeePercentageExceeded(uint256 assetManagerFeePercentage)
```

### swapFeePercentageExceeded

```solidity
error swapFeePercentageExceeded(uint256 swapFeePercentage)
```

### constructor

```solidity
constructor() public
```

### initialize

```solidity
function initialize(address _maintenanceAddr, address _permissionGranterAddr) public
```

### _authorizeUpgrade

```solidity
function _authorizeUpgrade(address newImplementation) internal
```

_Function that should revert when `msg.sender` is not authorized to upgrade the contract. Called by
{upgradeTo} and {upgradeToAndCall}.

Normally, this function will use an xref:access.adoc[access control] modifier such as {Ownable-onlyOwner}.

```solidity
function _authorizeUpgrade(address) internal override onlyOwner {}
```_

### setDepreciationBalance

```solidity
function setDepreciationBalance(uint256 _depreciationBalance) external
```

### setAccEnergyByMeter

```solidity
function setAccEnergyByMeter(address projectAddr, address meterAddr, uint256 accEnergy) external
```

### setPresentProjectFundingValue

```solidity
function setPresentProjectFundingValue(address projectAddr, uint256 newProjectFundingValue) external
```

### generatePurchaseTicket

```solidity
function generatePurchaseTicket(address projectAddr, uint256 pWattPrice, uint256 expiration, address user) external
```

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| projectAddr | address | The address of the project to buy pWatts from |
| pWattPrice | uint256 | The price of the pWatt |
| expiration | uint256 | The amount of time the ticket is valid |
| user | address | The user that will buy the pWatts |

### redeemTicket

```solidity
function redeemTicket(address projectAddr, address user) external view returns (struct PurchaseTicket)
```

### changePurchaseTicketUsed

```solidity
function changePurchaseTicketUsed(address projectAddr, address user) external
```

### setExternalHolderAddress

```solidity
function setExternalHolderAddress(address userAddress, address projectAddr, bool isExternalHolder) external
```

### setAssetManagerAddress

```solidity
function setAssetManagerAddress(address projectAddress, address newAssetManagerAddress) external
```

### setAssetManagerFeePercentage

```solidity
function setAssetManagerFeePercentage(address projectAddress, uint256 newassetManagerFeePercentage) external
```

### setSwapFeePercentage

```solidity
function setSwapFeePercentage(uint256 newSwapFeePercentage) external
```

### setOffChainMilestonePayment

```solidity
function setOffChainMilestonePayment(address _projectAddress, uint256 _milestoneIndex, uint256 _amountPaid) external
```

### setOffChainPaymentReport

```solidity
function setOffChainPaymentReport(address _projectAddress, uint256 _milestoneIndex, uint256 _amountPaid) external
```

### setStakingProtocolAddress

```solidity
function setStakingProtocolAddress(address newStakingProtocolAddress) external
```

### setProjectsManagerAddr

```solidity
function setProjectsManagerAddr(address projectsManagerAddr) external
```

### setUnergyBuyerAddr

```solidity
function setUnergyBuyerAddr(address unergyBuyerAddr) external
```

### setUnergyLogicReserveAddr

```solidity
function setUnergyLogicReserveAddr(address unergyLogicReserveAddr) external
```

### setUnergyEventAddr

```solidity
function setUnergyEventAddr(address unergyEventAddr, enum UnergyEventVersion version) external
```

### setUWattAddr

```solidity
function setUWattAddr(address uWattAddr) external
```

### setCleanEnergyAssetsAddr

```solidity
function setCleanEnergyAssetsAddr(address cleanEnergyAssetsAddr) external
```

### setMaintainerAddr

```solidity
function setMaintainerAddr(address maintainerAddr) external
```

### getPresentProjectFundingValue

```solidity
function getPresentProjectFundingValue(address projectAddr) external view returns (uint256)
```

### getAccEnergyByMeter

```solidity
function getAccEnergyByMeter(address projectAddr, address meterAddr) external view returns (uint256)
```

### transferOwnership

```solidity
function transferOwnership(address _pendingOwner) public
```

### acceptOwnership

```solidity
function acceptOwnership() external
```

### pause

```solidity
function pause() external
```

### unpause

```solidity
function unpause() external
```

## UnergyEvent

This contract serves as an events router that helps keep
track of token movements.

### BeforeTransferEvent

```solidity
event BeforeTransferEvent(address projectAddr, address from, address to, uint256 amount)
```

### AfterTransferEvent

```solidity
event AfterTransferEvent(address projectAddr, address from, address to, uint256 amount)
```

### constructor

```solidity
constructor(address unergyDataAddress_, address permissionGranterAddr_) public
```

### beforeTransferReceipt

```solidity
function beforeTransferReceipt(address _origin, address _from, address _to, uint256 _amount) external
```

### afterTransferReceipt

```solidity
function afterTransferReceipt(address _origin, address _from, address _to, uint256 _amount) external
```

## UnergyLogicReserve

This contract handles the energy reporting logic and the related payments.
It also manages financial logic to pay investors rewards.

### unergyData

```solidity
contract UnergyData unergyData
```

### potentialOwner

```solidity
address potentialOwner
```

### lastUserIndexProcessed

```solidity
mapping(address => uint256) lastUserIndexProcessed
```

### stableBalance

```solidity
uint256 stableBalance
```

_This variable is used to store the amount of stable coins collected from
     energy payments that will be used to buy pWatts. This is required because
     we do not want to use the raw `balanceOf` of the contract to provide
     transparency about the origin of the funds._

### EnergyReported

```solidity
event EnergyReported(address projectAddr, uint256 currentAccEnergy, uint256 deltaEnergy)
```

### InvoiceReport

```solidity
event InvoiceReport(address projectAddr, uint256 energyDelta, uint256 energyTariff, uint256 income, uint256 eventDepreciation)
```

### UWattsClaimed

```solidity
event UWattsClaimed(uint256 UWattsClaimed, address receiver)
```

### OwnerNominated

```solidity
event OwnerNominated(address pendingOwner)
```

### StandardSwapRequested

```solidity
event StandardSwapRequested(address projectAddress, uint256 blockNumber, uint256 usersToProccess)
```

### CustomSwapRequested

```solidity
event CustomSwapRequested(address projectAddress, uint256 blockNumber, address userAddress, uint256 swapFactor)
```

### TokenSwapped

```solidity
event TokenSwapped(address projectAddress, uint256 uWattsUnergy)
```

### ClaimRequested

```solidity
event ClaimRequested(address userAddress)
```

### StableCoinWithdraw

```solidity
event StableCoinWithdraw(address projectAddr, address reciver, uint256 amount)
```

### UWattsExchanged

```solidity
event UWattsExchanged(address projectAddress, address[] receiversAddresses, uint256[] pWattsAmounts, uint256[] uWattsAmounts, bytes32 txHash)
```

### ProjectAlreadyInProduction

```solidity
error ProjectAlreadyInProduction(address projectAddress)
```

### ProjectNotInProduction

```solidity
error ProjectNotInProduction(address projectsAddress, enum ProjectState state)
```

### InvoiceReportNotAvailable

```solidity
error InvoiceReportNotAvailable(enum ProjectState state)
```

### HistoricalSwapRegisterIsEmpty

```solidity
error HistoricalSwapRegisterIsEmpty()
```

### NoClaimsAvailable

```solidity
error NoClaimsAvailable(address user)
```

### NotEnoughStableBalance

```solidity
error NotEnoughStableBalance()
```

### ProjectNotFullySigned

```solidity
error ProjectNotFullySigned()
```

### UserHasNoTicket

```solidity
error UserHasNoTicket()
```

### TicketExpired

```solidity
error TicketExpired()
```

### TicketUsed

```solidity
error TicketUsed()
```

### NotEnoughPWattsToPurchase

```solidity
error NotEnoughPWattsToPurchase(uint256 pWattsAvailable, uint256 pWattsToPurchase)
```

### IsNotExternalHolder

```solidity
error IsNotExternalHolder(address userAddress)
```

### MaintenanceIncomeGreaterThanProfit

```solidity
error MaintenanceIncomeGreaterThanProfit(uint256 maintenanceIncome, uint256 profitIncome)
```

### InvalidArrayLengths

```solidity
error InvalidArrayLengths()
```

### UnderRegisteredEnergyError

```solidity
error UnderRegisteredEnergyError(uint256 lastAccEnergy, uint256 currentAccEnergy)
```

### MinimumUsersToProcessError

```solidity
error MinimumUsersToProcessError()
```

### InvalidSwapFactorError

```solidity
error InvalidSwapFactorError(uint256 actualSwapFactor, uint256 newSwapFactor)
```

### NotPotentialOwner

```solidity
error NotPotentialOwner(address potentialOwner)
```

### OriginatorCannotMakeAPurchase

```solidity
error OriginatorCannotMakeAPurchase(address originator)
```

### UserDoesNotHavePWatts

```solidity
error UserDoesNotHavePWatts(address userAddress, address projectAddress)
```

### projectInProduction

```solidity
modifier projectInProduction(address _projectAddr)
```

### projectNotInProduction

```solidity
modifier projectNotInProduction(address _projectAddr)
```

### isExternalHolder

```solidity
modifier isExternalHolder(address _userAddress, address _projectAddr)
```

### constructor

```solidity
constructor() public
```

### initialize

```solidity
function initialize(address _unergyDataAddr, address _permissionGranterAddr) external
```

### _authorizeUpgrade

```solidity
function _authorizeUpgrade(address newImplementation) internal
```

_Function that should revert when `msg.sender` is not authorized to upgrade the contract. Called by
{upgradeTo} and {upgradeToAndCall}.

Normally, this function will use an xref:access.adoc[access control] modifier such as {Ownable-onlyOwner}.

```solidity
function _authorizeUpgrade(address) internal override onlyOwner {}
```_

### energyReport

```solidity
function energyReport(address _projectAddr, uint256 _currentAccEnergy) external
```

An energy report is made by a meter to a project to generate new CleanEnergyAssets

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddr | address | the address of the project to which the energy report is made |
| _currentAccEnergy | uint256 | the current accumulated energy of the meter |

### invoiceReport

```solidity
function invoiceReport(address _projectAddr, uint256 _energyDelta, uint256 _energyTariff, uint256 _projectPresentValue) external
```

This function is in charge of represent the fiat payments mades in the protocol

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddr | address | The address of the project that is invoicing |
| _energyDelta | uint256 | The amount of energy that is being paid |
| _energyTariff | uint256 | The tariff of the energy that is being paid |
| _projectPresentValue | uint256 | The project present value in USD |

### _incomeCalculation

```solidity
function _incomeCalculation(struct Project project, address _projectAddr, uint256 amount, uint256 stableDecimals) internal returns (uint256 maintenanceIncome, uint256 assetManagerIncome, uint256 profitIncome)
```

### _invoiceReport

```solidity
function _invoiceReport(address _projectAddr, uint256 _income, uint256 _energyDelta, uint256 _energyTariff, uint256 _newProjectValue) internal
```

### pWattsTransfer

```solidity
function pWattsTransfer(address _projectAddr, address _receiver, uint256 _pWattsAmount) external
```

### buyPWatts

```solidity
function buyPWatts(address _projectAddr, uint256 _pWattsAmount) external
```

### buyPWattsFrom

```solidity
function buyPWattsFrom(address _from, address _projectAddr, uint256 _pWattsAmount) external
```

### _buyPWatts

```solidity
function _buyPWatts(address _user, address _projectAddr, uint256 _pWattsAmount) internal
```

### _transferPWattsAndUpdateTicket

```solidity
function _transferPWattsAndUpdateTicket(uint256 _amount, address _sender, struct Project _project, contract ERC20Abs _pWatt) internal
```

### _pWattsTransferUnergyBuyer

```solidity
function _pWattsTransferUnergyBuyer(struct Project _project, contract ERC20Abs _pWatt, uint256 _pWattsAmount, uint256 _pWattPrice) internal returns (uint256 pWattsFinalAmount)
```

### _pWattsTransferAnyUser

```solidity
function _pWattsTransferAnyUser(struct Project _project, contract ERC20Abs _pWatt, uint256 _pWattsAmount, struct PurchaseTicket buyTicket, address _from) internal returns (struct Project)
```

### requestStandardSwap

```solidity
function requestStandardSwap(address _projectAddr, uint256 _usersToProcess) external
```

We used this function to take a snapshot of the totalSupply and user balance
from an offchain service. This service then calls the swapToken function below.

### swapToken

```solidity
function swapToken(address _projectAddr, uint256 _usersToProcess) external
```

This function should be called by an admin when a project completes the funding stage and
is ready to swap pWatts for uWatts

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddr | address | The address of the project to swap Requirements: - The project must be funded completely - Only an admin can call this function |
| _usersToProcess | uint256 |  |

### _swapToken

```solidity
function _swapToken(address _projectAddr, address[] holders, struct Project project, contract ERC20Abs pWatt) internal
```

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _projectAddr | address | See { this.swapToken } |
| holders | address[] | Holders are the accounts that have pWatts relate to the project |
| project | struct Project | This is the project that will be swapped |
| pWatt | contract ERC20Abs | This is the pWatt token contract of the project |

### _customSwap

```solidity
function _customSwap(contract ERC20Abs pWatt, struct Project project, address _userAddr, uint256 holderPWatts, uint256 _swapFactor) internal
```

### _swap

```solidity
function _swap(address projectAddr, address pWattHolders, uint256 holderPWatts, uint256 _swapFactor, contract ERC20Abs pWatt) internal returns (uint256)
```

### _selectHoldersForSwap

```solidity
function _selectHoldersForSwap(address _projectAddr, uint256 _usersToProcess) internal returns (uint256, uint256, address[])
```

### requestCustomSwap

```solidity
function requestCustomSwap(address _projectAddr, address _userAddr, uint256 _swapFactor) external
```

We used this function to take a snapshot of the totalSupply and user balance
from an offchain service. This service then calls the swapToken function below.

### customSwap

```solidity
function customSwap(address _projectAddr, address _userAddr, uint256 _swapFactor) external
```

### _mintUWatts

```solidity
function _mintUWatts(address _receiver, uint256 _amount) internal virtual
```

### requestClaim

```solidity
function requestClaim() external
```

This function is used to trigger the claimable amount calculation in a offchain service.
This service then calls the claim function.

### requestClaimForUser

```solidity
function requestClaimForUser(address _userAddress) external
```

### claimUWatts

```solidity
function claimUWatts(address _holder, uint256 _amount) external
```

Use this function to claim you rewards from a reinvestment of your uWatts from Unergy

_reverts if the caller does not have any claims available_

### setLastUserIndexProcessed

```solidity
function setLastUserIndexProcessed(address _projectAddr, uint256 _lastUserIndexProcessed) external
```

### withdrawStableCoin

```solidity
function withdrawStableCoin(address _projectAddress, address _receiver, uint256 _amount) external
```

This function is used to withdraw funds left over from energy payments

### transferOwnership

```solidity
function transferOwnership(address _pendingOwner) public
```

### acceptOwnership

```solidity
function acceptOwnership() external
```

### _projectInProduction

```solidity
function _projectInProduction(address _projectAddr) internal view
```

### _projectNotInProduction

```solidity
function _projectNotInProduction(address _projectAddr) internal view
```

### _isExternalHolder

```solidity
function _isExternalHolder(address _userAddress, address _projectAddr) internal view
```

### _getAmountsWithFee

```solidity
function _getAmountsWithFee(uint256 userAmount, uint256 uWattMultiplier, uint256 swapFactorMultiplier) internal view returns (uint256, uint256)
```

Use this function to calculate the swap fee for a given amount of uWatts

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| userAmount | uint256 | The amount of uWatts to calculate the fee |
| uWattMultiplier | uint256 | The multiplier of the uWatt token |
| swapFactorMultiplier | uint256 | The multiplier of the swap factor |

#### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | uint256 | (protocolUWatts, userUWatts)  - protocolUWatts: The amount of uWatts that will be sent to the protocol  - userUWatts: The amount of uWatts that will be sent to the user |
| [1] | uint256 |  |

### pause

```solidity
function pause() external
```

### unpause

```solidity
function unpause() external
```

## SwapHelper

### SWAP_ROUTER

```solidity
contract ISwapRouter SWAP_ROUTER
```

### USDT

```solidity
address USDT
```

### UWATT

```solidity
address UWATT
```

### POOL_FEE

```solidity
uint24 POOL_FEE
```

### constructor

```solidity
constructor(uint24 poolFee, address swapRouterAddress, address uWattAddress, address usdtAddress) public
```

### swapExactInputSingle

```solidity
function swapExactInputSingle(uint256 amountIn, uint256 amountOutMinimum) external returns (uint256 amountOut)
```

swapExactInputSingle swaps a fixed amount of USDT for a maximum possible amount of UWATT
using the USDT/UWATT 0.05% pool by calling `exactInputSingle` in the swap router.

_The calling address must approve this contract to spend at least `amountIn` worth of its USDT for this function to succeed._

#### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| amountIn | uint256 | The exact amount of USDT that will be swapped for UWATT. |
| amountOutMinimum | uint256 |  |

#### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| amountOut | uint256 | The amount of UWATT received. |

## CommonAbstract

As we are mantaining two versions of the same contract, one upgradeable and one not,
        and difers only in the addition of a storage gap and the `initialize` function, we
        have created this abstract contract to ensure that both contracts have the same interface.

### SetPermissionGranter

```solidity
event SetPermissionGranter(address permissionGranterAddr)
```

### ZeroAddressNotAllowed

```solidity
error ZeroAddressNotAllowed()
```

### hasRoleInPermissionGranter

```solidity
modifier hasRoleInPermissionGranter(address _caller, address _contract, string _functionName)
```

### hasMeterPermissionOverProjectContract

```solidity
modifier hasMeterPermissionOverProjectContract(address _caller, address _logicContract, address _projectContract)
```

### setPermissionGranterAddr

```solidity
function setPermissionGranterAddr(address _permissionGranterAddr) public virtual
```

### getPermissionGranterAddress

```solidity
function getPermissionGranterAddress() public view virtual returns (address)
```

### _checkPermission

```solidity
function _checkPermission(bool _hasPermission, string _functionName, address _caller) internal pure virtual
```

### _notZeroAddress

```solidity
function _notZeroAddress(address _address) internal pure virtual
```

### _hasRoleInPermissionGranter

```solidity
function _hasRoleInPermissionGranter(address _caller, address _contract, string _functionName) internal virtual
```

### _hasMeterPermissionOverProjectContract

```solidity
function _hasMeterPermissionOverProjectContract(address _caller, address _logicContract, address _projectContract) internal view virtual
```

## IProjectsManager

### getProjectState

```solidity
function getProjectState(address _projectAddress) external view returns (enum ProjectState)
```

### getProject

```solidity
function getProject(address _projectAddress) external view returns (struct Project project)
```

## UniswapV3Pool

This contract is a mock of UniswapV3Pool.sol. It is used to test the UniV3PoolAdapter contract.

### Slot0

```solidity
struct Slot0 {
  uint160 sqrtPriceX96;
  int24 tick;
  uint16 observationIndex;
  uint16 observationCardinality;
  uint16 observationCardinalityNext;
  uint8 feeProtocol;
  bool unlocked;
}
```

### slot0

```solidity
function slot0() external pure returns (struct UniswapV3Pool.Slot0)
```

## ERC20StableCoin

### constructor

```solidity
constructor(string name_, string symbol_, address payable owner_) public
```

### mint

```solidity
function mint(address account, uint256 amount) external
```

### burn

```solidity
function burn(uint256 amount) external
```

### burnFrom

```solidity
function burnFrom(address account, uint256 amount) external
```

### decimals

```solidity
function decimals() public view virtual returns (uint8)
```

_Returns the number of decimals used to get its user representation.
For example, if `decimals` equals `2`, a balance of `505` tokens should
be displayed to a user as `5.05` (`505 / 10 ** 2`).

Tokens usually opt for a value of 18, imitating the relationship between
Ether and Wei. This is the value {ERC20} uses, unless this function is
overridden;

NOTE: This information is only used for _display_ purposes: it in
no way affects any of the arithmetic of the contract, including
{IERC20-balanceOf} and {IERC20-transfer}._

### transferOwnership

```solidity
function transferOwnership(address newOwner) public
```

_Transfers ownership of the contract to a new account (`newOwner`).
Can only be called by the current owner._

## HealthChecker

### HEALTH_CHECKER_ROLE

```solidity
bytes32 HEALTH_CHECKER_ROLE
```

### HealthCheck

```solidity
event HealthCheck()
```

### constructor

```solidity
constructor(address initialHealthChecker) public
```

### healthCheck

```solidity
function healthCheck() external
```

