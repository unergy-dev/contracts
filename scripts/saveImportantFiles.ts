import { saveFile } from "../utils/files";
import { resolvePath } from "../utils/resolvePath";
import { Web3Storage, getFilesFromPath } from "web3.storage";

type File = {
  name: string;
  filePath: string;
};

(async () => {
  const files: File[] = [
    {
      name: "upgrades-history-polygon",
      filePath: resolvePath(".openzeppelin/polygon.json"),
    },
    {
      name: "upgrades-history-mumbai",
      filePath: resolvePath(".openzeppelin/polygon-mumbai.json"),
    },
    {
      name: "deployment-report",
      filePath: resolvePath("./full-deployment-report.json"),
    },
    {
      name: "permissions-report",
      filePath: resolvePath("./permissions-report.json"),
    },
  ];

  const token = process.env.WEB3STORAGE_TOKEN ?? "";
  const storage = new Web3Storage({ token });

  const result = files.map(async (file: File) => {
    const pathFile = await getFilesFromPath(file.filePath);
    const cid = await storage.put(pathFile);

    console.log(`File ${file.name} pinned to IPFS with CID ${cid} ✅`);

    return {
      name: file.name,
      cid,
      createdAt: new Date().toISOString(),
    };
  });

  saveFile(
    "./ipfs-pinned-files.json",
    JSON.stringify(await Promise.all(result), null, 2)
  );

  console.log("Files pinned to IPFS ✅");
})();
