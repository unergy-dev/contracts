import hre, { ethers, network } from "hardhat";
import dotenv from "dotenv";
import { Wallet } from "ethers";
import input from "@inquirer/input";
import password from "@inquirer/password";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

import { ProjectInputStruct } from "../typechain-types/contracts/ProjectsManager";
import { PROTOCOL_CONTRACT_ROLE, factories } from "../utils/constants";
import { getAddressFromReceipt, projectAt } from "../utils/helpers";
import { MultiSignPermission } from "../utils/multiSignPermissions";
import contracts from "../data/full-deployment-report.json";
import { AddressType, reporter } from "../utils/reporter";
import { PermissionType } from "../types/contracts.types";
import { provider } from "../utils/provider";
import { logger } from "../utils/logger";
import {
  notEmpty,
  numberString,
  validAddress,
  validPrivateKey,
} from "../utils/validations";

dotenv.config();
const { ADMIN_PROD_PRIVATE_KEY, ADMIN_PRIVATE_KEY } = process.env;

const {
  ProjectsManager,
  UnergyLogicReserve,
  UnergyEvent,
  UnergyBuyer,
  ERC20StableCoin,
  PermissionGranter,
} = contracts;

const projectDecimals = 18;
const stableDecimals = 6;

let admin: SignerWithAddress | Wallet;

async function main() {
  const {
    PermissionGranterFactory,
    ProjectsManagerFactory,
    UnergyLogicReserveFactory,
    UnergyEventFactory,
    UnergyBuyerFactory,
    StableCoinFactory,
  } = await factories;

  if (network.name === "localhost") {
    admin = (await ethers.getSigners())[0];
  } else if (network.name === "polygon_mumbai") {
    admin = new ethers.Wallet(ADMIN_PRIVATE_KEY!, provider(network.name));
  } else if (network.name === "polygon") {
    admin = new ethers.Wallet(ADMIN_PROD_PRIVATE_KEY!, provider(network.name));
  } else {
    throw new Error("Network not supported");
  }

  const permissionGranter = PermissionGranterFactory.attach(
    PermissionGranter.address
  );
  const multisign = new MultiSignPermission(permissionGranter, [admin]);

  const projectsManager = ProjectsManagerFactory.attach(
    ProjectsManager.address
  );
  const unergyBuyer = UnergyBuyerFactory.attach(UnergyBuyer.address);
  const stableCoin = StableCoinFactory.attach(ERC20StableCoin.address);
  const unergyLogicReserve = UnergyLogicReserveFactory.attach(
    UnergyLogicReserve.address
  );
  const unergyEvent = UnergyEventFactory.attach(UnergyEvent.address);

  const projectName = await input({
    message: "Project name:",
    validate: notEmpty,
  });

  const projectSymbol = await input({
    message: "Project symbol:",
    validate: notEmpty,
  });

  const maintenancePercentage = await input({
    message: `Maintenance percentaje (number with ${projectDecimals} decimals):`,
    validate: numberString,
  });

  const initialProjectValue = await input({
    message: `Project initial value (number with ${stableDecimals} decimals):`,
    validate: numberString,
  });

  const currentProjectValue = await input({
    message: `Project current value (number with ${stableDecimals} decimals):`,
    validate: numberString,
  });

  const swapFactor = await input({
    message: `Swap factor (number with ${projectDecimals} decimals):`,
    validate: numberString,
  });

  const totalPWatts = await input({
    message: `Total project watts (number with ${projectDecimals} decimals):`,
    validate: numberString,
  });

  const projectAdminPK = await password({
    message: "Admin private key:",
    validate: validPrivateKey,
  });

  // * --- Admin Wallet from promted private key ---
  const projectAdmin = new hre.ethers.Wallet(
    projectAdminPK,
    provider("localhost")
  );
  // * ---------------------------------------------

  const installerAddr = await input({
    message: "Installer address:",
    validate: validAddress,
  });

  const originator = await input({
    message: "Originator address:",
    validate: validAddress,
  });

  const originatorFee = await input({
    message: `Operator fee (number with ${projectDecimals} decimals):`,
    validate: numberString,
  });

  const stableAddr = await input({
    message: "Stable coin address:",
    validate: validAddress,
  });

  const assetManagerAddr = await input({
    message: "Asset Manager address:",
    validate: validAddress,
  });

  const assetManagerFeePercentage = await input({
    message: "Asset Manager Fee Percentage:",
    validate: validAddress,
  });

  console.log("Let's create the project milestones");
  let milestones = [];

  const milestonesAmount = await input({
    message: "How many milestones do you want to create?",
    validate: numberString,
  });

  for (let i = 0; i < parseInt(milestonesAmount); i++) {
    const milestoneName = await input({
      message: `Milestone ${i + 1} name:`,
      validate: notEmpty,
    });

    const milestoneValue = await input({
      message: `Milestone ${i + 1} value:`,
      validate: numberString,
    });

    milestones.push({
      name: milestoneName,
      value: parseInt(milestoneValue),
    });
  }

  const projectInput: ProjectInputStruct = {
    maintenancePercentage,
    initialProjectValue,
    currentProjectValue,
    swapFactor,
    totalPWatts,
    adminAddr: projectAdmin.address,
    installerAddr,
    originator,
    originatorFee,
    stableAddr,
    assetManagerAddr,
    assetManagerFeePercentage,
  };

  const creationTx = await projectsManager
    .connect(admin)
    .createProject(projectInput, projectName, projectSymbol);

  const project = await projectAt(
    getAddressFromReceipt(await creationTx.wait())[0]
  );

  logger.log(`Project created at ${project.address}`);

  await permissionGranter.grantRole(PROTOCOL_CONTRACT_ROLE, project.address);

  await multisign.setPermission(
    projectsManager.address,
    project.address,
    "mint"
  );
  reporter.addPermissionToReport(
    projectName,
    project.address,
    projectsManager.address,
    {
      role: "mint",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(unergyBuyer.address, project.address, "burn");
  reporter.addPermissionToReport(
    projectName,
    project.address,
    unergyBuyer.address,
    {
      role: "burn",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(unergyBuyer.address, project.address, "burn");
  reporter.addPermissionToReport(
    projectName,
    project.address,
    unergyBuyer.address,
    {
      role: "burn",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    projectsManager.address,
    project.address,
    "approveSwap"
  );
  reporter.addPermissionToReport(
    projectName,
    project.address,
    projectsManager.address,
    {
      role: "approveSwap",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    UnergyEvent.address,
    project.address,
    "approveSwap"
  );
  reporter.addPermissionToReport(
    projectName,
    project.address,
    UnergyEvent.address,
    {
      role: "approveSwap",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    project.address,
    unergyEvent.address,
    "beforeTransferReceipt"
  );
  reporter.addPermissionToReport(
    projectName,
    project.address,
    unergyEvent.address,
    {
      role: "beforeTransferReceipt",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    project.address,
    unergyEvent.address,
    "afterTransferReceipt"
  );
  reporter.addPermissionToReport(
    projectName,
    project.address,
    unergyEvent.address,
    {
      role: "afterTransferReceipt",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(admin.address, project.address, "approveSwap");
  reporter.addPermissionToReport(
    projectName,
    project.address,
    admin.address,
    {
      role: "approveSwap",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    project.address,
    "approveSwap"
  );
  reporter.addPermissionToReport(
    projectName,
    project.address,
    unergyLogicReserve.address,
    {
      role: "approveSwap",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyBuyer.address,
    project.address,
    "approveSwap"
  );
  reporter.addPermissionToReport(
    projectName,
    project.address,
    unergyBuyer.address,
    {
      role: "approveSwap",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await projectsManager.configureProject(project.address);

  await project
    .connect(admin)
    .approveSwap(
      projectInput.adminAddr,
      unergyLogicReserve.address,
      hre.ethers.constants.MaxUint256
    );

  await stableCoin
    .connect(projectAdmin)
    .approve(unergyBuyer.address, hre.ethers.constants.MaxUint256);

  reporter.addToDeploymentReport(projectName, project.address, admin.address);
  logger.log("Permissions set!");

  logger.log("Creating milestones...");
  for (let i = 0; i < milestones.length; i++) {
    const milestone = milestones[i];

    await projectsManager
      .connect(admin)
      .addProjectMilestone(project.address, milestone.name, milestone.value);
  }
  logger.log(`Milestones for "${projectName}" created`);

  logger.log("Setting the required permissions...");
}

main()
  .then(() => console.log("Done!"))
  .catch((error) => console.error(error));
