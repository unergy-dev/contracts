import { network } from "hardhat";
import { provider } from "../utils/provider";
import { getImplementationAddress } from "@openzeppelin/upgrades-core";

// hh run scripts/getImpl.ts --network polygon_mumbai

async function main() {
  const names = [
    "UnergyData",
    "UnergyBuyer",
    "UnergyLogicReserve",
    "ProjectsManager",
  ];
  const addrs = [
    "0x4e4E1611B2Dd30B689091862b45B1C35336e897c",
    "0x5f7e554A914e536f295fd77AcAeEa7Fa04A01dC6",
    "0x92DFda837C23F6037566EE9dc9A93f7b19A39160",
    "0x56Fc93Cd3abF47D4a01645C5c63A04cdD2284A25",
  ];

  for (let index = 0; index < addrs.length; index++) {
    const addr = addrs[index];
    const name = names[index];

    const implAddress = await getImplementationAddress(
      provider(network.name),
      addr
    );

    console.log(`${name}: ${implAddress}`);
  }
}
main();
