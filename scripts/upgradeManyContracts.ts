import { input } from "@inquirer/prompts";
import { notEmpty, numberString, validAddress } from "../utils/validations";
import { upgradeContract } from "../utils/upgradeContract";
import { GasStationResponseInRange, getGasPrice } from "../utils/gasPrice";
import { logger } from "../utils/logger";

(async () => {
  const numberOfContracts = await input({
    message: "How many contracts do you want to upgrade?",
    validate: numberString,
  });

  const contractNames = await getContractsNames(Number(numberOfContracts));
  const contractsAddrs = await getContractsAddresses(contractNames);

  await upgradeMany(contractNames, contractsAddrs);
})();

async function getContractsNames(numberOfContract: number) {
  let names = [];

  for (let index = 0; index < numberOfContract; index++) {
    const contractName = await input({
      message: `Introduce the contract name #${index}:`,
      validate: notEmpty,
    });

    names.push(contractName);
  }

  return names;
}

async function getContractsAddresses(contractNames: string[]) {
  let addresses = [];

  for (let index = 0; index < contractNames.length; index++) {
    const contractAddress = await input({
      message: `Introduce the proxy address for ${contractNames[index]}:`,
      validate: validAddress,
    });

    addresses.push(contractAddress);
  }

  return addresses;
}

async function upgradeMany(
  contractNames: string[],
  contractAddresses: string[]
) {
  for (let index = 0; index < contractNames.length; index++) {
    const name = contractNames[index];
    const address = contractAddresses[index];

    const safeLowGasData = await getGasPrice<GasStationResponseInRange>(
      "safeLow"
    );

    if (safeLowGasData.maxFee > 800) {
      const msg = `The gas price is too high (${safeLowGasData.maxFee} gwei). Please try again later (${name}).`;
      logger.warn(msg);
    } else {
      await upgradeContract(name, address);
    }
  }
}
