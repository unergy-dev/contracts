import { Wallet } from "ethers";
import { select } from "@inquirer/prompts";
import hre, { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

import { permissionGranterCommunicator } from "../utils/contracts/permissionGranter";
import { ProjectInputStruct } from "../typechain-types/contracts/ProjectsManager";
import { projectsManagerCommunicator } from "../utils/contracts/projectsManager";
import { ProjectsManager } from "./../typechain-types/contracts/ProjectsManager";
import { factories, PROTOCOL_CONTRACT_ROLE } from "../utils/constants";
import { unergyDataCommunicator } from "../utils/contracts/unergyData";
import { getAddressFromReceipt, projectAt, reporter } from "../utils";
import { validateProject } from "../utils/validateProject";
import { PermissionType } from "../types/contracts.types";
import { createMilestone, wait } from "../utils/helpers";
import { ProjectFactory } from "../utils/projectFactory";
import { AddressType } from "../utils/reporter";
import { provider } from "../utils/provider";
import { write } from "../utils/files/write";
import { logger } from "../utils/logger";

const { ADMIN_PROD_PRIVATE_KEY, ADMIN_PRIVATE_KEY } = process.env;

const projectConfigFile = require(`../data/${network.name}/projects-creation-data.json`);
const contracts = require(`../data/${network.name}/full-deployment-report.json`);

const {
  ProjectsManager,
  UnergyLogicReserve,
  UnergyBuyer,
  UnergyEvent,
  PermissionGranter,
  UnergyData,
} = contracts;

let admin: SignerWithAddress | Wallet;
enum Stage {
  GetTransactions = "Get Transactions",
  CompleteConfiguration = "Complete Configuration",
  CreateDirectly = "Create Directly",
}

async function main() {
  const message = `
Select an option to execute the script:
1) Get Transactions. This option will give to you an array of unsigned trasaction to be sent throuh an external provider.
2) Complete configuration. Once the permission were granted you can run the rest of the config.   
3) Create directly. This option will create the projects directly by granting the permissions directly.
`;
  const stage: Stage = await select({
    message,
    choices: [
      {
        value: Stage.GetTransactions,
      },
      {
        value: Stage.CompleteConfiguration,
      },
      {
        value: Stage.CreateDirectly,
      },
    ],
  });

  const {
    PermissionGranterFactory,
    ProjectsManagerFactory,
    UnergyEventFactory,
    UnergyLogicReserveFactory,
    UnergyBuyerFactory,
    UnergyDataFactory,
  } = await factories;

  if (network.name === "localhost") {
    admin = (await ethers.getSigners())[0];
  } else if (network.name === "polygon_mumbai") {
    admin = new ethers.Wallet(ADMIN_PRIVATE_KEY!, provider(network.name));
  } else if (network.name === "polygon") {
    admin = new ethers.Wallet(ADMIN_PROD_PRIVATE_KEY!, provider(network.name));
  } else {
    throw new Error("Network not supported");
  }

  const permissionGranter = PermissionGranterFactory.attach(
    PermissionGranter.address
  );
  const projectsManager = ProjectsManagerFactory.attach(
    ProjectsManager.address
  );
  const unergyEvent = UnergyEventFactory.attach(UnergyEvent.address);
  const unergyBuyer = UnergyBuyerFactory.attach(UnergyBuyer.address);
  const unergyLogicReserve = UnergyLogicReserveFactory.attach(
    UnergyLogicReserve.address
  );
  const unergyData = UnergyDataFactory.attach(UnergyData.address);

  const projectConfig = new ProjectFactory(projectConfigFile)
    .parsedProjectConfig;

  /** ********************************************************************
   * * Communicators
   **********************************************************************/

  const pgCommunicator = permissionGranterCommunicator(permissionGranter);
  const udCommunicator = unergyDataCommunicator(unergyData);
  const pmCommunicator = projectsManagerCommunicator(projectsManager);

  /**
   * *
   * *
   * *
   */

  let transactions = [];

  /**
   * *
   * *
   * *
   */

  for (let i = 0; i < projectConfig.length; i++) {
    const project = projectConfig[i];

    const projectInput: ProjectInputStruct = {
      maintenancePercentage: project.info.maintenancePercentage,
      initialProjectValue: project.info.initialProjectValue,
      currentProjectValue: project.info.currentProjectValue,
      swapFactor: project.info.swapFactor,
      totalPWatts: project.info.totalPWatts,
      originatorFee: project.info.originatorFee,
      adminAddr: project.info.adminAddress,
      installerAddr: project.info.installerAddress,
      originator: project.info.originatorAddress,
      stableAddr: project.info.stableAddr,
      assetManagerAddr: project.info.assetManagerAddress,
      assetManagerFeePercentage: project.info.assetManagerFeePercentage,
    };

    await validateProject(projectInput);

    /** ********************************************************************
     * * Project creation
     **********************************************************************/

    const creationTx = await projectsManager
      .connect(admin)
      .createProject(projectInput, project.token.name, project.token.symbol);
    const contractAddress = getAddressFromReceipt(await creationTx.wait())[0];

    const projectContract = await projectAt(contractAddress);
    logger.log(`Project created at ${projectContract.address}`);

    /** ********************************************************************
     * * Contract Verification
     **********************************************************************/

    if (network.name === "polygon_mumbai" || network.name === "polygon") {
      await wait(20);
      try {
        await hre.run("verify:verify", {
          address: projectContract.address,
          constructorArguments: [
            project.token.name,
            project.token.symbol,
            UnergyData.address,
            PermissionGranter.address,
          ],
        });
      } catch (error) {
        logger.error(
          `Cannot verify project contract at ${projectContract.address}`
        );
      }
    }

    /** ********************************************************************
     * * Report
     **********************************************************************/

    reporter.addToDeploymentReport(
      project.token.symbol,
      projectContract.address,
      admin.address
    );

    /** ********************************************************************
     * * Permissions
     **********************************************************************/

    logger.log("Setting the required permissions...");

    if (stage === Stage.GetTransactions || stage === Stage.CreateDirectly) {
      const sendDirectly = stage === Stage.CreateDirectly;

      if (sendDirectly) {
        await pgCommunicator.grantRole(
          PROTOCOL_CONTRACT_ROLE,
          projectContract.address,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.grantRole(
            PROTOCOL_CONTRACT_ROLE,
            projectContract.address
          )
        );
      }

      /**
       * * MINT
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          projectsManager.address,
          projectContract.address,
          "mint",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            projectsManager.address,
            projectContract.address,
            "mint",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        project.token.name,
        projectContract.address,
        projectsManager.address,
        {
          role: "mint",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Wallet
      );

      /**
       * * BURN
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          unergyBuyer.address,
          projectContract.address,
          "burn",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            unergyBuyer.address,
            projectContract.address,
            "burn",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        project.token.name,
        projectContract.address,
        unergyBuyer.address,
        {
          role: "burn",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Contract
      );

      /**
       * * APPROVE SWAP (PM => PC)
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          projectsManager.address,
          projectContract.address,
          "approveSwap",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            projectsManager.address,
            projectContract.address,
            "approveSwap",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        project.token.name,
        projectContract.address,
        projectsManager.address,
        {
          role: "approveSwap",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Wallet
      );

      /**
       * * APPROVE SWAP (UE => PC)
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          unergyEvent.address,
          projectContract.address,
          "approveSwap",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            unergyEvent.address,
            projectContract.address,
            "approveSwap",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        project.token.name,
        projectContract.address,
        unergyEvent.address,
        {
          role: "approveSwap",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Wallet
      );

      /**
       * * BEFORE TRANSFER RECEIPT
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          projectContract.address,
          unergyEvent.address,
          "beforeTransferReceipt",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            projectContract.address,
            unergyEvent.address,
            "beforeTransferReceipt",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        "UnergyEvent",
        unergyEvent.address,
        projectContract.address,
        {
          role: "beforeTransferReceipt",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Wallet
      );

      /**
       * * AFTER TRANSFER RECEIPT
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          projectContract.address,
          unergyEvent.address,
          "afterTransferReceipt",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            projectContract.address,
            unergyEvent.address,
            "afterTransferReceipt",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        "UnergyEvent",
        unergyEvent.address,
        projectContract.address,
        {
          role: "afterTransferReceipt",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Wallet
      );

      /**
       * * APPROVE SWAP (ADMIN => PC)
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          admin.address,
          projectContract.address,
          "approveSwap",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            admin.address,
            projectContract.address,
            "approveSwap",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        project.token.name,
        projectContract.address,
        admin.address,
        {
          role: "approveSwap",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Wallet
      );

      /**
       * * APPROVE SWAP (ULR => PC)
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          unergyLogicReserve.address,
          projectContract.address,
          "approveSwap",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            unergyLogicReserve.address,
            projectContract.address,
            "approveSwap",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        project.token.name,
        projectContract.address,
        unergyLogicReserve.address,
        {
          role: "approveSwap",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Contract
      );

      /**
       * * APPROVE SWAP (UB => PC)
       */
      if (sendDirectly) {
        await pgCommunicator.setPermission(
          unergyBuyer.address,
          projectContract.address,
          "approveSwap",
          PermissionType.PERMANENT,
          0,
          { send: true }
        );
      } else {
        transactions.push(
          await pgCommunicator.setPermission(
            unergyBuyer.address,
            projectContract.address,
            "approveSwap",
            PermissionType.PERMANENT,
            0
          )
        );
      }
      reporter.addPermissionToReport(
        project.token.name,
        projectContract.address,
        unergyBuyer.address,
        {
          role: "approveSwap",
          type: PermissionType.PERMANENT,
          value: "0",
        },
        AddressType.Contract
      );

      logger.log("Permissions set successfully");

      /** ********************************************************************
       * * Meters permissions
       **********************************************************************/

      logger.log("Granting meters permissions");
      for (let i = 0; i < project.info.meterAddrs.length; i++) {
        const meter = project.info.meterAddrs[i];

        if (sendDirectly) {
          await pgCommunicator.setMeterPermission(
            meter,
            unergyLogicReserve.address,
            projectContract.address,
            { send: true }
          );
        } else {
          transactions.push(
            await pgCommunicator.setMeterPermission(
              meter,
              unergyLogicReserve.address,
              projectContract.address
            )
          );
        }
      }
      logger.log("Meters permissions granted");

      if (!sendDirectly) {
        write("data/project-permissions-txs.json", transactions);
      }
    } else {
      /** ********************************************************************
       * * External required config
       **********************************************************************/

      await udCommunicator.setExternalHolderAddress(
        projectInput.adminAddr,
        projectContract.address,
        true
      );

      await projectContract
        .connect(admin)
        .approveSwap(
          projectInput.adminAddr,
          unergyLogicReserve.address,
          hre.ethers.constants.MaxUint256
        );

      /** ********************************************************************
       * * Project config
       **********************************************************************/

      await pmCommunicator.configureProject(projectContract.address);
      logger.log("Project configured");

      /** ********************************************************************
       * * Milestones creation
       **********************************************************************/

      logger.log("Creating milestones...");
      for (let i = 0; i < project.info.milestones.length; i++) {
        const milestone = project.info.milestones[i];

        await createMilestone(
          projectContract.address,
          milestone.name,
          milestone.weight,
          { signer: admin, projectsManager }
        );
      }
      logger.log(`Milestones for "${project.token.name}" created`);
    }

    if (stage === Stage.CreateDirectly) {
      /** ********************************************************************
       * * External required config
       **********************************************************************/

      await udCommunicator.setExternalHolderAddress(
        projectInput.adminAddr,
        projectContract.address,
        true,
        { send: true }
      );

      await projectContract
        .connect(admin)
        .approveSwap(
          projectInput.adminAddr,
          unergyLogicReserve.address,
          hre.ethers.constants.MaxUint256
        );

      /** ********************************************************************
       * * Project config
       **********************************************************************/

      await pmCommunicator.configureProject(projectContract.address, {
        send: true,
      });
      logger.log("Project configured");

      /** ********************************************************************
       * * Milestones creation
       **********************************************************************/

      logger.log("Creating milestones...");
      for (let i = 0; i < project.info.milestones.length; i++) {
        const milestone = project.info.milestones[i];

        await createMilestone(
          projectContract.address,
          milestone.name,
          milestone.weight,
          { signer: admin, projectsManager }
        );
      }
      logger.log(`Milestones for "${project.token.name}" created`);
    }
  }
}

main();
