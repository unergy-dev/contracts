import { input } from "@inquirer/prompts";
import { notEmpty, validAddress } from "../utils/validations";
import { upgradeContract } from "../utils/upgradeContract";

async function main() {
  const proxyAddress = await input({
    message: "Introduce the current proxy address:",
    validate: validAddress,
  }).then((value) => value.trim());

  const newImplementationName = await input({
    message: "Introduce the new implementation name:",
    validate: notEmpty,
  });

  await upgradeContract(newImplementationName, proxyAddress);
}

main()
  .then(() => {
    process.exitCode = 0;
  })
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
