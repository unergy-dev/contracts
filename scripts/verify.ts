import { verify } from "../utils/helpers";

(async () => {
  await verify(
    "0x4188574FdB3409c76bdAFbd1E2fEA9682d31a830",
    [
      "0xaC1590D74621548390B634F9CE4A1605b7E1B969",
      "0x67EE85Ee49e70772fc0B6E4Ee435bA4fCEc1cB09",
      "0x819F7E838D2A90fF8fb7eFB5EA1816152aC6Da49",
      "0x3F16C20DE34D6199E44e370a33e3DC3dcB1cD9A3",
    ],
    0
  );
})();
