import { ethers, network } from "hardhat";

import {
  CLAIM_SERVICE_SIGNER_ADDRESS,
  STABLE_COIN_INITIAL_SUPPLY,
  factories,
} from "../utils/constants";
import { reporter } from "../utils";
import { provider } from "../utils/provider";
import { AddressType } from "../utils/reporter";
import { MultiSignPermission } from "../utils/multiSignPermissions";
import { PermissionType, UnergyEventVersion } from "../types/contracts.types";
import { unergyDataCommunicator } from "../utils/contracts/unergyData";
import { erc20Communicator } from "../utils/contracts/erc20";
import { permissionGranterCommunicator } from "../utils/contracts/permissionGranter";

const {
  UnergyData,
  UnergyBuyer,
  UnergyLogicReserve,
  ERC20UWatt,
  ERC20StableCoin,
  PermissionGranter,
  UnergyEvent,
  CleanEnergyAssets,
  ProjectsManager,
} = require(`../data/${network.name}/full-deployment-report.json`);

const w = ethers.Wallet;

let admin;

async function main() {
  const {
    UnergyDataFactory,
    UnergyBuyerFactory,
    UnergyLogicReserveFactory,
    ERC20UWattFactory,
    StableCoinFactory,
    PermissionGranterFactory,
    UnergyEventFactory,
    CleanEnergyAssetFactory,
    ProjectsManagerFactory,
  } = await factories;

  if (network.name === "localhost") {
    const signers = await ethers.getSigners();
    admin = signers[0];
  } else {
    admin = new w(process.env.ADMIN_PRIVATE_KEY ?? "", provider(network.name));
  }

  reporter.cleanEnergyReport();
  console.log("Setting up contracts...");

  /**
   * * UnergyData
   */
  const unergyData = UnergyDataFactory.attach(UnergyData.address);
  /**
   * * UnergyBuyer
   */
  const unergyBuyer = UnergyBuyerFactory.attach(UnergyBuyer.address);
  /**
   * * UnergyLogicReserve
   */
  const unergyLogicReserve = UnergyLogicReserveFactory.attach(
    UnergyLogicReserve.address
  );
  /**
   * * ERC20UWatt
   */
  const uWattToken = ERC20UWattFactory.attach(ERC20UWatt.address);
  /**
   * * StableCoin
   */
  const stableCoin = StableCoinFactory.attach(ERC20StableCoin.address);
  /**
   * * PermissionGranter
   */
  const permissionGranter = PermissionGranterFactory.attach(
    PermissionGranter.address
  );
  /**
   * * UnergyEvent
   */
  const unergyEvent = UnergyEventFactory.attach(UnergyEvent.address);
  /**
   * * CleanEnergyAssets
   */
  const cleanEnergyAsset = CleanEnergyAssetFactory.attach(
    CleanEnergyAssets.address
  );
  /**
   * * ProjectsManager
   */
  const projectsManager = ProjectsManagerFactory.attach(
    ProjectsManager.address
  );

  /***********************************
   *       * Initial Setup *         *
   ***********************************/

  const unergyDataCom = unergyDataCommunicator(unergyData);
  const stableCoinCom = erc20Communicator(stableCoin);
  const permissionGranterCom = permissionGranterCommunicator(permissionGranter);

  const multisign = new MultiSignPermission(permissionGranter, [admin]);

  /********************************************
   *       * Protocol Contract Role *         *
   ********************************************/

  await permissionGranterCom.grantProtocolContractRole(uWattToken.address, {
    send: true,
  });
  await permissionGranterCom.grantProtocolContractRole(unergyData.address, {
    send: true,
  });
  await permissionGranterCom.grantProtocolContractRole(unergyEvent.address, {
    send: true,
  });
  await permissionGranterCom.grantProtocolContractRole(unergyBuyer.address, {
    send: true,
  });
  await permissionGranterCom.grantProtocolContractRole(
    projectsManager.address,
    { send: true }
  );
  await permissionGranterCom.grantProtocolContractRole(
    cleanEnergyAsset.address,
    { send: true }
  );
  await permissionGranterCom.grantProtocolContractRole(
    unergyLogicReserve.address,
    { send: true }
  );

  /********************************************
   *            * Permissions *               *
   ********************************************/

  /**
   * * PermissionGranter setup
   */

  await multisign.setPermission(
    admin.address,
    projectsManager.address,
    "createProject"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    {
      role: "createProject",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    projectsManager.address,
    "setSignature"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    { role: "setSignature", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyBuyer.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyBuyer.address,
    projectsManager.address,
    "setSignature"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    { role: "setSignature", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyEvent.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    projectsManager.address,
    "setUnergyLogicReserveAddr"
  );
  reporter.addPermissionToReport(
    "ProjectsManager",
    projectsManager.address,
    admin.address,
    {
      role: "setUnergyLogicReserveAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  /**
   * * ERC20_uWatt setup
   */

  await multisign.setPermission(
    unergyLogicReserve.address,
    uWattToken.address,
    "mint"
  );
  reporter.addPermissionToReport(
    "uWatt",
    uWattToken.address,
    unergyLogicReserve.address,
    { role: "mint", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Contract
  );

  /**
   * * ERC20_stableCoin setup
   */

  await stableCoin.mint(admin.address, STABLE_COIN_INITIAL_SUPPLY);
  await stableCoin.approve(
    unergyLogicReserve.address,
    STABLE_COIN_INITIAL_SUPPLY
  );
  await stableCoin.approve(unergyBuyer.address, STABLE_COIN_INITIAL_SUPPLY);

  /**
   * * UnergyEvent setup
   */

  await multisign.setPermission(
    admin.address,
    unergyEvent.address,
    "setUWattAddr"
  );
  reporter.addPermissionToReport(
    "UnergyEvent",
    unergyEvent.address,
    admin.address,
    { role: "setUWattAddr", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyEvent.address,
    "beforeTransferReceipt"
  );
  reporter.addPermissionToReport(
    "UnergyEvent",
    unergyEvent.address,
    admin.address,
    {
      role: "beforeTransferReceipt",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyEvent.address,
    "afterTransferReceipt"
  );
  reporter.addPermissionToReport(
    "UnergyEvent",
    unergyEvent.address,
    admin.address,
    {
      role: "afterTransferReceipt",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    uWattToken.address,
    unergyEvent.address,
    "beforeTransferReceipt"
  );
  reporter.addPermissionToReport(
    "UnergyEvent",
    unergyEvent.address,
    uWattToken.address,
    {
      role: "beforeTransferReceipt",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    uWattToken.address,
    unergyEvent.address,
    "afterTransferReceipt"
  );
  reporter.addPermissionToReport(
    "UnergyEvent",
    unergyEvent.address,
    uWattToken.address,
    {
      role: "afterTransferReceipt",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyEvent.address,
    "setProjectsManagerAddr"
  );
  reporter.addPermissionToReport(
    "UnergyEvent",
    unergyEvent.address,
    admin.address,
    {
      role: "setProjectsManagerAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyEvent.address,
    "setUnergyBuyerAddr"
  );
  reporter.addPermissionToReport(
    "UnergyEvent",
    unergyEvent.address,
    admin.address,
    { role: "setUnergyBuyerAddr", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Contract
  );

  /**
   * * UnergyData setup
   */

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setProjectsManagerAddr"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "setProjectsManagerAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setUnergyBuyerAddr"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    { role: "setUnergyBuyerAddr", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setUnergyLogicReserveAddr"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "setUnergyLogicReserveAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setUnergyEventAddr"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    { role: "setUnergyEventAddr", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setUWattAddr"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    { role: "setUWattAddr", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setCleanEnergyAssetsAddr"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "setCleanEnergyAssetsAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "generatePurchaseTicket"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "generatePurchaseTicket",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setExternalHolderAddress"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "setExternalHolderAddress",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setExternalHolderAddress"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    {
      role: "setExternalHolderAddress",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setAccEnergyByMeter"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    { role: "setAccEnergyByMeter", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setDepreciationBalance"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    {
      role: "setDepreciationBalance",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "changePurchaseTicketUsed"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    {
      role: "changePurchaseTicketUsed",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "insertUWattsStatusSnapshot"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    {
      role: "insertUWattsStatusSnapshot",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "insertHistoricalSwap"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    {
      role: "insertHistoricalSwap",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "updateUWattsStatusSnapshotAtIndex"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    {
      role: "updateUWattsStatusSnapshotAtIndex",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setPresentProjectFundingValue"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "setPresentProjectFundingValue",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyBuyer.address,
    unergyData.address,
    "setPresentProjectFundingValue"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyBuyer.address,
    {
      role: "setPresentProjectFundingValue",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setPresentProjectFundingValue"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    unergyLogicReserve.address,
    {
      role: "setPresentProjectFundingValue",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    projectsManager.address,
    unergyData.address,
    "setAssetManagerAddress"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    projectsManager.address,
    {
      role: "setAssetManagerAddress",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    projectsManager.address,
    unergyData.address,
    "setAssetManagerFeePercentage"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    projectsManager.address,
    {
      role: "setAssetManagerFeePercentage",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setStakingProtocolAddress"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "setStakingProtocolAddress",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyData.address,
    "setSwapFeePercentage"
  );
  reporter.addPermissionToReport(
    "UnergyData",
    unergyData.address,
    admin.address,
    {
      role: "setSwapFeePercentage",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await unergyDataCom.setProjectsManagerAddr(projectsManager.address, {
    send: true,
  });
  await unergyDataCom.setUnergyBuyerAddr(unergyBuyer.address, {
    send: true,
  });
  await unergyDataCom.setUnergyLogicReserveAddr(unergyLogicReserve.address, {
    send: true,
  });
  await unergyDataCom.setUWattAddr(uWattToken.address, {
    send: true,
  });
  await unergyDataCom.setCleanEnergyAssetsAddr(cleanEnergyAsset.address, {
    send: true,
  });
  await unergyDataCom.setUnergyEventAddr(
    unergyEvent.address,
    UnergyEventVersion.V2,
    { send: true }
  );

  /**
   * * UnergyLogicReserve setup
   */

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "energyReport"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "energyReport", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "invoiceReport"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "invoiceReport", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "pWattsTransfer"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "pWattsTransfer", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "swapToken"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "swapToken", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    CLAIM_SERVICE_SIGNER_ADDRESS,
    unergyLogicReserve.address,
    "swapToken"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    CLAIM_SERVICE_SIGNER_ADDRESS,
    { role: "swapToken", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    CLAIM_SERVICE_SIGNER_ADDRESS,
    unergyLogicReserve.address,
    "requestSwap"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    CLAIM_SERVICE_SIGNER_ADDRESS,
    { role: "requestSwap", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "requestSwap"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "requestSwap", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "claimUWatts"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "claimUWatts", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    CLAIM_SERVICE_SIGNER_ADDRESS,
    unergyLogicReserve.address,
    "claimUWatts"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    CLAIM_SERVICE_SIGNER_ADDRESS,
    { role: "claimUWatts", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "setProjectsManagerAddr"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    {
      role: "setProjectsManagerAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "setCleanEnergyAssetsAddr"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    {
      role: "setCleanEnergyAssetsAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "setUnergyBuyerAddr"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "setUnergyBuyerAddr", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "requestClaim"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    admin.address,
    { role: "requestClaim", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  // -
  await multisign.setPermission(
    unergyEvent.address,
    unergyLogicReserve.address,
    "updateLastUWattStatus"
  );
  reporter.addPermissionToReport(
    "UnergyLogicReserve",
    unergyLogicReserve.address,
    unergyEvent.address,
    {
      role: "updateLastUWattStatus",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  /**
   * * UnergyBuyer setup
   */

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setOriginatorSign"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    { role: "setOriginatorSign", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setInstallerSign"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    {
      role: "setInstallerSign",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setPWattPrice"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    { role: "setPWattPrice", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setInitialProjectValue"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    {
      role: "setInitialProjectValue",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setCurrentProjectValue"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    {
      role: "setCurrentProjectValue",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setSwapFactor"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    { role: "setSwapFactor", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setTotalPWatts"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    { role: "setTotalPWatts", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "withdrawUWatts"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    { role: "withdrawUWatts", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    unergyBuyer.address,
    unergyBuyer.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    // @todo check this
    "UnergyBuyer",
    unergyBuyer.address,
    unergyBuyer.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyEvent.address,
    unergyBuyer.address,
    "updateProjectRelatedProperties"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    unergyEvent.address,
    {
      role: "updateProjectRelatedProperties",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    unergyBuyer.address,
    "setProjectState"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    unergyLogicReserve.address,
    { role: "setProjectState", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setUnergyLogicReserveAddr"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    unergyLogicReserve.address,
    {
      role: "setUnergyLogicReserveAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setProjectsManagerAddr"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    {
      role: "setProjectsManagerAddr",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setUWattAddr"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    { role: "setUWattAddr", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  await multisign.setPermission(
    admin.address,
    unergyBuyer.address,
    "setUneryDataAddress"
  );
  reporter.addPermissionToReport(
    "UnergyBuyer",
    unergyBuyer.address,
    admin.address,
    { role: "setUneryDataAddress", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Wallet
  );

  /**
   * * CleanEnergyToken setup
   */

  await multisign.setPermission(
    projectsManager.address,
    cleanEnergyAsset.address,
    "createProjectEnergyAsset"
  );
  reporter.addPermissionToReport(
    "CleanEnergyAssets",
    cleanEnergyAsset.address,
    projectsManager.address,
    {
      role: "createProjectEnergyAsset",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    cleanEnergyAsset.address,
    "mint"
  );
  reporter.addPermissionToReport(
    "CleanEnergyAssets",
    cleanEnergyAsset.address,
    unergyLogicReserve.address,
    { role: "mint", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    cleanEnergyAsset.address,
    "burn"
  );
  reporter.addPermissionToReport(
    "CleanEnergyAssets",
    cleanEnergyAsset.address,
    unergyLogicReserve.address,
    { role: "burn", type: PermissionType.PERMANENT, value: "0" },
    AddressType.Contract
  );

  await multisign.setPermission(
    unergyLogicReserve.address,
    cleanEnergyAsset.address,
    "createGeneralEnergyAsset"
  );
  reporter.addPermissionToReport(
    "CleanEnergyAssets",
    cleanEnergyAsset.address,
    unergyLogicReserve.address,
    {
      role: "createGeneralEnergyAsset",
      type: PermissionType.PERMANENT,
      value: "0",
    },
    AddressType.Contract
  );

  reporter.savePermissionReport();

  /********************************************
   *          * UnergyData setup *            *
   ********************************************/

  await unergyData.setCleanEnergyAssetsAddr(cleanEnergyAsset.address);
  await unergyData.setProjectsManagerAddr(projectsManager.address);
  await unergyData.setUnergyEventAddr(
    unergyEvent.address,
    UnergyEventVersion.V2
  );
  await unergyData.setUnergyEventAddr(
    unergyEvent.address,
    UnergyEventVersion.V1
  );
  await unergyData.setUnergyBuyerAddr(unergyBuyer.address);
  await unergyData.setUWattAddr(uWattToken.address);
  await unergyData.setUnergyLogicReserveAddr(unergyLogicReserve.address);

  /**
   * ! To interact with the protocol the project admin must approve the UnergyBuyer to
   * ! spend his tokens because the UnergyBuyer will use the tokens to pay to the installer
   * */

  if (network.name !== "polygon") {
    await stableCoinCom.mint(admin.address, STABLE_COIN_INITIAL_SUPPLY, {
      send: true,
    });
    await stableCoinCom.approve(
      unergyBuyer.address,
      STABLE_COIN_INITIAL_SUPPLY,
      { send: true }
    );
    await stableCoinCom.approve(
      unergyLogicReserve.address,
      STABLE_COIN_INITIAL_SUPPLY,
      { send: true }
    );
  }
}

main()
  .then(() => {
    console.log("Script `setupContracts` executed successfully ✅");
    console.log("All permission granted ✅");
    process.exitCode = 0;
  })
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
