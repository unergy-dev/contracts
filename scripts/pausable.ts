import { Wallet } from "ethers";
import { ethers, network } from "hardhat";
import dotenv from "dotenv";

import deploymentReport from "../full-deployment-report.json";
import { provider } from "../utils/provider";
import { factories } from "../utils/constants";
import { logger } from "../utils/logger";
import { wait } from "../utils/helpers";
import { select } from "@inquirer/prompts";
import {
  CleanEnergyAssets,
  ProjectsManager,
  UnergyBuyer,
  UnergyData,
  UnergyLogicReserve,
} from "../typechain-types";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

dotenv.config();

const { ADMIN_PRIVATE_KEY } = process.env;

const {
  UnergyData,
  UnergyBuyer,
  UnergyLogicReserve,
  CleanEnergyAssets,
  ProjectsManager,
} = deploymentReport;

const w = ethers.Wallet;

enum SupportedOperations {
  PauseAll = "Pause all contracts",
  UnpauseAll = "Unpause all contracts",
}

async function isPaused(contract: any) {
  return await contract.paused();
}

async function checkForPausedContracts(
  unergyData: UnergyData,
  unergyBuyer: UnergyBuyer,
  unergyLogicReserve: UnergyLogicReserve,
  cleanEnergyAsset: CleanEnergyAssets,
  projectsManager: ProjectsManager
) {
  logger.log("Checking for pausable contracts state...");

  logger.log(`UnergyData paused:         ${await isPaused(unergyData)}`);
  logger.log(`UnergyBuyer paused:        ${await isPaused(unergyBuyer)}`);
  logger.log(`ProjectsManager paused:    ${await isPaused(projectsManager)}`);
  logger.log(`CleanEnergyAssets paused:  ${await isPaused(cleanEnergyAsset)}`);
  logger.log(
    `UnergyLogicReserve paused: ${await isPaused(unergyLogicReserve)}`
  );
}

let admin: SignerWithAddress | Wallet;

async function main() {
  const operation = await select({
    message: "Select an operation",
    choices: [
      {
        name: "Pause all contracts",
        value: SupportedOperations.PauseAll,
      },
      {
        name: "Unpause all contracts",
        value: SupportedOperations.UnpauseAll,
      },
    ],
  });

  const {
    UnergyDataFactory,
    UnergyBuyerFactory,
    UnergyLogicReserveFactory,
    CleanEnergyAssetFactory,
    ProjectsManagerFactory,
  } = await factories;

  if (network.name === "localhost") {
    const signers = await ethers.getSigners();
    admin = signers[0];
  } else {
    admin = new w(ADMIN_PRIVATE_KEY ?? "", provider(network.name));
  }

  const unergyData = UnergyDataFactory.attach(UnergyData.address);
  const unergyBuyer = UnergyBuyerFactory.attach(UnergyBuyer.address);
  const unergyLogicReserve = UnergyLogicReserveFactory.attach(
    UnergyLogicReserve.address
  );
  const cleanEnergyAsset = CleanEnergyAssetFactory.attach(
    CleanEnergyAssets.address
  );
  const projectsManager = ProjectsManagerFactory.attach(
    ProjectsManager.address
  );

  switch (operation) {
    case SupportedOperations.PauseAll:
      logger.log("Pausing all contracts...");

      if (await isPaused(unergyData)) {
        logger.log("UnergyData is already paused. Skipping...");
      } else {
        await unergyData.connect(admin).pause();
      }

      if (await isPaused(unergyBuyer)) {
        logger.log("UnergyBuyer is already paused. Skipping...");
      } else {
        await unergyBuyer.connect(admin).pause();
      }

      if (await isPaused(unergyLogicReserve)) {
        logger.log("UnergyLogicReserve is already paused. Skipping...");
      } else {
        await unergyLogicReserve.connect(admin).pause();
      }

      if (await isPaused(cleanEnergyAsset)) {
        logger.log("CleanEnergyAssets is already paused. Skipping...");
      } else {
        await cleanEnergyAsset.connect(admin).pause();
      }

      if (await isPaused(projectsManager)) {
        logger.log("ProjectsManager is already paused. Skipping...");
      } else {
        await projectsManager.connect(admin).pause();
      }

      await wait(10);

      await checkForPausedContracts(
        unergyData,
        unergyBuyer,
        unergyLogicReserve,
        cleanEnergyAsset,
        projectsManager
      );

      break;
    case SupportedOperations.UnpauseAll:
      logger.log("Unpausing all contracts...");

      if (!(await isPaused(unergyData))) {
        logger.log("UnergyData is already unpaused. Skipping...");
      } else {
        await unergyData.connect(admin).unpause();
      }

      if (!(await isPaused(unergyBuyer))) {
        logger.log("UnergyBuyer is already unpaused. Skipping...");
      } else {
        await unergyBuyer.connect(admin).unpause();
      }

      if (!(await isPaused(unergyLogicReserve))) {
        logger.log("UnergyLogicReserve is already unpaused. Skipping...");
      } else {
        await unergyLogicReserve.connect(admin).unpause();
      }

      if (!(await isPaused(cleanEnergyAsset))) {
        logger.log("CleanEnergyAssets is already unpaused. Skipping...");
      } else {
        await cleanEnergyAsset.connect(admin).unpause();
      }

      if (!(await isPaused(projectsManager))) {
        logger.log("ProjectsManager is already unpaused. Skipping...");
      } else {
        await projectsManager.connect(admin).unpause();
      }

      await wait(10);

      await checkForPausedContracts(
        unergyData,
        unergyBuyer,
        unergyLogicReserve,
        cleanEnergyAsset,
        projectsManager
      );

      break;
    default:
      throw new Error("Unsupported operation");
  }
}

main()
  .then(() => {
    logger.log("Script `pauseAll` executed successfully ✅");
    process.exitCode = 0;
  })
  .catch((error) => {
    console.error(error);
    process.exitCode = 1;
  });
