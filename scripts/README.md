# Running Scripts

## Contents

- [Deploying contracts](#deploying-contracts)
  - [Running locally](#running-locally)
  - [Running on Mumbai Testnet](#running-on-mumbai-testnet)
- [Creating projects](#creating-projects)
- [Open Zeppelin Defender Utils](#open-zeppelin-defender-utils)
  - [Upload contracts to Defender](#upload-contracts-to-defender)
  - [Delete contracts from Defender](#delete-contracts-from-defender)
- [IPFS](#ipfs)
- [Upgrading contracts](#upgrading-contracts)
  - [Upgrading contracts on Mumbai Testnet](#upgrading-contracts-on-mumbai-testnet)
- [Custom Hardhat tasks](#custom-hardhat-tasks)
- [How to update a proxy SC via a Safe Wallet](#how-to-update-a-proxy-sc-via-a-safe-wallet)
- [Deploying one contract](#deploying-one-contract)

## Deploying contracts

### Running locally

This commands runs the `deploy.ts` and the `setupContracts.ts` scripts the on the `localhost` network. This commands will deploy the contracts and set them up with the required permissions.

> To run the script below you will need to have a local node running. To do so, run `npx hardhat node` in a separate terminal window.

```bash
npm run deploy:local
```

## Running on Mumbai Testnet

- Before run this command you will need to create a `.env` file in the root directory. See [.env.example](../.env.example)

```bash
npm run deploy:mumbai
```

## Creating projects

You can create projects in two ways:

### 1. Creating multiple projects from a JSON file

> This is the recommended and up to date way to create projects.

To create multiple projects, first create a file and named it `project-creation-data.json`
inside the `data` directory. That file must contains objects that match with the ProjectConfig
interface:

You can see an example in [data/project-creation-data.json](../data/projects-creation-data.json)

```typescript
type ProjectConfigFile = ProjectConfig[];

interface ProjectConfig {
  info: InfoInput;
  token: Token;
}

export interface InfoInput {
  maintenancePercentage: string;
  initialProjectValue: string;
  currentProjectValue: string;
  swapFactor: string;
  totalPWatts: string;
  adminAddress: string;
  installerAddress: string;
  originatorAddress: string;
  originatorFee: string;
  stableAddr: string;
  meterAddrs: string[];
  assetManagerAddress: string;
  assetManagerFeePercentage: string;
  milestones: Milestone[];
}

interface Milestone {
  name: string;
  weight: number;
}

interface Token {
  name: string;
  symbol: string;
}
```

Then, execute the following command:

```bash
npm run createProjects:local

# or

npm run createProjects:mumbai
```

### Usage

When you run the script, you will be asked to select one of the following options:

- 1. **Get Transactions.** This options will give you the calldatas required to executed
     the minimum required permissions from the Gnosis Safe. The result will be saved in the
     `data/projects-permissions.txs.json` file.
- 2. **Complete Configuration.** You want to run this option after you have executed the
     transactions from the previous option. This option will complete the configuration of
     the projects.

- 3. **Create Directly.** This is the option you choose in testing environments. This option
     will create the projects directly by granting the required permissions directly from the admin account.

### 2. Creating a single project

To create a single project, execute the following command:

```bash
npm run createProject:local

# or

npm run createProject:mumbai
```

## Open Zeppelin Defender Utils

To use this scripts, you need to add this variables to your `.env` file:

```python
OZ_DEFENDER_API_KEY =  <your Open Zeppelin Defender API key>
OZ_DEFENDER_SECRET = <your Open Zeppelin Defender secret>
```

### Upload contracts to Defender

To upload the last deployed contracts (tracked on `full-deployment-report.json` file) to Defender, execute the following command:

```bash
npm run uploadContractsToDefender
```

### Delete contracts from Defender

To delete all contracts from Defender, execute the following command:

```bash
npm run deleteContractsFromDefender
```

## IPFS

> ⚠️ This script does not work because our IPFS gateway free tier has been disabled by Pinata ⚠️

### Uploading files to IPFS

A new script has been added to upload the reports files to IPFS. To run the script, execute the following command:

> Do not run this command if you don't are making a new testnet/mainnet deployment.

```bash
npm run saveReports
```

### Getting file from IPFS

If you want to get a file from IPFS just maka a GET request to the following URL:

```bash
https://gateway.pinata.cloud/ipfs/<CID>
```

Where `<CID>` is the CID of the file you want to get. You can get the CID from the `importantFilesCID.json` file in the root directory.

## Upgrading contracts

### Upgrading contracts on Mumbai Testnet

To upgrade the contracts on Mumbai Testnet, you first need to update the constants `PROXY_ADDRESS` and `NEW_IMPLEMENTATION_NAME`
at the top of the`scripts/upgradeContracts.ts` file. Then, execute the following command:

```bash
npm run upgrade:mumbai
```

## Custom Hardhat tasks

### `set-balance`

This task allows you to set the balance of an account. To run this task, execute the following command:

```bash
npx hardhat set-balance --account <account> --balance <balance>
```

Where `<account>` is the address of the account you want to set the balance and `<balance>` is the balance you want to set in hex format (default: `0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff`)

### `decode-error`

This task allows you to decode an error message. To run this task, execute the following command:

```bash
npx hardhat decode-error --bytes <bytes>
```

Where `<bytes>` is the encode error message you want to decode.

## How to update a proxy SC via a Safe Wallet

Since the Safe has the role required to authorise an upgrade, it's necessary to run the upgrade through it. So the first step is to deploy the new implementation.

You can do this by using the [`deployOne.ts`](../scripts/deployOne.ts) script in the main repo. **To do this, you need to change the `CONTRACT_NAME` value to the desired one and pass the required constructor args to the contract**. Upgradable contracts do not require any arguments.

Once you have the above solved, you simply run the command below and wait the deployment to finish.

```shell
npx hardhat run scripts/deployOne.ts --network <network_name>
```

Once the new implementation has been deployed, you will need to copy the address and run the upgradeTo method in the current proxy address through the authorised Safe Wallet. You can do this using the **Transaction Builder**.

> If you have any problems deploying the new implementation, first check the result of the transaction in Block Explorer or with your RPC provider. If the scripts fail but the contract is already deployed, you can verify it using the [`verify.ts`](../scripts/verify.ts).

## Deploying one contract

To deploy a single contract, you can use the `deployOne` script. To do so, you first need to update the [`scripts/deployOne.ts`](/scripts/deployOne.ts) file with the contract you want to deploy and the constructor arguments. Then, execute the following command:

```bash
npm run deployOne:<network>
```

Where `<network>` is the network you want to deploy the contract to. The supported networks are:

- `local`
- `mumbai`
- `polygon`
