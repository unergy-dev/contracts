/**
 * ! ------------------------------------------------
 *
 * ! Verify the addresses before running this script
 *
 * ! ------------------------------------------------
 */

import { ethers } from "hardhat";
import { simulateTx } from "../utils/simulateTx";

const contractsAddrs = {
  UnergyData: {
    address: "0xFF772af2fADA9992950FDc53454b2571cbc9865b",
    deployer: "0x541355615AA6ad7e333D3ddA9566ed9aA94DfED4",
  },
  pWattSomer: {
    address: "0xf2F55a9a9209178521efe964ab82E46896f64f27",
    deployer: "0x541355615AA6ad7e333D3ddA9566ed9aA94DfED4",
  },

  pWattUruaco: {
    address: "0x38a2EC3D7891aB55Ee925dd800FdA3d95C202C8E",
    deployer: "0x541355615AA6ad7e333D3ddA9566ed9aA94DfED4",
  },

  pWattAcanto: {
    address: "0x2Fb657a5994BC00dDb957707eC14b8715aF98783",
    deployer: "0x541355615AA6ad7e333D3ddA9566ed9aA94DfED4",
  },

  pWattPola: {
    address: "0xfA640c16aCA45038b8b7Ddd458C9E6cFCf69D40f",
    deployer: "0x541355615AA6ad7e333D3ddA9566ed9aA94DfED4",
  },

  pWattBaraya: {
    address: "0x1E4A3F6e4071E5AC72e6A61766D9d11cC36cFEbc",
    deployer: "0x541355615AA6ad7e333D3ddA9566ed9aA94DfED4",
  },

  pWattIBES: {
    address: "0x84d370fBa06640c4786fa48a0b92F4003f83a215",
    deployer: "0x541355615AA6ad7e333D3ddA9566ed9aA94DfED4",
  },
};

const contractsAddrsMap = new Map(Object.entries(contractsAddrs));

const rawData = `
pWattBaraya, 0x4A9311817b3b5340Dd05110F6A2d2FEdD70A79b7
pWattBaraya, 0x21AaCE1ffdBdC1C06525Fc9E5D85a61C82060E88
pWattBaraya, 0x515d0fb09E194b65B93E63a8Ff5c29D27a1036ec
pWattBaraya, 0x995599Ef4f7f02B33861d9c7F0fd8FD45e4F6dB3
pWattBaraya, 0x820B1Cf0eC1c26cFA205103791cc62c5113fC241
pWattBaraya, 0xEB95252EEc340e1AA2fF62162059938E4F4B34Dd
pWattBaraya, 0xbc8904B86D6425355988b0C915792E37a5ba9C6c
`;

const data = rawData
  .trim()
  .split("\n")
  .map((line) => line.split(", "));

const parsedData = data.map(([name, address]) => ({
  contractAddress: contractsAddrsMap.get(name)?.address,
  userAddress: address,
}));

(async () => {
  const signer = (await ethers.getSigners())[0];

  const unergyData = await ethers.getContractAt(
    "UnergyData",
    contractsAddrsMap.get("UnergyData")?.address || "",
    signer
  );

  for (let index = 0; index < parsedData.length; index++) {
    const userAddress = parsedData[index].userAddress || "";
    const contractAddress = parsedData[index].contractAddress || "";

    const tx = await unergyData.populateTransaction.setExternalHolderAddress(
      userAddress,
      contractAddress,
      true
    );

    const result = await simulateTx({
      from: tx.from || "",
      to: tx.to || "",
      data: tx.data || "",
    });

    if (result) {
      console.log(
        `🤔 Adding external holder ${userAddress} to ${contractAddress}`
      );

      const txResponse = await signer.sendTransaction(tx);
      await txResponse.wait();

      console.log(
        `✅ External holder ${userAddress} added to ${contractAddress}`
      );
    }
  }
})();
