// @todo - this is a work in progress, and is not yet functional.
// require("dotenv").config();
// import { Alchemy, Log, Network, Utils } from "alchemy-sdk";
// import { userSelect } from "../utils/IO/input";
// import { write } from "../utils/files/write";
// import { PermissionType } from "../types/contracts.types";
// import { AddressType } from "../utils/reporter";
// import { AbiCoder } from "@ethersproject/abi";

// function parseLog(cInterface: Utils.Interface, log: Log) {
//   const decodedLog = cInterface.parseLog({
//     data: log.data,
//     topics: log.topics,
//   });

//   return decodedLog;
// }

// async function generatePermissionReport(
//   alchemy: Alchemy,
//   cInterface: Utils.Interface,
//   address: string,
//   topics: Array<string | Array<string> | null>,
//   fromBlock: string = "0x0",
//   toBlock: string = "latest"
// ) {
//   const logs = await alchemy.core.getLogs({
//     fromBlock,
//     toBlock,
//     address,
//     topics,
//   });

//   let parsedPermissions: any[] = [];

//   const abiCodex = new AbiCoder();

//   for (const log of logs) {
//     const parsedLog = parseLog(cInterface, log);

//     // const contract = "ProjectsManager";
//     // const authorizedAddress = parsedLog[1];
//     // const contractAddress = parsedLog._contract;
//     // const permission = {
//     //   role: cInterface.parseLog,
//     //   type: 0,
//     //   value: "0",
//     // };
//     // const addressType = AddressType.Wallet;

//     parsedPermissions.push(parsedLog);
//   }

//   return parsedPermissions;
// }

// async function main() {
//   const selectedNetwork = await userSelect<Network>("Select a network", [
//     {
//       name: "Polygon Mumbai",
//       value: Network.MATIC_MUMBAI,
//     },
//     {
//       name: "Polygon Mainnet",
//       value: Network.MATIC_MAINNET,
//     },
//   ]);

//   // Optional Config object, but defaults to demo api-key and eth-mainnet.
//   const settings = {
//     apiKey: process.env.ALCHEMY_API_KEY,
//     network: selectedNetwork,
//   };

//   const alchemy = new Alchemy(settings);

//   const PERMISSION_GRANTER_ADDRESS =
//     "0x2082332DbcEDbD966EA47c9322284F00BAD64Ecb";

//   const permissionGranterAbi =
//     '[{"anonymous":"false","inputs":[{"indexed":true,"internalType":"address","name":"_address","type":"address"},{"indexed":true,"internalType":"address","name":"_contract","type":"address"},{"indexed":"false","internalType":"string","name":"_fname","type":"string"}],"name":"PermissionGranted","type":"event"}]';
//   const PG_INTERFACE = new Utils.Interface(permissionGranterAbi);

//   const topics = [
//     "0x0c4ba8e5831e90ded64e90941a693876f983fe1c572217d161809c00bc41604e",
//   ];

//   const report = await generatePermissionReport(
//     alchemy,
//     PG_INTERFACE,
//     PERMISSION_GRANTER_ADDRESS,
//     topics
//   );

//   console.log(report);
//   //   write("data/permission-grants.json", report);
// }

// main();
