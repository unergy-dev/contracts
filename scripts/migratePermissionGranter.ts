import { network } from "hardhat";

import contracts from "../full-deployment-report.json";
import permissions from "../partial-permissions.json";
import { PermissionObject } from "../utils/reporter";
import { factories } from "../utils/constants";
import { logger } from "../utils/logger";

if (network.name !== "polygon") throw new Error("Wrong network. Polygon only");

(async () => {
  // * Start initial config ----------------------------------------------
  const { PermissionGranter: PG } = contracts;
  const { PermissionGranterFactory } = await factories;

  const permissionGranter = PermissionGranterFactory.attach(PG.address);

  // * End initial config ------------------------------------------------

  // * Set permissions ---------------------------------------------------
  for (let index = 0; index < permissions.length; index++) {
    const permission = permissions[index] as unknown as PermissionObject;

    const tx = await permissionGranter.setPermission(
      permission.authorizedAddress,
      permission.contractAddress,
      permission.permission.role,
      permission.permission.type,
      0
    );
    await tx.wait();

    console.log(
      `Permission [ ${permission.permission.role} ] set for [ ${permission.authorizedAddress} ] on [ ${permission.contractAddress} ]`
    );
  }
  // * End set permissions ------------------------------------------------

  logger.log("Done ✅");

  process.exit(0);
})();
