import { factories } from "../utils/constants";
import { deployer } from "../utils/deployer";
import contracts from "../full-deployment-report.json";
import { logger } from "../utils/logger";

async function main() {
  const {
    UnergyDataFactory,
    UnergyBuyerFactory,
    UnergyLogicReserveFactory,
    ERC20UWattFactory,
    StableCoinFactory,
    PermissionGranterFactory,
    UnergyEventFactory,
    CleanEnergyAssetFactory,
    ProjectsManagerFactory,
  } = await factories;

  const {
    UnergyData,
    UnergyBuyer,
    UnergyLogicReserve,
    ERC20UWatt,
    ProjectsManager,
    ERC20StableCoin,
    PermissionGranter,
    UnergyEvent,
    CleanEnergyAssets,
  } = contracts;

  const unergyData = UnergyDataFactory.attach(UnergyData.address);
  const unergyBuyer = UnergyBuyerFactory.attach(UnergyBuyer.address);
  const unergyLogicReserve = UnergyLogicReserveFactory.attach(
    UnergyLogicReserve.address
  );
  const ERC20uWatt = ERC20UWattFactory.attach(ERC20UWatt.address);
  const stableCoin = StableCoinFactory.attach(ERC20StableCoin.address);
  const permissionGranter = PermissionGranterFactory.attach(
    PermissionGranter.address
  );
  const unergyEvents = UnergyEventFactory.attach(UnergyEvent.address);
  const cleanEnergyAsset = CleanEnergyAssetFactory.attach(
    CleanEnergyAssets.address
  );
  const projectsManager = ProjectsManagerFactory.attach(
    ProjectsManager.address
  );

  logger.log("Adding contracts to Defender...");

  await deployer.addContractToOZDefender(unergyData, "UnergyData");
  await deployer.addContractToOZDefender(unergyBuyer, "UnergyBuyer");
  await deployer.addContractToOZDefender(
    unergyLogicReserve,
    "UnergyLogicReserve"
  );
  await deployer.addContractToOZDefender(ERC20uWatt, "ERC20uWatt");
  await deployer.addContractToOZDefender(stableCoin, "StableCoin");
  await deployer.addContractToOZDefender(
    permissionGranter,
    "PermissionGranter"
  );
  await deployer.addContractToOZDefender(unergyEvents, "UnergyEvents");
  await deployer.addContractToOZDefender(cleanEnergyAsset, "CleanEnergyAssets");
  await deployer.addContractToOZDefender(projectsManager, "ProjectsManager");

  logger.log("Contracts added to Defender ✅");
}

main();
