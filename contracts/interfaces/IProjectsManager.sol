// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {ProjectState, Project} from "../Types.sol";

interface IProjectsManager {
    function getProjectState(
        address _projectAddress
    ) external view returns (ProjectState);

    function getProject(
        address _projectAddress
    ) external view returns (Project memory project);
}
