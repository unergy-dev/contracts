// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./Common.sol";
import "./UnergyData.sol";
import "./UnergyBuyer.sol";
import "./UnergyLogicReserve.sol";
import "./ERC20Project.sol";
import "./ProjectsManager.sol";

/**
 * @title UnergyEvent.
 * @notice This contract serves as an events router that helps keep
 * track of token movements.
 */
contract UnergyEvent is Common {
    UnergyData private unergyData;

    event BeforeTransferEvent(
        address projectAddr,
        address from,
        address to,
        uint256 amount
    );
    event AfterTransferEvent(
        address projectAddr,
        address from,
        address to,
        uint256 amount
    );

    constructor(
        address unergyDataAddress_,
        address permissionGranterAddr_
    ) Common(permissionGranterAddr_) {
        unergyData = UnergyData(unergyDataAddress_);
    }

    function beforeTransferReceipt(
        address _origin,
        address _from,
        address _to,
        uint256 _amount
    )
        external
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "beforeTransferReceipt"
        )
    {
        emit BeforeTransferEvent(_origin, _from, _to, _amount);
    }

    function afterTransferReceipt(
        address _origin,
        address _from,
        address _to,
        uint256 _amount
    )
        external
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "afterTransferReceipt"
        )
    {
        bool isRefunding = UnergyBuyer(unergyData.unergyBuyerAddress())
            .isRefunding();

        if (isRefunding != true) {
            // it is necessary to allow the swap of the project token to uWatt
            ERC20Project(_origin).approveSwap(
                _to,
                unergyData.unergyLogicReserveAddress(),
                type(uint256).max
            );

            if (_amount > 0) {
                ProjectsManager(unergyData.projectsManagerAddress())
                    .addProjectHolder(_origin, _to);
            }
        }

        emit AfterTransferEvent(_origin, _from, _to, _amount);
    }
}
