// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

enum PermissionType {
    UNAUTHORIZED,
    PERMANENT,
    EXECUTIONS,
    TIMELOCK,
    ONETIME
}

struct Permission {
    PermissionType _type; // The _ was used here because `type` is a Solidity keyword
    uint256 _value;
    uint256 _timesUsed;
}

enum ProjectState {
    FUNDING,
    INSTALLED,
    PRODUCTION,
    CLOSED,
    CANCELLED,
    INREFUND
}

/**
 * @param INSTALLER The project is signed by the installer
 * @param ORIGINATOR The project is signed by Unergy
 */
enum Signature {
    INSTALLER,
    ORIGINATOR
}

struct Signatures {
    bool isSignedByInstaller;
    bool isSignedByOriginator;
}

struct Milestone {
    string name;
    bool isReached;
    uint256 weight;
    bool wasSignedByInstaller;
    bool wasSignedByOriginator;
}

/**
 * @param id This indicates the id of the project
 * @param maintenancePercentage This indicates the percentage in USD that will be used to maintain the project
 * @param currentProjectValue This indicates how much it will cost to build the project
 * @param swapFactor This indicates the factor that will be used to swap the pWatts to uWatts
 * @param state This indicates the state of the project
 * @param addr This indicates the address of the project ERC20 compatible contract
 * @param adminAddr This indicates the address of the admin of the project
 * @param installerAddr This indicates the address of the installer of the project
 * @param usdDepreciated This indicates how much the project has depreciated in USD
 * @param pWattsSupply This indicates how much pWatts are in the system
 * @param holders This indicates the addresses of the holders of the pWatts
 * @param signatures This indicates the signatures of the project
 * @param originator This indicates the address of the originator of the project (Unergy)
 * @param originatorFee This indicates the fee that the originator will charge to the project
 */
struct Project {
    uint256 id;
    uint256 maintenancePercentage;
    uint256 initialProjectValue;
    uint256 currentProjectValue;
    uint256 swapFactor;
    uint256 pWattsSupply;
    uint256 usdDepreciated;
    uint256 originatorFee;
    ProjectState state;
    Signatures signatures;
    address addr;
    address adminAddr;
    address installerAddr;
    address originator;
    address stableAddr;
}

struct ProjectInput {
    uint256 maintenancePercentage;
    uint256 initialProjectValue;
    uint256 currentProjectValue;
    uint256 swapFactor;
    uint256 totalPWatts;
    uint256 originatorFee;
    address adminAddr;
    address installerAddr;
    address originator;
    address stableAddr;
    address assetManagerAddr;
    uint256 assetManagerFeePercentage;
}

struct PurchaseTicket {
    address projectAddr;
    uint256 pWattPrice;
    uint256 expiration;
    address receiver;
    bool used;
}

enum UnergyEventVersion {
    V1,
    V2
}
