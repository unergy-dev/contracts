// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "solmate/src/tokens/ERC20.sol";

import {ProjectState, Project} from "./Types.sol";

import "./UnergyData.sol";
import {UnergyEvent} from "./UnergyEvent.sol";
import {ProjectsManager} from "./ProjectsManager.sol";
import "./Common.sol";

/**
 * @title ERC20Project.
 * @notice This contract handles the logic for raising the financing needed for a new clean energy project.
 */

contract ERC20Project is ERC20, Common {
    UnergyData private unergyData;

    event UnergyEventAddressUpdated(address unergyEventAddr);
    event ProjectsManagerAddressUpdated(address projectsManagerAddr);

    error ProjectIsNotInFundingOrInstalledState();
    error TransferToHimself(address destinationAddress);

    constructor(
        string memory name_,
        string memory symbol_,
        address _unergyDataAddress,
        address _permissionGranterAddres
    )
        ERC20(name_, symbol_, 18)
        Ownable()
        Common(_permissionGranterAddres)
        notZeroAddress(_unergyDataAddress)
        notZeroAddress(_permissionGranterAddres)
    {
        unergyData = UnergyData(_unergyDataAddress);
    }

    modifier inFundingOrInstalled() {
        Project memory project = ProjectsManager(
            unergyData.projectsManagerAddress()
        ).getProject(address(this));

        if (
            project.state != ProjectState.FUNDING &&
            project.state != ProjectState.INSTALLED
        ) revert ProjectIsNotInFundingOrInstalledState();
        _;
    }

    modifier transferHooks(
        address from,
        address to,
        uint256 amount
    ) {
        _beforeTokenTransfer(from, to, amount);
        _;
        _afterTokenTransfer(from, to, amount);
    }

    function mint(
        address to,
        uint256 amount
    )
        external
        hasRoleInPermissionGranter(msg.sender, address(this), "mint")
        transferHooks(address(0), to, amount)
    {
        _mint(to, amount);
    }

    function burn(
        address from,
        uint256 amount
    )
        external
        hasRoleInPermissionGranter(msg.sender, address(this), "burn")
        transferHooks(from, address(0), amount)
    {
        _burn(from, amount);
    }

    function transfer(
        address to,
        uint256 amount
    )
        public
        virtual
        override
        inFundingOrInstalled
        transferHooks(msg.sender, to, amount)
        returns (bool)
    {
        return super.transfer(to, amount);
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual override transferHooks(from, to, amount) returns (bool) {
        return super.transferFrom(from, to, amount);
    }

    /**
     * @notice This method is used to swap the user pWatts for uWatts.
     * @param holder is the public address who has pWatts.
     * @param spender is the public address who spend pWatts.
     * @param amount is the amount allowed to spend.
     */
    function approveSwap(
        address holder,
        address spender,
        uint256 amount
    )
        public
        hasRoleInPermissionGranter(msg.sender, address(this), "approveSwap")
        returns (bool)
    {
        allowance[holder][spender] = amount;

        emit Approval(holder, spender, amount);

        return true;
    }

    function approve(
        address spender,
        uint256 amount
    )
        public
        virtual
        override
        hasRoleInPermissionGranter(msg.sender, address(this), "approve")
        returns (bool)
    {
        return super.approve(spender, amount);
    }

    function transferOwnership(
        address newOwner
    ) public override onlyOwner notZeroAddress(newOwner) {
        _transferOwnership(newOwner);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {
        UnergyEvent(unergyData.unergyEventV2Address()).beforeTransferReceipt(
            address(this),
            from,
            to,
            amount
        );
    }

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {
        UnergyEvent(unergyData.unergyEventV2Address()).afterTransferReceipt(
            address(this),
            from,
            to,
            amount
        );
    }
}
