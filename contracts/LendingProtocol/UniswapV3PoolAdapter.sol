// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {FixedPointMathLib} from "solmate/src/utils/FixedPointMathLib.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {IUniswapV3Pool} from "@uniswap/v3-core/contracts/interfaces/IUniswapV3Pool.sol";

contract UniswapV3PoolAdapter {
    using FixedPointMathLib for uint256;

    IUniswapV3Pool public uWattUSDTPool;

    constructor(address _uWattUSDTPool) {
        uWattUSDTPool = IUniswapV3Pool(_uWattUSDTPool);
    }

    function getUWattsIn(
        uint256 usdtOut
    ) external view returns (uint256 uWattIn) {
        uint256 usdtPriceInUwatt = getUSDTPriceInUWatt();
        uWattIn = usdtOut.mulDivDown(usdtPriceInUwatt, 1e6);
    }

    function getUSDTPriceInUWatt() public view returns (uint256) {
        (uint160 sqrtPriceX96, , , , , , ) = uWattUSDTPool.slot0();

        // calculated as decribed in
        // https://blog.uniswap.org/uniswap-v3-math-primer#how-do-i-calculate-the-current-exchange-rate
        uint256 sqrtPrice = uint256(sqrtPriceX96) / 2 ** 96;
        uint256 price = sqrtPrice ** 2; // uWatt/USDT
        uint256 uWattPrice = price.mulDivUp(1e18, 10 ** 12); // adj uWatt/USDT

        return uWattPrice;
    }
}
