// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {Owned} from "solmate/src/auth/Owned.sol";
import {ERC721} from "solmate/src/tokens/ERC721.sol";
import {Strings} from "@openzeppelin/contracts/utils/Strings.sol";
import {SafeERC20, IERC20} from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

import {IPosition} from "./IPosition.sol";
import {UnergyData} from "../UnergyData.sol";
import {LendingManager} from "./LendingManager.sol";
import {UniswapV3PoolAdapter} from "./UniswapV3PoolAdapter.sol";
import {IProjectsManager, ProjectState} from "../interfaces/IProjectsManager.sol";

contract Position is IPosition, ERC721, Owned, UniswapV3PoolAdapter {
    /*****************************************
     *           Type Definitions            *
     ****************************************/

    using Strings for uint256;
    using SafeERC20 for IERC20;

    /*****************************************
     *               Storage                 *
     ****************************************/

    IERC20 public immutable uWatt; // token0
    IERC20 public immutable USDT; // token1

    uint8 public constant LENDER_ID = 1;
    uint8 public constant BORROWER_ID = 2;

    address unergyLogicAddress;
    LendingManager public immutable lendingManager;

    PositionMetadata private metadata;

    /*****************************************
     *                Errors                 *
     ****************************************/

    error Position_InvalidId();
    error Position_Unauthorized();
    error Position_AlreadyCompensated();
    error Position_DefaultDateNotReached();
    error Position_DelayedDateNotReached();
    error Position_CompensationNotAvailable();
    error Position_InvalidProjectState(ProjectState currentState);
    error Position_InvalidPositionState(PositionState currentState);

    /*****************************************
     *               Modifier                *
     ****************************************/

    modifier onlyOnwerOf(uint256 id) {
        _onlyOnwerOf(id);
        _;
    }

    /*****************************************
     *            Constructor 👷             *
     ****************************************/

    constructor(
        string memory _name,
        string memory _symbol,
        address _uWattAddress,
        address _stableAddress,
        address _unergyLogicAddress,
        address _uniV3Pool,
        PositionMetadata memory _metadata
    )
        ERC721(_name, _symbol)
        Owned(msg.sender)
        UniswapV3PoolAdapter(_uniV3Pool)
    {
        metadata = _metadata;

        unergyLogicAddress = _unergyLogicAddress;
        uWatt = IERC20(_uWattAddress);
        USDT = IERC20(_stableAddress);

        // This is only the case if the Lending Manager initiates the contract
        // which is always true in the Unergy Protocol context
        address lendingManagerAddress = msg.sender;
        lendingManager = LendingManager(lendingManagerAddress);
        // The LendingManager will hold both NFTs until the position is closed
        _mint(lendingManagerAddress, LENDER_ID);
        _mint(lendingManagerAddress, BORROWER_ID);
    }

    /*****************************************
     *                Public                *
     ****************************************/

    function transferFrom(
        address from,
        address to,
        uint256 id
    ) public virtual override {
        if (id == LENDER_ID) {
            metadata.lender = to;
        } else if (id == BORROWER_ID) {
            metadata.borrower = to;
        } else {
            revert Position_InvalidId();
        }

        super.transferFrom(from, to, id);
    }

    function tokenURI(
        uint256 id
    ) public pure virtual override returns (string memory) {
        string memory baseURI = "https://example.com";

        if (id == LENDER_ID) {
            return string.concat(baseURI, "/lender");
        }

        if (id == BORROWER_ID) {
            return string.concat(baseURI, "/borrower");
        }

        revert Position_InvalidId();
    }

    /*****************************************
     *               External                *
     ****************************************/

    function burn(uint256 id) external virtual onlyOnwerOf(id) {
        _burn(id);
    }

    function withdrawProfit() external {
        address lender = ownerOf(LENDER_ID);
        address borrower = ownerOf(BORROWER_ID);

        uint256 revenueInUWatt = _getExpectedProfitInUWatt();
        uint256 uWattBalance = uWatt.balanceOf(address(this));

        if (uWattBalance >= revenueInUWatt) {
            uWatt.safeTransfer(lender, revenueInUWatt);
        } else {
            uWatt.safeTransfer(lender, uWattBalance);
        }

        uint256 collateral = metadata.collateral;
        uint256 USDTBalance = USDT.balanceOf(address(this));

        // A higher USDT balance balance is not expected, but in the case
        // we want to prevent withdrawal of more than the collateral
        if (USDTBalance >= collateral) {
            USDT.safeTransfer(borrower, collateral);
        } else {
            USDT.safeTransfer(borrower, USDTBalance);
        }

        // The remaining uWatts are transfered to the borrower
        // as compensation for the risk taken
        uWattBalance = uWatt.balanceOf(address(this));

        if (uWattBalance > 0) {
            uWatt.safeTransfer(borrower, uWattBalance);
        }

        _closePosition();
    }

    function withdrawCompensation() external {
        PositionMetadata memory posMeta = metadata;

        if (posMeta.state != PositionState.Delayed) {
            revert Position_InvalidPositionState(posMeta.state);
        }

        if (posMeta.payedCompensation == posMeta.collateral) {
            revert Position_AlreadyCompensated();
        }

        uint256 secondsFromStartToNow = (block.timestamp - posMeta.startDate);
        uint256 lockedPeriodInSeconds = posMeta.lockedPeriod * 1 days;
        uint256 elapsedDays = (secondsFromStartToNow - lockedPeriodInSeconds) /
            1 days;

        if (elapsedDays > 0) {
            uint256 dailyCompensation = lendingManager.getDailyProfit(
                posMeta.lockedAmount,
                posMeta.defaultProfitPercentage
            );
            uint256 earnedCompensation = (elapsedDays * dailyCompensation) -
                posMeta.payedCompensation;
            metadata.payedCompensation += earnedCompensation;

            USDT.safeTransfer(posMeta.lender, earnedCompensation);
        } else {
            revert Position_CompensationNotAvailable();
        }
    }

    function declareDelay() external {
        PositionMetadata memory posMeta = metadata;

        if (posMeta.state != PositionState.Active) {
            revert Position_InvalidPositionState(posMeta.state);
        }

        ProjectState projectState = lendingManager.getProjectState(
            posMeta.projectAddress
        );

        uint256 secondsFromStartToNow = block.timestamp - posMeta.startDate;
        uint256 lockPeriodInSeconds = posMeta.lockedPeriod * 1 days;

        if (
            secondsFromStartToNow > lockPeriodInSeconds &&
            projectState != ProjectState.PRODUCTION
        ) {
            //
            metadata.state = PositionState.Delayed;
            //
        } else {
            revert Position_DelayedDateNotReached();
        }
    }

    function declareDefault() external {
        PositionMetadata memory posMeta = metadata;

        if (posMeta.state != PositionState.Delayed) {
            revert Position_InvalidPositionState(posMeta.state);
        }

        ProjectState projectState = lendingManager.getProjectState(
            posMeta.projectAddress
        );

        uint256 secondsFromStartToNow = (block.timestamp - posMeta.startDate);
        uint256 lockedPeriodInSeconds = (posMeta.lockedPeriod +
            posMeta.additionalPeriod) * 1 days;

        if (
            projectState != ProjectState.PRODUCTION &&
            secondsFromStartToNow > lockedPeriodInSeconds
        ) {
            //
            metadata.state = PositionState.Default;

            _transferPWatts(posMeta.projectAddress, posMeta.lender);

            _burn(LENDER_ID);
            _burn(BORROWER_ID);
            //
        } else {
            revert Position_DefaultDateNotReached();
        }
    }

    function refund() external virtual {
        PositionMetadata memory posMeta = metadata;

        if (posMeta.state != PositionState.Canceled)
            revert Position_InvalidPositionState(posMeta.state);

        address lender = posMeta.lender;
        uint256 lockedAmount = posMeta.lockedAmount;

        USDT.safeTransfer(lender, lockedAmount);
    }

    function getMetadata() external view returns (PositionMetadata memory) {
        return metadata;
    }

    function approveStableSpend() external onlyOwner {
        USDT.approve(unergyLogicAddress, metadata.lockedAmount);
    }

    function updateState(PositionState _state) public onlyOwner {
        metadata.state = _state;
    }

    function setBorrower(address _borrower) external onlyOwner {
        metadata.borrower = _borrower;
    }

    function setStartDate(uint256 _startDate) external onlyOwner {
        metadata.startDate = _startDate;
    }

    /*****************************************
     *                Internal                *
     ****************************************/

    function _getExpectedProfitInUWatt() internal view returns (uint256) {
        PositionMetadata memory _metadata = metadata;

        uint256 revenueInUSDT = _metadata.lockedAmount + _metadata.periodProfit;
        uint256 revenueInUWatt = this.getUWattsIn(revenueInUSDT);

        return revenueInUWatt;
    }

    function _onlyOnwerOf(uint256 id) internal view {
        if (msg.sender != ownerOf(id)) revert Position_Unauthorized();
    }

    function _closePosition() internal {
        metadata.state = PositionState.Closed;
        _burn(LENDER_ID);
        _burn(BORROWER_ID);
    }

    function _burn(uint256 id) internal override {
        metadata.borrower = address(0);
        metadata.lender = address(0);

        super._burn(id);
    }

    function _transferPWatts(address projectAddress, address lender) internal {
        IERC20 project = IERC20(projectAddress);
        uint256 pWattsBalance = project.balanceOf(address(this));

        project.safeTransfer(lender, pWattsBalance);
    }
}
