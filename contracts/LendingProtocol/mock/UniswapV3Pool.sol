// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * This contract is a mock of UniswapV3Pool.sol. It is used to test the UniV3PoolAdapter contract.
 */

contract UniswapV3Pool {
    struct Slot0 {
        // the current price
        uint160 sqrtPriceX96;
        // the current tick
        int24 tick;
        // the most-recently updated index of the observations array
        uint16 observationIndex;
        // the current maximum number of observations that are being stored
        uint16 observationCardinality;
        // the next maximum number of observations to store, triggered in observations.write
        uint16 observationCardinalityNext;
        // the current protocol fee as a percentage of the swap fee taken on withdrawal
        // represented as an integer denominator (1/x)%
        uint8 feeProtocol;
        // whether the pool is locked
        bool unlocked;
    }

    function slot0() external pure returns (Slot0 memory) {
        /**
         * This is the slot0 of the uWatt/USDT pool on the Polygon Mainnet at block 51631493.
         * See https://polygonscan.com/address/0x15455aE10dD0c3F896498B0EaDe476e05A929Ed2#readContract
         */
        return
            Slot0({
                sqrtPriceX96: 86472863524095632406288644541256691,
                tick: 278074,
                observationIndex: 0,
                observationCardinality: 1,
                observationCardinalityNext: 1,
                feeProtocol: 0,
                unlocked: true
            });
    }
}
