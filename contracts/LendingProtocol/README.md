# Lending Protocol

The Unergy Lending Protocol is a series of smart contracts that allow an investor to lend a fixed amount of Polygon USDT to be used to purchase pWatts in a specified project managed by the Unergy Protocol, in return of obtaining a predefined amount of uWatts in a fixed time frame. This protocol allows the borrower to assume the project origination risk instead of the lender; with the expectation of a higher reward, while the lender provides the capital at a lower risk.

## Addresses

- Polygon Mumbai: `0x4188574FdB3409c76bdAFbd1E2fEA9682d31a830`

## Definitions

| Symbol   | Description                                                                                                                                                                                                                                                    |
| -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| R        | Annual profitability (APR) in % to be obtained by the Lender.                                                                                                                                                                                                  |
| R\*      | Annual profitability (APR) in % that the Lender will obtain in case of delay of the Project. To make sense, it must be fulfilled that R\* > R, otherwise, it would not be a compensation for the Lender, who put the money for the development of the project. |
| T        | Period of time, in days, that the position will last.                                                                                                                                                                                                          |
| T\*      | Maximum additional period of time that the position could be extended, receiving an indemnification.                                                                                                                                                           |
| M        | Amount of USDT provided by the Lender.                                                                                                                                                                                                                         |
| C        | Amount of USDT provided by the Borrower as collateral for the loan.                                                                                                                                                                                            |
| pWatts   | Tokens that represent the origination cost of a energy project.                                                                                                                                                                                                |
| uWatts   | Tokens that represent partial ownership of the Reserve of clean energy generating assets                                                                                                                                                                       |
| Lender   | The person who provides the funds to the project.                                                                                                                                                                                                              |
| Borrower | The person who asssumes the origination risk of the project and provides a collateral for taking the loan.                                                                                                                                                     |

> Details about the pWatts and uWatts tokens can be found in the [Unergy Protocol](https://docs.unergy.io/introduction/abstract) documentation.

## LendingManager

### Management

As we want to offer a safe and trustworthy financial product, we can introduce some rate limits to the Position creation to reduce the risks of fraud or any kind of malicious behaviour within our protocol. **The rates are used when a new position is opened. However, subsequent changes won't affect positions already opened**.

The functions that allow this management are shown below:

```ts
/**
* @notice This limit the profit rate that can be expected by the Lender for a specific project.
*/
function setProfitRate(
    address _projectAddress,
    uint256[2] calldata _rate
) onlyRole(ADMIN_ROLE);

/**
* @notice This limit the period that the Lender can lock the funds for a specific project.
*/
function setLoanTermLimits(
    address _projectAddress,
    uint256[2] calldata _rate
) onlyRole(ADMIN_ROLE);

/**
 * @notice This limit the additional waiting period that the Lender will wait to receive compensation for a specific project.
 */
function setAdditionalLoanTermLimits(
    address _projectAddress,
    uint256[2] calldata _rate
) onlyRole(ADMIN_ROLE);

/**
* @notice This limit the compensation rate that can be expected by the Lender for a specific project.
*/
function setCompensationRate(
    address _projectAddress,
    uint256[2] calldata _rate
) onlyRole(ADMIN_ROLE);
```

These values can be queried later using the following functions:

```ts
/**
* @dev This method is used to get the profit rate for a specific project.
* @notice This rate is managed by the Unergy team and can be changed at any time.
* @param _projectAddress The address of the project the rate are related to.
*/
function getProfitRate(
    address _projectAddress
) public view returns (uint256[2] memory);

/**
* @dev This method is used to get the compensation rate for a specific project.
* @notice This rate is managed by the Unergy team and can be changed at any time.
* @param _projectAddress The address of the project the rate are related to.
*/
function getCompensationRate(
    address _projectAddress
) public view returns (uint256[2] memory):

/**
* @dev This method is used to get the period rate for a specific project.
* @notice This rate is managed by the Unergy team and can be changed at any time.
* @param _projectAddress The address of the project the rate are related to.
*/
function getLoanTermsLimits(
    address _projectAddress
) public view returns (uint256[2] memory);

/**
* @dev This method is used to get the default waiting period for a specific project.
* @notice This rate is managed by the Unergy team and can be changed at any time.
* @param _projectAddress The address of the project the rate are related to.
*/
function getAdditionalLoanTermLimits(
    address _projectAddress
) public view returns (uint256[2] memory);

/**
* @dev This method is used to get the default waiting period for a specific project.
* @notice This rate is managed by the Unergy team and can be changed at any time.
* @param _projectAddress The address of the project the rate are related to.
*/
function getDefaultLoadTermLimits(
    address _projectAddress
) public view returns (uint256[2] memory);
```

### User actions

The following are the core components of the Lending Protocol, which can be executed by anyone.

#### Create Position

Creating a position means: providing a fixed amount of USDT to a specific project in order to receive uWatts when the underlying project goes into production.

> You will need to approve the LendingManager contract to spend at least `_lockedAmount` USDT.

- You can only create a position if the project is in the `FUNDING` state and your parameters are within the limits set by the Unergy team.
- You will be able to close your position before it is closed (what it means to close a position is explained later).
- You can supply as many USDT as you want, but note that a project has a limited supply of pWatts. (**You can withdraw the unused USDT**).

```ts
/**
 * @notice Use this method to open a position as `Lender`.
 * @param _projectAddress The address of the project that will receive the funds.
 * @param _anualRevenuePercentage The annual profit rate that the project will pay to the Lender.
 * @param _anualCompensationPercentage The annual profit rate that the project will pay to the Lender if the project is delayed.
 * @param _lockedPeriod The period of time that the Lender will lock the funds (in days).
 * @param _additionalPeriod The maximum time the lender will wait to receive compensation (in days).
 * @param _lockedAmount The amount of stable coins that the Lender will provide.
 **/
function createPosition(
    address _projectAddress,
    uint256 _anualRevenuePercentage,
    uint256 _anualCompensationPercentage,
    uint256 _lockedPeriod,
    uint256 _additionalPeriod,
    uint256 _lockedAmount
) external returns (address positionAddress);
```

#### Close Position

Closing a position means: providing the required collateral to pay the lender the corresponding amount of USDT.
This action delivers the Position NFTs to both parties to be used to withdraw your profits. You can transfer
your Position NFTs to anyone, by transferring the NFT you are transferring the rights to receive any profits or
payments generated by the position.

> You will need to approve the LendingManager contract to spend at least [`Position.metadata.collateral`](#getting-position-metadata) USDT.

```ts
/**
 * @notice Closing a position means providing collateral for an open position.
 */
function closePosition(address _positionAddress) external;
```

#### Cancel Position

Canceling a position means withdrawing the funds you have given to a particular project. **You can do this at any time before the position is closed**.

> This action will burn the Lender and Borrower NFTs for the Position.

```ts
/**
 * @notice Cancel a position means that the Lender withdraw the funds that he has provided.
 * @dev This method can only be called by the Lender and if the position is still Pending.
 */
function cancelPosition(address _positionAddress) external;
```

### Miscelaneous

Two more functions are available to query useful information about the Lending Protocol.

#### Get positions

You can query a list of all the created Positions independently of their state.

```ts
function getPositions() external view returns (address[] memory);
```

#### Get Project state

This is just an utility function to query the state of a project from the Unergy ProjectsManager contract.

```ts
function getProjectState(
    address _projectAddress
) external view returns (ProjectState);


enum ProjectState {
    FUNDING,
    INSTALLED,
    PRODUCTION,
    CLOSED,
    CANCELLED,
    INREFUND
}
```

#### Get daily profit

This is just an utility function to estimate the daily profit of a position based on the `lockedAmount` and the `APR`.

```ts
function getDailyProfit(
    uint256 _lockedAmount,
    uint256 _annualPercentage
) public pure returns (uint256);
```

## Position

A Position is a smart contract that represents a lending position in the Unergy Lending Protocol. It is created by the LendingManager contract when a Lender provides funds to a project.

### Transfer From

The standard ERC721 `transferFrom` function is overriden to fail if the `msg.sender` is not the Lender or the Borrower.

```ts
function transferFrom(
    address from,
    address to,
    uint256 id
) public virtual override;
```

### Burn

You can burn your position NFTs at any time.

**⚠️ Remember that you will permanently lose access to the position profit and we can't recover it. Please be careful**.

```ts
function burn(uint256 id) public virtual onlyOnwerOf(id);
```

### Refund

If you cancel a Position previously, this is the function you can call to get your funds back.

> This function can be executed by anyone.

```ts
function refund() public virtual;
```

### Withdraw Profit

When the project you has invested in goes into production, you can withdraw your profits using this function. This will
transfer the corresponding amount of uWatts to the Lender and the Borrower.

> The Lender and Borrower NFTs will be burned.

```ts
function withdrawProfit() external;
```

### Declare delay

If the project you has invested in is not operational by the appropriate date ([T](#definitions)), you can declare a delay using this function.

> This function can be executed by anyone.

```ts
function declareDelay() external;
```

### Withdraw Compensation

If the project you has invested was declared in delay, you can withdraw your daily compensation using this function.

> This function can be executed by anyone.

```ts
function withdrawCompensation() external;
```

### Declare Default

If the project you has invested in is not operational by the maximum date ([T + T\*](#definitions)), you can declare a default using this function. **Declaring a Position in default let you withdraw the bought pWatts**.

> This function can be executed by anyone and burn the Lender and Borrower NFTs.

```ts
function declareDefault() external;
```

### Commons

#### Getting Position metadata

A position metadata is described by the interface below. **You can query it by calling the following function at the Position address**:

```ts
function getMetadata() external view returns (PositionMetadata memory);
```

```ts
struct PositionMetadata {
    PositionState state;
    address lender;
    address borrower;
    address projectAddress;
    uint256 profitPercentage; // R
    uint256 defaultProfitPercentage; // R*
    uint256 lockedAmount; // M
    uint256 periodProfit;
    uint256 lockedPeriod; // T
    uint256 additionalPeriod; // T*
    uint256 collateral; // C
    uint256 payedCompensation;
    uint256 startDate;
}


enum PositionState {
    // 0 - Inexistent: If the position has not been created yet
    Inexistent,
    // 1 - Pending: If a position has been opened by a lender but not yet closed by a borrower
    Pending,
    // 2 - Active: When a borrower provides the collateral and the funds are used to take part in a project
    Active,
    // 3 - Delayed: When the project is not operational by the appropriate date (T)
    Delayed,
    // 4 - Default: If the project is not operational by the maximum date (T + T*)
    Default,
    // 5 - Canceled: If the lender withdraws the funds before the borrower provides the collateral
    Canceled,
    // 6 - Closed: If the project is in production and the pWatts are swapped
    Closed
}
```
