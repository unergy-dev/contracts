// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";

import {Project, PurchaseTicket} from "./Types.sol";
import {ERC20Abs, ERC1155Abs} from "./Abstracts.sol";
import "./UnergyData.sol";
import "./CommonUpgradeable.sol";
import "./ERC1155CleanEnergyAssets.sol";
import "./ProjectsManager.sol";
import "./UnergyBuyer.sol";

// @custom:update Contract updated at 2023-28-12 17:31 GMT-5 - See details in the comments below

/**
 * @title UnergyLogicReserve.
 * @notice This contract handles the energy reporting logic and the related payments.
 * It also manages financial logic to pay investors rewards.
 */
contract UnergyLogicReserve is
    Initializable,
    ReentrancyGuardUpgradeable,
    PausableUpgradeable,
    CommonUpgradeable,
    UUPSUpgradeable
{
    using SafeERC20Upgradeable for IERC20Upgradeable;

    UnergyData public unergyData;
    address public potentialOwner;

    //ProjectAddress to last user index processed
    // @custom:update Modified in a contract upgrade at 2023-14-12 14:15 GMT-5
    mapping(address => uint256) public lastUserIndexProcessed;
    /**
     * @dev This variable is used to store the amount of stable coins collected from
     *      energy payments that will be used to buy pWatts. This is required because
     *      we do not want to use the raw `balanceOf` of the contract to provide
     *      transparency about the origin of the funds.
     */
    uint256 stableBalance;

    event EnergyReported(
        address projectAddr,
        uint256 currentAccEnergy,
        uint256 deltaEnergy
    );
    event InvoiceReport(
        address projectAddr,
        uint256 energyDelta,
        uint256 energyTariff,
        uint256 income,
        uint256 eventDepreciation
    );
    event UWattsClaimed(
        uint256 indexed UWattsClaimed,
        address indexed receiver
    );
    event OwnerNominated(address pendingOwner);
    // @custom:update Event modified in a contract upgrade at 2023-23-11
    event StandardSwapRequested(
        address indexed projectAddress,
        uint256 indexed blockNumber,
        uint256 usersToProccess
    );
    // @custom:update Event created in a contract upgrade at 2023-23-11
    event CustomSwapRequested(
        address indexed projectAddress,
        uint256 indexed blockNumber,
        address userAddress,
        uint256 swapFactor
    );
    event TokenSwapped(address projectAddress, uint256 uWattsUnergy);
    event ClaimRequested(address userAddress);
    event StableCoinWithdraw(
        address projectAddr,
        address reciver,
        uint256 amount
    );
    // @custom:update This function was added in a update to the contract at 2023-10-12 17:35 GMT-5
    // @custom:update Removed in a contract upgrade at 2023-14-12 14:15 GMT-5
    // @custom:update Added again in a contract upgrade at 2023-14-12 14:50 GMT-5
    event UWattsExchanged(
        address projectAddress,
        address[] receiversAddresses,
        uint256[] pWattsAmounts,
        uint256[] uWattsAmounts,
        bytes32 txHash
    );

    error ProjectAlreadyInProduction(address projectAddress);
    error ProjectNotInProduction(address projectsAddress, ProjectState state);
    error InvoiceReportNotAvailable(ProjectState state);
    error HistoricalSwapRegisterIsEmpty();
    error NoClaimsAvailable(address user);
    error NotEnoughStableBalance();
    error ProjectNotFullySigned();
    error UserHasNoTicket();
    error TicketExpired();
    error TicketUsed();
    error NotEnoughPWattsToPurchase(
        uint256 pWattsAvailable,
        uint256 pWattsToPurchase
    );
    error IsNotExternalHolder(address userAddress);
    error MaintenanceIncomeGreaterThanProfit(
        uint256 maintenanceIncome,
        uint256 profitIncome
    );
    // @custom:update This function was added in a update to the contract at 2023-10-12 17:35 GMT-5
    // @custom:update Removed in a contract upgrade at 2023-14-12 14:15 GMT-5
    // @custom:update Added again in a contract upgrade at 2023-14-12 14:50 GMT-5
    error InvalidArrayLengths();
    error UnderRegisteredEnergyError(
        uint256 lastAccEnergy,
        uint256 currentAccEnergy
    );
    error MinimumUsersToProcessError();
    error NotPotentialOwner(address potentialOwner);
    error OriginatorCannotMakeAPurchase(address originator);
    // @custom:update Event created in a contract upgrade at 2023-23-11
    error UserDoesNotHavePWatts(address userAddress, address projectAddress);

    modifier projectInProduction(address _projectAddr) {
        _projectInProduction(_projectAddr);
        _;
    }

    modifier projectNotInProduction(address _projectAddr) {
        _projectNotInProduction(_projectAddr);
        _;
    }

    modifier isExternalHolder(address _userAddress, address _projectAddr) {
        _isExternalHolder(_userAddress, _projectAddr);
        _;
    }

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    function initialize(
        address _unergyDataAddr,
        address _permissionGranterAddr
    ) external notZeroAddress(_unergyDataAddr) initializer {
        __ReentrancyGuard_init();
        __Pausable_init();
        __Common_init(_permissionGranterAddr);
        __UUPSUpgradeable_init();

        unergyData = UnergyData(_unergyDataAddr);
    }

    function _authorizeUpgrade(
        address newImplementation
    ) internal override onlyOwner {}

    /// @notice An energy report is made by a meter to a project to generate new CleanEnergyAssets
    /// @param _projectAddr the address of the project to which the energy report is made
    /// @param _currentAccEnergy the current accumulated energy of the meter
    function energyReport(
        address _projectAddr,
        uint256 _currentAccEnergy
    )
        external
        whenNotPaused
        projectInProduction(_projectAddr)
        hasMeterPermissionOverProjectContract(
            msg.sender,
            address(this),
            _projectAddr
        )
    {
        uint256 lastAccEnergy = unergyData.getAccEnergyByMeter(
            _projectAddr,
            msg.sender
        );
        if (lastAccEnergy >= _currentAccEnergy) {
            revert UnderRegisteredEnergyError(lastAccEnergy, _currentAccEnergy);
        }

        uint256 energyDelta = (_currentAccEnergy - lastAccEnergy);

        if (lastAccEnergy == 0) {
            unergyData.setAccEnergyByMeter(
                _projectAddr,
                msg.sender,
                _currentAccEnergy
            );
        } else {
            unergyData.setAccEnergyByMeter(
                _projectAddr,
                msg.sender,
                _currentAccEnergy
            );

            CleanEnergyAssets(unergyData.cleanEnergyAssetsAddress()).mint(
                _projectAddr,
                energyDelta
            );
        }

        emit EnergyReported(_projectAddr, _currentAccEnergy, energyDelta);
    }

    /// @notice This function is in charge of represent the fiat payments mades in the protocol
    /// @param _projectAddr The address of the project that is invoicing
    /// @param _energyDelta The amount of energy that is being paid
    /// @param _energyTariff The tariff of the energy that is being paid
    /// @param _projectPresentValue The project present value in USD
    function invoiceReport(
        address _projectAddr,
        uint256 _energyDelta,
        uint256 _energyTariff,
        uint256 _projectPresentValue
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "invoiceReport")
    {
        uint256 energyDecimals = CleanEnergyAssets(
            unergyData.cleanEnergyAssetsAddress()
        ).energyDecimals();

        Project memory project = ProjectsManager(
            unergyData.projectsManagerAddress()
        ).getProject(_projectAddr);

        uint256 stableDecimals = (10 **
            ERC20Abs(project.stableAddr).decimals());

        uint256 energyAssetsDecimals = (10 ** energyDecimals);

        uint256 amount = MathUpgradeable.mulDiv(
            _energyDelta * stableDecimals,
            _energyTariff,
            energyAssetsDecimals * stableDecimals
        );

        _incomeCalculation(project, _projectAddr, amount, stableDecimals);

        _invoiceReport(
            _projectAddr,
            amount,
            _energyDelta,
            _energyTariff,
            _projectPresentValue
        );
    }

    function _incomeCalculation(
        Project memory project,
        address _projectAddr,
        uint256 amount,
        uint256 stableDecimals
    )
        internal
        returns (
            uint256 maintenanceIncome,
            uint256 assetManagerIncome,
            uint256 profitIncome
        )
    {
        maintenanceIncome = MathUpgradeable.mulDiv(
            amount * stableDecimals,
            project.maintenancePercentage,
            100 * stableDecimals * (10 ** 18) //maintenance percentage decimals
        );

        assetManagerIncome = MathUpgradeable.mulDiv(
            amount * stableDecimals,
            unergyData.assetManagerFeePercentage(_projectAddr),
            100 * stableDecimals * (10 ** 18) //maintenance percentage decimals
        );

        profitIncome = amount - maintenanceIncome - assetManagerIncome;

        stableBalance += profitIncome;

        ///transfer profitIncome funds to this contract
        IERC20Upgradeable(project.stableAddr).safeTransferFrom(
            msg.sender,
            address(this),
            profitIncome
        );

        ///transfer maintenanceIncome funds to maintenance address
        IERC20Upgradeable(project.stableAddr).safeTransferFrom(
            msg.sender,
            unergyData.maintainerAddress(),
            maintenanceIncome
        );

        ///transfer assetManagerIncome funds to Asset Manager address
        IERC20Upgradeable(project.stableAddr).safeTransferFrom(
            msg.sender,
            unergyData.assetManagerAddress(_projectAddr),
            assetManagerIncome
        );
    }

    function _invoiceReport(
        address _projectAddr,
        uint256 _income,
        uint256 _energyDelta,
        uint256 _energyTariff,
        uint256 _newProjectValue //this is the new project value in USD (6 decimals)
    ) internal {
        Project memory project = ProjectsManager(
            unergyData.projectsManagerAddress()
        ).getProject(_projectAddr);
        uint256 usdDepreciated = unergyData.depreciationBalance();
        uint256 projectTotalSupply = IERC20Upgradeable(address(_projectAddr))
            .totalSupply();

        uint256 usdDepreciatedPerProject;

        //When the project status is INSTALLED, it is not necessary to count depreciation.
        //Otherwise, the depreciation is counted in usd
        if (project.state == ProjectState.INSTALLED) {
            usdDepreciatedPerProject = 0;
        } else if (project.state == ProjectState.PRODUCTION) {
            if (_newProjectValue < project.currentProjectValue) {
                //
                uint256 projectDecimals = (10 **
                    ERC20Abs(_projectAddr).decimals());
                uint256 pWattPercentageIntoReserve = MathUpgradeable.mulDiv(
                    project.pWattsSupply,
                    /**
                     * project.pWattsSupply has 18 decimals
                     * projectTotalSupply has 18 decimals
                     * We need 18 decimals on pWattPercentageIntoReserve.
                     * So, we multiply by 100 ** (projectDecimals)
                     */
                    100 * projectDecimals,
                    projectTotalSupply
                );

                uint256 deltaDepreciation = project.currentProjectValue -
                    _newProjectValue;
                uint256 stableDecimals = (10 **
                    ERC20Abs(project.stableAddr).decimals());

                uint256 leftDepreciation = project.initialProjectValue -
                    project.usdDepreciated;

                if (leftDepreciation <= deltaDepreciation) {
                    project.usdDepreciated += leftDepreciation;

                    usdDepreciated += MathUpgradeable.mulDiv(
                        leftDepreciation * stableDecimals,
                        pWattPercentageIntoReserve,
                        100 * stableDecimals * projectDecimals
                    );
                    project.currentProjectValue = 0;
                } else {
                    project.usdDepreciated += deltaDepreciation;

                    usdDepreciated += MathUpgradeable.mulDiv(
                        deltaDepreciation * stableDecimals,
                        pWattPercentageIntoReserve,
                        100 * stableDecimals * projectDecimals
                    );
                    project.currentProjectValue = _newProjectValue;
                }

                unergyData.setDepreciationBalance(usdDepreciated);
            } else if (_newProjectValue > project.currentProjectValue) {
                project.currentProjectValue = _newProjectValue;
            } else {
                //When _newProjectValue is equal to project.currentProjectValue, is not necessary update any value
                CleanEnergyAssets(unergyData.cleanEnergyAssetsAddress()).burn(
                    _projectAddr,
                    _energyDelta
                );

                emit InvoiceReport(
                    _projectAddr,
                    _energyDelta,
                    _energyTariff,
                    _income,
                    usdDepreciatedPerProject
                );

                return;
            }
        } else {
            //When the project status is FUNDING, CLOSED or CANCELLED, is not necessary update any value
            revert InvoiceReportNotAvailable(project.state);
        }

        ProjectsManager(unergyData.projectsManagerAddress()).updateProject(
            _projectAddr,
            project
        );

        //Burn the paid energy delta
        CleanEnergyAssets(unergyData.cleanEnergyAssetsAddress()).burn(
            _projectAddr,
            _energyDelta
        );

        emit InvoiceReport(
            _projectAddr,
            _energyDelta,
            _energyTariff,
            _income,
            usdDepreciatedPerProject
        );
    }

    function pWattsTransfer(
        address _projectAddr,
        address _receiver,
        uint256 _pWattsAmount
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "pWattsTransfer")
    {
        PurchaseTicket memory buyTicket = unergyData.redeemTicket(
            _projectAddr,
            _receiver
        );

        if (buyTicket.used) revert TicketUsed();
        if (buyTicket.expiration < block.timestamp) revert TicketExpired();

        Project memory project = ProjectsManager(
            unergyData.projectsManagerAddress()
        ).getProject(_projectAddr);
        address projectAdmin = project.adminAddr;
        ERC20Abs pWatt = ERC20Abs(project.addr);
        uint256 pWattAdminBalance = pWatt.balanceOf(projectAdmin);

        if (_pWattsAmount > pWattAdminBalance) {
            revert NotEnoughPWattsToPurchase(pWattAdminBalance, _pWattsAmount);
        }

        uint256 pWattsFinalAmount = _pWattsAmount;
        address unergyBuyerAddress = unergyData.unergyBuyerAddress();
        if (_receiver == address(unergyBuyerAddress)) {
            //If the receiver is the `UnergyBuyer` contract
            pWattsFinalAmount = _pWattsTransferUnergyBuyer(
                project,
                pWatt,
                _pWattsAmount,
                buyTicket.pWattPrice
            );
        } else {
            project = _pWattsTransferAnyUser(
                project,
                pWatt,
                _pWattsAmount,
                buyTicket,
                address(0)
            );
        }

        _transferPWattsAndUpdateTicket(
            pWattsFinalAmount,
            _receiver,
            project,
            pWatt
        );
    }

    // @custom:update Modified in a contract upgrade at 2023-28-12 17:31 GMT-5
    function buyPWatts(address _projectAddr, uint256 _pWattsAmount) external {
        address caller = msg.sender;
        _buyPWatts(caller, _projectAddr, _pWattsAmount);
    }

    // @custom:update Modified in a contract upgrade at 2023-28-12 17:31 GMT-5
    function buyPWattsFrom(
        address _from,
        address _projectAddr,
        uint256 _pWattsAmount
    ) external {
        _buyPWatts(_from, _projectAddr, _pWattsAmount);
    }

    // @custom:update Modified in a contract upgrade at 2023-28-12 17:31 GMT-5
    function _buyPWatts(
        address _user,
        address _projectAddr,
        uint256 _pWattsAmount
    ) internal nonReentrant whenNotPaused {
        PurchaseTicket memory buyTicket = unergyData.redeemTicket(
            _projectAddr,
            _user
        );

        if (buyTicket.receiver == address(0)) revert UserHasNoTicket();
        if (buyTicket.used) revert TicketUsed();
        if (buyTicket.expiration < block.timestamp) revert TicketExpired();

        Project memory project = ProjectsManager(
            unergyData.projectsManagerAddress()
        ).getProject(_projectAddr);
        address projectAdmin = project.adminAddr;
        ERC20Abs pWatt = ERC20Abs(project.addr);
        uint256 pWattAdminBalance = pWatt.balanceOf(projectAdmin);

        if (_pWattsAmount > pWattAdminBalance) {
            revert NotEnoughPWattsToPurchase(pWattAdminBalance, _pWattsAmount);
        }

        project = _pWattsTransferAnyUser(
            project,
            pWatt,
            _pWattsAmount,
            buyTicket,
            _user
        );

        _transferPWattsAndUpdateTicket(_pWattsAmount, _user, project, pWatt);
    }

    function _transferPWattsAndUpdateTicket(
        uint256 _amount,
        address _sender,
        Project memory _project,
        ERC20Abs _pWatt
    ) internal {
        //change used state on buyTicket
        unergyData.changePurchaseTicketUsed(_project.addr, _sender);

        //transfer pWatts to reserve(UnergyLogicReserve) and
        //get permission for future Swap process
        if (_amount > 0) {
            IERC20Upgradeable(address(_pWatt)).safeTransferFrom(
                _project.adminAddr,
                _sender,
                _amount
            );
        }
    }

    function _pWattsTransferUnergyBuyer(
        Project memory _project,
        ERC20Abs _pWatt,
        uint256 _pWattsAmount,
        uint256 _pWattPrice
    ) internal returns (uint256 pWattsFinalAmount) {
        uint256 stableAmountDepreciated = unergyData.depreciationBalance();

        uint256 stableAmount = MathUpgradeable.mulDiv(
            _pWattsAmount,
            _pWattPrice,
            10 ** _pWatt.decimals()
        );

        //if the total cost of pWatts is less than or equal to the accumulated depreciation amount
        //it is necessary to subtract from the amount of depreciation the amount used for this purchase
        uint256 pWattsToReserve = 0;
        if (stableAmount <= stableAmountDepreciated) {
            //depreciation amount in pWatts
            pWattsToReserve = _pWattsAmount;

            stableAmountDepreciated -= stableAmount;

            // all the pWatts were bought to compensate the depreciation of the asset.
            // Therefore, UnergyBuyer is left with none
            pWattsFinalAmount = 0;
        } else {
            //amount of depreciation in pWatts
            pWattsToReserve = MathUpgradeable.mulDiv(
                stableAmountDepreciated,
                (10 ** _pWatt.decimals()),
                _pWattPrice
            );
            stableAmountDepreciated = 0;

            //the pWatts that UnergyBuyer will stay with, taking away the depreciated ones
            pWattsFinalAmount = _pWattsAmount - pWattsToReserve;
        }

        address unergyBuyerAddress = unergyData.unergyBuyerAddress();
        // stableCoins leave the Funding contract

        if (stableBalance < stableAmount) revert NotEnoughStableBalance();

        stableBalance -= stableAmount;
        IERC20Upgradeable(_project.stableAddr).safeTransfer(
            address(unergyBuyerAddress),
            stableAmount
        );

        //Update present project funding value
        uint256 presentProjectFundingValue = unergyData
            .getPresentProjectFundingValue(address(_pWatt));
        unergyData.setPresentProjectFundingValue(
            address(_pWatt),
            presentProjectFundingValue + stableAmount
        );

        //uWatts that are directly passed to the reserve due to depreciation
        IERC20Upgradeable(address(_pWatt)).safeTransferFrom(
            _project.adminAddr,
            address(this),
            pWattsToReserve
        );

        unergyData.setDepreciationBalance(stableAmountDepreciated);
    }

    function _pWattsTransferAnyUser(
        Project memory _project,
        ERC20Abs _pWatt,
        uint256 _pWattsAmount,
        PurchaseTicket memory buyTicket,
        address _from
    ) internal returns (Project memory) {
        if (buyTicket.receiver == _project.originator) {
            revert OriginatorCannotMakeAPurchase(buyTicket.receiver);
        }
        // Institutional Wallet
        if (
            unergyData.isExternalHolderAddress(
                buyTicket.receiver,
                _project.addr
            )
        ) {
            _project.pWattsSupply -= _pWattsAmount;
            ProjectsManager(unergyData.projectsManagerAddress()).updateProject(
                _project.addr,
                _project
            );
        }

        uint256 stableAmount = MathUpgradeable.mulDiv(
            _pWattsAmount,
            buyTicket.pWattPrice,
            10 ** _pWatt.decimals()
        );

        address unergyBuyerAddress = unergyData.unergyBuyerAddress();
        //stableCoins come from the function caller
        address payer = _from == address(0) ? msg.sender : _from;
        IERC20Upgradeable(_project.stableAddr).safeTransferFrom(
            payer,
            address(unergyBuyerAddress),
            stableAmount
        );

        //Update present project funding value
        uint256 presentProjectFundingValue = unergyData
            .getPresentProjectFundingValue(address(_pWatt));
        unergyData.setPresentProjectFundingValue(
            address(_pWatt),
            presentProjectFundingValue + stableAmount
        );

        return _project;
    }

    // @custom:update Method modified in a contract upgrade at 2023-23-11
    /**
     * @notice We used this function to take a snapshot of the totalSupply and user balance
     * from an offchain service. This service then calls the swapToken function below.
     */
    function requestStandardSwap(
        address _projectAddr,
        uint256 _usersToProcess
    )
        external
        whenNotPaused
        projectNotInProduction(_projectAddr)
        // We mantain the old permission for this function because it is actively used
        // and create a new one would be a breaking change
        hasRoleInPermissionGranter(msg.sender, address(this), "requestSwap")
    {
        emit StandardSwapRequested(_projectAddr, block.number, _usersToProcess);
    }

    /**
     * @notice This function should be called by an admin when a project completes the funding stage and
     * is ready to swap pWatts for uWatts
     * @param _projectAddr The address of the project to swap
     * Requirements:
     *- The project must be funded completely
     *- Only an admin can call this function
     */
    function swapToken(
        address _projectAddr,
        uint256 _usersToProcess
    )
        external
        whenNotPaused
        projectNotInProduction(_projectAddr)
        hasRoleInPermissionGranter(msg.sender, address(this), "swapToken")
    {
        ERC20Abs pWatt = ERC20Abs(_projectAddr);
        Project memory project = ProjectsManager(
            unergyData.projectsManagerAddress()
        ).getProject(_projectAddr);

        address unergyBuyerAddress = unergyData.unergyBuyerAddress();

        if (!UnergyBuyer(unergyBuyerAddress).wasFullySigned(_projectAddr))
            revert ProjectNotFullySigned();

        (
            uint256 lastUserIndex,
            uint256 totalHolders,
            address[] memory holders
        ) = _selectHoldersForSwap(_projectAddr, _usersToProcess);

        _swapToken(_projectAddr, holders, project, pWatt);

        if (lastUserIndex == totalHolders) {
            UnergyBuyer(unergyBuyerAddress).setProjectState(
                _projectAddr,
                ProjectState.PRODUCTION
            );
        }
    }

    /// @param _projectAddr See { this.swapToken }
    /// @param holders Holders are the accounts that have pWatts relate to the project
    /// @param project This is the project that will be swapped
    /// @param pWatt This is the pWatt token contract of the project
    function _swapToken(
        address _projectAddr,
        address[] memory holders,
        Project memory project,
        ERC20Abs pWatt
    ) internal {
        uint256 unergyUWatts;

        for (uint256 i; i < holders.length; i++) {
            if (holders[i] != address(this)) {
                uint256 pWattsPerHolder = pWatt.balanceOf(holders[i]);
                bool isExternalHolderAddress = unergyData
                    .isExternalHolderAddress(holders[i], _projectAddr);

                if (!isExternalHolderAddress && pWattsPerHolder > 0) {
                    IERC20Upgradeable(_projectAddr).safeTransferFrom(
                        holders[i],
                        address(this),
                        pWattsPerHolder
                    );

                    uint256 uWattsPerHolder = _swap(
                        _projectAddr,
                        holders[i],
                        pWattsPerHolder,
                        project.swapFactor,
                        pWatt
                    );

                    if (
                        holders[i] == address(unergyData.unergyBuyerAddress())
                    ) {
                        unergyUWatts = uWattsPerHolder;
                    }
                }
            }
        }

        emit TokenSwapped(_projectAddr, unergyUWatts);
    }

    // @custom:update Method created in a contract upgrade at 2023-23-11
    function _customSwap(
        ERC20Abs pWatt,
        address _userAddr,
        uint256 holderPWatts,
        uint256 _swapFactor
    ) internal {
        uint256 uWattsUnergy;
        address projectAddress = address(pWatt);

        IERC20Upgradeable(projectAddress).safeTransferFrom(
            _userAddr,
            address(this),
            holderPWatts
        );

        uint256 uWattsPerHolder = _swap(
            projectAddress,
            _userAddr,
            holderPWatts,
            _swapFactor,
            pWatt
        );

        if (_userAddr == unergyData.unergyBuyerAddress()) {
            uWattsUnergy = uWattsPerHolder;
        }

        emit TokenSwapped(projectAddress, uWattsUnergy);
    }

    function _swap(
        address projectAddr,
        address pWattHolders,
        uint256 holderPWatts,
        uint256 _swapFactor,
        ERC20Abs pWatt
    ) internal returns (uint256) {
        ERC20Abs uWatt = ERC20Abs(address(unergyData.uWattAddress()));

        uint256 uWattMultiplier = 10 ** uWatt.decimals();
        uint256 pWattMultiplier = 10 ** pWatt.decimals();
        uint256 swapFactorMultiplier = 10 ** 18;

        uint256 uWattsPerHolderBeforeFee = MathUpgradeable.mulDiv(
            holderPWatts * uWattMultiplier,
            _swapFactor,
            pWattMultiplier * swapFactorMultiplier
        );

        (
            uint256 uWattsForProtocolStaking,
            uint256 uWattsPerHolder
        ) = _getAmountsWithFee(
                uWattsPerHolderBeforeFee,
                uWattMultiplier,
                swapFactorMultiplier
            );

        _mintUWatts(pWattHolders, uWattsPerHolder);
        _mintUWatts(
            unergyData.stakingProtocolAddress(),
            uWattsForProtocolStaking
        );

        //the user to be swapped is marked as external holder
        unergyData.setExternalHolderAddress(pWattHolders, projectAddr, true);

        return uWattsPerHolder;
    }

    function _selectHoldersForSwap(
        address _projectAddr,
        uint256 _usersToProcess
    ) internal returns (uint256, uint256, address[] memory) {
        if (_usersToProcess == 0) {
            revert MinimumUsersToProcessError();
        }

        address[] memory fullHoldersArray = ProjectsManager(
            unergyData.projectsManagerAddress()
        ).getProjectHolders(_projectAddr);

        uint256 lastUserIndex = lastUserIndexProcessed[_projectAddr];
        uint256 endIndex = lastUserIndex + _usersToProcess;

        address[] memory holdersForSwap;

        // Ensure we don't go beyond the array length
        if (endIndex > fullHoldersArray.length) {
            holdersForSwap = new address[](
                fullHoldersArray.length - lastUserIndex
            );
            endIndex = fullHoldersArray.length;
        } else {
            holdersForSwap = new address[](_usersToProcess);
        }

        uint256 j = 0;

        for (uint256 i = lastUserIndex; i < endIndex; i++) {
            holdersForSwap[j] = fullHoldersArray[i];
            j++;
        }

        lastUserIndexProcessed[_projectAddr] = endIndex;

        return (endIndex, fullHoldersArray.length, holdersForSwap);
    }

    // @custom:update Method created in a contract upgrade at 2023-23-11
    /**
     * @notice We used this function to take a snapshot of the totalSupply and user balance
     * from an offchain service. This service then calls the swapToken function below.
     */
    function requestCustomSwap(
        address _projectAddr,
        address _userAddr,
        uint256 _swapFactor
    )
        external
        whenNotPaused
        // This modifier is added here to prevent custom swaps request that will failed
        isExternalHolder(_userAddr, _projectAddr)
        hasRoleInPermissionGranter(msg.sender, address(this), "requestSwap")
    {
        emit CustomSwapRequested(
            _projectAddr,
            block.number,
            _userAddr,
            _swapFactor
        );
    }

    // @custom:update Method updated in a contract upgrade at 2023-23-11
    function customSwap(
        address _projectAddr,
        address _userAddr,
        uint256 _swapFactor
    )
        external
        whenNotPaused
        // This modifier is added here to prevent invalid custom swaps that do not
        // follow the request-action pattern
        isExternalHolder(_userAddr, _projectAddr)
        hasRoleInPermissionGranter(msg.sender, address(this), "swapToken")
    {
        ERC20Abs pWatt = ERC20Abs(_projectAddr);
        uint256 holderPWatts = pWatt.balanceOf(_userAddr);

        if (holderPWatts <= 0) {
            revert UserDoesNotHavePWatts(_userAddr, _projectAddr);
        }

        _customSwap(pWatt, _userAddr, holderPWatts, _swapFactor);
    }

    function _mintUWatts(address _receiver, uint256 _amount) internal virtual {
        address uWattAddress = unergyData.uWattAddress();
        ERC20Abs uWatt = ERC20Abs(uWattAddress);

        uWatt.mint(_receiver, _amount);
    }

    /**
     * This function is used to trigger the claimable amount calculation in a offchain service.
     * This service then calls the claim function.
     */
    function requestClaim() external whenNotPaused {
        emit ClaimRequested(msg.sender);
    }

    function requestClaimForUser(
        address _userAddress
    )
        external
        hasRoleInPermissionGranter(msg.sender, address(this), "requestClaim")
        whenNotPaused
    {
        emit ClaimRequested(_userAddress);
    }

    /**
     * @notice Use this function to claim you rewards from a reinvestment of your uWatts from Unergy
     * @dev reverts if the caller does not have any claims available
     */
    function claimUWatts(
        address _holder,
        uint256 _amount
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(msg.sender, address(this), "claimUWatts")
    {
        address unergyBuyerAddress = unergyData.unergyBuyerAddress();
        UnergyBuyer(unergyBuyerAddress).payUWattReward(_holder, _amount);
        emit UWattsClaimed(_amount, _holder);
    }

    // @custom:update Added in a contract upgrade at 2023-14-12 14:15 GMT-5
    function setLastUserIndexProcessed(
        address _projectAddr,
        uint256 _lastUserIndexProcessed
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "setLastUserIndexProcessed"
        )
    {
        lastUserIndexProcessed[_projectAddr] = _lastUserIndexProcessed;
    }

    /**
     * @notice This function is used to withdraw funds left over from energy payments
     */
    function withdrawStableCoin(
        address _projectAddress,
        address _receiver,
        uint256 _amount
    )
        external
        whenNotPaused
        hasRoleInPermissionGranter(
            msg.sender,
            address(this),
            "withdrawStableCoin"
        )
    {
        address projectsManagerAddress = unergyData.projectsManagerAddress();
        Project memory project = ProjectsManager(projectsManagerAddress)
            .getProject(_projectAddress);

        IERC20Upgradeable(project.stableAddr).safeTransfer(_receiver, _amount);

        emit StableCoinWithdraw(_projectAddress, _receiver, _amount);
    }

    function transferOwnership(
        address _pendingOwner
    ) public override onlyOwner whenNotPaused notZeroAddress(_pendingOwner) {
        potentialOwner = _pendingOwner;
        emit OwnerNominated(_pendingOwner);
    }

    function acceptOwnership() external {
        if (msg.sender == potentialOwner) {
            _transferOwnership(msg.sender);
            potentialOwner = address(0);
        } else {
            revert NotPotentialOwner(msg.sender);
        }
    }

    function _projectInProduction(address _projectAddr) internal view {
        ProjectsManager projectsManager = ProjectsManager(
            unergyData.projectsManagerAddress()
        );
        ProjectState projectState = projectsManager.getProjectState(
            _projectAddr
        );
        if (projectState != ProjectState.PRODUCTION)
            revert ProjectNotInProduction(_projectAddr, projectState);
    }

    function _projectNotInProduction(address _projectAddr) internal view {
        ProjectsManager projectsManager = ProjectsManager(
            unergyData.projectsManagerAddress()
        );
        ProjectState projectState = projectsManager.getProjectState(
            _projectAddr
        );
        if (projectState == ProjectState.PRODUCTION)
            revert ProjectAlreadyInProduction(_projectAddr);
    }

    function _isExternalHolder(
        address _userAddress,
        address _projectAddr
    ) internal view {
        if (!unergyData.isExternalHolderAddress(_userAddress, _projectAddr)) {
            revert IsNotExternalHolder(_userAddress);
        }
    }

    /**
     * @notice Use this function to calculate the swap fee for a given amount of uWatts
     * @param userAmount The amount of uWatts to calculate the fee
     * @param uWattMultiplier The multiplier of the uWatt token
     * @param swapFactorMultiplier The multiplier of the swap factor
     * @return (protocolUWatts, userUWatts)
     *  - protocolUWatts: The amount of uWatts that will be sent to the protocol
     *  - userUWatts: The amount of uWatts that will be sent to the user
     */
    function _getAmountsWithFee(
        uint256 userAmount,
        uint256 uWattMultiplier,
        uint256 swapFactorMultiplier
    ) internal view returns (uint256, uint256) {
        uint256 swapFeePercentage = unergyData.swapFeePercentage();

        uint256 protocolUWatts = MathUpgradeable.mulDiv(
            userAmount * uWattMultiplier,
            swapFeePercentage,
            100 * uWattMultiplier * swapFactorMultiplier
        );
        uint256 userUWatts = userAmount - protocolUWatts;

        return (protocolUWatts, userUWatts);
    }

    function pause() external onlyOwner whenNotPaused {
        _pause();
    }

    function unpause() external onlyOwner whenPaused {
        _unpause();
    }
}
