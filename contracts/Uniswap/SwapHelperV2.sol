// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@uniswap/v3-periphery/contracts/libraries/TransferHelper.sol";
import "@uniswap/v3-periphery/contracts/interfaces/ISwapRouter.sol";

contract SwapHelperV2 {
    ISwapRouter public immutable SWAP_ROUTER;
    address public immutable USDT;
    address public immutable UWATT;

    uint24 public immutable POOL_FEE;

    constructor(
        uint24 poolFee,
        address swapRouterAddress,
        address uWattAddress,
        address usdtAddress
    ) {
        POOL_FEE = poolFee;
        SWAP_ROUTER = ISwapRouter(swapRouterAddress);
        UWATT = uWattAddress;
        USDT = usdtAddress;
    }

    /// @notice swapExactInputSingle swaps a fixed amount of USDT for a maximum possible amount of UWATT
    /// using the USDT/UWATT 0.05% pool by calling `exactInputSingle` in the swap router.
    /// @dev The calling address must approve this contract to spend at least `amountIn` worth of its USDT for this function to succeed.
    /// @param amountIn The exact amount of USDT that will be swapped for UWATT.
    /// @return amountOut The amount of UWATT received.
    function swapExactInputSingle(
        uint256 amountIn,
        uint256 amountOutMinimum,
        uint160 sqrtPriceLimitX96
    ) external returns (uint256 amountOut) {
        // msg.sender must approve this contract

        // Transfer the specified amount of USDT to this contract.
        TransferHelper.safeTransferFrom(
            USDT,
            msg.sender,
            address(this),
            amountIn
        );

        // Approve the router to spend USDT.
        TransferHelper.safeApprove(USDT, address(SWAP_ROUTER), amountIn);

        // We set the sqrtPriceLimitx96 to be 0 to ensure we swap our exact input amount.
        ISwapRouter.ExactInputSingleParams memory params = ISwapRouter
            .ExactInputSingleParams({
                tokenIn: USDT,
                tokenOut: UWATT,
                fee: POOL_FEE,
                recipient: msg.sender,
                deadline: block.timestamp,
                amountIn: amountIn,
                amountOutMinimum: amountOutMinimum,
                sqrtPriceLimitX96: sqrtPriceLimitX96
            });

        // The call to `exactInputSingle` executes the swap.
        amountOut = SWAP_ROUTER.exactInputSingle(params);
    }
}
