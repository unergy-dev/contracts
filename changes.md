## **Pre-report V1**


### **CommonUpgradeable.sol (CUB)**


* ***[CUB-01 | Lack of Storage Gap in Upgradeable Contract](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CUB-01)***
    
    A [gap variable](contracts/CommonUpgradeable.sol#L21) is added to the [CommonUpgradeable](contracts/CommonUpgradeable.sol) contract to prevent the overwriting of storage slots in future upgrades of proxy upgradeable contract implementations.


### **ERC1155CleanEnergyAssets.sol (ERC)**


* ***[ERC-01 | Discussion on Purpose of `createGeneralEnergyAsset()`](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ERC-01)***

    The [`_createGeneralEnergyAsset()`](contracts/ERC1155CleanEnergyAssets.sol#L43) function is made internal and is called from the constructor function of the [ERC1155CleanEnergyAssets](contracts/ERC1155CleanEnergyAssets.sol) contract.


### **ERC20Project.sol (ERP)**


* ***[ERP-01 | Discussion on pWatts Transfers](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ERP-01)***

    The [`approve()`](contracts/ERC20Project.sol#L138) function of the [ERC20Project](contracts/ERC20Project.sol) contract is overwritten and restricted.


### **ProjectsManager.sol (PMB)**


* ***[PMB-01 | Lack of Limits on Project Parameters](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=PMB-01)***

   Verification for [`maintenancePercentage`](contracts/ProjectsManager.sol#L148) and [`operatorFee`](contracts/ProjectsManager.sol#L153) is added to the [`_createProject()`](contracts/ProjectsManager.sol#L143) function of the [ProjectsManager](contracts/ProjectsManager.sol) contract.


* ***[PMB-02 | Possibly Inaccurate pWatt Holders](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=PMB-02)***

    This comment highlights two issues, explained below.

    1. This function doesn't check for balances, allowing addresses in the holders array that may not possess pWatt tokens.

    2. Additionally, there's no method to remove addresses from the holders array. Since this array is used in swapping pWatts for uWatts, having unnecessary addresses can make these operations costly.

    The first issue has been identified and resolved in the [`afterTransferReceipt()`](contracts/UnergyEvent.sol#L97) function of the [UnergyEvent](contracts/UnergyEvent.sol) contract, only adding users if they do not exist in the holders array and if the value received in the transaction is greater than zero. However, addressing the second one is more complex than simply removing a holder from the array if their balance is 0. Removing a holder from the array would introduce a time complexity of O(n) every time a pWatt is moved, leading to higher transaction costs for protocol users. Consequently, we accept the increased cost in the swap operation.


* ***[PMB-03 | Lack of Check on Milestone Weights](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=PMB-03)***

    A function called [`_checkMilestoneWeights()`](contracts/ProjectsManager.sol#L394) is created to check the weights of milestones in two scenarios:

    1. [When a new milestone is added to a project](contracts/ProjectsManager.sol#L305).

    2. When a project milestone is updated
        * [All milestones](contracts/ProjectsManager.sol#L340)
        * [By Index](contracts/ProjectsManager.sol#L370)
    

### **UnergyBuyer.sol (UBB)**


* ***[UBB-02 | Incorrect Check on Fully Signed](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=UBB-02)***

    Now the [`_wasFullySigned()`](contracts/UnergyBuyer.sol#186) function verifies that both the installer and the originator have signed all the milestones, marking them as fully signed.


* ***[UBB-03 | Missing Installer Signature Check](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=UBB-03)***

    [A 'require' statement](contracts/UnergyBuyer.sol#L151) has been added to the [`setOriginatorSign()`](contracts/UnergyBuyer.sol#L131) function (formerly `setUnergySign()`) to ensure that a milestone can only be signed by the originator if it has been previously signed by the installer.


### **UnergyLogicReserve.sol (ULR)**


* ***[ULR-02 | Investors Not Able to Claim uWatt Rewards Even if They Have Been Generated](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-02)***

    * Issue 1: The `calcUWattsToClaim()` function has been removed, and the claiming functionality is now handled by an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).

    * Issue 2: The `_claimUWatt()` function has been removed, and the functionality is now handled by an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-05 | Incorrect Snapshot Update due to Default Values Returned by No Snapshot Found](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-05)***

    The `_setAvailableToClaim()` function has been removed, and the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-06 | Potentially Unable to Burn Energy Asset](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-06)***
    
   The [whitepaper](https://unergy.io/whitepaper) will clarify why energy tokens are not issued in the initial energy report.


* ***[ULR-07 | Discussion on Inconsistent Logic for _lastImportantSnapshot](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-07)***

    The `calcUWattsToClaim()` function has been removed, and the claiming functionality is now handled by an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-08 | Possible For A Snapshot's Balance To Exceed Historical Total Supply](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-08)*** 

    The `updateLastUWattStatus()` function has been removed, and the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-09 | Total Claimable UWatts Can Exceed Available UWatts](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-09)***

    The `updateLastUWattStatus()` function has been removed, and the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-10 | Users Potentially Have Zero Claimable uWatt Rewards Because of Rounding Issue](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-10)*** 

    This issue was resolved by modifying the [`ERC20UWatt`](contracts/ERC20UWatt.sol) contract to have 18 decimal places. Additionally, the uWatt claim calculation will now be performed off-chain in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154), ensuring that users, regardless of how small their uWatt balance may be, will always be able to make a claim.


* ***[ULR-12 | Check Effect Interaction Pattern Violated](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-12)*** 

    The [`nonReentrant` modifier](contracts/UnergyLogicReserve.sol#L416) is added to the `buyPWatts()` function.


* ***[ULR-13 | Potential Arithmetic over/underflow](https://skyharbor.certik.com/report/60050d87-a868-`4031-ba09-8c030c536972?findingIndex=ULR-13)*** 

    The [`_swapToken()`](contracts/UnergyLogicReserve.sol#L648) function has been modified and no longer calls the `_setAvailableToClaim()` function, thus avoiding Arithmetic over/underflow errors.


* ***[ULR-14 | Incorrect totalSupply for New Snapshot While Claiming Rewards](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-14)***

    The `insertNewSnapshot()` function has been removed, and the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789)that listens for all uWatts transfer events in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-15 | Potentially Locked Stable Coin in Reserve](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-15)***

    Functions have been added to [UnergyBuyer](contracts/UnergyBuyer.sol) and [UnergyLogicReserve](contracts/UnergyLogicReserve.sol) to withdraw stableCoin. More details on this process are provided below:

    1. [`withdrawStableCoin()`](contracts/UnergyBuyer.sol#L457) in [UnergyBuyer](contracts/UnergyBuyer.sol): This function is used to withdraw remaining balances from pWatt purchases and is also used to return the stableCoin received for pWatt purchases when a project is canceled.

    2. [`withdrawStableCoin()`](contracts/UnergyLogicReserve.sol#L850) in [UnergyLogicReserve](contracts/UnergyLogicReserve.sol): This function is used to withdraw remaining balances from energy payments and prevent funds from becoming trapped in contracts.


* ***[ULR-16 | Potential Out-of-Gas Issue](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-16)***

    The `_setAvailableToClaim()` function and `historicalSwaps` structure have been removed, and we avoid the iterations of projects and holders within the [UnergyLogicReserve](contracts/UnergyLogicReserve.sol), Additionally the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-17 | Code Inefficiency in `_claimUWatt()`](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-17)***

    The `_claimUWatt()` function has been removed, and the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-18 | Possible Incorrect Total Supply](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-18)***


    The `_claimUWatt()` function has been removed, and the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-19 | Incorrect Distribution of uWatts](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-19)***

    The `_setAvailableToClaim()` function has been removed, and the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-20 | Possible For Claimable Project IDs to Exceed Number of Historical Swaps](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-20)***

    The `historicalSwaps` structure have been removed, and we avoid the iterations of projects and holders within the [UnergyLogicReserve](contracts/UnergyLogicReserve.sol), Additionally the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


* ***[ULR-21 | Possible Underflow For Project ID When Claiming](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=ULR-21)***

    The `historicalSwaps` structure have been removed, and we avoid the iterations of projects and holders within the [UnergyLogicReserve](contracts/UnergyLogicReserve.sol), Additionally the status snapshot of a uWatt holder is now updated using [event listeners](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=1406,-1252,1696,1049&embedId=40174689789) in an [external service](https://miro.com/app/board/uXjVMlLN6kc=/?moveToViewport=-2731,-1298,1471,1186&embedId=400736835154).


### **Common Issues**


* ***[CON-03 | Minting Centralization Risk](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-03)***

    [A multi-signature system](contracts/PermissionGranter.sol#L81) is implemented within the [PermissionGranter](contracts/PermissionGranter.sol) contract, where the execution of the `setPermission()` function requires a certain number of signatures for permission approval.

    The [whitepaper](https://unergy.io/whitepaper) discusses the creation of a governance system that oversees the protocol's operations, aiming to enhance transparency and decentralization.


* ***[CON-09 | Pull-Over-Push Pattern In `transferOwnership()` Function](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-09)***

    The `pull-over-push pattern` is implemented over in some contracts for transferOwnership functionality.

    * [UnergyData](contracts/UnergyData.sol#L307)

    * [UnergyBuyer](contracts/UnergyBuyer.sol#L502)

    * [UnergyLogicReserve](contracts/UnergyLogicReserve.sol#L872)

    * [ProjectsManager](contracts/ProjectsManager.sol#L494)

    * [ERC20UWatt](contracts/ERC20UWatt.sol#103)

    In a future version, access to functions will depend solely on the permissions granted by the PermissionGranter and the multi-signature system, eliminating the need for the `ownable` library.


* ***[CON-10 | Centralized Control of Contract Upgrade](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-10)***

    In a future version of the proxy upgradeable contracts, implementations upgrades will be executed through a signature granted by the governance protocol.

    The [whitepaper](https://unergy.io/whitepaper) discusses the creation of a governance system that oversees the protocol's operations, aiming to enhance transparency and decentralization.


* ***[CON-11 | Centralization Related Risks](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-11)***

    [A multi-signature system](contracts/PermissionGranter.sol#L81) is implemented within the [PermissionGranter](contracts/PermissionGranter.sol) contract, where the execution of the `setPermission()` function requires a certain number of signatures for permission approval.

    We have also added a parameter called `type` to the permission system, which configures permissions based on the following criteria:

    * [TimeLock](contracts/Types.sol#L7)

    * [Executions](contracts/Types.sol#L6)

    * [Permanent](contracts/Types.sol#L5)

    * [Single execution](contracts/Types.sol#L8)


* ***[CON-12 | Missing Input Validation](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-12)***
    
    The following validations have been added.

    1.In the [ERC1155CleanEnergyAssets](contracts/ERC1155CleanEnergyAssets.sol) contract, [validation has been added](contracts/ERC1155CleanEnergyAssets.sol#L157) to the `setEnergyLimit()` function to ensure that the `_energyLimit` parameter is greater than zero.

    2. In the [UnergyLogicReserve](contracts/UnergyLogicReserve.sol) contract, in the `setUnergyBuyer()` function, validation was added, but then it was decided that the configuration and queries of the public addresses of other contracts would be centralized in the [UnergyData](contracts/UnergyData.sol#L25) contract.


* ***[CON-13 | Inadequate Validation for Array Index](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-13)***

    We have concluded that we can disregard the `deleteMilestone()` function, which has been removed from the[ProjectsManager](contracts/ProjectsManager.sol) contract. The validation for removing a milestone from the array has also been removed.


* ***[CON-14 | Function `initialize()` Is Unprotected](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-14)***

    The initializers of proxy-upgradeable contracts have been deactivated.

    * [UnergyData](contracts/UnergyData.sol#L54)

    * [UnergyBuyer](contracts/UnergyBuyer.sol#L58)

    * [UnergyLogicReserve](contracts/UnergyLogicReserve.sol#L111)

    * [ProjectsManager](contracts/ProjectsManager.sol#L80)


* ***[CON-15 | Unchecked ERC-20 `transfer()`/`transferFrom()` Call](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-15)***

    The `SafeERC20` library has been implemented for all contracts transfers.

    * [UnergyBuyer](contracts/UnergyBuyer.sol#L32)

        * [StableCoin transfer](contracts/UnergyBuyer.sol#L236) on `_installerPayment()`

        * [uWatt transfer](contracts/UnergyBuyer.sol#L375) on `payUWattReward()`

        * [uWatt transfer](contracts/UnergyBuyer.sol#L446) on `withdrawUWatts()`

        * [StableCoin transfer](contracts/UnergyBuyer.sol#L482) on `withdrawStableCoin()`
    
    * [UnergyLogicReserve](contracts/UnergyLogicReserve.sol)

        * [StableCoin transfer](contracts/UnergyLogicReserve.sol#L224) on `invoiceReport()` payment

        * [StableCoin transfer](contracts/UnergyLogicReserve.sol#L231) for maintenance income payment on `invoiceReport()`

        * [pWatts transfer](contracts/UnergyLogicReserve.sol#L464) on `_transferPWattsAndUpdateTicket()`

        * [StableCoin transfer](contracts/UnergyLogicReserve.sol#L517)  on `_pWattsTransferUnergyBuyer()`

        * [pWatts transfer](contracts/UnergyLogicReserve.sol#L531) on `_pWattsTransferUnergyBuyer()`

        * [stableCoin transfer](contracts/UnergyLogicReserve.sol#L568) on `_pWattsTransferAnyUser()`

        * [pWatts transfer](contracts/UnergyLogicReserve.sol#L670) on `_swapToken()`

        * [pWatts transfer](contracts/UnergyLogicReserve.sol#L774) on `customInstitutionalSwap()`

        * [stableCoin transfer](contracts/UnergyLogicReserve.sol#L867) on `withdrawStableCoin()`


* ***[CON-16 | Missing Emit Events](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-16)***

    Events have been added to sensitive functions.

    * [ERC1155CleanEnergyAssets](contracts/ERC1155CleanEnergyAssets.sol)

        * `setURI()` [event added](contracts/ERC1155CleanEnergyAssets.sol#L147)
    
    * [ERC20Project](contracts/ERC20Project.sol)

        * `setUnergyEventAddr()` has been removed.

        * `setProjectsManagerAddr()` has been removed.

    * [ERC20UWatt](contracts/ERC20UWatt.sol)

        * `setUnergyEventAddr()` has been removed.

    * [ProjectsManager](contracts/ProjectsManager.sol)

        * `setCleanEnergyAssetsAddr()` has been removed.

        * `setUnergyEventAddr()` has been removed.

        * `setUnergyLogicReserveAddr()` has been removed.

    * [UnergyBuyer](contracts/UnergyBuyer.sol)

        * `setCleanEnergyAssetsAddr()` has been removed.

        * `setUWattsAddr()` has been removed.

        * `setUnergyLogicReserveAddr()` has been removed.

        * `setProjectsManagerAddr()` has been removed.

    * [UnergyEvent](contracts/UnergyEvent.sol)

        * `setPermissionGranterAddr()` has been moved to [Common](contracts/Common.sol#L18) contract.

        * `setUnergyBuyerAddr()` has been removed.

        * `setUnergyLogicReserveAddr()` has been removed.

        * `setProjectsManagerAddr()` has been removed.

        * `toggleAllowAllTransfer()` [event added](contracts/UnergyEvent.sol#L107)

        * `addToWhiteList()` [event added](contracts/UnergyEvent.sol#L112)

        * `removeFromWhiteList()` [event added](contracts/UnergyEvent.sol#L117)

    * [UnergyLogicReserve](contracts/UnergyLogicReserve.sol)

        * `setUnergyBuyerAddr()` has been removed.
        
        * `setCleanEnergyAssetsAddr()` has been removed.

        * `setProjectsManagerAddr()` has been removed.
    
    * [UnergyData](contracts/UnergyData.sol)

        The configuration of all the public addresses of the contracts has been centralized in the [UnergyData](contracts/UnergyData.sol) contract.

        * `setProjectsManagerAddr()` [event added](contracts/UnergyData.sol#L199)

        * `setUnergyBuyerAddr()` [event added](contracts/UnergyData.sol#L215)

        * `setUnergyLogicReserveAddr()` [event added](contracts/UnergyData.sol#L231)

        * `setUnergyEventAddr()` [event added](contracts/UnergyData.sol#L247)

        * `setUWattAddr()` [event added](contracts/UnergyData.sol#L259)

        * `setCleanEnergyAssetsAddr()` [event added](contracts/UnergyData.sol#L275)


* ***[CON-17 | Unused Definitions: All unused definitions are removed](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-17)***

    All unused definitions have been removed or is now being used.

    * [ProjectsManager](contracts/ProjectsManager.sol)

        * Error `holdersNotFound` has been removed.

        * Error `invalidSignature` has been removed.

        * Error `invalidBalance` has been removed.

    * [Types](contracts/Types.sol)

        * Structure `UWattStatus` has been removed.

    * [UnergyLogicReserve](contracts/UnergyLogicReserve.sol)

        * Event `ProjectCreated` has been removed.

        * Error `ProjectDepreciationIsBiggerThanProjectValue` has been removed.

        * Error `ProjectNotInProduction` [is now being used](contracts/UnergyLogicReserve.sol#L896).


*  ***[CON-18 | Typos](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-18)***

    All typos found have been corrected.

    * [ERC1155CleanEnergyAssets](contracts/ERC1155CleanEnergyAssets.sol)

        * [transferred](contracts/ERC1155CleanEnergyAssets.sol#L194) typo has been corrected.
    
    * [Types](contracts/Types.sol)

        * `possibility` typo has been removed.
    
    * [UnergyBuyer](contracts/UnergyBuyer.sol)

        * [_stableCoinAddr](contracts/UnergyBuyer.sol#L206) typo has been removed.
    
    * [UnergyData](contracts/UnergyData.sol)

        * `uWattsStatusSnapshot` typo has been removed.

    * [UnergyEvent](contracts/UnergyEvent.sol)

        * [necessary](contracts/UnergyEvent.sol#L90) typo has been corrected.
    
    * [UnergyLogicReserve](contracts/UnergyLogicReserve.sol)

        * [maintainerAddress](contracts/UnergyLogicReserve.sol#L233) typo has been corrected.

        * [transferred](contracts/UnergyLogicReserve.sol#L662) typo has been corrected.

        * `according` typo has been removed.

        * `underlying` typo has been removed.


* ***[CON-19 | Missing Initialization of Upgradeable Contracts](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-19)***

    Initialization of proxy upgradeable contracts has been added.

    * [UnergyData](contracts/UnergyData.sol#L57)

    * [UnergyBuyer](contracts/UnergyBuyer.sol#L61)

    * [UnergyLogicReserve](contracts/UnergyLogicReserve.sol#L114)

    * [ProjectsManager](contracts/ProjectsManager.sol#L83)


* ***[CON-20 | Lack of `whenNotPaused` Modifier](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=CON-20)***

    `whenNotPaused` modifier added to the following functions.

    * `whenNotPaused` modifier added to [`payUWattReward()`](contracts/UnergyBuyer.sol#L367) function on [UnergyBuyer](contracts/UnergyBuyer.sol) contract.

    * `whenNotPaused` modifier added to [`setProjectState()`](contracts/UnergyBuyer.sol#L332) function on [UnergyBuyer](contracts/UnergyBuyer.sol) contract.

    * `whenNotPaused` modifier added to [`_beforeTokenTransfer()`](contracts/ERC1155CleanEnergyAssets.sol#L215) function on [ERC1155CleanEnergyAssets](contracts/ERC1155CleanEnergyAssets.sol) contract.


### [**Global Issues**](https://skyharbor.certik.com/report/60050d87-a868-4031-ba09-8c030c536972?findingIndex=GLOBAL-01)

* What happens if a project is funded but fails to go into production.

    The [`refund()`](contracts/UnergyBuyer.sol#L385) functionality has been added and can be called by a user when the project has the `INREFUND` status and also the user has pWatts of the cancelled project.


## **Pre-report V2**


### **ERC1155CleanEnergyAssets.sol (ERE)**


* ***[ERE-01 | State Variable Should Be Declared Constant](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=ERE-01)***

    The `constant` attribute has been added to state variables that never change.

    * [`recDecimals`](contracts/ERC1155CleanEnergyAssets.sol#L19)

    * [`energyDecimals`](contracts/ERC1155CleanEnergyAssets.sol#L20)


### **ERC20Project.sol (ECP)**


* ***[ECP-01 | Potential Overflow](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=ECP-01)***

    The [`unchecked`](contracts/ERC20Project.sol#L157) block has been removed.


### **ERC20UWatt.sol (ERW)**


* ***[ERW-01 | Missing `afterTransferReceipt()` Call In `_afterTokenTransfer()`](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=ERW-01)***

    The `afterTransferReceipt()` method in the uWatt token was originally used to manage uWatt rewards distribution when the holder was a contract. However, since we are now handling 'claim' logic in a separate system, this call has been removed as it is no longer necessary, similarly, all uWatt interactions have been removed from the [`UnergyEvent.sol`](contracts/UnergyEvent.sol#L56) contract.
    

### **PermissionGranter.sol (PGA)**


* ***[PGA-01 | Unnecessary Storage Read Access in For Loop](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PGA-01)***

    All loops were removed from the `PermissionGranter.sol` contract. These loops were used for counting signatures in the multi-signature. The multi-signature has also been removed from the [`PermissionGranter.sol`](contracts/PermissionGranter.sol) contract and is now a part of an external contract created with Gnosis-Safe{Wallet}.


* ***[PGA-02 | Insufficient `_required` Check in Multi-Signature Permission Mechanism](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PGA-02)***

    The `PermissionGranter.sol` contract [`constructor`](contracts/PermissionGranter.sol#L41) function no longer includes a check for multi-signature signers. The multi-signature has been removed from the `PermissionGranter.sol` contract and is now a part of an external contract created with Gnosis Safe{Wallet}.


* ***[PGA-03 | Lack of Differentiation Between Multisig Approvals](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PGA-03)***

    The [`setPermission()`](contracts/PermissionGranter.sol#L60) function of the `PermissionGranter.sol` contract no longer has the integrated multi-signature system. The multi-signature has been removed from the `PermissionGranter.sol` and is now a part of an external contract created with Gnosis Safe{Wallet}.


* ***[PGA-04 | All Multisig Checks Can Be Bypassed](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PGA-04)***

    The `setProjectsManagerPermissions()` function has been removed from the `PermissionGranter.sol`contract.


* ***[PGA-05 | Lack of Access Control of `getAndUpdatePermission()`](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PGA-05)***

    A new role, [`PROTOCOL_CONTRACT_ROLE`](contracts/PermissionGranter.sol#L20), has been added to the `PermissionGranter.sol`. This role is granted to all contracts for obtaining and updating permissions.

    Now, the [`getAndUpdatePermissions()`](contracts/PermissionGranter.sol#L165) function has a single access modifier for the `PROTOCOL_CONTRACT_ROLE`.


* ***[PGA-06 | Deployer May Not Be Owner](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PGA-06)***

    The owners array has been removed from the [`PermissionGranter.sol`](contracts/PermissionGranter.sol) contract, and the integrated multi-signature system is no longer present within it. The multi-signature functionality has been relocated to an external contract created with Gnosis Safe{Wallet}.


* ***[PGA-07 | Possible To Not Remove a Signer When Replacing Signers](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PGA-07)***

    The owners array and the `replaceSigner()`function has been removed from the [`PermissionGranter.sol`](contracts/PermissionGranter.sol) contract and the integrated multi-signature system is no longer present within it. The multi-signature functionality has been relocated to an external contract created with Gnosis Safe{Wallet}.



### **ProjectsManager.sol (PMA)**


* ***[PMA-01 | Unused State Variable](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PMA-01)***

    The `signatures` mapping has been removed from the [`ProjectsManager.sol`](contracts/ProjectsManager.sol) contract.


* ***[PMA-02 | Redundant Code](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PMA-02)***

    The `getProject()` function call has been removed, and the [`ifProjectExist`](contracts/ProjectsManager.sol#L279) modifier has been included.


* ***[PMA-03 | Last Milestone Not Accumulated in `_checkMilestoneWeights()` Function](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PMA-03)***

    All milestones have been included into the weight check on [`_checkMilestoneWeights()`](contracts/ProjectsManager.sol#L401) function.


* ***[PMA-04 | Duplicate `approveSwap()` Call](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PMA-04)***

    The duplicate call to the `approveSwap()` on the [`configureProject()`](contracts/ProjectsManager.sol#L178) function has been removed.


* ***[PMA-05 | Possible To Configure Project Several Times](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PMA-05)***

    A [`projectConfigured`](contracts/ProjectsManager.sol#L44) flag has been added to [`configureProject()`](contracts/ProjectsManager.sol#185) function in order to determine whether a project has been configured.



* ***[PMA-06 | Possible Mismatch When Configuring a Project](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=PMA-06)***

    The [`configureProject()`](contracts/ProjectsManager.sol#L178) function now reads the project structure from the `getProject()` function, rather than from the input parameters of the function.


### **UnergyBuyer.sol (UBA)**


* ***[UBA-01 | Potential Overpayment During `_refund()`](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=UBA-01)***

    In the `ProjectsManager.sol`, contract, a [`initialTotalSupply`](contracts/ProjectsManager.sol#L45) mapping has been added to store the initial totalSupply of a project upon its creation.

    Previously, the `percentageToClaim` was calculated in relation to the `project.pWattsSupply`, representing an error. Now, the [`percentageToClaim`](contracts/UnergyBuyer.sol#L457) is calculated based on the initial totalSupply, with deductions for the administrator's pWatts and the operator's fee. This approach ensures that the `refundAmount` cannot exceed the user's initial expenditure.


* ***[UBA-02 | No Upper Limits for `_maintenancePercentage`](https://skyharbor.certik.com/report/f2669f1f-c21d-4224-be9d-28c8c2cef049?findingIndex=UBA-02)***

    The `setMaintenancePercentage()` function has been removed from `UnergyBuyer.sol` contract; now, the [`updateProject()`](contracts/ProjectsManager.sol#L207) function is used to update the `maintenancePercentage` value.

    The `_maintenancePercentage` value on a [`_createProject()`](contracts/ProjectsManager.sol#L151) & [`_updateProject()`](contracts/ProjectsManager.sol#L223) is subjected to an upper limit through the use of the [`_checkMaintenancePercentage()`](contracts/ProjectsManager.sol#L400) function.


## **Pre-report V3**

### **Commons (CON)**


* ***[CON-21 | Inefficient Memory Parameter](https://skyharbor.certik.com/report/878bf551-9374-4753-ae1f-0a6edc31cb3a?findingIndex=CON-21)***

    Memory access has been replaced with calldata read to optimize gas usage.

    * `ERC1155CleanEnergyAssets.sol` contract on [`_tokenName`](contracts/ERC1155CleanEnergyAssets.sol#L55) input parameter of  the `createProjectEnergyAsset()` function.
    * `ERC1155CleanEnergyAssets.sol` contract on [`_newURI`](contracts/ERC1155CleanEnergyAssets.sol#L145) input parameter of  the `setURI()` function.
    * `PermissionGranter.sol` contract on [`_fname`](contracts/PermissionGranter.sol#L71) input parameter of  the `setPermission()` function.
    * `PermissionGranter.sol` contract on [`_fname`](contracts/PermissionGranter.sol#L155) input parameter of  the `getPermission()` function.


### **PermissionGranter.sol (PGL)**


* ***[PGL-01 | Missing Zero Address Validation](https://skyharbor.certik.com/report/878bf551-9374-4753-ae1f-0a6edc31cb3a?findingIndex=PGL-01)***

    Zero address validation has been added to:

    * `ERC20Project.sol`contract [constructor function](contracts/ERC20Project.sol#L38).
    * `PermissionGranter.sol` contract [initialize function](contracts/PermissionGranter.sol#L53).
    * `ProjectsManager.sol` contract [initialize function](contracts/ProjectsManager.sol#L91).
    * `UnergyBuyer.sol` contract [initialize function](contracts/UnergyBuyer.sol#L71).
    * `UnergyData.sol` contract [initialize function](contracts/UnergyData.sol#L70).
    * `UnergyLogicReserve.sol` contract [initialize function](contracts/UnergyLogicReserve.sol#L132).


### **ProjectsManager.sol (PML)**


* ***[PML-01 | Insufficient Token Allowance](https://skyharbor.certik.com/report/878bf551-9374-4753-ae1f-0a6edc31cb3a?findingIndex=PML-01)***

   The `mint`/`approve` pattern has been replaced by [two mints](contracts/ProjectsManager.sol#L216), and the required configuration for the old pattern has been removed.


### **UnergyLogicReserve.sol (UNR)**


* ***[UNR-01 | Discussion on Purpose of `exchangeUWattPerPWatt()`](https://skyharbor.certik.com/report/878bf551-9374-4753-ae1f-0a6edc31cb3a?findingIndex=UNR-01)***
    
    `exchangeUWattPerPWatt()` method has been removed from [`UnergyLogicReserve.sol`](contracts/UnergyLogicReserve.sol) contract.


## **Pre-report V4**

### **UnergyData.sol (UDT)**


* ***[UDT-01 | No Upper Limits For Fees](https://skyharbor.certik.com/report/aabb28de-03d6-43d1-88a3-9f7490873700?findingIndex=UDT-01)***

    Limits have been established for the [`assetManagerFeePercentage`](contracts/UnergyData.sol#L229) and [`swapFeePercentage`](contracts/UnergyData.sol#L250) variables. Additionally, the [`maintenanceFeePercentage`](contracts/ProjectsManager.sol#427) limit has been modified to ensure that, when adding the `assetManagerFeePercentage`, it can never exceed 100%.


## **Pre-report V5**


* ***[UBI-01 | Refund Restriction for Project Originator](https://skyharbor.certik.com/report/c3223dc0-aa0e-4bbd-a4e3-3bc317b786fc?findingIndex=UBI-01)***

    The error `OperatorCannotMakeRefund` has been changed to [`OriginatorCannotMakeRefund`](contracts/UnergyBuyer.sol#L515).

    [The originator's purchase of pWatts](contracts/UnergyLogicReserve.sol#L616) has been restricted.


* ***[UBI-01 | Possible Underflow for installerPayment](https://skyharbor.certik.com/report/c3223dc0-aa0e-4bbd-a4e3-3bc317b786fc?findingIndex=UBI-01)***

    [Validation was added to `offchainInstallerPayment`](contracts/UnergyBuyer.sol#L316) to prevent underflow.


* ***[UBI-01 | Off-chain Milestone PaymentReport Validation](https://skyharbor.certik.com/report/c3223dc0-aa0e-4bbd-a4e3-3bc317b786fc?findingIndex=UBI-01)***
   
   Off-chain payment amount [validation] (contracts/UnergyBuyer.sol#L191) has been added to `offChainMilestonePaymentReport`, enabling the originator user to sign the milestone and make payments to the installer (`_installerPayment`) without restriction on the amount of off-chain payments. This is achieved by validating that the off-chain payment never exceeds the total milestone payment.