import { BigNumberish } from "@ethersproject/bignumber/src.ts/bignumber";
import { z } from "zod";

export interface ProjectConfig {
  info: InfoInput;
  token: Token;
}

export interface Info {
  maintenancePercentage: bigint | number | BigNumberish;
  initialProjectValue: bigint | number | BigNumberish;
  currentProjectValue: bigint | number | BigNumberish;
  swapFactor: bigint | number | BigNumberish;
  totalPWatts: bigint | number | BigNumberish;
  adminAddress: string;
  installerAddress: string;
  originatorAddress: string;
  originatorFee: bigint | number | BigNumberish;
  stableAddr: string;
  assetManagerAddress: string;
  assetManagerFeePercentage: bigint | number | BigNumberish;
  meterAddrs: string[];
  milestones: Milestone[];
}

export interface InfoInput {
  maintenancePercentage: string;
  initialProjectValue: string;
  currentProjectValue: string;
  swapFactor: string;
  totalPWatts: string;
  adminAddress: string;
  installerAddress: string;
  originatorAddress: string;
  originatorFee: string;
  stableAddr: string;
  meterAddrs: string[];
  assetManagerAddress: string;
  assetManagerFeePercentage: string;
  milestones: Milestone[];
}

export interface Milestone {
  name: string;
  weight: number;
}

export interface Token {
  name: string;
  symbol: string;
}

export const projectConfigSchema = z.array(
  z.object({
    info: z.object({
      maintenancePercentage: z.string(),
      initialProjectValue: z.string(),
      currentProjectValue: z.string(),
      swapFactor: z.string(),
      totalPWatts: z.string(),
      adminAddress: z.string(),
      installerAddress: z.string(),
      originatorAddress: z.string(),
      originatorFee: z.string(),
      stableAddr: z.string(),
      assetManagerAddress: z.string(),
      assetManagerFeePercentage: z.string(),
      meterAddrs: z.array(z.string()),
      milestones: z.array(
        z.object({
          name: z.string(),
          weight: z.number(),
        })
      ),
    }),
    token: z.object({
      name: z.string(),
      symbol: z.string(),
    }),
  })
);
