import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { ethers, network } from "hardhat";
import { expect } from "chai";

import { deployFixture, createProjectFixture } from "./fixtures";
import { parseUnits } from "ethers/lib/utils";
import {
  HARDHAT_DEFAULT_PRIVATE_KEY,
  externalDependenciesEnabled,
} from "../utils/constants";
import { getTimestampInSeconds } from "../utils/helpers";

describe("ERC20 uWatt Contract", () => {
  it("Should set the admin as owner of ERC20 uWatt contract", async function () {
    const { uWattToken, admin } = await loadFixture(deployFixture);

    expect(await uWattToken.owner()).to.deep.equal(admin.address);
  });

  it("Should set the rigth decimals", async function () {
    const { uWattToken } = await loadFixture(deployFixture);

    const decimals = 18; //All protocol ERC20's have 2 decimals

    expect(await uWattToken.decimals()).to.deep.equal(decimals);
  });

  describe("Transfer", () => {
    it("Should transfer using transferFrom function", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const { user1, user2, user3, meter1, projectHelper, uWattToken } =
        await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      await projectHelper.bringToProduction(
        project1,
        { users, amounts: [] },
        meter1
      );

      const amountToTransfer = 100;

      await uWattToken.connect(user1).approve(user2.address, amountToTransfer);

      await uWattToken
        .connect(user2)
        .transferFrom(user1.address, user3.address, amountToTransfer);

      const user3Balance = await uWattToken.balanceOf(user3.address);

      expect(user3Balance.toString()).to.deep.equal(
        amountToTransfer.toString()
      );
    });

    it("Should revert if user try to send pWatts to himself using transfer function", async function () {
      const { user1, user2, meter1, projectHelper, uWattToken } =
        await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      await projectHelper.bringToProduction(
        project1,
        { users, amounts: [] },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      await expect(
        uWattToken.connect(user1).transfer(user1.address, 100)
      ).to.be.revertedWithCustomError(uWattToken, "TransferToHimself");
    });

    it("Should revert if user try to send pWatts to himself using transferFrom function", async function () {
      const { user1, user2, meter1, projectHelper, uWattToken } =
        await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      await projectHelper.bringToProduction(
        project1,
        { users, amounts: [] },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      await uWattToken.connect(user1).approve(user1.address, 100);

      await expect(
        uWattToken
          .connect(user1)
          .transferFrom(user1.address, user1.address, 100)
      ).to.be.revertedWithCustomError(uWattToken, "TransferToHimself");
    });
  });

  describe("Burn", () => {
    it("Should run burnFrom function", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        unergyBuyer,
        user1,
        user2,
        meter1,
        admin,
        projectHelper,
        uWattToken,
      } = await loadFixture(deployFixture);

      const { project1, project2 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      await projectHelper.bringToProduction(
        project1,
        { users, amounts: [] },
        meter1
      );

      users.push(unergyBuyer.address);

      const project2Decimals = await project2.decimals();

      const pWattThatWillBeBoughtByTheBuyer = parseUnits(
        "36000",
        project2Decimals
      ); // 36.000,00 pWatts

      await projectHelper.bringToProduction(
        project2,
        {
          users,
          amounts: [
            parseUnits("18450", project2Decimals).toBigInt(),
            parseUnits("18450", project2Decimals).toBigInt(),
            pWattThatWillBeBoughtByTheBuyer.toBigInt(),
          ],
        },
        meter1
      );

      const user1UWattBalanceT1 = await uWattToken.balanceOf(user1.address);
      const valueToBurn = 1000000;

      //user1 approves admin to spend (burn) their tokens
      await uWattToken.connect(user1).approve(admin.address, 1000000);

      //brunFrom
      await uWattToken.burnFrom(user1.address, valueToBurn);
      //user1 balance after burn
      const user1UWattBalanceT2 = await uWattToken.balanceOf(user1.address);
      //supossed user 1 balance after burn
      const supposedUser1Balance = user1UWattBalanceT1.sub(valueToBurn);

      expect(user1UWattBalanceT2).to.equal(supposedUser1Balance);
    });
  });

  describe("Transfer Ownership", function () {
    it("Should transfer the ownership of UnergyBuyer contract", async function () {
      const { uWattToken, notAdmin } = await loadFixture(deployFixture);

      await uWattToken.transferOwnership(notAdmin.address);

      await uWattToken.connect(notAdmin).acceptOwnership();

      expect(await uWattToken.owner()).to.deep.equal(notAdmin.address);
    });
  });

  describe("Non Authorized Callers", function () {
    it("Should revert if the caller is not authorized", async function () {
      const { uWattToken, permissionGranter, notAdmin, user1 } =
        await loadFixture(deployFixture);

      expect(
        uWattToken.connect(notAdmin).mint(user1.address, 100)
      ).to.be.revertedWith("NotAuthorizedToCallFunction");

      expect(uWattToken.connect(notAdmin).burn(100)).to.be.revertedWith(
        "NotAuthorizedToCallFunction"
      );

      expect(
        uWattToken.connect(notAdmin).burnFrom(user1.address, 100)
      ).to.be.revertedWith("NotAuthorizedToCallFunction");

      expect(
        uWattToken
          .connect(notAdmin)
          .setPermissionGranterAddr(permissionGranter.address)
      ).to.be.revertedWith("NotAuthorizedToCallFunction");

      expect(
        uWattToken.connect(notAdmin).transferOwnership(user1.address)
      ).to.be.revertedWith("NotAuthorizedToCallFunction");
    });
  });

  describe("Contract Addresses setup", function () {
    it("Should revert if an admin try to set address 0 as a contract address", async () => {
      const { admin, uWattToken, multiSignPermission } = await loadFixture(
        deployFixture
      );

      await multiSignPermission.setPermission(
        admin.address,
        uWattToken.address,
        "setPermissionGranterAddr"
      );

      expect(
        uWattToken.setPermissionGranterAddr(ethers.constants.AddressZero)
      ).to.be.revertedWith("ZeroAddressNotAllowed");

      expect(
        uWattToken.transferOwnership(ethers.constants.AddressZero)
      ).to.be.revertedWith("ZeroAddressNotAllowed");
    });
  });

  describe("Votes extension", async () => {
    it("Should delegate votes to itself", async () => {
      const { uWattToken, admin, multiSignPermission } = await loadFixture(
        deployFixture
      );

      await multiSignPermission.setPermission(
        admin.address,
        uWattToken.address,
        "mint"
      );

      const AMOUNT_TO_MINT = BigInt(1000 * 10 ** 18);

      await uWattToken.mint(admin.address, AMOUNT_TO_MINT);
      await uWattToken.connect(admin).delegate(admin.address);

      const adminVotes = await uWattToken.getVotes(admin.address);

      expect(adminVotes).to.equal(AMOUNT_TO_MINT);
    });

    it("Should delegate votes to another account", async () => {
      const { uWattToken, admin, user1, multiSignPermission } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        uWattToken.address,
        "mint"
      );

      const AMOUNT_TO_MINT = BigInt(1000 * 10 ** 18);

      await uWattToken.mint(admin.address, AMOUNT_TO_MINT);
      await uWattToken.connect(admin).delegate(user1.address);

      const user1Votes = await uWattToken.getVotes(user1.address);
      const adminVotes = await uWattToken.getVotes(admin.address);

      expect(user1Votes).to.equal(AMOUNT_TO_MINT);
      expect(adminVotes).to.equal(BigInt(0));
    });

    it("Should delegate by sign", async function () {
      const { uWattToken, admin, user1, multiSignPermission, projectHelper } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        uWattToken.address,
        "mint"
      );

      const AMOUNT_TO_MINT = BigInt(1000 * 10 ** 18);

      await uWattToken.mint(admin.address, AMOUNT_TO_MINT);

      const nonce = await uWattToken.nonces(admin.address);

      const domain = {
        name: await uWattToken.name(),
        version: "1",
        chainId: network.config.chainId,
        verifyingContract: uWattToken.address,
      };

      const types = {
        Delegation: [
          {
            name: "delegatee",
            type: "address",
          },
          {
            name: "nonce",
            type: "uint256",
          },
          {
            name: "expiry",
            type: "uint256",
          },
        ],
      };

      const EXPIRY = getTimestampInSeconds() + 4200;

      const values = {
        delegatee: user1.address,
        nonce,
        expiry: EXPIRY,
      };

      const signature = await admin._signTypedData(domain, types, values);

      const sig = ethers.utils.splitSignature(signature);

      // const recovered = ethers.utils.verifyTypedData(
      //   domain,
      //   types,
      //   values,
      //   sig
      // );
      // console.log({ recovered });

      await uWattToken.delegateBySig(
        user1.address,
        nonce,
        EXPIRY,
        sig.v,
        sig.r,
        sig.s
      );

      const user1Votes = await uWattToken.getVotes(user1.address);

      expect(user1Votes).to.equal(AMOUNT_TO_MINT);
    });

    it("Should permit to another account", async () => {
      const { uWattToken, admin, user1, multiSignPermission, projectHelper } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        uWattToken.address,
        "mint"
      );

      const AMOUNT_TO_MINT = ethers.utils.parseEther("100");

      await uWattToken.mint(admin.address, AMOUNT_TO_MINT);

      const nonce = await uWattToken.nonces(admin.address);

      const domain = {
        name: await uWattToken.name(),
        version: "1",
        chainId: network.config.chainId,
        verifyingContract: uWattToken.address,
      };

      const types = {
        Permit: [
          {
            name: "owner",
            type: "address",
          },
          {
            name: "spender",
            type: "address",
          },
          {
            name: "value",
            type: "uint256",
          },
          {
            name: "nonce",
            type: "uint256",
          },
          {
            name: "deadline",
            type: "uint256",
          },
        ],
      };

      const DEADLINE = getTimestampInSeconds() + 4200;

      const values = {
        owner: admin.address,
        spender: user1.address,
        value: AMOUNT_TO_MINT,
        nonce,
        deadline: DEADLINE,
      };

      const signature = await admin._signTypedData(domain, types, values);

      const sig = ethers.utils.splitSignature(signature);

      // const recovered = ethers.utils.verifyTypedData(
      //   domain,
      //   types,
      //   values,
      //   sig
      // );
      // console.log({ recovered });

      await uWattToken.permit(
        admin.address,
        user1.address,
        AMOUNT_TO_MINT,
        DEADLINE,
        sig.v,
        sig.r,
        sig.s
      );

      const user1Allowance = await uWattToken.allowance(
        admin.address,
        user1.address
      );

      expect(user1Allowance).to.equal(AMOUNT_TO_MINT);
    });
  });
});
