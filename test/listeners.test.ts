import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { deployFixture } from "./fixtures";
import { input, select } from "@inquirer/prompts";
import { externalDependenciesEnabled } from "../utils/constants";

describe("Listeners", () => {
  it("Should be able to listen to events", async function () {
    if (!externalDependenciesEnabled) this.skip();

    const { admin, uWattToken, multiSignPermission } = await loadFixture(
      deployFixture
    );

    await multiSignPermission.setPermission(
      admin.address,
      uWattToken.address,
      "mint"
    );

    let eventsSend = 0;

    async function sendBulkEvents(): Promise<any> {
      const eventsAmount = parseInt(
        await input({
          message: "How many events do you want to send?",
        })
      );

      for (let i = 0; i < eventsAmount; i++) {
        await uWattToken.mint(admin.address, 1);
        eventsSend++;
      }

      const repeat = await select({
        message: "Do you want to send more events?",
        choices: [
          {
            description: "Yes",
            value: true,
          },
          {
            description: "No",
            value: false,
          },
        ],
      });

      if (repeat) return await sendBulkEvents();
    }

    await sendBulkEvents();

    console.log("Events send: ", eventsSend);
  });
});
