import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { deployFixture, createProjectFixture } from "./fixtures";
import { expect } from "chai";

describe("Load Test", () => {
  describe("Swap", () => {
    it("Swap for random users", async () => {
      const { unergyLogicReserve, projectsManager, projectHelper, loadUsers } =
        await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const users = await loadUsers.randomUsersNumber(1, 100);

      await projectHelper.setInstallerSign(project1, {
        users,
        amounts: [],
      });

      const project1Users = await projectsManager.getProjectHolders(
        project1.address
      );

      const swapTx = await unergyLogicReserve.swapToken(
        project1.address,
        project1Users.length
      );

      const receipt = await swapTx.wait();

      // Get the actual gas cost from the receipt
      const actualGasCost = receipt.gasUsed.toNumber();

      expect(actualGasCost).to.be.below(30000000);
    });

    it("Should swap users by parts", async () => {
      const {
        unergyLogicReserve,
        unergyData,
        projectHelper,
        loadUsers,
        projectsManager,
        uWattToken,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const users = await loadUsers.randomUsersNumber(100, 100);

      await projectHelper.setInstallerSign(project1, {
        users,
        amounts: [],
      });

      const project1Struct = await projectsManager.getProject(project1.address);

      const project1Users = await projectsManager.getProjectHolders(
        project1.address
      );

      const projectAdminPWattBalance = await project1.balanceOf(
        project1Struct.adminAddr
      );

      //Swap
      const usersPerSwap = 10;
      const usersModule = project1Users.length % usersPerSwap;
      const usersDiv = Math.floor(project1Users.length / usersPerSwap);

      let iterationLimit;
      if (usersModule > 0) {
        iterationLimit = usersDiv + 1;
      } else {
        iterationLimit = usersDiv;
      }

      for (let i = 0; i < iterationLimit; i++) {
        const swapTx = await unergyLogicReserve.swapToken(
          project1.address,
          usersPerSwap
        );
        const receipt = await swapTx.wait();

        // Get the actual gas cost from the receipt
        const actualGasCost = receipt.gasUsed.toNumber();

        expect(actualGasCost).to.be.below(30000000);
      }

      const rawExpectedBalance = project1Struct.pWattsSupply
        .sub(project1Struct.originatorFee)
        .div(BigInt(users.length))
        .mul(project1Struct.swapFactor)
        .div(BigInt(10 ** 18));

      const swapFeePercentage = await unergyData.swapFeePercentage();
      const divFactor = BigInt(100 * 10 ** 18 * 10 ** 80);
      const swapFee1 = BigInt(
        BigInt(rawExpectedBalance.toString()) * BigInt(10 ** 80)
      );
      const swapFee2 =
        BigInt(swapFee1.toString()) * BigInt(swapFeePercentage.toString());
      const swapFee = swapFee2 / divFactor;
      const expectedBalance = rawExpectedBalance.sub(swapFee).toBigInt();

      for (let i = 0; i < project1Users.length - 1; i++) {
        const userBalance = await uWattToken.balanceOf(project1Users[i]);
        if (
          project1Users[i] == project1Struct.adminAddr ||
          project1Users[i] == project1Struct.originator
        ) {
          continue;
        }
        expect(userBalance.toBigInt()).to.be.closeTo(expectedBalance, 500000);
      }
    });
  });
});
