import { expect } from "chai";
import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";

import { parseUnits } from "../utils";
import { deployFixture } from "./fixtures";

describe("UniswapV3PoolAdapter", () => {
  it("Should return the correct amount of uWatts in", async () => {
    const { uniswapV3PoolAdapter } = await loadFixture(deployFixture);

    const uWattPrice = await uniswapV3PoolAdapter.getUSDTPriceInUWatt();
    const requiredUWatt = await uniswapV3PoolAdapter.getUWattsIn(
      parseUnits(1000, 6)
    );

    const expectedRequiredUWatt = parseUnits(1000, 6).mul(uWattPrice).div(1e6);

    expect(requiredUWatt).to.equal(expectedRequiredUWatt);
  });
});
