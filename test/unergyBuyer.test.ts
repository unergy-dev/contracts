import { ZERO_ADDRESS } from "./../utils/constants";
import { ProjectStruct } from "./../typechain-types/contracts/ProjectsManager";
import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { BigNumber } from "ethers";
import { ethers } from "hardhat";
import { expect } from "chai";

import { deployFixture, createProjectFixture } from "./fixtures";
import {
  calcMaxPWattThatCanBuyTheUnergyBuyer,
  parseUnits,
  toBn,
} from "../utils";
import { externalDependenciesEnabled } from "../utils/constants";
import { PermissionType } from "../types/contracts.types";

describe("UnergyBuyer Contract", () => {
  it("Should set the admin as owner of UnergyBuyer contract", async function () {
    const { unergyBuyer, admin } = await loadFixture(deployFixture);

    expect(await unergyBuyer.owner()).to.deep.equal(admin.address);
  });

  it("Should set all contracts addresses on UnergyBuyer contract", async function () {
    const { unergyBuyer, unergyData, permissionGranter } = await loadFixture(
      deployFixture
    );

    const unergyDataInstance = await unergyBuyer.unergyData();
    const permissionGranterInstance = await unergyBuyer.permissionGranter();

    expect([unergyDataInstance, permissionGranterInstance]).to.deep.equal([
      unergyData.address,
      permissionGranter.address,
    ]);
  });

  it("Should revert if index is grater than milestones length", async function () {
    const { unergyBuyer, stableCoin, projectsManager } = await loadFixture(
      deployFixture
    );

    const { project1 } = await loadFixture(createProjectFixture);

    const milestones = await projectsManager.getProjectMilestones(
      project1.address
    );

    await expect(
      unergyBuyer.setOriginatorSign(project1.address, milestones.length + 1)
    ).to.be.revertedWithCustomError(unergyBuyer, "InvalidIndex");
  });

  describe("Milestones", function () {
    it("Should create milestones for all projects from UnergyBuyer contract", async function () {
      const { projectsManager } = await loadFixture(deployFixture);

      const {
        project1,
        project2,
        project3,
        project4,
        project5,
        project6,
        milestones,
      } = await loadFixture(createProjectFixture);

      const projectsAddresses = [
        project1.address,
        project2.address,
        project3.address,
        project4.address,
        project5.address,
        project6.address,
      ];

      for (let i = 0; i < projectsAddresses.length; i++) {
        let projectMilestones = await projectsManager.getProjectMilestones(
          projectsAddresses[i]
        );

        for (let j = 0; j < milestones.length; j++) {
          expect([
            projectMilestones[j].name,
            projectMilestones[j].weight,
          ]).to.deep.equal([milestones[j].name, milestones[j].weight]);
        }
      }
    });

    it("Should change all milestone's names of all projects from UnergyBuyer contract", async function () {
      const { projectsManager } = await loadFixture(deployFixture);

      const {
        project1,
        project2,
        project3,
        project4,
        project5,
        project6,
        milestones,
      } = await loadFixture(createProjectFixture);

      const projectsAddresses = [
        project1.address,
        project2.address,
        project3.address,
        project4.address,
        project5.address,
        project6.address,
      ];

      const newMilestonesNames = ["m1", "m2", "m3", "m4", "m5", "m6"];

      //change milestone names for all projects
      for (let i = 0; i < projectsAddresses.length; i++) {
        for (let j = 0; j < milestones.length; j++) {
          const milestone: any = milestones[j];
          milestone.name = newMilestonesNames[j];
          await projectsManager.updateMilestoneAtIndex(
            projectsAddresses[i],
            j,
            milestone
          );
        }
      }

      //read all projects milestones names
      for (let i = 0; i < projectsAddresses.length; i++) {
        let projectMilestones = await projectsManager.getProjectMilestones(
          projectsAddresses[i]
        );
        for (let j = 0; j < milestones.length; j++) {
          expect([projectMilestones[j].name]).to.deep.equal([
            newMilestonesNames[j],
          ]);
        }
      }
    });
  });

  describe("Installer/Unergy Signatures and Installer Payments", function () {
    it("Should set installer sign on all milestones of all projects from UnergyBuyer contract", async function () {
      const { projectsManager, unergyBuyer } = await loadFixture(deployFixture);

      const {
        project1,
        project2,
        project3,
        project4,
        project5,
        project6,
        milestones,
      } = await loadFixture(createProjectFixture);

      const projectsAddresses = [
        project1.address,
        project2.address,
        project3.address,
        project4.address,
        project5.address,
        project6.address,
      ];

      //change milestone state for all projects (Installer signature)
      for (let i = 0; i < projectsAddresses.length; i++) {
        for (let j = 0; j < milestones.length; j++) {
          await unergyBuyer.setInstallerSign(projectsAddresses[i]);
        }
      }

      //read all milestones states
      for (let i = 0; i < projectsAddresses.length; i++) {
        let projectMilestones = await projectsManager.getProjectMilestones(
          projectsAddresses[i]
        );
        for (let j = 0; j < milestones.length; j++) {
          expect(projectMilestones[j].wasSignedByInstaller).to.deep.equal(true);
        }
      }
    });

    it("Should set unergy sign on all milestones of all projects from UnergyBuyer contract", async function () {
      const {
        unergyData,
        projectsManager,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        user1,
        user2,
        user3,
        user4,
        user5,
        user6,
        user7,
      } = await loadFixture(deployFixture);

      const {
        project1,
        project2,
        project3,
        project4,
        project5,
        project6,
        milestones,
      } = await loadFixture(createProjectFixture);

      const projectsAddresses = [
        project1.address,
        project2.address,
        project3.address,
        project4.address,
        project5.address,
        project6.address,
      ];

      const users = [
        user1.address,
        user2.address,
        user3.address,
        user4.address,
        user5.address,
        user6.address,
        user7.address,
      ];

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();
      const pWattPrices = [
        parseUnits(1.1, STABLE_COIN_DECIMALS),
        parseUnits(1.3, STABLE_COIN_DECIMALS),
        parseUnits(1.03, STABLE_COIN_DECIMALS),
        parseUnits(1.02, STABLE_COIN_DECIMALS),
        parseUnits(1.01, STABLE_COIN_DECIMALS),
        parseUnits(1.01, STABLE_COIN_DECIMALS),
      ];

      const PWATT_PRICE = parseUnits(1, STABLE_COIN_DECIMALS);
      const LIFE_TIME = 100_000;

      //pWatts purchase
      for (let i = 0; i < projectsAddresses.length; i++) {
        for (let j = 0; j < users.length; j++) {
          await unergyData.generatePurchaseTicket(
            projectsAddresses[i],
            pWattPrices[i],
            LIFE_TIME,
            users[j]
          );

          const project = await projectsManager.getProject(
            projectsAddresses[i]
          );
          const PWATTS_TO_TRANSFER = (
            await projectsManager.getTotalPWatts(projectsAddresses[i])
          )
            .sub(project.originatorFee)
            .div(toBn(users.length));

          await unergyLogicReserve.pWattsTransfer(
            projectsAddresses[i],
            users[j],
            PWATTS_TO_TRANSFER
          );
        }
      }

      //change milestone state for all projects (Installer signature - Unergy signature)
      for (let i = 0; i < projectsAddresses.length; i++) {
        for (let j = 0; j < milestones.length; j++) {
          await unergyBuyer.setInstallerSign(projectsAddresses[i]);
          await unergyBuyer.setOriginatorSign(projectsAddresses[i], j);
        }
      }

      //read all milestones states
      for (let i = 0; i < projectsAddresses.length; i++) {
        let projectMilestones = await projectsManager.getProjectMilestones(
          projectsAddresses[i]
        );
        for (let j = 0; j < milestones.length; j++) {
          expect([projectMilestones[j].wasSignedByOriginator]).to.deep.equal([
            true,
          ]);
        }
      }
    });

    it("Should revert if the admin project doesn't have sufficient balance for make installer payment", async function () {
      const { unergyData, unergyLogicReserve, unergyBuyer, stableCoin, user1 } =
        await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();
      const PWATT_PRICE = parseUnits(1, STABLE_COIN_DECIMALS);
      const LIFE_TIME = 100_000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyLogicReserve.pWattsTransfer(
        project1.address,
        user1.address,
        100
      );

      //change milestone state for all projects (Installer signature - Unergy signature)
      for (let j = 0; j < milestones.length; j++) {
        await unergyBuyer.setInstallerSign(project1.address);

        let txsetOriginatorSign;
        try {
          txsetOriginatorSign = unergyBuyer.setOriginatorSign(
            project1.address,
            j
          );
        } catch {
          expect(txsetOriginatorSign).to.be.revertedWith(
            "There's no sufficient balance for make payment, the milestone cannot be completed"
          );
        }
      }
    });
  });

  describe("Off-Chain Payments", function () {

    it("Should make off-chain payments and report them and the project state changes", async function () {
      const {
        unergyData,
        projectsManager,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        user1,
        user2,
        installerProject1,
        projectOriginator1,
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();

      const pWattPrice = parseUnits(1.1, STABLE_COIN_DECIMALS);

      const LIFE_TIME = 100_000;

      //pWatts purchase

      for (let j = 0; j < users.length; j++) {
        await unergyData.generatePurchaseTicket(
          project1.address,
          pWattPrice,
          LIFE_TIME,
          users[j]
        );

        const project = await projectsManager.getProject(project1.address);
        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(project1.address)
        )
          .sub(project.originatorFee)
          .div(toBn(users.length));

        await unergyLogicReserve.pWattsTransfer(
          project1.address,
          users[j],
          PWATTS_TO_TRANSFER
        );
      }

      const offChainPaymentOnEachMilestone = parseUnits(1000, 6);

      let accumulatedOffChainPayment = parseUnits(0, 6);

      //change milestone state for all projects (Installer signature - Unergy signature)

      for (let j = 0; j < milestones.length; j++) {
        await unergyBuyer.setInstallerSign(project1.address);
        await unergyBuyer
          .connect(projectOriginator1)
          .offChainMilestonePaymentReport(
            project1.address,
            j,
            offChainPaymentOnEachMilestone
          );
        await unergyBuyer
          .connect(installerProject1)
          .offChainMilestonePaymentReport(
            project1.address,
            j,
            offChainPaymentOnEachMilestone
          );
        await unergyBuyer.setOriginatorSign(project1.address, j);
        accumulatedOffChainPayment = accumulatedOffChainPayment.add(
          offChainPaymentOnEachMilestone
        );
      }  

      const installerStableCoinBalance = await stableCoin.balanceOf(
        installerProject1.address
      );

      const project = await projectsManager.getProject(project1.address);
      const projectInitialValue = project.initialProjectValue;

      const projectState = project.state;

      expect(projectInitialValue).to.deep.equal(
        installerStableCoinBalance.add(accumulatedOffChainPayment)
      );

      expect(projectState).to.deep.equal(1);//INSTALLED
    });

    it("Should report multiple off-chain payments and report them and the project state changes", async function () {
      const {
        unergyData,
        projectsManager,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        user1,
        user2,
        installerProject1,
        projectOriginator1,
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();

      const pWattPrice = parseUnits(0.5, STABLE_COIN_DECIMALS);

      const LIFE_TIME = 100_000;

      //pWatts purchase

      for (let j = 0; j < users.length; j++) {
        await unergyData.generatePurchaseTicket(
          project1.address,
          pWattPrice,
          LIFE_TIME,
          users[j]
        );

        const project = await projectsManager.getProject(project1.address);
        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(project1.address)
        )
          .sub(project.originatorFee)
          .div(toBn(users.length));

        await unergyLogicReserve.pWattsTransfer(
          project1.address,
          users[j],
          PWATTS_TO_TRANSFER
        );
      }

      //reserve funds for pwatts purchase

      const reserveBalance = await stableCoin.balanceOf(unergyBuyer.address);

      const offChainPaymentsPerMilestone = 2; //number of off-chain payments per milestone 

      const offChainMilestonePayment = 11998;// Minimum milestone payment

      //change milestone state for all projects (Installer signature - Unergy signature)

      for (let j = 0; j < milestones.length; j++) {
        await unergyBuyer.setInstallerSign(project1.address);

        for (let k = 0; k < offChainPaymentsPerMilestone; k++) {

          const offChainPaymentOnEachMilestone = parseUnits(
            Math.floor(
              offChainMilestonePayment/offChainPaymentsPerMilestone
            ),
            6 //decimals
          );
          //originator off-chain milestone report
          await unergyBuyer
          .connect(projectOriginator1)
          .offChainMilestonePaymentReport(
            project1.address,
            j,
            offChainPaymentOnEachMilestone
          );

          //installer off-chain milestone report
          await unergyBuyer
          .connect(installerProject1)
          .offChainMilestonePaymentReport(
            project1.address,
            j,
            offChainPaymentOnEachMilestone
          );

        }
  
        await unergyBuyer.setOriginatorSign(project1.address, j)
        
      }
  
      const project = await projectsManager.getProject(project1.address);

      const projectState = project.state;

      expect(projectState).to.deep.equal(1);//INSTALLED
    });

    it("Should revert if an off-chain payment is not confirmed", async function () {
      const {
        unergyData,
        projectsManager,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        user1,
        user2,
        installerProject1,
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();

      const pWattPrice = parseUnits(1.1, STABLE_COIN_DECIMALS);

      const LIFE_TIME = 100_000;

      //pWatts purchase

      for (let j = 0; j < users.length; j++) {
        await unergyData.generatePurchaseTicket(
          project1.address,
          pWattPrice,
          LIFE_TIME,
          users[j]
        );

        const project = await projectsManager.getProject(project1.address);
        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(project1.address)
        )
          .sub(project.originatorFee)
          .div(toBn(users.length));

        await unergyLogicReserve.pWattsTransfer(
          project1.address,
          users[j],
          PWATTS_TO_TRANSFER
        );
      }

      const offChainPaymentOnEachMilestone = parseUnits(1000, 6);

      //change milestone state for all projects (Installer signature - Unergy signature)

      for (let j = 0; j < milestones.length; j++) {
        await unergyBuyer.setInstallerSign(project1.address);

        await expect(
          unergyBuyer
            .connect(installerProject1)
            .offChainMilestonePaymentReport(
              project1.address,
              j,
              offChainPaymentOnEachMilestone
            )
        ).revertedWithCustomError(unergyBuyer, "OffChainPaymentNotConfirmed");
      }
    });

    it("Should revert if the off-chain payment amount doesn't match", async function () {
      const {
        unergyData,
        projectsManager,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        user1,
        user2,
        installerProject1,
        projectOriginator1,
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();

      const pWattPrice = parseUnits(1.1, STABLE_COIN_DECIMALS);

      const LIFE_TIME = 100_000;

      //pWatts purchase

      for (let j = 0; j < users.length; j++) {
        await unergyData.generatePurchaseTicket(
          project1.address,
          pWattPrice,
          LIFE_TIME,
          users[j]
        );

        const project = await projectsManager.getProject(project1.address);
        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(project1.address)
        )
          .sub(project.originatorFee)
          .div(toBn(users.length));

        await unergyLogicReserve.pWattsTransfer(
          project1.address,
          users[j],
          PWATTS_TO_TRANSFER
        );
      }

      const offChainPaymentOnEachMilestone = parseUnits(1000, 6);

      const offChainPaymentReportedByInstaller = parseUnits(900, 6);

      //change milestone state for all projects (Installer signature - Unergy signature)

      for (let j = 0; j < milestones.length; j++) {
        await unergyBuyer.setInstallerSign(project1.address);
        await unergyBuyer
          .connect(projectOriginator1)
          .offChainMilestonePaymentReport(
            project1.address,
            j,
            offChainPaymentOnEachMilestone
          );
        await expect(
          unergyBuyer
            .connect(installerProject1)
            .offChainMilestonePaymentReport(
              project1.address,
              j,
              offChainPaymentReportedByInstaller
            )
        ).revertedWithCustomError(
          unergyBuyer,
          "ConfirmationAndAcknowledgeMismatch"
        );
      }
    });

    it("Should revert if the off-chain payment is greater than the milestone payment", async function () {
      const {
        unergyData,
        projectsManager,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        projectOriginator1,
        user1,
        user2,
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();

      const pWattPrice = parseUnits(1.1, STABLE_COIN_DECIMALS);

      const LIFE_TIME = 100_000;

      const projectStruct = await projectsManager.getProject(project1.address);

      //pWatts purchase

      for (let j = 0; j < users.length; j++) {
        await unergyData.generatePurchaseTicket(
          project1.address,
          pWattPrice,
          LIFE_TIME,
          users[j]
        );

        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(project1.address)
        )
          .sub(projectStruct.originatorFee)
          .div(toBn(users.length));

        await unergyLogicReserve.pWattsTransfer(
          project1.address,
          users[j],
          PWATTS_TO_TRANSFER
        );
      }

      const projectInitialValue = projectStruct.initialProjectValue;

      const offChainPaymentOnEachMilestone = projectInitialValue;

      let accumulatedOffChainPayment = parseUnits(0, 6);

      //change milestone state for all projects (Installer signature - Unergy signature)

      for (let j = 0; j < milestones.length; j++) {
        await unergyBuyer.setInstallerSign(project1.address);

         await expect(
          unergyBuyer
          .connect(projectOriginator1)
            .offChainMilestonePaymentReport(
              project1.address,
              j,
              offChainPaymentOnEachMilestone
            )
        ).revertedWithCustomError(
          unergyBuyer,
          "AmountExceedsRequired"
        );

      }
      
        
    });

    it("Should revert if the off-chain payment caller is not authorized", async function () {
      const {
        unergyData,
        projectsManager,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        projectOriginator1,
        user1,
        user2,
        notAdmin
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();

      const pWattPrice = parseUnits(1.1, STABLE_COIN_DECIMALS);

      const LIFE_TIME = 100_000;

      const projectStruct = await projectsManager.getProject(project1.address);

      //pWatts purchase

      for (let j = 0; j < users.length; j++) {
        await unergyData.generatePurchaseTicket(
          project1.address,
          pWattPrice,
          LIFE_TIME,
          users[j]
        );

        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(project1.address)
        )
          .sub(projectStruct.originatorFee)
          .div(toBn(users.length));

        await unergyLogicReserve.pWattsTransfer(
          project1.address,
          users[j],
          PWATTS_TO_TRANSFER
        );
      }

      const projectInitialValue = projectStruct.initialProjectValue;

      const offChainPaymentOnEachMilestone = projectInitialValue;

      //change milestone state for all projects (Installer signature - Unergy signature)

      for (let j = 0; j < milestones.length; j++) {
        await unergyBuyer.setInstallerSign(project1.address);

         await expect(
          unergyBuyer
          .connect(notAdmin)
            .offChainMilestonePaymentReport(
              project1.address,
              j,
              offChainPaymentOnEachMilestone
            )
        ).revertedWithCustomError(
          unergyBuyer,
          "NotAuthorizedToCallFunction"
        );

      }
      
        
    });

  });

  describe("Update pWatts Holders", function () {
    it("Should update pWatts holders list on all projects from UnergyBuyer contract", async function () {
      const {
        user1,
        user2,
        user3,
        user4,
        unergyData,
        stableCoin,
        project1Admin,
        projectsManager,
        projectOriginator1,
        unergyLogicReserve,
      } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      const holders = [
        user1.address,
        user2.address,
        user3.address,
        user4.address,
      ];

      //stable coin decimals, Watt price and expiration definition
      const STABLE_COIN_DECIMALS = await stableCoin.decimals();
      const PWATT_PRICE = parseUnits(1, STABLE_COIN_DECIMALS);
      const LIFE_TIME = 100_000;

      //pWatts purchase
      for (let i = 0; i < holders.length; i++) {
        await unergyData.generatePurchaseTicket(
          project1.address,
          PWATT_PRICE,
          LIFE_TIME,
          holders[i]
        );

        const project = await projectsManager.getProject(project1.address);
        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(project1.address)
        )
          .sub(project.originatorFee)
          .div(toBn(holders.length));

        await unergyLogicReserve.pWattsTransfer(
          project1.address,
          holders[i],
          PWATTS_TO_TRANSFER
        );
      }

      holders.push(project1Admin.address);
      holders.push(projectOriginator1.address);

      let projectHolders = await projectsManager.getProjectHolders(
        project1.address
      );

      expect(projectHolders.length).to.deep.equal(holders.length);
    });
  });

  describe("Update Project parameters", function () {
    it("Should change all modificable project parameters from UnergyBuyer contract", async function () {
      const { projectsManager, unergyBuyer, stableCoin } = await loadFixture(
        deployFixture
      );
      const { project1 } = await loadFixture(createProjectFixture);

      //stable coin decimals definition
      const stableDecimals = await stableCoin.decimals();
      //new Project values
      const newMaintenancePercentage = parseUnits(20, 2);
      const newProjectValue = parseUnits(125000, stableDecimals);
      const newSwapFactor = parseUnits(90, 2);

      const projectObj = await projectsManager.getProject(project1.address);

      const newProjectObj: ProjectStruct = {
        ...projectObj,
        maintenancePercentage: newMaintenancePercentage,
      };

      //Set new values on project struct
      await projectsManager.updateProject(project1.address, newProjectObj);
      await unergyBuyer.setCurrentProjectValue(
        project1.address,
        newProjectValue
      );
      await unergyBuyer.setSwapFactor(project1.address, newSwapFactor);

      let project = await projectsManager.getProject(project1.address);

      expect([
        project.maintenancePercentage,
        project.currentProjectValue,
        project.swapFactor,
      ]).to.deep.equal([
        newMaintenancePercentage,
        newProjectValue,
        newSwapFactor,
      ]);
    });
  });

  describe("Contract Addresses setup", function () {
    it("Should change all contracts addresses on UnergyBuyer contract", async function () {
      const { unergyBuyer, multiSignPermission, admin } = await loadFixture(
        deployFixture
      );

      await multiSignPermission.setPermission(
        admin.address,
        unergyBuyer.address,
        "setPermissionGranterAddr"
      );

      const randomWallet = ethers.Wallet.createRandom();

      // Set the permissionGranter address again because it is needed to set the other addresses
      await unergyBuyer.setUnergyDataAddr(randomWallet.address);
      await unergyBuyer.setPermissionGranterAddr(randomWallet.address);

      const unergyDataInstance = await unergyBuyer.unergyData();
      const permissionGranterAddress =
        await unergyBuyer.getPermissionGranterAddress();

      expect([permissionGranterAddress, unergyDataInstance]).to.deep.equal([
        randomWallet.address,
        randomWallet.address,
      ]);
    });

    it("Should revert if an admin try to set address 0 as a contract address", async () => {
      const { admin, unergyBuyer, multiSignPermission } = await loadFixture(
        deployFixture
      );

      await multiSignPermission.setPermission(
        admin.address,
        unergyBuyer.address,
        "setPermissionGranterAddr"
      );

      await expect(
        unergyBuyer.setPermissionGranterAddr(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(unergyBuyer, "ZeroAddressNotAllowed");

      await expect(
        unergyBuyer.transferOwnership(ethers.constants.AddressZero)
      ).to.be.revertedWithCustomError(unergyBuyer, "ZeroAddressNotAllowed");
    });
  });

  describe("uWatts Withdraw", function () {
    it("Should withdraw uWatts from UnergyBuyer contract", async function () {
      if (!externalDependenciesEnabled) this.skip();

      const {
        unergyBuyer,
        user1,
        user2,
        meter1,
        meter2,
        notAdmin,
        projectHelper,
        stableCoin,
        uWattToken,
        unergyData,
        unergyLogicReserve,
        projectsManager,
        multiSignPermission,
      } = await loadFixture(deployFixture);

      const { project1, project2 } = await loadFixture(createProjectFixture);

      const users = [user1.address, user2.address];

      const project1Struct = await projectsManager.getProject(project1.address);

      await projectHelper.bringToProduction(
        project1,
        { users, amounts: [] },
        meter1
      );

      const stableAmountDepreciated = await unergyData.depreciationBalance();
      const stableCoinDecimals = await stableCoin.decimals();

      const project2Struct = await projectsManager.getProject(project2.address);
      const project2Decimals = await project2.decimals();
      let balanceOfReserve = await stableCoin.balanceOf(
        unergyLogicReserve.address
      );

      const pWattPrice = parseUnits(1, stableCoinDecimals);

      const maxPWattThatCanBuyTheUnergyBuyer =
        calcMaxPWattThatCanBuyTheUnergyBuyer(
          project2Decimals,
          project2Struct,
          balanceOfReserve,
          true
        ).div(2);

      users.push(unergyBuyer.address);

      const usersPWatts = project2Struct.pWattsSupply
        .sub(project2Struct.originatorFee)
        .sub(maxPWattThatCanBuyTheUnergyBuyer)
        .div(2);

      await projectHelper.bringToProduction(
        project2,
        {
          users,
          amounts: [
            usersPWatts.toBigInt(),
            usersPWatts.toBigInt(),
            maxPWattThatCanBuyTheUnergyBuyer.toBigInt(),
          ],
        },
        meter2,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        pWattPrice
      );

      const project2FromContract = await projectsManager.getProject(
        project2.address
      );
      const p2SwapFactor = project2FromContract.swapFactor;

      await multiSignPermission.setPermission(
        notAdmin.address,
        unergyBuyer.address,
        "withdrawUWatts"
      );

      await unergyBuyer.connect(notAdmin).withdrawUWatts();

      const adminuWattsBalance3 = await uWattToken.balanceOf(notAdmin.address);

      const pWattPayedAsDepreciation = stableAmountDepreciated
        .mul(parseUnits(1, project2Decimals))
        .div(parseUnits(1, stableCoinDecimals));

      const uWattsToWithdraw = BigNumber.from(
        maxPWattThatCanBuyTheUnergyBuyer.sub(pWattPayedAsDepreciation)
      )
        .mul(p2SwapFactor)
        .div(10000);

      expect(adminuWattsBalance3.toString()).to.deep.equal(
        uWattsToWithdraw.toString()
      );
    });
  });

  describe("stableCoin Withdraw", function () {
    it("Should withdraw stableCoin after a pWatt purchase", async function () {
      const {
        unergyData,
        unergyLogicReserve,
        unergyBuyer,
        stableCoin,
        user1,
        notAdmin,
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      const STABLE_COIN_DECIMALS = await stableCoin.decimals();
      const PWATT_PRICE = parseUnits(1, STABLE_COIN_DECIMALS);
      const LIFE_TIME = 100_000;

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      const pWattsToPurchase = BigInt(100000000000000000000);
      await unergyLogicReserve.pWattsTransfer(
        project1.address,
        user1.address,
        pWattsToPurchase
      );

      const stableToWithdraw = BigInt(50000000);

      await unergyBuyer.withdrawStableCoin(
        project1.address,
        notAdmin.address,
        stableToWithdraw
      );

      const notAdminStableBalance = await stableCoin.balanceOf(
        notAdmin.address
      );

      expect(notAdminStableBalance).to.deep.equal(stableToWithdraw);
    });
  });

  describe("Refund internal wallets", function () {
    it("Should refund stableCoin after a project cancellation and none milestone reported as finished and only one investor", async function () {
      const {
        user1,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1000000";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      const user1StableBalanceBeforePurchase = await stableCoin.balanceOf(
        user1.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.refund(project1.address, user1.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue
      );

      expect([user1StableBalanceAfterRefund]).to.deep.equal([
        expectUser1Balance,
      ]);
    });

    it("Should refund stableCoin after a project cancellation and one milestone reported as finished", async function () {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      await stableCoin
        .connect(user2)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      const milestonesToSign = 1;

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.refund(project1.address, user1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });

    it("Should refund stableCoin after a project cancellation and two milestones reported as finished", async function () {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      await stableCoin
        .connect(user2)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      const milestonesToSign = 2;

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.refund(project1.address, user1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });

    it("Should refund stableCoin after a project cancellation and three milestones reported as finished", async function () {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      await stableCoin
        .connect(user2)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      const milestonesToSign = 3;

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.refund(project1.address, user1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });

    it("Should refund stableCoin after a project cancellation and four milestones reported as finished", async function () {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      await stableCoin
        .connect(user2)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      const milestonesToSign = 4;

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.refund(project1.address, user1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });

    it("Should refund stableCoin after a project cancellation and five milestones reported as finished", async function () {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      await stableCoin
        .connect(user2)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      const milestonesToSign = 5;

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.refund(project1.address, user1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });

    it("Should refund stableCoin after a project cancellation and six milestones reported as finished", async function () {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      await stableCoin
        .connect(user2)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      const milestonesToSign = 6;

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.refund(project1.address, user1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });
  });

  describe("Refund external wallets", function () {
    it("Should refund stableCoin after cancellation of a project and a milestone reported as completed when called by external wallet", async () => {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      // await stableCoin.connect(user2).approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      const milestonesToSign = 1;

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setInstallerSign(project1.address);
      }

      for (let i = 0; i < milestonesToSign; i++) {
        await unergyBuyer.setOriginatorSign(project1.address, i);
      }

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.connect(user1).externalRefund(project1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });
  });

  describe("Refund mixed wallets", function () {
    it("Should refund stableCoin after cancellation of a project and none milestone reported as completed", async () => {
      const {
        user1,
        user2,
        unergyBuyer,
        unergyData,
        unergyLogicReserve,
        stableCoin,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const PWATTS_TO_BUY = parseUnits(55000, 18);

      const PWATT_PRICE_1 = "1000000";
      const PWATT_PRICE_2 = "1090910";
      const LIFE_TIME = 1000;

      //generate tickets
      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE_1,
        LIFE_TIME,
        user1.address
      );

      await unergyData.generatePurchaseTicket(
        project1.address,
        PWATT_PRICE_2,
        LIFE_TIME,
        user2.address
      );

      //approve reserve to take stablecoin from users
      await stableCoin
        .connect(user1)
        .approve(unergyLogicReserve.address, PWATTS_TO_BUY);
      // await stableCoin.connect(user2).approve(unergyLogicReserve.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user1)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      await unergyLogicReserve
        .connect(user2)
        .buyPWatts(project1.address, PWATTS_TO_BUY);

      const user1StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterPurchase = await stableCoin.balanceOf(
        user2.address
      );

      //Project cancellation
      await projectsManager.setProjectState(project1.address, 5);

      const presentProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      //refund
      await unergyBuyer.connect(user1).externalRefund(project1.address);
      await unergyBuyer.refund(project1.address, user2.address);

      const user1StableBalanceAfterRefund = await stableCoin.balanceOf(
        user1.address
      );
      const user2StableBalanceAfterRefund = await stableCoin.balanceOf(
        user2.address
      );

      const expectUser1Balance = user1StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );
      const expectUser2Balance = user2StableBalanceAfterPurchase.add(
        presentProjectFundingValue.div(2)
      );

      expect([
        user1StableBalanceAfterRefund,
        user2StableBalanceAfterRefund,
      ]).to.deep.equal([expectUser1Balance, expectUser2Balance]);
    });
  });

  describe("Pause/UnPause", function () {
    it("Should revert if the contract is paused", async function () {
      const {
        unergyBuyer,
        cleanEnergyAsset,
        uWattToken,
        permissionGranter,
        projectsManager,
        user1,
      } = await loadFixture(deployFixture);

      const { project1, milestones } = await loadFixture(createProjectFixture);

      //pause
      await unergyBuyer.pause();
      expect(await unergyBuyer.paused()).to.equal(true);

      let txOwner;
      try {
        txOwner = await unergyBuyer.owner();
      } catch {
        expect(txOwner).to.be.revertedWith("Pausable: paused");
      }

      let txSetInstallerSign;
      try {
        txSetInstallerSign = await unergyBuyer.setInstallerSign(
          project1.address
        );
      } catch {
        expect(txSetInstallerSign).to.be.revertedWith("Pausable: paused");
      }

      let txsetOriginatorSign;
      try {
        txsetOriginatorSign = await unergyBuyer.setOriginatorSign(
          project1.address,
          0
        );
      } catch {
        expect(txsetOriginatorSign).to.be.revertedWith("Pausable: paused");
      }

      let txsetCurrentProjectValue;
      try {
        txsetCurrentProjectValue = await unergyBuyer.setCurrentProjectValue(
          project1.address,
          1
        );
      } catch {
        expect(txsetCurrentProjectValue).to.be.revertedWith("Pausable: paused");
      }

      let txSetSwapFactor;
      try {
        txSetSwapFactor = await unergyBuyer.setSwapFactor(project1.address, 1);
      } catch {
        expect(txSetSwapFactor).to.be.revertedWith("Pausable: paused");
      }

      let txWithdrawUWatts;
      try {
        txWithdrawUWatts = await unergyBuyer.withdrawUWatts();
      } catch {
        expect(txWithdrawUWatts).to.be.revertedWith("Pausable: paused");
      }

      let txSetPermissionGranterAddr;
      try {
        txSetPermissionGranterAddr = await unergyBuyer.setPermissionGranterAddr(
          permissionGranter.address
        );
      } catch {
        expect(txSetPermissionGranterAddr).to.be.revertedWith(
          "Pausable: paused"
        );
      }

      let txTransferOwnership;
      try {
        txTransferOwnership = await unergyBuyer.transferOwnership(
          user1.address
        );
      } catch {
        expect(txTransferOwnership).to.be.revertedWith("Pausable: paused");
      }

      let txPause;
      try {
        txPause = await unergyBuyer.pause();
      } catch {
        expect(txPause).to.be.revertedWith("Pausable: paused");
      }

      //unpause
      await unergyBuyer.unpause();
      expect(await unergyBuyer.paused()).to.equal(false);
    });
  });

  describe("Transfer Ownership", function () {
    it("Should transfer the ownership of UnergyBuyer contract", async function () {
      const { unergyBuyer, notAdmin } = await loadFixture(deployFixture);

      await unergyBuyer.transferOwnership(notAdmin.address);

      await unergyBuyer.connect(notAdmin).acceptOwnership();

      expect(await unergyBuyer.owner()).to.deep.equal(notAdmin.address);
    });
  });

  describe("Non Authorized Callers", function () {
    it("Should revert if the caller is not authorized", async function () {
      const {
        unergyBuyer,
        cleanEnergyAsset,
        uWattToken,
        permissionGranter,
        projectsManager,
        notAdmin,
        user1,
      } = await loadFixture(deployFixture);

      const { project1 } = await loadFixture(createProjectFixture);

      let txSetInstallerSign;
      try {
        txSetInstallerSign = await unergyBuyer
          .connect(notAdmin)
          .setInstallerSign(project1.address);
      } catch {
        expect(txSetInstallerSign).to.be.revertedWith(
          "NotAuthorizedToCallFunction"
        );
      }

      let txsetOriginatorSign;
      try {
        txsetOriginatorSign = await unergyBuyer
          .connect(notAdmin)
          .setOriginatorSign(project1.address, 0);
      } catch {
        expect(txsetOriginatorSign).to.be.revertedWith(
          "NotAuthorizedToCallFunction"
        );
      }

      let txsetCurrentProjectValue;
      try {
        txsetCurrentProjectValue = await unergyBuyer
          .connect(notAdmin)
          .setCurrentProjectValue(project1.address, 1);
      } catch {
        expect(txsetCurrentProjectValue).to.be.revertedWith(
          "NotAuthorizedToCallFunction"
        );
      }

      let txSetSwapFactor;
      try {
        txSetSwapFactor = await unergyBuyer
          .connect(notAdmin)
          .setSwapFactor(project1.address, 1);
      } catch {
        expect(txSetSwapFactor).to.be.revertedWith(
          "NotAuthorizedToCallFunction"
        );
      }

      let txWithdrawUWatts;
      try {
        txWithdrawUWatts = await unergyBuyer.connect(notAdmin).withdrawUWatts();
      } catch {
        expect(txWithdrawUWatts).to.be.revertedWith(
          "NotAuthorizedToCallFunction"
        );
      }

      let txSetPermissionGranterAddr;
      try {
        txSetPermissionGranterAddr = await unergyBuyer
          .connect(notAdmin)
          .setPermissionGranterAddr(permissionGranter.address);
      } catch {
        expect(txSetPermissionGranterAddr).to.be.revertedWith(
          "NotAuthorizedToCallFunction"
        );
      }

      let txTransferOwnership;
      try {
        txTransferOwnership = await unergyBuyer
          .connect(notAdmin)
          .transferOwnership(user1.address);
      } catch {
        expect(txTransferOwnership).to.be.revertedWith(
          "NotAuthorizedToCallFunction"
        );
      }

      let txPause;
      try {
        txPause = await unergyBuyer.connect(notAdmin).pause();
      } catch {
        expect(txPause).to.be.revertedWith("NotAuthorizedToCallFunction");
      }

      let txUnPause;
      try {
        txUnPause = await unergyBuyer.connect(notAdmin).unpause();
      } catch {
        expect(txUnPause).to.be.revertedWith("NotAuthorizedToCallFunction");
      }
    });
  });

  describe("Unconventional Incomes", () => {
    it("Should revert if the caller is not the owner", async () => {
      const { unergyBuyer, notAdmin, permissionGranter } = await loadFixture(
        deployFixture
      );
      const { project1 } = await loadFixture(createProjectFixture);

      await expect(
        unergyBuyer
          .connect(notAdmin)
          .reportUnconventionalIncome(project1.address, 1, "test")
      ).to.be.revertedWithCustomError(
        permissionGranter,
        "NotAuthorizedToCallFunction"
      );
    });

    it("Should revert if the project address is 0", async () => {
      const { unergyBuyer } = await loadFixture(deployFixture);

      await expect(
        unergyBuyer.reportUnconventionalIncome(
          ethers.constants.AddressZero,
          1,
          "test"
        )
      ).to.be.revertedWithCustomError(unergyBuyer, "ZeroAddressNotAllowed");
    });

    it("Should revert if the string is empty", async () => {
      const { unergyBuyer, permissionGranter, admin } = await loadFixture(
        deployFixture
      );
      const { project1 } = await loadFixture(createProjectFixture);

      await permissionGranter.setPermission(
        admin.address,
        unergyBuyer.address,
        "reportUnconventionalIncome",
        PermissionType.ONETIME,
        0
      );

      await expect(
        unergyBuyer.reportUnconventionalIncome(project1.address, 1, "")
      ).to.be.revertedWithCustomError(unergyBuyer, "EmptyString");
    });

    it("Should revert if the amount is 0", async () => {
      const { unergyBuyer, permissionGranter, admin } = await loadFixture(
        deployFixture
      );
      const { project1 } = await loadFixture(createProjectFixture);

      await permissionGranter.setPermission(
        admin.address,
        unergyBuyer.address,
        "reportUnconventionalIncome",
        PermissionType.ONETIME,
        0
      );

      await expect(
        unergyBuyer.reportUnconventionalIncome(project1.address, 0, "test")
      ).to.be.revertedWithCustomError(unergyBuyer, "ZeroValueNotAllowed");
    });

    it("Should report an unconventional income", async () => {
      const {
        unergyBuyer,
        permissionGranter,
        projectsManager,
        admin,
        projectHelper,
        user1,
        unergyData,
        stableCoin,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      await permissionGranter.setPermission(
        admin.address,
        unergyBuyer.address,
        "reportUnconventionalIncome",
        PermissionType.ONETIME,
        0
      );

      const amountBoughtByUser1 = ethers.utils.parseEther("10000");

      await projectHelper.transferPWattToUsers(
        project1,
        {
          users: [user1.address],
          amounts: [amountBoughtByUser1.toBigInt()],
        },
        undefined,
        ethers.utils.parseUnits("1", 6)
      );

      const amountReportedAsUnconventionalIncome = ethers.utils.parseUnits(
        "1000",
        6
      );

      await stableCoin.mint(
        admin.address,
        amountReportedAsUnconventionalIncome
      );
      await stableCoin.approve(
        unergyBuyer.address,
        amountReportedAsUnconventionalIncome
      );

      await unergyBuyer.reportUnconventionalIncome(
        project1.address,
        amountReportedAsUnconventionalIncome,
        "test"
      );

      const newProjectFundingValue =
        await unergyData.getPresentProjectFundingValue(project1.address);

      const projectAdminBalance = await stableCoin.balanceOf(
        unergyBuyer.address
      );

      expect(newProjectFundingValue).to.equal(projectAdminBalance);
    });
  });
});
