import { ethers, network, upgrades } from "hardhat";

import {
  ERC20StableCoin,
  PermissionGranter,
  ProjectsManager,
  UnergyBuyer,
  UnergyLogicReserve,
} from "../../typechain-types";
import {
  maxUintHex,
  swapFeePercentage,
  PROTOCOL_CONTRACT_ROLE,
  assetManagerFeePercentage,
  STABLE_COIN_INITIAL_SUPPLY,
  CLAIM_SERVICE_SIGNER_ADDRESS,
} from "../../utils/constants";
import { logger } from "../../utils/logger";
import { parseUnits } from "../../utils/helpers";
import { LoadUsers } from "../../utils/loadUsers";
import { ProjectHelper } from "../../utils/projectHelpers";
import { MultiSignPermission } from "../../utils/multiSignPermissions";
import { UnergyData } from "../../typechain-types/contracts/UnergyData";

export async function deployFixture() {
  const [
    admin,
    admin2,
    admin3,
    user1,
    user2,
    user3,
    user4,
    user5,
    user6,
    user7,
    user8,
    meter1,
    meter2,
    meter3,
    meter4,
    meter5,
    meter6,
    meter7,
    meter8,
    mantainer,
    project1Admin,
    project2Admin,
    project3Admin,
    project4Admin,
    project5Admin,
    project6Admin,
    installerProject1,
    installerProject2,
    installerProject3,
    installerProject4,
    installerProject5,
    installerProject6,
    assetManager,
    stakingProtocolAddress,
    notAdmin,
    projectOriginator1,
    projectOriginator2,
    randomUser1,
    randomUser2,
    randomUser3,
    randomUser4,
    randomUser5,
  ] = await ethers.getSigners();

  if (network.name === "hardhat") {
    network.provider.send("hardhat_setBalance", [
      CLAIM_SERVICE_SIGNER_ADDRESS, // This is the account that will be used by the external claim service
      maxUintHex,
    ]);
  }

  /**
   * * PermissionGranter
   */
  const PermissionGranter = await ethers.getContractFactory(
    "PermissionGranter"
  );
  const permissionGranter = (await upgrades.deployProxy(
    PermissionGranter,
    [admin.address],
    {
      kind: "uups",
    }
  )) as PermissionGranter;
  permissionGranter.deployed();
  logger.log(`PermissionGranter deployed to: ${permissionGranter.address}`);

  const multiSignPermission = new MultiSignPermission(permissionGranter, [
    admin,
  ]);

  /**
   * * UnergyData
   */
  const UnergyData = await ethers.getContractFactory("UnergyData");
  const unergyData = (await upgrades.deployProxy(
    UnergyData,
    [mantainer.address, permissionGranter.address],
    {
      kind: "uups",
    }
  )) as UnergyData;
  logger.log(`UnergyData deployed to: ${unergyData.address}`);

  // UnergyDataV2 (a non set up contract for testing purposes)
  const UnergyDataV2 = await ethers.getContractFactory("UnergyData");
  const unergyDataV2 = (await upgrades.deployProxy(
    UnergyDataV2,
    [mantainer.address, permissionGranter.address],
    {
      kind: "uups",
    }
  )) as UnergyData;

  /**
   * * UnergyEvent
   */
  const UnergyEvent = await ethers.getContractFactory("UnergyEvent");
  const unergyEvent = await UnergyEvent.deploy(
    unergyData.address,
    permissionGranter.address
  );
  await unergyEvent.deployed();
  logger.log(`UnergyEvents deployed to: ${unergyEvent.address}`);

  // UnergyEventV2 (a non set up contract for testing purposes)
  const UnergyEventV2 = await ethers.getContractFactory("UnergyEvent");
  const unergyEventV2 = await UnergyEventV2.deploy(
    unergyData.address,
    permissionGranter.address
  );
  await unergyEventV2.deployed();

  /**
   * * uWatt Token
   **/
  const UWattToken = await ethers.getContractFactory("ERC20UWatt");
  const uWattToken = await UWattToken.deploy(
    unergyData.address,
    permissionGranter.address
  );
  uWattToken.deployed();
  logger.log(`uWattToken deployed to: ${uWattToken.address}`);

  // uWatt Token V2 (a non set up contract for testing purposes)
  const UWattTokenV2 = await ethers.getContractFactory("ERC20UWatt");
  const uWattTokenV2 = await UWattTokenV2.deploy(
    unergyData.address,
    permissionGranter.address
  );
  uWattTokenV2.deployed();

  /**
   * * UnergyLogicReserve
   */
  const UnergyLogicReserve = await ethers.getContractFactory(
    "UnergyLogicReserve"
  );
  const unergyLogicReserve = (await upgrades.deployProxy(
    UnergyLogicReserve,
    [unergyData.address, permissionGranter.address],
    {
      kind: "uups",
    }
  )) as UnergyLogicReserve;
  logger.log(`UnergyLogicReserve deployed to: ${unergyLogicReserve.address}`);

  // UnergyLogicReserveV2 (a non set up contract for testing purposes)
  const UnergyLogicReserveV2 = await ethers.getContractFactory(
    "UnergyLogicReserve"
  );
  const unergyLogicReserveV2 = (await upgrades.deployProxy(
    UnergyLogicReserveV2,
    [unergyData.address, permissionGranter.address],
    { kind: "uups" }
  )) as UnergyLogicReserve;

  /**
   * * UnergyBuyer
   */
  const UnergyBuyer = await ethers.getContractFactory("UnergyBuyer");
  const unergyBuyer = (await upgrades.deployProxy(
    UnergyBuyer,
    [unergyData.address, permissionGranter.address],
    { kind: "uups" }
  )) as UnergyBuyer;
  logger.log(`UnergyBuyer deployed to: ${unergyBuyer.address}`);

  // UnergyBuyerV2 (a non set up contract for testing purposes)
  const UnergyBuyerV2 = await ethers.getContractFactory("UnergyBuyer");
  const unergyBuyerV2 = (await upgrades.deployProxy(
    UnergyBuyerV2,
    [unergyData.address, permissionGranter.address],
    { kind: "uups" }
  )) as UnergyBuyer;

  /**
   * * StableCoin
   */
  const StableCoin = await ethers.getContractFactory("ERC20StableCoin");
  const stableCoin = await StableCoin.deploy("USDC", "USDC", admin.address);
  logger.log(`StableCoin deployed to: ${stableCoin.address}`);

  // DummyERC20_1
  const dummyERC20_1 = await StableCoin.deploy(
    "DummyERC20_1",
    "DummyERC20_1",
    admin.address
  );
  logger.log(`DummyERC20_1 deployed to: ${dummyERC20_1.address}`);

  // DummyERC20_2
  const dummyERC20_2 = await StableCoin.deploy(
    "DummyERC20_2",
    "DummyERC20_2",
    admin.address
  );
  logger.log(`DummyERC20_2 deployed to: ${dummyERC20_2.address}`);

  /**
   * * CleanEnergyToken
   */
  const CleanEnergyAssets = await ethers.getContractFactory(
    "CleanEnergyAssets"
  );
  const cleanEnergyAsset = await CleanEnergyAssets.deploy(
    permissionGranter.address
  );
  logger.log(`CleanEnergyToken deployed to: ${cleanEnergyAsset.address}`);

  /**
   * * ProjectsManager
   */
  const ProjectsManager = await ethers.getContractFactory("ProjectsManager");
  const projectsManager = (await upgrades.deployProxy(
    ProjectsManager,
    [unergyData.address, permissionGranter.address],
    { kind: "uups" }
  )) as ProjectsManager;
  logger.log(`ProjectsManager deployed to: ${projectsManager.address}`);

  // ProjectsManagerV2 (a non set up contract for testing purposes)
  const ProjectsManagerV2 = await ethers.getContractFactory("ProjectsManager");
  const projectsManagerV2 = (await upgrades.deployProxy(
    ProjectsManagerV2,
    [unergyData.address, permissionGranter.address],
    { kind: "uups" }
  )) as ProjectsManager;

  /**
   * * HealthChecker
   */
  const HealthChecker = await ethers.getContractFactory("HealthChecker");
  const healthChecker = await HealthChecker.deploy(
    CLAIM_SERVICE_SIGNER_ADDRESS
  );

  logger.log(`HealthChecker deployed to: ${healthChecker.address}`);

  // * Initial Setup
  await permissionGranter.grantRole(
    PROTOCOL_CONTRACT_ROLE,
    projectsManager.address
  );

  await multiSignPermission.setPermission(
    admin.address,
    projectsManager.address,
    "createProject"
  );
  await multiSignPermission.setPermission(
    admin.address,
    projectsManager.address,
    "configureProject"
  );
  await multiSignPermission.setPermission(
    admin.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    admin.address,
    projectsManager.address,
    "setSignature"
  );
  await multiSignPermission.setPermission(
    unergyBuyer.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    unergyBuyer.address,
    projectsManager.address,
    "setSignature"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    unergyEvent.address,
    projectsManager.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    admin.address,
    projectsManager.address,
    "setUnergyEventAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    projectsManager.address,
    "setUnergyLogicReserveAddr"
  );

  // uWattToken setup ✅
  await permissionGranter.grantRole(PROTOCOL_CONTRACT_ROLE, uWattToken.address);

  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    uWattToken.address,
    "mint"
  );
  await multiSignPermission.setPermission(
    admin.address,
    uWattToken.address,
    "setUnergyEventAddr"
  );

  // stableCoin setup ✅
  await stableCoin.mint(admin.address, STABLE_COIN_INITIAL_SUPPLY.mul(2));
  await stableCoin.approve(
    unergyLogicReserve.address,
    STABLE_COIN_INITIAL_SUPPLY
  );
  await stableCoin.approve(unergyBuyer.address, STABLE_COIN_INITIAL_SUPPLY);

  const users = [user1, user2, user3, user4, user5, user6, user7, user8];

  //Transfer stableCoin to all users
  for (let i = 0; i < users.length; i++) {
    await stableCoin
      .connect(admin)
      .transfer(users[i].address, parseUnits(1_000_000, 6));

    await stableCoin
      .connect(users[i])
      .approve(unergyLogicReserve.address, STABLE_COIN_INITIAL_SUPPLY);
  }

  // unergyEvent setup ✅
  await permissionGranter.grantRole(
    PROTOCOL_CONTRACT_ROLE,
    unergyEvent.address
  );

  await multiSignPermission.setPermission(
    admin.address,
    unergyEvent.address,
    "setUWattAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyEvent.address,
    "setProjectsManagerAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyEvent.address,
    "setUnergyBuyerAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyEvent.address,
    "setUnergyLogicReserveAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyEvent.address,
    "beforeTransferReceipt"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyEvent.address,
    "afterTransferReceipt"
  );
  await multiSignPermission.setPermission(
    uWattToken.address,
    unergyEvent.address,
    "beforeTransferReceipt"
  );
  await multiSignPermission.setPermission(
    uWattToken.address,
    unergyEvent.address,
    "afterTransferReceipt"
  );

  // unergyData setup ✅
  await permissionGranter.grantRole(PROTOCOL_CONTRACT_ROLE, unergyData.address);

  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setProjectsManagerAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setUnergyBuyerAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setUnergyLogicReserveAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setUnergyEventAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setUWattAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setCleanEnergyAssetsAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "generatePurchaseTicket"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setExternalHolderAddress"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setExternalHolderAddress"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setAccEnergyByMeter"
  );
  await multiSignPermission.setPermission(
    unergyBuyer.address,
    unergyData.address,
    "setPresentProjectFundingValue"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setPresentProjectFundingValue"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setPresentProjectFundingValue"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "setDepreciationBalance"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "changePurchaseTicketUsed"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "insertUWattsStatusSnapshot"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "insertHistoricalSwap"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyData.address,
    "updateUWattsStatusSnapshotAtIndex"
  );
  await multiSignPermission.setPermission(
    projectsManager.address,
    unergyData.address,
    "setAssetManagerAddress"
  );
  await multiSignPermission.setPermission(
    projectsManager.address,
    unergyData.address,
    "setAssetManagerFeePercentage"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setStakingProtocolAddress"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setSwapFeePercentage"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setOffChainMilestonePayment"
  );
  await multiSignPermission.setPermission(
    unergyBuyer.address,
    unergyData.address,
    "setOffChainMilestonePayment"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyData.address,
    "setOffChainPaymentReport"
  );
  await multiSignPermission.setPermission(
    unergyBuyer.address,
    unergyData.address,
    "setOffChainPaymentReport"
  );

  await unergyData.setProjectsManagerAddr(projectsManager.address);
  await unergyData.setUnergyBuyerAddr(unergyBuyer.address);
  await unergyData.setUnergyLogicReserveAddr(unergyLogicReserve.address);
  await unergyData.setUnergyEventAddr(unergyEvent.address, 0);
  await unergyData.setUnergyEventAddr(unergyEvent.address, 1);
  await unergyData.setUWattAddr(uWattToken.address);
  await unergyData.setCleanEnergyAssetsAddr(cleanEnergyAsset.address);
  await unergyData.setStakingProtocolAddress(stakingProtocolAddress.address);
  await unergyData.setSwapFeePercentage(swapFeePercentage);

  /**
   * * UniswapV3Pool mock
   */
  const UniswapV3Pool = await ethers.getContractFactory("UniswapV3Pool");
  const uniswapV3Pool = await UniswapV3Pool.deploy();
  await uniswapV3Pool.deployed();
  logger.log(`UniswapV3Pool (mock) deployed to: ${uniswapV3Pool.address}`);

  /**
   * * UniswapV3PoolAdapter
   */

  const UniswapV3PoolAdapter = await ethers.getContractFactory(
    "UniswapV3PoolAdapter"
  );
  const uniswapV3PoolAdapter = await UniswapV3PoolAdapter.deploy(
    uniswapV3Pool.address
  );
  await uniswapV3PoolAdapter.deployed();

  logger.log(
    `UniswapV3PoolAdapter deployed to: ${uniswapV3PoolAdapter.address}`
  );

  /**
   * * LendingManager
   */
  const LendigManager = await ethers.getContractFactory("LendingManager");
  const lendingManager = await LendigManager.deploy(
    admin.address,
    stableCoin.address,
    unergyData.address,
    uniswapV3Pool.address
  );
  await lendingManager.deployed();

  logger.log(`LendingManager deployed to: ${lendingManager.address}`);

  // unergyLogicReserve setup ✅
  await permissionGranter.grantRole(
    PROTOCOL_CONTRACT_ROLE,
    unergyLogicReserve.address
  );

  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "energyReport"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "invoiceReport"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "pWattsTransfer"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "swapToken"
  );
  await multiSignPermission.setPermission(
    CLAIM_SERVICE_SIGNER_ADDRESS,
    unergyLogicReserve.address,
    "swapToken"
  );
  await multiSignPermission.setPermission(
    CLAIM_SERVICE_SIGNER_ADDRESS,
    unergyLogicReserve.address,
    "requestSwap"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "requestSwap"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "claimUWatts"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "withdrawStableCoin"
  );
  await multiSignPermission.setPermission(
    CLAIM_SERVICE_SIGNER_ADDRESS,
    unergyLogicReserve.address,
    "claimUWatts"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "setProjectsManagerAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "setCleanEnergyAssetsAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "setUnergyBuyerAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyLogicReserve.address,
    "requestClaim"
  );

  // unergyBuyer setup ✅
  await permissionGranter.grantRole(
    PROTOCOL_CONTRACT_ROLE,
    unergyBuyer.address
  );

  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setOriginatorSign"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setInstallerSign"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setPWattPrice"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setInitialProjectValue"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setCurrentProjectValue"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setSwapFactor"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setTotalPWatts"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "withdrawUWatts"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "withdrawStableCoin"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "refund"
  );
  await multiSignPermission.setPermission(
    unergyBuyer.address,
    unergyBuyer.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    unergyEvent.address,
    unergyBuyer.address,
    "updateProjectRelatedProperties"
  );
  await multiSignPermission.setPermission(
    unergyEvent.address,
    unergyBuyer.address,
    "updateLastUWattStatus"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    unergyBuyer.address,
    "setProjectState"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setUnergyLogicReserveAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setProjectsManagerAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setUWattAddr"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "setUneryDataAddress"
  );
  await multiSignPermission.setPermission(
    projectOriginator1.address,
    unergyBuyer.address,
    "offChainMilestonePaymentReport"
  );
  await multiSignPermission.setPermission(
    admin.address,
    unergyBuyer.address,
    "offChainMilestonePaymentReport"
  );
  await multiSignPermission.setPermission(
    installerProject1.address,
    unergyBuyer.address,
    "offChainMilestonePaymentReport"
  );

  // cleanEnergyToken setup ✅
  await permissionGranter.grantRole(
    PROTOCOL_CONTRACT_ROLE,
    cleanEnergyAsset.address
  );

  await multiSignPermission.setPermission(
    projectsManager.address,
    cleanEnergyAsset.address,
    "createProjectEnergyAsset"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    cleanEnergyAsset.address,
    "mint"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    cleanEnergyAsset.address,
    "burn"
  );
  await multiSignPermission.setPermission(
    unergyLogicReserve.address,
    cleanEnergyAsset.address,
    "createGeneralEnergyAsset"
  );

  // * Permission Granter Setup
  const STABLE_COIN_DECIMALS = await stableCoin.decimals();
  const ENERGY_ASSET_DECIMALS = (
    await cleanEnergyAsset.energyDecimals()
  ).toNumber();

  const projectHelper = new ProjectHelper(
    unergyData,
    projectsManager,
    unergyLogicReserve,
    unergyBuyer,
    stableCoin as ERC20StableCoin,
    uWattToken
  );

  const loadUsers = new LoadUsers();

  return {
    // Accounts
    admin,
    admin2,
    admin3,
    user1,
    user2,
    user3,
    user4,
    user5,
    user6,
    user7,
    user8,
    meter1,
    meter2,
    meter3,
    meter4,
    meter5,
    meter6,
    meter7,
    meter8,
    mantainer,
    project1Admin,
    project2Admin,
    project3Admin,
    project4Admin,
    project5Admin,
    project6Admin,
    assetManager,
    stakingProtocolAddress,
    installerProject1,
    installerProject2,
    installerProject3,
    installerProject4,
    installerProject5,
    installerProject6,
    notAdmin,
    projectOriginator1,
    projectOriginator2,
    randomUser1,
    randomUser2,
    randomUser3,
    randomUser4,
    randomUser5,
    // Contracts
    dummyERC20_1,
    dummyERC20_2,
    stableCoin,
    unergyData,
    unergyDataV2,
    projectsManager,
    projectsManagerV2,
    permissionGranter,
    uWattToken,
    uWattTokenV2,
    unergyLogicReserve,
    unergyLogicReserveV2,
    unergyBuyer,
    unergyBuyerV2,
    cleanEnergyAsset,
    unergyEvent,
    unergyEventV2,
    healthChecker,
    lendingManager,
    uniswapV3Pool,
    uniswapV3PoolAdapter,
    // Constants
    STABLE_COIN_DECIMALS,
    ENERGY_ASSET_DECIMALS,
    assetManagerFeePercentage,
    swapFeePercentage,
    // Helpers
    projectHelper,
    loadUsers,
    multiSignPermission,
  };
}
