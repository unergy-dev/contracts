import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { deployFixture } from "./deployFixture";
import { ProjectInputStruct } from "../../typechain-types/contracts/ProjectsManager";
import {
  MilestoneInput,
  getAddressFromReceipt,
  parseUnits,
  projectAt,
} from "../../utils";
import { MultiSignPermission } from "../../utils/multiSignPermissions";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Project } from "../../typechain-types";
import { PROTOCOL_CONTRACT_ROLE } from "../../utils/constants";
import { logger } from "../../utils/logger";

type ProjectArgs = [
  ProjectInputStruct,
  `Project ${number}`,
  `PRJ${number}`,
  SignerWithAddress,
  SignerWithAddress
];

export async function createProjectFixture() {
  const {
    admin,
    admin2,
    admin3,
    meter1,
    meter2,
    meter3,
    meter4,
    meter5,
    meter6,
    stableCoin,
    project1Admin,
    installerProject1,
    projectsManager,
    project2Admin,
    installerProject2,
    project3Admin,
    installerProject3,
    project4Admin,
    installerProject4,
    project5Admin,
    installerProject5,
    project6Admin,
    installerProject6,
    assetManager,
    unergyBuyer,
    unergyLogicReserve,
    permissionGranter,
    unergyEvent,
    projectOriginator1,
    assetManagerFeePercentage,
  } = await loadFixture(deployFixture);

  const stableDecimals = await stableCoin.decimals();
  const PROJECTS_DECIMALS = 18;

  const defaultProjectInput: ProjectInputStruct = {
    maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
    initialProjectValue: parseUnits(120000, stableDecimals), // 10%
    currentProjectValue: parseUnits(120000, stableDecimals), // 120k
    swapFactor: parseUnits(1, PROJECTS_DECIMALS), // 100% (2 decimals)
    totalPWatts: parseUnits(120000, PROJECTS_DECIMALS), // 120k pWatts (2 decimals)
    adminAddr: project1Admin.address,
    installerAddr: installerProject1.address,
    originator: projectOriginator1.address,
    originatorFee: parseUnits(10_000, PROJECTS_DECIMALS), // 10k pWatts (2 decimals)
    stableAddr: stableCoin.address,
    assetManagerAddr: assetManager.address,
    assetManagerFeePercentage: assetManagerFeePercentage,
  };

  const multiSignPermission = new MultiSignPermission(permissionGranter, [
    admin,
  ]);

  const project1Input: ProjectInputStruct = {
    maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
    initialProjectValue: parseUnits(120000, stableDecimals),
    currentProjectValue: parseUnits(120000, stableDecimals),
    swapFactor: parseUnits(9135, 14),
    totalPWatts: parseUnits(120_000, PROJECTS_DECIMALS),
    adminAddr: project1Admin.address,
    installerAddr: installerProject1.address,
    originator: projectOriginator1.address,
    originatorFee: parseUnits(10_000, PROJECTS_DECIMALS),
    stableAddr: stableCoin.address,
    assetManagerAddr: assetManager.address,
    assetManagerFeePercentage: assetManagerFeePercentage,
  };
  const project1Args: ProjectArgs = [
    project1Input,
    "Project 1",
    "PRJ1",
    project1Admin,
    meter1,
  ];

  const project2Input: ProjectInputStruct = {
    maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
    initialProjectValue: parseUnits(94400, stableDecimals),
    currentProjectValue: parseUnits(94400, stableDecimals),
    swapFactor: parseUnits(10102, 14),
    totalPWatts: parseUnits(94_400, PROJECTS_DECIMALS),
    adminAddr: project2Admin.address,
    installerAddr: installerProject2.address,
    originator: projectOriginator1.address,
    originatorFee: parseUnits(21_500, PROJECTS_DECIMALS),
    stableAddr: stableCoin.address,
    assetManagerAddr: assetManager.address,
    assetManagerFeePercentage: assetManagerFeePercentage,
  };
  const project2Args: ProjectArgs = [
    project2Input,
    "Project 2",
    "PRJ2",
    project2Admin,
    meter2,
  ];

  const project3Input: ProjectInputStruct = {
    maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
    initialProjectValue: parseUnits(1380000, stableDecimals),
    currentProjectValue: parseUnits(1380000, stableDecimals),
    swapFactor: parseUnits(5, 17),
    totalPWatts: parseUnits(1_380_000, PROJECTS_DECIMALS),
    adminAddr: project3Admin.address,
    installerAddr: installerProject3.address,
    originator: projectOriginator1.address,
    originatorFee: parseUnits(31_000, PROJECTS_DECIMALS),
    stableAddr: stableCoin.address,
    assetManagerAddr: assetManager.address,
    assetManagerFeePercentage: assetManagerFeePercentage,
  };
  const project3Args: ProjectArgs = [
    project3Input,
    "Project 3",
    "PRJ3",
    project3Admin,
    meter3,
  ];

  const project4Input: ProjectInputStruct = {
    maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
    initialProjectValue: parseUnits(1380000, stableDecimals),
    currentProjectValue: parseUnits(1_380_000, stableDecimals),
    swapFactor: parseUnits(1, PROJECTS_DECIMALS),
    totalPWatts: parseUnits(1380000, PROJECTS_DECIMALS),
    adminAddr: project4Admin.address,
    installerAddr: installerProject4.address,
    originator: projectOriginator1.address,
    originatorFee: parseUnits(16_000, PROJECTS_DECIMALS),
    stableAddr: stableCoin.address,
    assetManagerAddr: assetManager.address,
    assetManagerFeePercentage: assetManagerFeePercentage,
  };
  const project4Args: ProjectArgs = [
    project4Input,
    "Project 4",
    "PRJ4",
    project4Admin,
    meter4,
  ];

  const project5Input: ProjectInputStruct = {
    maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
    initialProjectValue: parseUnits(1_380_000, stableDecimals),
    currentProjectValue: parseUnits(1_380_000, stableDecimals),
    swapFactor: parseUnits(5, 17),
    totalPWatts: parseUnits(1380000, PROJECTS_DECIMALS),
    adminAddr: project5Admin.address,
    installerAddr: installerProject5.address,
    originator: projectOriginator1.address,
    originatorFee: parseUnits(13_000, PROJECTS_DECIMALS),
    stableAddr: stableCoin.address,
    assetManagerAddr: assetManager.address,
    assetManagerFeePercentage: assetManagerFeePercentage,
  };
  const project5Args: ProjectArgs = [
    project5Input,
    "Project 5",
    "PRJ5",
    project5Admin,
    meter5,
  ];

  const project6Input: ProjectInputStruct = {
    maintenancePercentage: parseUnits(10, PROJECTS_DECIMALS),
    initialProjectValue: parseUnits(1380000, stableDecimals),
    currentProjectValue: parseUnits(1380000, stableDecimals),
    swapFactor: parseUnits(5, 17),
    totalPWatts: parseUnits(1380000, PROJECTS_DECIMALS),
    adminAddr: project6Admin.address,
    installerAddr: installerProject6.address,
    originator: projectOriginator1.address,
    originatorFee: parseUnits(10_000, PROJECTS_DECIMALS),
    stableAddr: stableCoin.address,
    assetManagerAddr: assetManager.address,
    assetManagerFeePercentage: assetManagerFeePercentage,
  };

  const project6Args: ProjectArgs = [
    project6Input,
    "Project 6",
    "PRJ6",
    project6Admin,
    meter6,
  ];

  const projects: ProjectArgs[] = [
    project1Args,
    project2Args,
    project3Args,
    project4Args,
    project5Args,
    project6Args,
  ];

  let installers = [
    installerProject1,
    installerProject2,
    installerProject3,
    installerProject4,
    installerProject5,
    installerProject6,
  ];

  let projectAddresses: string[] = [];
  let createdProjects: ERC20Project[] = [];

  for (let index = 0; index < projects.length; index++) {
    const projectArgs = projects[index];

    const creationTx = await projectsManager.createProject(
      projectArgs[0],
      projectArgs[1],
      projectArgs[2]
    );

    const contractAddress = getAddressFromReceipt(await creationTx.wait())[0];

    const project = await projectAt(contractAddress);

    projectAddresses.push(project.address);
    createdProjects.push(project);

    await permissionGranter.grantRole(PROTOCOL_CONTRACT_ROLE, project.address);

    await multiSignPermission.setPermission(
      admin.address,
      project.address,
      "approveSwap"
    );
    await multiSignPermission.setPermission(
      unergyLogicReserve.address,
      project.address,
      "approveSwap"
    );
    await multiSignPermission.setPermission(
      admin.address,
      project.address,
      "approve"
    );
    await multiSignPermission.setPermission(
      unergyBuyer.address,
      project.address,
      "setState"
    );
    await multiSignPermission.setPermission(
      projectsManager.address,
      project.address,
      "mint"
    );
    await multiSignPermission.setPermission(
      unergyBuyer.address,
      project.address,
      "burn"
    );
    await multiSignPermission.setPermission(
      project.address,
      unergyEvent.address,
      "beforeTransferReceipt"
    );
    await multiSignPermission.setPermission(
      project.address,
      unergyEvent.address,
      "afterTransferReceipt"
    );
    await multiSignPermission.setPermission(
      unergyEvent.address,
      project.address,
      "approveSwap"
    );
    await multiSignPermission.setPermission(
      installers[index].address,
      unergyBuyer.address,
      "offChainMilestonePaymentReport"
    );

    await projectsManager.configureProject(project.address);
    await multiSignPermission.setMeterPermission(
      projectArgs[4].address,
      unergyLogicReserve.address,
      project.address
    );
    await project.approveSwap(
      projectArgs[3].address,
      unergyLogicReserve.address,
      projectArgs[0].totalPWatts
    );
  }

  const milestones: MilestoneInput[] = [
    {
      name: "Desarrollo de ingeniería de detalle del proyecto",
      weight: 10,
    },
    {
      name: "Anticipo de paneles, inversores y estructuras",
      weight: 20,
    },
    {
      name: "Equipos en sitio",
      weight: 20,
    },
    {
      name: "Realizar instalación del proyecto",
      weight: 30,
    },
    {
      name: "Obtener certificaciones y pólizas",
      weight: 10,
    },
    {
      name: "Realizar proceso con Operador de Red",
      weight: 10,
    },
  ];

  for (let i = 0; i < milestones.length; i++) {
    for (let j = 0; j < milestones.length; j++) {
      await projectsManager.addProjectMilestone(
        projectAddresses[i],
        milestones[j].name,
        milestones[j].weight
      );
    }
  }

  return {
    milestones,
    project1Input,
    project2Input,
    project3Input,
    project4Input,
    project5Input,
    project6Input,
    defaultProjectInput,
    project1: createdProjects[0],
    project2: createdProjects[1],
    project3: createdProjects[2],
    project4: createdProjects[3],
    project5: createdProjects[4],
    project6: createdProjects[5],
  };
}
