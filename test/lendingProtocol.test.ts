import { TimeExpression } from "./../utils/time";
import { expect } from "chai";
import { BigNumber } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { BigNumberish } from "@ethersproject/bignumber/src.ts/bignumber";

import {
  parseUnits,
  dealAndApprove,
  pressEnterToContinue,
  getAddressFromReceipt,
  increaseTime,
  formatAccessControlError,
} from "../utils/helpers";
import {
  ERC20Project,
  LendingManager,
  ERC20StableCoin,
} from "../typechain-types";
import { parseEther } from "../utils";
import { wallet } from "../utils/wallet";
import { logger } from "../utils/logger";
import { Position } from "../utils/contracts/Position";
import { ProjectHelper } from "./../utils/projectHelpers";
import { createProjectFixture, deployFixture } from "./fixtures";
import { PositionState } from "../utils/contracts/Position/types";
import {
  ADMIN_ROLE,
  DEFAULT_ADMIN_ROLE,
  NOT_MINTED_ERROR,
  WAD,
  ZERO_ADDRESS,
} from "../utils/constants";
import { parseMetadataTuple } from "../utils/contracts/Position/tasks";

type Rate = [BigNumberish, BigNumberish];

async function fundProject(
  lendingManager: LendingManager,
  project: ERC20Project,
  stableCoin: ERC20StableCoin,
  projectHelper: ProjectHelper,
  amount: BigNumber,
  meter1: SignerWithAddress,
  user1: SignerWithAddress
) {
  const lender = await wallet.getSignerWithBalance();
  const borrower = await wallet.getSignerWithBalance();

  /**
   * * Position opened
   */
  let { position, metadata: meta } = await createPosition(
    { lendingManager, project, stableCoin },
    lender,
    {
      amount,
      anualRevenuePerc,
      anualCompensationPerc,
      lockedPeriod,
      additionalPeriod,
    }
  );

  await dealAndApprove(
    stableCoin,
    borrower,
    lendingManager.address,
    meta.collateral,
    { parseUnits: false }
  );

  /**
   * * Position closed
   */
  await lendingManager.connect(borrower).closePosition(position.address);

  meta = await position.getMetadata();

  /**
   * * Providing liquidity
   */
  await projectHelper.buyPWattsFrom(
    position.address,
    project.address,
    amount.mul(WAD).div(STABLE_SCALE).mul(2),
    { pWattPrice: parseUnits(0.5, 6) }
  );

  await projectHelper.bringToProduction(
    project,
    {
      users: [user1.address],
      amounts: [],
    },
    meter1,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    true
  );

  return {
    position,
    positionMetadata: meta,
    borrower,
    lender,
  };
}

function getPerioProfit(
  lockedAmount: BigNumber,
  anualRevenuePerc: BigNumber,
  lockedPeriod: number
): BigNumberish {
  const anualValue = lockedAmount
    .mul(anualRevenuePerc)
    .div(BigInt(1e18))
    .mul(BigInt(1e6))
    .div(BigInt(100e6));
  const dailyValue = anualValue.mul(BigInt(1e6)).div(BigInt(365e6));
  const periodProfit = dailyValue.mul(BigInt(lockedPeriod));

  return periodProfit;
}

async function createPosition(
  contracts: {
    lendingManager: LendingManager;
    project: ERC20Project;
    stableCoin: ERC20StableCoin;
  },
  lender: SignerWithAddress,
  args: {
    amount: BigNumberish;
    anualRevenuePerc: BigNumberish;
    anualCompensationPerc: BigNumberish;
    lockedPeriod: BigNumberish;
    additionalPeriod: BigNumberish;
  }
) {
  await dealAndApprove(
    contracts.stableCoin,
    lender,
    contracts.lendingManager.address,
    args.amount,
    {
      parseUnits: false,
    }
  );

  const createTx = await contracts.lendingManager
    .connect(lender)
    .createPosition(
      contracts.project.address,
      args.anualRevenuePerc,
      args.anualCompensationPerc,
      args.lockedPeriod,
      args.additionalPeriod,
      args.amount
    );

  const positionId = getAddressFromReceipt(await createTx.wait())[0];
  logger.log(`Position created: ${positionId}`);
  const position = await Position(positionId);
  const metadata = await position.getMetadata();

  return { position, metadata };
}

const defaultProfitRate: Rate = [parseEther(100), parseEther(150)];
const defaultPeriodRate: Rate = [2 /** months */, 12 /** months */];

const anualRevenuePerc = parseEther(10); // 10% APR
const anualCompensationPerc = anualRevenuePerc.add(parseEther(5)); // 10 + 5% APR
const lockedPeriod = 4 * 30; /** 4 months in days */
const additionalPeriod = 2 * 30; /** 2 months in days */

const STABLE_SCALE = BigInt(1e6);

const TEN_DAYS = TimeExpression.Day * 10;
const FIFTEEN_DAYS = TimeExpression.Day * 15; // = additionalPeriod / 4
const LOCKED_PERIOD = lockedPeriod * TimeExpression.Day;
const ADDITIONAL_PERIOD = additionalPeriod * TimeExpression.Day;
const LOCKED_PERIOD_PLUS_TEN_DAYS = LOCKED_PERIOD + TEN_DAYS;

describe("Lending Protocol", () => {
  describe("LendingManager", () => {
    it("Should initialize the lending manager", async () => {
      const { admin, stableCoin, unergyData, lendingManager } =
        await loadFixture(deployFixture);

      expect(
        await lendingManager.hasRole(DEFAULT_ADMIN_ROLE, admin.address)
      ).to.equal(true);
      expect(await lendingManager.stableCoin()).to.equal(stableCoin.address);
      expect(await lendingManager.unergyData()).to.equal(unergyData.address);
    });

    describe("setProfitRate", () => {
      it("Should revert is the caller is not the owner", async () => {
        const { lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const user = await wallet.getSignerWithBalance();

        const project1ProfitRate: [BigNumberish, BigNumberish] = [
          parseEther(100),
          parseEther(150),
        ];

        await expect(
          lendingManager
            .connect(user)
            .setProfitRate(project1.address, project1ProfitRate)
        ).to.be.revertedWith(
          formatAccessControlError(user.address, ADMIN_ROLE)
        );
      });

      it("Should set the profit rate", async () => {
        const { lendingManager, admin } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        await lendingManager.grantRole(ADMIN_ROLE, admin.address);
        await lendingManager.setProfitRate(project1.address, defaultProfitRate);

        expect(
          await lendingManager.getProfitRate(project1.address)
        ).to.deep.equal(defaultProfitRate);
      });
    });

    describe("setLoanTermLimits", () => {
      it("Should revert is the caller is not the owner", async () => {
        const { lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const user = await wallet.getSignerWithBalance();

        await expect(
          lendingManager
            .connect(user)
            .setLoanTermLimits(project1.address, defaultProfitRate)
        ).to.be.revertedWith(
          formatAccessControlError(user.address, ADMIN_ROLE)
        );
      });

      it("Should set the period rate", async () => {
        const { lendingManager, admin } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        await lendingManager.grantRole(ADMIN_ROLE, admin.address);
        await lendingManager.setLoanTermLimits(
          project1.address,
          defaultPeriodRate
        );

        expect(
          await lendingManager.getLoanTermsLimits(project1.address)
        ).to.deep.equal(defaultPeriodRate);
      });
    });

    describe("createPosition", () => {
      it("Should revert if the project is not in FUNDING state", async () => {
        const { stableCoin, lendingManager, projectsManager } =
          await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());
        const lender = await wallet.getSignerWithBalance();

        await projectsManager.getProjectState(project1.address);

        const INVALID_PROJECT_STATES = [
          /* 0 - is the valid state, */ 1, 2, 3, 4, 5,
        ];

        for await (const invalidState of INVALID_PROJECT_STATES) {
          await projectsManager.setProjectState(project1.address, invalidState);

          await expect(
            lendingManager
              .connect(lender)
              .createPosition(
                project1.address,
                anualRevenuePerc,
                anualCompensationPerc,
                lockedPeriod,
                additionalPeriod,
                amount
              )
          ).to.be.revertedWithCustomError(
            lendingManager,
            "LendingManager_InvalidProjectState"
          );
        }
      });

      it("Should create a position", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();

        let { metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        const periodProfit = getPerioProfit(
          amount,
          anualRevenuePerc,
          lockedPeriod
        );
        const collateral = getPerioProfit(
          amount,
          anualCompensationPerc,
          additionalPeriod
        );

        expect(meta.state).to.equal(PositionState.Pending);
        expect(meta.lender).to.equal(lender.address);
        expect(meta.borrower).to.equal(ZERO_ADDRESS);
        expect(meta.projectAddress).to.equal(project1.address);
        expect(meta.profitPercentage).to.equal(anualRevenuePerc);
        expect(meta.defaultProfitPercentage).to.equal(anualCompensationPerc);
        expect(meta.lockedAmount).to.equal(amount);
        expect(meta.lockedPeriod).to.equal(lockedPeriod);
        expect(meta.additionalPeriod).to.equal(additionalPeriod);
        expect(meta.periodProfit).to.equal(periodProfit);
        expect(meta.collateral).to.equal(collateral);
      });
    });

    describe("closePosition", () => {
      it("Should revert if the position does not exists", async () => {
        const { lendingManager } = await loadFixture(deployFixture);

        const randomAddress = wallet.getRandom({ type: "address" }) as string;

        await expect(
          lendingManager.closePosition(randomAddress)
        ).to.be.revertedWithCustomError(
          lendingManager,
          "LendingManager_PositionDoesNotExists"
        );
      });

      it("Should revert if the position is closed", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        let { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          { parseUnits: false }
        );

        await lendingManager.connect(borrower).closePosition(position.address);

        await expect(
          lendingManager.connect(borrower).closePosition(position.address)
        ).to.be.revertedWithCustomError(
          lendingManager,
          "LendingManager_PositionNotAvailable"
        );
      });

      it("Should close the position", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        let { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          {
            parseUnits: false,
          }
        );

        await lendingManager.connect(borrower).closePosition(position.address);
        meta = await position.getMetadata();

        const ownerOfLenderNFT = await position.ownerOf(
          await position.LENDER_ID()
        );
        const ownerOfBorrowerNFT = await position.ownerOf(
          await position.BORROWER_ID()
        );

        const balanceOfPosition = await stableCoin.balanceOf(position.address);

        expect(ownerOfLenderNFT).to.equal(lender.address);
        expect(ownerOfBorrowerNFT).to.equal(borrower.address);
        expect(balanceOfPosition).to.equal(amount.add(meta.collateral));
        expect(meta.borrower).to.equal(borrower.address);
      });
    });

    describe("cancelPosition", () => {
      it("Should revert if the position does not exists", async () => {
        const { lendingManager } = await loadFixture(deployFixture);

        const randomAddress = wallet.getRandom({ type: "address" }) as string;

        await expect(
          lendingManager.cancelPosition(randomAddress)
        ).to.be.revertedWithCustomError(
          lendingManager,
          "LendingManager_PositionDoesNotExists"
        );
      });

      it("Should revert if the position is closed", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        const { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          { parseUnits: false }
        );

        await lendingManager.connect(borrower).closePosition(position.address);

        await expect(
          lendingManager.connect(lender).cancelPosition(position.address)
        ).to.be.revertedWithCustomError(
          lendingManager,
          "LendingManager_PositionNotAvailable"
        );
      });

      it("Should revert if the caller is not the lender", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);
        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();
        const randomUser = await wallet.getSignerWithBalance();

        const { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          { parseUnits: false }
        );

        await expect(
          lendingManager.connect(randomUser).cancelPosition(position.address)
        ).to.be.revertedWithCustomError(
          lendingManager,
          "LendingManager_PositionNotAvailable"
        );
      });

      it("Should allow the lender to cancel the position and withdraw the locked funds", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();

        let { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        const balanceOfLenderBeforeWithdraw = await stableCoin.balanceOf(
          lender.address
        );

        await lendingManager.connect(lender).cancelPosition(position.address);

        meta = await position.getMetadata();

        const balanceOfLenderAfterWithdraw = await stableCoin.balanceOf(
          lender.address
        );

        const ownerOfLender = position.ownerOf(await position.LENDER_ID());
        const ownerOfBorrower = position.ownerOf(await position.BORROWER_ID());

        expect(meta.state).to.equal(PositionState.Canceled);
        await expect(ownerOfLender).to.be.revertedWith(NOT_MINTED_ERROR);
        await expect(ownerOfBorrower).to.be.revertedWith(NOT_MINTED_ERROR);
        expect(balanceOfLenderAfterWithdraw).to.equal(
          balanceOfLenderBeforeWithdraw.add(amount)
        );
      });
    });

    describe("declareDelay", async () => {
      it("Should revert if the position is canceled", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        const { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await lendingManager.connect(lender).cancelPosition(position.address);

        await expect(position.declareDelay()).to.be.revertedWithCustomError(
          position,
          "Position_InvalidPositionState"
        );
      });

      it("Should revert if the position is not in delayed waiting period", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        const { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          { parseUnits: false }
        );

        await lendingManager.connect(borrower).closePosition(position.address);

        await expect(position.declareDelay()).to.be.revertedWithCustomError(
          position,
          "Position_DelayedDateNotReached"
        );
      });

      it("Should set the position in delayed state", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        let { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          { parseUnits: false }
        );

        await lendingManager.connect(borrower).closePosition(position.address);

        /**
         * * Advance time to the default waiting period
         */
        await increaseTime(LOCKED_PERIOD_PLUS_TEN_DAYS);

        await position.declareDelay();

        meta = await position.getMetadata();

        expect(meta.state).to.equal(PositionState.Delayed);
      });
    });

    describe("declareDefault", () => {
      it("Should declare a Position in Default and allow the lender to withdraw the pWatts", async () => {
        const { stableCoin, lendingManager, projectHelper } = await loadFixture(
          deployFixture
        );
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        let { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          { parseUnits: false }
        );

        await lendingManager.connect(borrower).closePosition(position.address);

        meta = await position.getMetadata();

        /**
         * * Providing liquidity
         */
        await projectHelper.buyPWattsFrom(
          position.address,
          project1.address,
          amount.mul(WAD).div(STABLE_SCALE).mul(2),
          { pWattPrice: parseUnits(0.5, 6) }
        );

        const obtainedPWatts = await project1.balanceOf(position.address);

        /**
         * * Advance time to the default waiting period + 10 days
         */
        await increaseTime(LOCKED_PERIOD + 1);

        /**
         * * Declaring delay
         */

        meta = await position.getMetadata();
        await position.declareDelay();

        /**
         * * Getting compensated
         */

        /**
         * * First 15 days
         */
        await increaseTime(FIFTEEN_DAYS);
        await position.withdrawCompensation();

        /**
         * * Second 15 days
         */
        await increaseTime(FIFTEEN_DAYS);
        await position.withdrawCompensation();

        /**
         * * Third 15 days
         */
        await increaseTime(FIFTEEN_DAYS);
        await position.withdrawCompensation();

        /**
         * * Fourth 15 days
         */
        await increaseTime(FIFTEEN_DAYS);
        await position.withdrawCompensation();

        /**
         * * Declaring default
         */

        await position.declareDefault();

        /**
         * * Actual balances
         */

        meta = await position.getMetadata();
        const ownerOfLenderNFT = position.ownerOf(await position.LENDER_ID());
        const ownerOfBorrowerNFT = position.ownerOf(
          await position.BORROWER_ID()
        );
        const pWatt_balanceOfLenderAfterDefault = await project1.balanceOf(
          lender.address
        );
        const pWatt_balanceOfBorrowerAfterDefault = await project1.balanceOf(
          borrower.address
        );
        const pWatt_balanceOfPositionAfterDefault = await project1.balanceOf(
          position.address
        );

        expect(meta.state).to.equal(PositionState.Default);
        await expect(ownerOfLenderNFT).to.be.revertedWith(NOT_MINTED_ERROR);
        await expect(ownerOfBorrowerNFT).to.be.revertedWith(NOT_MINTED_ERROR);
        expect(pWatt_balanceOfLenderAfterDefault).to.equal(obtainedPWatts);
        expect(pWatt_balanceOfBorrowerAfterDefault).to.equal(0);
        expect(pWatt_balanceOfPositionAfterDefault).to.equal(0);
      });
    });

    describe("transferFrom", () => {
      it("Should update the position metadata when the position NFT is transferred", async () => {
        const { stableCoin, lendingManager } = await loadFixture(deployFixture);
        const { project1 } = await loadFixture(createProjectFixture);

        const amount = parseUnits(1000, await stableCoin.decimals());

        const lender = await wallet.getSignerWithBalance();
        const borrower = await wallet.getSignerWithBalance();

        const futureLender = await wallet.getSignerWithBalance();
        const futureBorrower = await wallet.getSignerWithBalance();

        const { position, metadata: meta } = await createPosition(
          { lendingManager, project: project1, stableCoin },
          lender,
          {
            amount,
            anualRevenuePerc,
            anualCompensationPerc,
            lockedPeriod,
            additionalPeriod,
          }
        );

        await dealAndApprove(
          stableCoin,
          borrower,
          lendingManager.address,
          meta.collateral,
          { parseUnits: false }
        );

        await lendingManager.connect(borrower).closePosition(position.address);

        await position
          .connect(lender)
          .transferFrom(
            lender.address,
            futureLender.address,
            await position.LENDER_ID()
          );

        await position
          .connect(borrower)
          .transferFrom(
            borrower.address,
            futureBorrower.address,
            await position.BORROWER_ID()
          );

        const ownerOfLenderNFT = await position.ownerOf(
          await position.LENDER_ID()
        );
        const ownerOfBorrowerNFT = await position.ownerOf(
          await position.BORROWER_ID()
        );
        const positionMeta = await position.getMetadata();

        expect(ownerOfLenderNFT).to.equal(futureLender.address);
        expect(ownerOfBorrowerNFT).to.equal(futureBorrower.address);
        expect(positionMeta.lender).to.equal(futureLender.address);
        expect(positionMeta.borrower).to.equal(futureBorrower.address);
      });
    });
  });

  describe("Providing Liquidity", () => {
    it("Should provide liquidity in a position to be used to fund a project and withdraw profit", async () => {
      const {
        user1,
        meter1,
        stableCoin,
        uWattToken,
        projectHelper,
        lendingManager,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);
      const project1Meta = await projectsManager.getProject(project1.address);

      const amount = parseUnits(1000, await stableCoin.decimals());

      const { position, positionMetadata, borrower, lender } =
        await fundProject(
          lendingManager,
          project1,
          stableCoin,
          projectHelper,
          amount,
          meter1,
          user1
        );

      /**
       * * Position balances after project swap
       */
      const USDT_balanceOfPositionAfterProjectSwap = await stableCoin.balanceOf(
        position.address
      );
      const uWatt_balanceOfPositionAfterProjectSwap =
        await uWattToken.balanceOf(position.address);

      const expectedBalanceWithoutFee = amount
        .mul(project1Meta.swapFactor)
        .div(STABLE_SCALE)
        .mul(2);
      const expectedBalanceWithFee = expectedBalanceWithoutFee.sub(
        await projectHelper.getFeeAmount(expectedBalanceWithoutFee)
      );

      /**
       * * Withdraw revenue
       */
      await position.connect(lender).withdrawProfit();

      const USDT_balanceOfBorrowerAfterWithdraw = await stableCoin.balanceOf(
        borrower.address
      );
      const USDT_balanceOfPositionAfterWithdraw = await stableCoin.balanceOf(
        position.address
      );

      /**
       * * Expected balances
       */
      const lender_uWatt_expectedBalance = await position.getUWattsIn(
        positionMetadata.lockedAmount.add(positionMetadata.periodProfit)
      );
      const borrower_uWatt_expectedBalance =
        uWatt_balanceOfPositionAfterProjectSwap.sub(
          lender_uWatt_expectedBalance
        );

      /**
       * * Actual balances
       */
      const uWatt_balanceOfLenderAfterWithdraw = await uWattToken.balanceOf(
        lender.address
      );
      const uWatt_balanceOfBorrowerAfterWithdraw = await uWattToken.balanceOf(
        borrower.address
      );

      expect(uWatt_balanceOfPositionAfterProjectSwap).to.equal(
        expectedBalanceWithFee
      );
      expect(USDT_balanceOfPositionAfterProjectSwap).to.equal(
        positionMetadata.collateral
      );
      expect(USDT_balanceOfPositionAfterWithdraw).to.equal(0);
      expect(USDT_balanceOfBorrowerAfterWithdraw).to.equal(
        positionMetadata.collateral
      );
      expect(uWatt_balanceOfLenderAfterWithdraw).to.equal(
        lender_uWatt_expectedBalance
      );
      expect(uWatt_balanceOfBorrowerAfterWithdraw).to.equal(
        borrower_uWatt_expectedBalance
      );
    });

    it("Should provide liquidity in a position to be used to fund a project. Get compensated for 10 days and withdraw profit", async () => {
      const {
        user1,
        meter1,
        uWattToken,
        stableCoin,
        projectHelper,
        lendingManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const amount = parseUnits(1000, await stableCoin.decimals());
      const lender = await wallet.getSignerWithBalance();
      const borrower = await wallet.getSignerWithBalance();

      const dailyCompenstionProfit = await lendingManager.getDailyProfit(
        amount,
        anualCompensationPerc
      );
      const USDT_tenDaysCompensation = dailyCompenstionProfit.mul(10);

      /**
       * * Position opened
       */
      let { position, metadata: meta } = await createPosition(
        { lendingManager, project: project1, stableCoin },
        lender,
        {
          amount,
          anualRevenuePerc,
          anualCompensationPerc,
          lockedPeriod,
          additionalPeriod,
        }
      );

      await dealAndApprove(
        stableCoin,
        borrower,
        lendingManager.address,
        meta.collateral,
        { parseUnits: false }
      );

      /**
       * * Position closed
       */
      await lendingManager.connect(borrower).closePosition(position.address);

      meta = await position.getMetadata();

      /**
       * * Providing liquidity
       */
      await projectHelper.buyPWattsFrom(
        position.address,
        project1.address,
        amount.mul(WAD).div(STABLE_SCALE).mul(2),
        { pWattPrice: parseUnits(0.5, 6) }
      );

      /**
       * * Advance time to the default waiting period + 10 days
       */
      await increaseTime(LOCKED_PERIOD_PLUS_TEN_DAYS);

      /**
       * * Declaring default
       */
      meta = await position.getMetadata();

      await position.declareDelay();

      /**
       * * Getting compensated
       */

      await position.withdrawCompensation();

      /**
       * * Getting project to production
       */

      await projectHelper.bringToProduction(
        project1,
        {
          users: [user1.address],
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      /**
       * * Getting profit
       */
      await position.withdrawProfit();

      /**
       * * Expected balances
       */

      const USDT_expectedBalanceOfLenderAfterProfit = USDT_tenDaysCompensation;
      const USDT_expectedBalanceOfBorrowerAfterProfit = meta.collateral.sub(
        USDT_tenDaysCompensation
      );

      /**
       * * Actual balances
       */

      meta = await position.getMetadata();
      const USDT_balanceOfPositionAfterProjectSwap = await stableCoin.balanceOf(
        position.address
      );
      const USDT_balanceOfLenderAfterProjectSwap = await stableCoin.balanceOf(
        lender.address
      );
      const USDT_balanceOfBorrowerAfterProjectSwap = await stableCoin.balanceOf(
        borrower.address
      );
      const uWatt_balanceOfPositionAfterProjectSwap =
        await uWattToken.balanceOf(position.address);

      expect(meta.payedCompensation).to.equal(USDT_tenDaysCompensation);
      expect(USDT_balanceOfPositionAfterProjectSwap).to.equal(0);
      expect(uWatt_balanceOfPositionAfterProjectSwap).to.equal(0);
      expect(USDT_balanceOfLenderAfterProjectSwap).to.equal(
        USDT_expectedBalanceOfLenderAfterProfit
      );
      expect(USDT_balanceOfBorrowerAfterProjectSwap).to.equal(
        USDT_expectedBalanceOfBorrowerAfterProfit
      );
    });

    it("Should be compensated progressively until the collateral is fully paid and then withdraw profit", async () => {
      const { user1, meter1, stableCoin, projectHelper, lendingManager } =
        await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const amount = parseUnits(1000, await stableCoin.decimals());
      const lender = await wallet.getSignerWithBalance();
      const borrower = await wallet.getSignerWithBalance();

      const dailyCompenstionProfit = await lendingManager.getDailyProfit(
        amount,
        anualCompensationPerc
      );
      const USDT_fifteenDaysCompensation = dailyCompenstionProfit.mul(15);

      /**
       * * Position opened
       */
      let { position, metadata: meta } = await createPosition(
        { lendingManager, project: project1, stableCoin },
        lender,
        {
          amount,
          anualRevenuePerc,
          anualCompensationPerc,
          lockedPeriod,
          additionalPeriod,
        }
      );

      await dealAndApprove(
        stableCoin,
        borrower,
        lendingManager.address,
        meta.collateral,
        { parseUnits: false }
      );

      /**
       * * Position closed
       */
      await lendingManager.connect(borrower).closePosition(position.address);

      meta = await position.getMetadata();

      /**
       * * Providing liquidity
       */
      await projectHelper.buyPWattsFrom(
        position.address,
        project1.address,
        amount.mul(WAD).div(STABLE_SCALE).mul(2),
        { pWattPrice: parseUnits(0.5, 6) }
      );

      /**
       * * Advance time to the default waiting period + 10 days
       */
      await increaseTime(LOCKED_PERIOD + 1);

      /**
       * * Declaring delay
       */

      meta = await position.getMetadata();
      await position.declareDelay();

      /**
       * * Getting compensated
       */

      /**
       * * First 15 days
       */
      await increaseTime(FIFTEEN_DAYS);
      await position.withdrawCompensation();

      /**
       * * Second 15 days
       */
      await increaseTime(FIFTEEN_DAYS);
      await position.withdrawCompensation();

      /**
       * * Third 15 days
       */
      await increaseTime(FIFTEEN_DAYS);
      await position.withdrawCompensation();

      /**
       * * Fourth 15 days
       */
      await increaseTime(FIFTEEN_DAYS);
      await position.withdrawCompensation();

      /**
       * * Getting project to production
       */

      await projectHelper.bringToProduction(
        project1,
        {
          users: [user1.address],
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      /**
       * * Getting profit
       */
      await position.withdrawProfit();

      /**
       * * Expected balances
       */

      const USDT_expectedBalanceOfLenderAfterProfit =
        USDT_fifteenDaysCompensation.mul(4);
      const USDT_expectedBalanceOfBorrowerAfterProfit = meta.collateral.sub(
        USDT_fifteenDaysCompensation.mul(4)
      );

      /**
       * * Actual balances
       */

      meta = await position.getMetadata();
      const USDT_balanceOfPositionAfterProjectSwap = await stableCoin.balanceOf(
        position.address
      );
      const USDT_balanceOfLenderAfterProjectSwap = await stableCoin.balanceOf(
        lender.address
      );
      const USDT_balanceOfBorrowerAfterProjectSwap = await stableCoin.balanceOf(
        borrower.address
      );

      expect(meta.payedCompensation).to.equal(meta.collateral);
      expect(USDT_balanceOfPositionAfterProjectSwap).to.equal(0);
      expect(USDT_balanceOfLenderAfterProjectSwap).to.equal(
        USDT_expectedBalanceOfLenderAfterProfit
      );
      expect(USDT_balanceOfBorrowerAfterProjectSwap).to.equal(
        USDT_expectedBalanceOfBorrowerAfterProfit
      );
    });

    it("Should provide liquidity in a position to be used to fund a project, transfer position access and withdraw profit", async () => {
      const {
        user1,
        meter1,
        stableCoin,
        uWattToken,
        projectHelper,
        lendingManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);

      const amount = parseUnits(1000, await stableCoin.decimals());

      const { position, positionMetadata, borrower, lender } =
        await fundProject(
          lendingManager,
          project1,
          stableCoin,
          projectHelper,
          amount,
          meter1,
          user1
        );

      /**
       * * Position balances after project swap
       */

      const uWatt_balanceOfPositionAfterProjectSwap =
        await uWattToken.balanceOf(position.address);

      /**
       * * Transfering position access
       */

      const newLender = await wallet.getSignerWithBalance();
      const newBorrower = await wallet.getSignerWithBalance();

      await position
        .connect(lender)
        .transferFrom(
          lender.address,
          newLender.address,
          await position.LENDER_ID()
        );

      await position
        .connect(borrower)
        .transferFrom(
          borrower.address,
          newBorrower.address,
          await position.BORROWER_ID()
        );

      /**
       * * Withdraw revenue
       */
      await position.withdrawProfit();

      /**
       * * Expected balances
       */
      const lender_uWatt_expectedBalance = await position.getUWattsIn(
        positionMetadata.lockedAmount.add(positionMetadata.periodProfit)
      );
      const borrower_uWatt_expectedBalance =
        uWatt_balanceOfPositionAfterProjectSwap.sub(
          lender_uWatt_expectedBalance
        );

      /**
       * * Actual balances
       */
      const uWatt_balanceOfLenderAfterWithdraw = await uWattToken.balanceOf(
        newLender.address
      );
      const uWatt_balanceOfBorrowerAfterWithdraw = await uWattToken.balanceOf(
        newBorrower.address
      );

      expect(uWatt_balanceOfLenderAfterWithdraw).to.equal(
        lender_uWatt_expectedBalance
      );
      expect(uWatt_balanceOfBorrowerAfterWithdraw).to.equal(
        borrower_uWatt_expectedBalance
      );
    });

    it("demo", async function () {
      this.skip();

      const {
        user1,
        meter1,
        stableCoin,
        uWattToken,
        projectHelper,
        lendingManager,
        projectsManager,
      } = await loadFixture(deployFixture);
      const { project1 } = await loadFixture(createProjectFixture);
      const project1Meta = await projectsManager.getProject(project1.address);

      const data = {
        pWattPrice: "0.5 USD",
        swapFactor: "0.9135",
        uWattPrice: "0.9 USD",
        swapFee: "2%",
        M: "1000 USD",
        T: "4 meses",
        "T*": "2 meses",
        R: "10%",
        "R*": "15%",
        C: "25 USD",
        totalProfit: "~ 33.33 USD",
        obtainedPWatts: "2000",
        obtainedUWatts: "1827",
        obtainedUWattsAfterFee: "1790.46",
        lenderUWatts: "1148.14",
        borrowerUWatts: "642.32",
      };
      console.log("\n\nExpected values\n\n");
      console.table(data);
      await pressEnterToContinue();

      const amount = parseUnits(1000, await stableCoin.decimals());

      const lender = await wallet.getSignerWithBalance();
      const borrower = await wallet.getSignerWithBalance();

      /**
       * * Position opened
       */
      let { position, metadata: meta } = await createPosition(
        { lendingManager, project: project1, stableCoin },
        lender,
        {
          amount,
          anualRevenuePerc,
          anualCompensationPerc,
          lockedPeriod,
          additionalPeriod,
        }
      );

      const USDT_balanceOfPosition = await stableCoin.balanceOf(
        position.address
      );

      console.log("\n\nPosition opened\n\n");
      console.log({
        meta: parseMetadataTuple(meta),
        USDT_balanceOfPositionAfterOpen: USDT_balanceOfPosition,
      });
      await pressEnterToContinue();

      await dealAndApprove(
        stableCoin,
        borrower,
        lendingManager.address,
        meta.collateral,
        { parseUnits: false }
      );

      /**
       * * Position closed
       */
      await lendingManager.connect(borrower).closePosition(position.address);

      meta = await position.getMetadata();
      const USDT_balanceOfPositionAfterClose = await stableCoin.balanceOf(
        position.address
      );
      console.log("\n\nPosition closed\n\n");
      console.log({
        meta: parseMetadataTuple(meta),
        USDT_balanceOfPositionAfterClose,
      });
      await pressEnterToContinue();

      /**
       * * Providing liquidity
       */
      await projectHelper.buyPWattsFrom(
        position.address,
        project1.address,
        amount.mul(WAD).div(STABLE_SCALE).mul(2),
        { pWattPrice: parseUnits(0.5, 6) }
      );

      await projectHelper.bringToProduction(
        project1,
        {
          users: [user1.address],
          amounts: [],
        },
        meter1,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        true
      );

      const USDT_balanceOfPositionAfterProjectSwap = await stableCoin.balanceOf(
        position.address
      );
      const uWatt_balanceOfPositionAfterProjectSwap =
        await uWattToken.balanceOf(position.address);

      console.log("\n\nProviding liquidity\n\n");
      console.table({
        USDT_balanceOfPositionAfterProjectSwap:
          USDT_balanceOfPositionAfterProjectSwap.toString(),
        uWatt_balanceOfPositionAfterProjectSwap:
          uWatt_balanceOfPositionAfterProjectSwap.toString(),
      });
      await pressEnterToContinue();

      const expectedBalanceWithoutFee = amount
        .mul(project1Meta.swapFactor)
        .div(STABLE_SCALE)
        .mul(2);
      const expectedBalanceWithFee = expectedBalanceWithoutFee.sub(
        await projectHelper.getFeeAmount(expectedBalanceWithoutFee)
      );

      expect(uWatt_balanceOfPositionAfterProjectSwap).to.equal(
        expectedBalanceWithFee
      );
      expect(USDT_balanceOfPositionAfterProjectSwap).to.equal(meta.collateral);

      await position.connect(lender).withdrawProfit();

      const uWatt_balanceOfLenderAfterWithdraw = await uWattToken.balanceOf(
        lender.address
      );
      const uWatt_balanceOfBorrowerAfterWithdraw = await uWattToken.balanceOf(
        borrower.address
      );
      const uWatt_balanceOfPositionAfterWithdraw = await uWattToken.balanceOf(
        position.address
      );
      const USDT_balanceOfBorrowerAfterWithdraw = await stableCoin.balanceOf(
        borrower.address
      );
      const USDT_balanceOfPositionAfterWithdraw = await stableCoin.balanceOf(
        position.address
      );

      console.log("\n\nWithdraw\n\n");
      console.table({
        uWatt_balanceOfLenderAfterWithdraw:
          uWatt_balanceOfLenderAfterWithdraw.toString(),
        uWatt_balanceOfBorrowerAfterWithdraw:
          uWatt_balanceOfBorrowerAfterWithdraw.toString(),
        uWatt_balanceOfPositionAfterWithdraw:
          uWatt_balanceOfPositionAfterWithdraw.toString(),
        USDT_balanceOfPositionAfterWithdraw:
          USDT_balanceOfPositionAfterWithdraw.toString(),
        USDT_balanceOfBorrowerAfterWithdraw:
          USDT_balanceOfBorrowerAfterWithdraw.toString(),
      });

      expect(USDT_balanceOfPositionAfterWithdraw).to.equal(0);
      expect(USDT_balanceOfBorrowerAfterWithdraw).to.equal(meta.collateral);
    });
  });
});
