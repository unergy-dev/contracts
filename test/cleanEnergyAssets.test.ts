import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { ethers } from "hardhat";
import { expect } from "chai";

import { deployFixture } from "./fixtures";
import { parseUnits } from "../utils";

describe("Clean Energy Asset contract", () => {
  it("Should deploy with the rigth values", async () => {
    const { cleanEnergyAsset, admin } = await loadFixture(deployFixture);

    const TOKEN_ID = await cleanEnergyAsset.tokenId();

    const ENERGY_LIMIT = await cleanEnergyAsset.energyLimit();

    const ENERGY_DECIMALS = await cleanEnergyAsset.energyDecimals();

    const REC_DECIMALS = await cleanEnergyAsset.recDecimals();

    const OWNER = await cleanEnergyAsset.owner();

    expect(TOKEN_ID).to.equal(1);
    expect(ENERGY_LIMIT).to.equal(parseUnits(1, 18));
    expect(ENERGY_DECIMALS).to.equal(18);
    expect(REC_DECIMALS).to.equal(18);
    expect(OWNER).to.equal(admin.address);
  });

  describe("Support Interface", () => {
    it("Should return false if not support the interface", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      const NOT_SUPPORTED_INTERFACE_ID = "0xd0007a26";
      const isSupported = await cleanEnergyAsset.supportsInterface(
        NOT_SUPPORTED_INTERFACE_ID
      );

      expect(isSupported).to.equal(false);
    });

    it("Should return true if support the interface", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      // see: https://eips.ethereum.org/EIPS/eip-1155#a-solidity-example-of-the-keccak256-generated-constants-for-the-various-magic-values-these-may-be-used-by-implementation
      const IERC1155_INTERFACE_ID = "0xd9b67a26";
      const IERC_METADATA_URI_INTERFACE_ID = "0x0e89341c";

      let isSupported = await cleanEnergyAsset.supportsInterface(
        IERC1155_INTERFACE_ID
      );

      expect(isSupported).to.equal(true);

      isSupported = await cleanEnergyAsset.supportsInterface(
        IERC_METADATA_URI_INTERFACE_ID
      );

      expect(isSupported).to.equal(true);
    });
  });

  describe("Create General Energy Assets", () => {
    it("Should revert if the contract is paused", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      await cleanEnergyAsset.pause();

      await expect(
        cleanEnergyAsset.createProjectEnergyAsset(
          "Project1",
          cleanEnergyAsset.address
        )
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is no authorized", async () => {
      const { cleanEnergyAsset, permissionGranter, notAdmin } =
        await loadFixture(deployFixture);

      await expect(
        cleanEnergyAsset
          .connect(notAdmin)
          .createProjectEnergyAsset("Project1", cleanEnergyAsset.address)
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("createProjectEnergyAsset", notAdmin.address);
    });
  });

  describe("Create Project Energy Asset", () => {
    it("Should revert if the contract is paused", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      await cleanEnergyAsset.pause();

      await expect(
        cleanEnergyAsset.createProjectEnergyAsset(
          "Project Name",
          ethers.constants.AddressZero
        )
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { cleanEnergyAsset, notAdmin, permissionGranter } =
        await loadFixture(deployFixture);

      await expect(
        cleanEnergyAsset
          .connect(notAdmin)
          .createProjectEnergyAsset(
            "Project Name",
            ethers.constants.AddressZero
          )
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("createProjectEnergyAsset", notAdmin.address);
    });

    it("Should create a project energy asset", async () => {
      const {
        cleanEnergyAsset,
        permissionGranter,
        admin,
        multiSignPermission,
      } = await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "createProjectEnergyAsset"
      );

      const PROJECT_NAME = "Project Name";
      const PROJECT_ADDRESS = "0x0000000000000000000000000000000000011122";

      await cleanEnergyAsset.createProjectEnergyAsset(
        PROJECT_NAME,
        PROJECT_ADDRESS
      );

      const energyTokenName = await cleanEnergyAsset.tokenNameByTokenID(1);
      const energyProjectId = await cleanEnergyAsset.tokenIdByProjectAddress(
        PROJECT_ADDRESS
      );

      const RECTokenName = await cleanEnergyAsset.tokenNameByTokenID(2);
      const isREC = await cleanEnergyAsset.isREC(2);
      const RECid = await cleanEnergyAsset.RECIdByAddress(PROJECT_ADDRESS);

      const REC_TOKEN_NAME = `REC ${PROJECT_NAME}`;

      expect(energyTokenName).to.equal(PROJECT_NAME);
      expect(energyProjectId).to.equal(1);
      expect(RECTokenName).to.equal(REC_TOKEN_NAME);
      expect(isREC).to.equal(true);
      expect(RECid).to.equal(2);
    });
  });

  describe("Mint", () => {
    it("Should revert if the contract is paused", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      await cleanEnergyAsset.pause();

      await expect(
        cleanEnergyAsset.mint(ethers.constants.AddressZero, 1)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { cleanEnergyAsset, notAdmin, permissionGranter } =
        await loadFixture(deployFixture);

      await expect(
        cleanEnergyAsset.connect(notAdmin).mint(ethers.constants.AddressZero, 1)
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("mint", notAdmin.address);
    });

    it("Should revert if the amount is 0", async () => {
      const { cleanEnergyAsset, admin, multiSignPermission } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "mint"
      );

      await expect(
        cleanEnergyAsset.mint(ethers.constants.AddressZero, 0)
      ).to.be.revertedWithCustomError(cleanEnergyAsset, "ZeroOrNegativeAmount");
    });

    it("Should revert if the token has not been created", async () => {
      const { cleanEnergyAsset, admin, multiSignPermission } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "mint"
      );

      await expect(cleanEnergyAsset.mint(ethers.constants.AddressZero, 10))
        .to.be.revertedWithCustomError(cleanEnergyAsset, "TokenNotCreatedYet")
        .withArgs(ethers.constants.AddressZero);
    });

    it("Should mint the general and project energy assets and the REC token", async () => {
      const { cleanEnergyAsset, admin, multiSignPermission } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "mint"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "createProjectEnergyAsset"
      );

      const TOKEN_NAME = "Project Name";
      const PROJECT_ADDRESS = "0x00000000000000000000af2711bbab0000000022";

      await cleanEnergyAsset.createProjectEnergyAsset(
        TOKEN_NAME,
        PROJECT_ADDRESS
      );

      const ENERGY_AMOUNT = parseUnits(10, 18);

      await cleanEnergyAsset.mint(PROJECT_ADDRESS, ENERGY_AMOUNT);

      const projectEnergyTokenBalance = await cleanEnergyAsset.balanceOf(
        cleanEnergyAsset.address,
        1
      );
      const generalEnergyTokenBalance = await cleanEnergyAsset.balanceOf(
        cleanEnergyAsset.address,
        0
      );
      const recBalances = await cleanEnergyAsset.balanceOf(
        cleanEnergyAsset.address,
        2
      );

      const mintedEnergyByProject =
        await cleanEnergyAsset.mintedEnergyByProject(PROJECT_ADDRESS);

      expect([
        projectEnergyTokenBalance,
        generalEnergyTokenBalance,
        recBalances,
      ]).to.deep.equal([ENERGY_AMOUNT, ENERGY_AMOUNT, ENERGY_AMOUNT]);
      expect(mintedEnergyByProject).to.equal(0);
    });
  });

  describe("Burn/Burn RECs", () => {
    it("Should revert if the contract is paused", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      await cleanEnergyAsset.pause();

      await expect(
        cleanEnergyAsset.burn(ethers.constants.AddressZero, 1)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { cleanEnergyAsset, notAdmin, permissionGranter } =
        await loadFixture(deployFixture);

      await expect(
        cleanEnergyAsset.connect(notAdmin).burn(ethers.constants.AddressZero, 1)
      )
        .to.be.revertedWithCustomError(
          permissionGranter,
          "NotAuthorizedToCallFunction"
        )
        .withArgs("burn", notAdmin.address);
    });

    it("Should revert if the amount is 0", async () => {
      const { cleanEnergyAsset, admin, multiSignPermission } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "burn"
      );

      await expect(
        cleanEnergyAsset.connect(admin).burn(ethers.constants.AddressZero, 0)
      ).to.be.revertedWithCustomError(cleanEnergyAsset, "ZeroOrNegativeAmount");
    });

    it("Should burn the general and project energy assets", async () => {
      const {
        cleanEnergyAsset,
        permissionGranter,
        admin,
        multiSignPermission,
      } = await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "mint"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "burn"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "createProjectEnergyAsset"
      );

      const TOKEN_NAME = "Project Name";
      const PROJECT_ADDRESS = "0x00000000000000000000af2711bbab0000000022";

      await cleanEnergyAsset.createProjectEnergyAsset(
        TOKEN_NAME,
        PROJECT_ADDRESS
      );

      let ENERGY_AMOUNT = parseUnits(10, 18);

      await cleanEnergyAsset.mint(PROJECT_ADDRESS, ENERGY_AMOUNT);

      ENERGY_AMOUNT = ENERGY_AMOUNT.div(2);

      await cleanEnergyAsset.burn(PROJECT_ADDRESS, ENERGY_AMOUNT);

      const projectEnergyTokenBalance = await cleanEnergyAsset.balanceOf(
        cleanEnergyAsset.address,
        1
      );
      const generalEnergyTokenBalance = await cleanEnergyAsset.balanceOf(
        cleanEnergyAsset.address,
        0
      );

      expect([
        projectEnergyTokenBalance,
        generalEnergyTokenBalance,
      ]).to.deep.equal([ENERGY_AMOUNT, ENERGY_AMOUNT]);
    });

    it("Should revert if attempting to burn a non-REC token", async () => {
      const { cleanEnergyAsset, admin, multiSignPermission } =
        await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "mint"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "burn"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "createProjectEnergyAsset"
      );

      const TOKEN_NAME = "Project Name";
      const PROJECT_ADDRESS = "0x00000000000000000000af2711bbab0000000022";

      await cleanEnergyAsset.createProjectEnergyAsset(
        TOKEN_NAME,
        PROJECT_ADDRESS
      );

      let ENERGY_AMOUNT = parseUnits(10, 18);

      await cleanEnergyAsset.mint(PROJECT_ADDRESS, ENERGY_AMOUNT);

      ENERGY_AMOUNT = ENERGY_AMOUNT.div(2);

      await expect(
        cleanEnergyAsset.burnREC(0, ENERGY_AMOUNT.div(2))
      ).to.be.revertedWithCustomError(cleanEnergyAsset, "NotREC");
    });

    it("Should burn the RECs", async () => {
      const {
        cleanEnergyAsset,
        permissionGranter,
        admin,
        multiSignPermission,
      } = await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "mint"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "burn"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "createProjectEnergyAsset"
      );

      const TOKEN_NAME = "Project Name";
      const PROJECT_ADDRESS = "0x00000000000000000000af2711bbab0000000022";

      await cleanEnergyAsset.createProjectEnergyAsset(
        TOKEN_NAME,
        PROJECT_ADDRESS
      );

      let ENERGY_AMOUNT = parseUnits(10, 18);

      await cleanEnergyAsset.mint(PROJECT_ADDRESS, ENERGY_AMOUNT);

      ENERGY_AMOUNT = ENERGY_AMOUNT.div(2);

      await cleanEnergyAsset.safeTransferFrom(
        cleanEnergyAsset.address,
        admin.address,
        2,
        ENERGY_AMOUNT,
        "0x"
      );

      await cleanEnergyAsset.burnREC(2, ENERGY_AMOUNT);

      const RECBalance = await cleanEnergyAsset.balanceOf(
        cleanEnergyAsset.address,
        2
      );

      expect(RECBalance).to.deep.equal(ENERGY_AMOUNT);
    });
  });

  describe("Withdraw RECs", () => {
    it("Should revert if the contract is paused", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      await cleanEnergyAsset.pause();

      await expect(
        cleanEnergyAsset.withdrawRECs(2, ethers.constants.AddressZero, 1)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("Should revert if the caller is not authorized", async () => {
      const { cleanEnergyAsset, notAdmin } = await loadFixture(deployFixture);

      await expect(
        cleanEnergyAsset
          .connect(notAdmin)
          .withdrawRECs(2, ethers.constants.AddressZero, 1)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("Should withdraw REC", async () => {
      const {
        cleanEnergyAsset,
        permissionGranter,
        admin,
        multiSignPermission,
      } = await loadFixture(deployFixture);

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "mint"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "burn"
      );

      await multiSignPermission.setPermission(
        admin.address,
        cleanEnergyAsset.address,
        "createProjectEnergyAsset"
      );

      const TOKEN_NAME = "Project Name";
      const PROJECT_ADDRESS = "0x00000000000000000000af2711bbab0000000022";

      await cleanEnergyAsset.createProjectEnergyAsset(
        TOKEN_NAME,
        PROJECT_ADDRESS
      );

      let ENERGY_AMOUNT = parseUnits(10, 18);

      await cleanEnergyAsset.mint(PROJECT_ADDRESS, ENERGY_AMOUNT);

      await cleanEnergyAsset.withdrawRECs(2, admin.address, ENERGY_AMOUNT);

      const RECBalance = await cleanEnergyAsset.balanceOf(
        cleanEnergyAsset.address,
        2
      );
      const adminRECBalance = await cleanEnergyAsset.balanceOf(
        admin.address,
        2
      );

      expect(RECBalance).to.deep.equal(0);
      expect(adminRECBalance).to.deep.equal(ENERGY_AMOUNT);
    });
  });

  describe("Setters", () => {
    describe("Permission Granter", () => {
      it("Should revert if the caller is not authorized", async () => {
        const { cleanEnergyAsset, notAdmin } = await loadFixture(deployFixture);

        await expect(
          cleanEnergyAsset
            .connect(notAdmin)
            .setPermissionGranterAddr(ethers.constants.AddressZero)
        )
          .to.be.revertedWithCustomError(
            cleanEnergyAsset,
            "NotAuthorizedToCallFunction"
          )
          .withArgs("setPermissionGranterAddr", notAdmin.address);
      });

      it("Should set the permission granter address", async () => {
        const { admin, cleanEnergyAsset, multiSignPermission } =
          await loadFixture(deployFixture);

        await multiSignPermission.setPermission(
          admin.address,
          cleanEnergyAsset.address,
          "setPermissionGranterAddr"
        );

        const randomAddr = ethers.Wallet.createRandom().address;

        await cleanEnergyAsset.setPermissionGranterAddr(randomAddr);

        const permissionGranterAddr =
          await cleanEnergyAsset.permissionGranter();
        expect(permissionGranterAddr).to.deep.equal(randomAddr);
      });

      it("Should revert if an admin try to set address 0 as a contract address", async () => {
        const { admin, cleanEnergyAsset, multiSignPermission } =
          await loadFixture(deployFixture);

        await multiSignPermission.setPermission(
          admin.address,
          cleanEnergyAsset.address,
          "setPermissionGranterAddr"
        );

        await expect(
          cleanEnergyAsset.setPermissionGranterAddr(
            ethers.constants.AddressZero
          )
        ).to.be.revertedWithCustomError(
          cleanEnergyAsset,
          "ZeroAddressNotAllowed"
        );
      });
    });

    describe("URI", () => {
      it("Should revert if the contract is paused", async () => {
        const { cleanEnergyAsset } = await loadFixture(deployFixture);

        await cleanEnergyAsset.pause();

        await expect(cleanEnergyAsset.setURI("")).to.be.revertedWith(
          "Pausable: paused"
        );
      });

      it("Should revert if the caller is not authorized", async () => {
        const { cleanEnergyAsset, notAdmin } = await loadFixture(deployFixture);

        await expect(
          cleanEnergyAsset.connect(notAdmin).setURI("")
        ).to.be.revertedWith("Ownable: caller is not the owner");
      });

      it("Should set the URI", async () => {
        const { cleanEnergyAsset } = await loadFixture(deployFixture);

        const URI = "https://example.com";

        await cleanEnergyAsset.setURI(URI);

        expect(await cleanEnergyAsset.uri(0)).to.deep.equal(URI);
      });
    });

    describe("Energy Limit", () => {
      it("Should revert if the contract is paused", async () => {
        const { cleanEnergyAsset, admin } = await loadFixture(deployFixture);

        await cleanEnergyAsset.pause();

        await expect(
          cleanEnergyAsset.connect(admin).setEnergyLimit(1)
        ).to.be.revertedWith("Pausable: paused");
      });

      it("Should revert if the caller is not authorized", async () => {
        const { cleanEnergyAsset, notAdmin, permissionGranter } =
          await loadFixture(deployFixture);

        await expect(cleanEnergyAsset.connect(notAdmin).setEnergyLimit(1))
          .to.be.revertedWithCustomError(
            permissionGranter,
            "NotAuthorizedToCallFunction"
          )
          .withArgs("setEnergyLimit", notAdmin.address);
      });

      it("Should set the energy limit", async () => {
        const {
          cleanEnergyAsset,
          admin,
          permissionGranter,
          multiSignPermission,
        } = await loadFixture(deployFixture);

        await multiSignPermission.setPermission(
          admin.address,
          cleanEnergyAsset.address,
          "setEnergyLimit"
        );

        const ENERGY_LIMIT = 1;

        await cleanEnergyAsset.setEnergyLimit(ENERGY_LIMIT);

        expect(await cleanEnergyAsset.energyLimit()).to.deep.equal(
          ENERGY_LIMIT
        );
      });
    });
  });

  describe("ERC1155 standard receive methods", () => {
    it("Should return the rigth selector for onERC1155Received method", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      const selector = (
        await cleanEnergyAsset.onERC1155Received(
          ethers.constants.AddressZero,
          ethers.constants.AddressZero,
          0,
          0,
          "0x"
        )
      ).data
        // get the first 4 bytes (+ 0x) of the data
        .substring(0, 10);

      // bytes4(keccak256("onERC1155Received(address,address,uint256,uint256,bytes)"))
      expect(selector).to.equal("f23a6e61".to0xString());
    });

    it("Should return the rigth selector for onERC1155BatchReceived method", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      const selector = (
        await cleanEnergyAsset.onERC1155BatchReceived(
          ethers.constants.AddressZero,
          ethers.constants.AddressZero,
          [0],
          [0],
          "0x"
        )
      ).data
        // get the first 4 bytes (+ 0x) of the data
        .substring(0, 10);

      // bytes4(keccak256("onERC1155BatchReceived(address,address,uint256[],uint256[],bytes)"))
      expect(selector).to.equal("bc197c81".to0xString());
    });
  });

  describe("Pause/unpause", () => {
    it("Should revert if the caller is not authorized", async () => {
      const { cleanEnergyAsset, notAdmin } = await loadFixture(deployFixture);

      await expect(
        cleanEnergyAsset.connect(notAdmin).pause()
      ).to.be.revertedWith("Ownable: caller is not the owner");

      await expect(
        cleanEnergyAsset.connect(notAdmin).unpause()
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("Should revert if trying to pause when already pause", async () => {
      const { cleanEnergyAsset, admin } = await loadFixture(deployFixture);

      await cleanEnergyAsset.pause();

      await expect(cleanEnergyAsset.connect(admin).pause()).to.be.revertedWith(
        "Pausable: paused"
      );
    });

    it("Should revert if trying to unpause when already unpaused", async () => {
      const { cleanEnergyAsset, admin } = await loadFixture(deployFixture);

      await expect(
        cleanEnergyAsset.connect(admin).unpause()
      ).to.be.revertedWith("Pausable: not paused");
    });

    it("Should pause and unpause correctly", async () => {
      const { cleanEnergyAsset } = await loadFixture(deployFixture);

      await cleanEnergyAsset.pause();

      expect(await cleanEnergyAsset.paused()).to.equal(true);

      await cleanEnergyAsset.unpause();

      expect(await cleanEnergyAsset.paused()).to.equal(false);
    });
  });
});
