import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";

import { createProjectFixture, deployFixture } from "./fixtures";
import { toBn, parseUnits } from "../utils";
import { BigNumber } from "ethers";

describe("Contracts Decimals", () => {
  it("Should correctly calculate the payment to the installer", async function () {
    const {
      unergyData,
      projectsManager,
      unergyLogicReserve,
      unergyBuyer,
      stableCoin,
      user1,
      user2,
      user3,
      user4,
      user5,
      user6,
      user7,
      installerProject1,
      installerProject2,
      installerProject3,
      installerProject4,
      installerProject5,
      installerProject6,
    } = await loadFixture(deployFixture);

    const {
      project1,
      project2,
      project3,
      project4,
      project5,
      project6,
      milestones,
    } = await loadFixture(createProjectFixture);

    const projectsAddresses = [
      project1.address,
      project2.address,
      project3.address,
      project4.address,
      project5.address,
      project6.address,
    ];

    const users = [
      user1.address,
      user2.address,
      user3.address,
      user4.address,
      user5.address,
      user6.address,
      user7.address,
    ];

    const installers = [
      installerProject1.address,
      installerProject2.address,
      installerProject3.address,
      installerProject4.address,
      installerProject5.address,
      installerProject6.address,
    ];

    const STABLE_COIN_DECIMALS = await stableCoin.decimals();

    const pWattPrices = [
      parseUnits(1.1, STABLE_COIN_DECIMALS),
      parseUnits(1.3, STABLE_COIN_DECIMALS),
      parseUnits(1.03, STABLE_COIN_DECIMALS),
      parseUnits(1.02, STABLE_COIN_DECIMALS),
      parseUnits(1.01, STABLE_COIN_DECIMALS),
      parseUnits(1.01, STABLE_COIN_DECIMALS),
    ];

    const LIFE_TIME = 100_000;

    //pWatts purchase
    for (let i = 0; i < projectsAddresses.length; i++) {
      for (let j = 0; j < users.length; j++) {
        await unergyData.generatePurchaseTicket(
          projectsAddresses[i],
          pWattPrices[i],
          LIFE_TIME,
          users[j]
        );

        const project = await projectsManager.getProject(projectsAddresses[i]);
        const PWATTS_TO_TRANSFER = (
          await projectsManager.getTotalPWatts(projectsAddresses[i])
        )
          .sub(project.originatorFee)
          .div(toBn(users.length));

        await unergyLogicReserve.pWattsTransfer(
          projectsAddresses[i],
          users[j],
          PWATTS_TO_TRANSFER
        );
      }
    }

    //change milestone state for all projects (Installer signature - Unergy signature)

    for (let i = 0; i < projectsAddresses.length; i++) {
      const installer = installers[i];
      const projectStruct = await projectsManager.getProject(
        projectsAddresses[i]
      );
      const currentProjectValue = projectStruct.currentProjectValue; //6 decimals
      let milestonePayment: BigNumber = toBn(0);

      for (let j = 0; j < milestones.length; j++) {
        const milestone = milestones[j];
        await unergyBuyer.setInstallerSign(projectsAddresses[i]);
        await unergyBuyer.setOriginatorSign(projectsAddresses[i], j);

        const milestoneWeigth = milestone.weight;
        milestonePayment = milestonePayment.add(
          currentProjectValue.mul(milestoneWeigth).div(100)
        );

        const installerStableBalance = await stableCoin.balanceOf(installer);

        expect(installerStableBalance).to.deep.equal(milestonePayment);
      }
    }
  });

  it("Should correctly calculate the payment amount of invoice report", async () => {
    const {
      user1,
      user2,
      meter1,
      stableCoin,
      projectHelper,
      unergyLogicReserve,
      projectsManager,
      unergyData,
      STABLE_COIN_DECIMALS,
    } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    await projectHelper.setInstallerSign(project1, {
      users: [user1.address, user2.address],
      amounts: [],
    });

    const projectDecimals = await project1.decimals();
    const project1Struct = await projectsManager.getProject(project1.address);
    const currentProjectValue = project1Struct.currentProjectValue;

    await unergyLogicReserve.swapToken(project1.address, 10);

    const FIRST_REPORT_ENERGY = parseUnits(1, projectDecimals);
    await unergyLogicReserve
      .connect(meter1)
      .energyReport(project1.address, FIRST_REPORT_ENERGY);

    const SECOND_REPORT_ENERGY = parseUnits(2, projectDecimals);
    await unergyLogicReserve
      .connect(meter1)
      .energyReport(project1.address, SECOND_REPORT_ENERGY);

    const ENERGY_DELTA = parseUnits(1, projectDecimals);
    const ENERGY_TARIFF = parseUnits(1, STABLE_COIN_DECIMALS);
    const NEW_PROJECT_VALUE = currentProjectValue.sub(
      parseUnits(1, STABLE_COIN_DECIMALS)
    );
    const maintenancePercentage = project1Struct.maintenancePercentage;
    const managerAssetPercentage = await unergyData.assetManagerFeePercentage(
      project1.address
    );

    const paymentIncome = ENERGY_DELTA.mul(ENERGY_TARIFF).div(
      parseUnits(1, projectDecimals)
    );
    const maintenanceIncome = paymentIncome
      .mul(maintenancePercentage)
      .div(BigInt(100 * 10 ** 18));

    const managerAssetIncome = paymentIncome
      .mul(managerAssetPercentage)
      .div(BigInt(100 * 10 ** 18));

    const reserveIncome = paymentIncome
      .sub(maintenanceIncome)
      .sub(managerAssetIncome);

    await unergyLogicReserve.invoiceReport(
      project1.address,
      ENERGY_DELTA,
      ENERGY_TARIFF,
      NEW_PROJECT_VALUE
    );

    const unergyLogicReserveStableBalance = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );
    const maintenerAddress = await unergyData.maintainerAddress();
    const maintenerStableBalance = await stableCoin.balanceOf(maintenerAddress);
    const assetManagerAddress = await unergyData.assetManagerAddress(
      project1.address
    );
    const assetManagerStableBalance = await stableCoin.balanceOf(
      assetManagerAddress
    );

    expect([
      unergyLogicReserveStableBalance,
      maintenerStableBalance,
      assetManagerStableBalance,
    ]).to.deep.equal([reserveIncome, maintenanceIncome, managerAssetIncome]);
  });

  it("Should correctly calculate the pWatts percentage inside the reserve", async function () {
    const {
      user1,
      user2,
      meter1,
      stableCoin,
      projectHelper,
      unergyLogicReserve,
      projectsManager,
      unergyData,
      STABLE_COIN_DECIMALS,
    } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    await projectHelper.setInstallerSign(project1, {
      users: [user1.address, user2.address],
      amounts: [],
    });

    const projectDecimals = await project1.decimals();
    const project1Struct = await projectsManager.getProject(project1.address);
    const currentProjectValue = project1Struct.currentProjectValue;

    await unergyLogicReserve.swapToken(project1.address, 10);

    const FIRST_REPORT_ENERGY = parseUnits(1, projectDecimals);
    await unergyLogicReserve
      .connect(meter1)
      .energyReport(project1.address, FIRST_REPORT_ENERGY);

    const SECOND_REPORT_ENERGY = parseUnits(2, projectDecimals);
    await unergyLogicReserve
      .connect(meter1)
      .energyReport(project1.address, SECOND_REPORT_ENERGY);

    const ENERGY_DELTA = parseUnits(1, projectDecimals);
    const ENERGY_TARIFF = parseUnits(1, STABLE_COIN_DECIMALS);
    const NEW_PROJECT_VALUE = currentProjectValue.sub(
      parseUnits(1, STABLE_COIN_DECIMALS)
    );

    const maintenancePercentage = project1Struct.maintenancePercentage;
    const managerAssetPercentage = await unergyData.assetManagerFeePercentage(
      project1.address
    );

    const paymentIncome = ENERGY_DELTA.mul(ENERGY_TARIFF).div(
      parseUnits(1, projectDecimals)
    );
    const maintenanceIncome = paymentIncome
      .mul(maintenancePercentage)
      .div(BigInt(100 * 10 ** 18));

    const assetManagerIncome = paymentIncome
      .mul(managerAssetPercentage)
      .div(BigInt(100 * 10 ** 18));

    const reserveIncome = paymentIncome
      .sub(maintenanceIncome)
      .sub(assetManagerIncome);

    await unergyLogicReserve.invoiceReport(
      project1.address,
      ENERGY_DELTA,
      ENERGY_TARIFF,
      NEW_PROJECT_VALUE
    );

    const unergyLogicReserveStableBalance = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );
    const maintenerAddress = await unergyData.maintainerAddress();
    const maintenerStableBalance = await stableCoin.balanceOf(maintenerAddress);

    expect([
      unergyLogicReserveStableBalance,
      maintenerStableBalance,
    ]).to.deep.equal([reserveIncome, maintenanceIncome]);
  });

  it("Should correctly calculate the pWatts percentage inside the reserve", async function () {
    const { user1, user2, meter1, projectHelper, projectsManager, unergyData } =
      await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    // * Set institutional address user 2
    await unergyData.setExternalHolderAddress(
      user2.address,
      project1.address,
      true
    );

    const users = [user1.address, user2.address];

    const projectStruct = await projectsManager.getProject(project1.address);
    const projectValueBeforeInvoiceReport = projectStruct.currentProjectValue;

    await projectHelper.bringToProduction(
      project1,
      {
        users,
        amounts: [],
      },
      meter1,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      true
    );

    const projectStructAfterInvoiceReport = projectsManager.getProject(
      project1.address
    );

    const projectValueAfterInvoiceReport = (
      await projectStructAfterInvoiceReport
    ).currentProjectValue;

    const usdDepreciated = await unergyData.depreciationBalance();

    const user2PwattsBalance = await project1.balanceOf(user2.address);

    const project1TotalSupply = await project1.totalSupply();

    const pWattsPercentageOutside = user2PwattsBalance
      .mul(BigInt(10 ** 20))
      .div(project1TotalSupply);

    const completePercentage = parseUnits(100, 18);

    const pWattsPercentageInside = completePercentage.sub(
      pWattsPercentageOutside
    );

    const reportedDepreciation = projectValueBeforeInvoiceReport.sub(
      projectValueAfterInvoiceReport
    );

    const supposedDepreciation = reportedDepreciation
      .mul(pWattsPercentageInside)
      .div(parseUnits(100, 18));

    expect(usdDepreciated).to.deep.equal(supposedDepreciation);
  });

  it("Should correctly calculate the payment amount on reserve pWatts purchase and depreciation", async function () {
    const {
      admin,
      user1,
      user2,
      user3,
      meter1,
      unergyBuyer,
      unergyData,
      unergyLogicReserve,
      projectHelper,
      projectsManager,
      stableCoin,
    } = await loadFixture(deployFixture);
    const { project1, project2 } = await loadFixture(createProjectFixture);

    const stableCoinDecimals = await stableCoin.decimals();

    const usersProject1 = [user1.address, user2.address];

    const pWattPriceP1 = parseUnits(1.3, stableCoinDecimals);

    await projectHelper.bringToProduction(
      project1,
      {
        users: usersProject1,
        amounts: [],
      },
      meter1,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      pWattPriceP1,
      true
    );

    const project2Decimals = await project2.decimals();
    let balanceOfReserveBeforePurchase = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );

    const usersProject2 = [unergyBuyer.address, user3.address];

    const totalReserveDepreciation = await unergyData.depreciationBalance();

    const pWattPriceP2 = parseUnits(1.5, stableCoinDecimals);

    await projectHelper.setInstallerSign(
      project2,
      {
        users: usersProject2,
        amounts: [],
      },
      undefined,
      pWattPriceP2
    );

    const balanceOfReserveAfterPurchase = await stableCoin.balanceOf(
      unergyLogicReserve.address
    );

    const project2ReservePWattBalance = await project2.balanceOf(
      unergyBuyer.address
    );

    const usedBalanceForProject2Purchase = project2ReservePWattBalance
      .mul(pWattPriceP2)
      .div(BigInt(10 ** 18));

    const project1StructAfterPurchase = await projectsManager.getProject(
      project1.address
    );
    const project1DepreciatedValue = project1StructAfterPurchase.usdDepreciated;

    //how many pWatts of project 2 should be purchased by the reserve to offset depreciation
    const project2PWattsForDepreciationCompensation = totalReserveDepreciation
      .mul(BigInt(10 ** project2Decimals))
      .div(pWattPriceP2);

    const p2PWattReserveBalanceForDepreciationCompensation =
      await project2.balanceOf(unergyLogicReserve.address);

    expect([
      balanceOfReserveBeforePurchase,
      p2PWattReserveBalanceForDepreciationCompensation,
    ]).to.deep.equal([
      balanceOfReserveAfterPurchase
        .add(usedBalanceForProject2Purchase)
        .add(project1DepreciatedValue),
      project2PWattsForDepreciationCompensation,
    ]);
  });

  it("Should correclty calculate the uWatts amount on Swap function", async () => {
    const {
      unergyLogicReserve,
      unergyData,
      user1,
      user2,
      projectHelper,
      projectsManager,
      uWattToken,
    } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    const users = [user1.address, user2.address];
    await projectHelper.setInstallerSign(project1, {
      users,
      amounts: [],
    });

    const project1Struct = await projectsManager.getProject(project1.address);

    const project1Users = await projectsManager.getProjectHolders(
      project1.address
    );

    const user1PwattBalance = await project1.balanceOf(user1.address);

    await unergyLogicReserve.swapToken(project1.address, project1Users.length);

    const uWattBalanceOfUser1 = await uWattToken.balanceOf(user1.address);
    const uWattBalanceOfUser2 = await uWattToken.balanceOf(user2.address);

    const project = await projectsManager.getProject(project1.address);
    const rawExpectedBalance = user1PwattBalance
      .mul(project1Struct.swapFactor)
      .div(BigInt(10 ** 18));

    const swapFeePercentage = await unergyData.swapFeePercentage();
    const divFactor = BigInt(100 * 10 ** 18 * 10 ** 80);
    const swapFee1 = BigInt(
      BigInt(rawExpectedBalance.toString()) * BigInt(10 ** 80)
    );
    const swapFee2 =
      BigInt(swapFee1.toString()) * BigInt(swapFeePercentage.toString());
    const swapFee = swapFee2 / divFactor;
    const expectedBalance = rawExpectedBalance.sub(swapFee);

    expect(uWattBalanceOfUser1).to.be.closeTo(expectedBalance, 20000);
    expect(uWattBalanceOfUser2).to.closeTo(expectedBalance, 20000);
  });

  it("Should correclty calculate the uWatts amount on Custom Institutional Swap function", async () => {
    const {
      user1,
      user2,
      meter1,
      unergyData,
      unergyLogicReserve,
      projectHelper,
      projectsManager,
      uWattToken,
      swapFeePercentage,
    } = await loadFixture(deployFixture);
    const { project1 } = await loadFixture(createProjectFixture);

    //set user 2 as institutional address
    await unergyData.setExternalHolderAddress(
      user2.address,
      project1.address,
      true
    );

    const usersProject1 = [user1.address, user2.address];
    await projectHelper.bringToProduction(
      project1,
      {
        users: usersProject1,
        amounts: [],
      },
      meter1,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      true
    );

    const project1Struct = await projectsManager.getProject(project1.address);

    //user 2 project 1 balance of
    const user2P1PWatts = await project1.balanceOf(user2.address);

    //custom institutional swap execution
    await unergyLogicReserve.customSwap(
      project1.address,
      user2.address,
      project1Struct.swapFactor
    );

    const user2UWattBalance = await uWattToken.balanceOf(user2.address);
    const swapFactor = project1Struct.swapFactor;
    const rawSupposedUser2UWattBalance = user2P1PWatts
      .mul(swapFactor)
      .div(BigInt(10 ** 18));

    const swapFee = rawSupposedUser2UWattBalance
      .mul(swapFeePercentage)
      .div(BigInt(100e18));

    const supposedUser2UWattBalance = rawSupposedUser2UWattBalance.sub(swapFee);

    expect(user2UWattBalance).to.equal(supposedUser2UWattBalance);
  });
});
