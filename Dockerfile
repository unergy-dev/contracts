FROM node:18-alpine

WORKDIR usr/src/apps/protocol

COPY package*.json ./

RUN npm install

COPY . .

CMD [ "npx", "hardhat", "node" ]